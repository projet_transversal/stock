<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 02/08/17
 * Time: 00:41
 */

namespace StockBundle\Controller;


use StockBundle\Entity\Commercant;
use StockBundle\Form\CommnercantFormType;
use StockBundle\Repository\StaticMathod;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CommercantController extends Controller
{
    public function addCommercantAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager ();
        $repository = $em->getRepository (Commercant::class);
        $form = $this->createForm(CommnercantFormType::class);
        $message = StaticMathod::persistData ($request, $form, $em);
        $commercant = $form->getData ();
        if ($message == "ok"){
            if ($repository->verifiData("", $commercant->getLogin(),
                $commercant->getEmail(), $commercant->getTelephone())){
                $em->flush ();

            }else{
                $this->addFlash ("error", "objet deja existant");
            }
            return $this->redirectToRoute ("lister_commercant");
        }
        return $this->render('StockBundle:Commercant:addCommercant.html.twig',
            array(
                'form'=>$form->createView()
            )
        );
    }
    public function listerCommercantAction()
    {
        $repository = $this->getDoctrine()->getManager ()->getRepository (Commercant::class);
        $liste = $repository->findAll ();
        return $this->render('StockBundle:Commercant:listerCommercant.html.twig',
            array(
                'liste'=>$liste
            )
        );
    }

    public function editerCommercantAction(Request $request, Commercant $commercant)
    {
        $em = $this->getDoctrine()->getManager ();
        $form = $this->createForm(CommnercantFormType::class, $commercant);
        $message = StaticMathod::persistData ($request, $form, $em);
        if ($message == "ok"){
            return $this->redirectToRoute("lister_commercant");
        }else {
            return $this->render('StockBundle:Commercant:addCommercant.html.twig',
                array(
                    'form'=>$form->createView()
                )
            );
        }
    }

    public function deleteCommercantAction(Commercant $commercant)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($commercant);
        $em->flush ();
        return $this->redirectToRoute("lister_commercant");

    }

}