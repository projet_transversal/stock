<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 09/07/2017
 * Time: 23:01
 */

namespace StockBundle\Controller;

use StockBundle\Entity\ContratMarchandise;
use StockBundle\Form\ContratMarchandiseFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use StockBundle\Repository\StaticMathod;


class ContratMarchandiseController extends Controller
{

    public function addContratMarchandiseAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(ContratMarchandiseFormType::class);
        $message = StaticMathod::persistData($request, $form, $em);
        if ($message == "ok"){
            return $this->redirectToRoute("lister_contrat");
        }else {
            return $this->render('StockBundle:ContratMarchandise:addContratMarchandise.html.twig',
                array(
                    'form'=>$form->createView()
                )
            );
        }
    }

    public function listerAction()
    {
        $repository = $this->getDoctrine()->getManager ()->getRepository (ContratMarchandise::class);
        $liste = $repository->findAll ();
        return $this->render('StockBundle:ContratMarchandise:listerContratMarchandise.html.twig',
            array(
                'liste'=>$liste
            )
        );
    }

    public function editerContratAction(Request $request, ContratMarchandise $marchandise)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(ContratMarchandiseFormType::class, $marchandise);
        $message = StaticMathod::persistData($request, $form, $em);
        if ($message == "ok"){
            return $this->redirectToRoute("lister_contrat");
        }else {
            return $this->render('StockBundle:ContratMarchandise:addContratMarchandise.html.twig',
                array(
                    'form'=>$form->createView()
                )
            );
        }
    }
    public function visualiserContratAction( $id){
        // On récupère l'EntityManager
        $em = $this->getDoctrine()->getManager();
        // Pour récupérer un contrat unique : on utilise find()
        $contrat = $em->getRepository('StockBundle:ContratMarchandise')->find($id);
        // Puis modifiez la ligne du render comme ceci, pour prendre en compte les variables :
        return $this->render('StockBundle:ContratMarchandise:visualiserContrat.html.twig', array(
            'contrat'=>$contrat));
    }

    public function deleteContratAction(ContratMarchandise $marchandise)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove ($marchandise);
        $em->flush ();
        return $this->redirectToRoute("lister_contrat");
    }

    public function valideContratAction(ContratMarchandise $marchandise, Request $request){
        if(isset($_POST['proformat'])){
            $marchandise->setNoteDebit ((int)$_POST ["_prix"]);
            $marchandise->setValide (true);
            $em = $this->getDoctrine()->getManager ();
            $em->persist ($marchandise);
            $em->flush ();
            return $this->redirectToRoute('lister_contrat');
        }else{
            return $this->render('StockBundle:ContratMarchandise:validerContrat.html.twig',
                array(
                    'contrat'=>$marchandise
                )
            );
        }
    }
}