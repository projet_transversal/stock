<?php

namespace StockBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use StockBundle\Entity\Transporteur;
use StockBundle\Entity\Representant;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('StockBundle:Default:accueil.html.twig');

    }

    public function utilisateurAction(){
        $session = new Session();
        $em = $this->getDoctrine()->getManager ();
        $trans = $em->getRepository (Transporteur::class);
        $rep = $em->getRepository (Representant::class);
        //$com = $em->getRepository (Commercant::class);

        if (isset($_POST['login'])){
            if($rep->getConnexion($_POST['login'], $_POST['password']) != null){
                $session->set ("profil", "Representant");
                $session->set ("nom", $_POST['login']);
                return $this->redirectToRoute("representant_lister_marchandise");
            }
            if($trans->getConnexion($_POST['login'], $_POST['password']) != null){
                $session->set ("profil", "Transporteur");
                $session->set ("nom", $_POST['login']);
//                $id = $session->getId();
//                $nom = $session->getName();
                return $this->redirectToRoute("transporteur_listerContrat");
            }
//        if($com->getConnexion($_POST['login'], $_POST['password']) != null){
//            $session->set ("profil", "Commercant");
//            $session->set ("nom", $_POST['login']);
//            var_dump ('commmercant');die();// a mettre url on a va lui orienter
//        }
        }
        return $this->render('StockBundle:Default:utilisateur.html.twig');
    }
}
