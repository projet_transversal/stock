<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 09/07/2017
 * Time: 23:01
 */

namespace StockBundle\Controller;

//use StockBundle\Entity\InstructionLivraison;
use StockBundle\Form\InstructionLivraisonFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use StockBundle\Repository\StaticMathod;
use StockBundle\Entity\Representant;


class InstructionLivraisonController extends Controller
{
    public function addInstructionLivraisonAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository (InstructionLivraison::class);
        $form = $this->createForm(InstructionLivraisonFormType::class);
        $message = StaticMathod::persistData($request, $form, $em);
        if ($message == "ok"){
            return $this->redirectToRoute("lister_instruction");
        }else {
            return $this->render('StockBundle:InstructionLivraison:addInstructionLivraison.html.twig',
                array(
                    'form'=>$form->createView()
                )
            );
        }
    }
    public function listerAction()
    {
        $repository = $this->getDoctrine()->getManager ()->getRepository (InstructionLivraison::class);
        $liste = $repository->findAll ();
        return $this->render('StockBundle:InstructionLivraison:listerInstructionLivraison.html.twig',
            array(
                'liste'=>$liste
            )
        );
    }

    public function editerInstructionAction(Request $request, Representant $representant)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(InstructionLivraisonFormType::class, $representant);
        $message = StaticMathod::persistData($request, $form, $em);
        if ($message == "ok"){
            return $this->redirectToRoute("lister_instruction");
        }else {
            return $this->render('StockBundle:Representant:addRepresentant.html.twig',
                array(
                    'form' => $form->createView()
                )
            );
        }
    }

    public function deleteInstructionAction(InstructionLivraison $instruction)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($instruction);
        $em->flush ();
        return $this->redirectToRoute("lister_instruction");

    }
}