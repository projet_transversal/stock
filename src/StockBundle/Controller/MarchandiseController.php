<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 04/07/17
 * Time: 09:14
 */

namespace StockBundle\Controller;

use StockBundle\Entity\Marchandise;
use StockBundle\Form\MarchandiseFormType;
use StockBundle\Repository\StaticMathod;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MarchandiseController extends Controller
{
    public function addProduitAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(MarchandiseFormType::class);
        $message = StaticMathod::persistData($request, $form, $em);
        if ($message == 'ok'){
            return $this->redirectToRoute("lister_produit");
        }else{
            return $this->render('StockBundle:Marchandise:addMarchandise.html.twig',
                array(
                    'form'=>$form->createView(),
                    'message'=>$message
                )
            );
        }

    }
    public function listerAction()
    {
        $repository = $this->getDoctrine()->getManager ()->getRepository (Marchandise::class);
        $liste = $repository->findAll ();
        return $this->render('StockBundle:Marchandise:listerMarchandise.html.twig',
            array(
                'liste'=>$liste
            )
        );
    }

    public function editerMarchandiseAction(Request $request, Marchandise $marchandise)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(Marchandise::class, $marchandise);
        $message = StaticMathod::persistData($request, $form, $em);
        if ($message == 'ok') {
            return $this->redirectToRoute("lister_produit");
        } else {
            return $this->render('StockBundle:Marchandise:addMarchandise.html.twig',
                array(
                    'form' => $form->createView(),
                    'message' => $message
                )
            );
        }
    }

    public function deleteMarchandiseAction(Marchandise $marchandise)
    {
        $em = $this->getDoctrine()->getManager ();
        $em->remove ($marchandise);
        $em->flush ();
        return $this->redirectToRoute("lister_produit");

    }
}