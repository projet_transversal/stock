<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 02/08/17
 * Time: 01:08
 */

namespace StockBundle\Controller;


use StockBundle\Entity\OdreLivraison;
use StockBundle\Entity\OdreLivraisonFormType;
use StockBundle\Repository\StaticMathod;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class OdreLivraisonController extends Controller
{
    public function addOdreLivraisonAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager ();
        $form = $this->createForm(OdreLivraisonFormType::class);
        $message = StaticMathod::persistData ($request, $form, $em);
        if ($message == "ok"){
            return $this->redirectToRoute("lister_odre");
        }else {
            return $this->render('StockBundle:OdreLivrason:addOdreLivraison.html.twig',
                array(
                    'form'=>$form->createView()
                )
            );
        }
    }
    public function listerOdreLivraisonAction()
    {
        $repository = $this->getDoctrine()->getManager ()->getRepository (OdreLivraison::class);
        $liste = $repository->findAll ();
        return $this->render('StockBundle:OdreLivraison:listerOdreLivraison.html.twig',
            array(
                'liste'=>$liste
            )
        );
    }

    public function editerOdreLivraisonAction(Request $request, OdreLivraison $livraison)
    {
        $em = $this->getDoctrine()->getManager ();
        $form = $this->createForm(OdreLivraisonFormType::class, $livraison);
        $message = StaticMathod::persistData ($request, $form, $em);
        if ($message == "ok"){
            $em->flush ();
            return $this->redirectToRoute("lister_odre");
        }else {
            return $this->render('StockBundle:OdreLivraison:addOdreLivraison.html.twig',
                array(
                    'form'=>$form->createView()
                )
            );
        }
    }

    public function deleteOdreLivraisonAction(OdreLivraison $livraison)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($livraison);
        $em->flush ();
        return $this->redirectToRoute("lister_odre");

    }

    public function valideOdreLivraisonAction(OdreLivraison $livraison, Request $request){
        if(isset($_POST['odre'])){
            $livraison->setValider (true);
            $em = $this->getDoctrine()->getManager ();
            $em->persist ($livraison);
            $em->flush ();
            return $this->redirectToRoute('lister_odre');
        }else{
            return $this->render('StockBundle:OdreLivraison:validerOdreLovraison.html.twig',
                array(
                    'order'=>$livraison
                )
            );
        }
    }

}