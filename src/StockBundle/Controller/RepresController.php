<?php


namespace StockBundle\Controller;

use StockBundle\Form\CommnercantFormType;
use StockBundle\Form\LivrerMarchandiseFormType;
use StockBundle\Form\RepresentantFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use StockBundle\Repository\StaticMathod;
use StockBundle\Entity\ContratMarchandise;


class RepresController extends Controller
{
    public function livrerMarchandiseAction()
    {
        $form = $this->createForm(LivrerMarchandiseFormType::class);
        return $this->render('StockBundle:Repres:livrerMarchandise.html.twig',
            array(
                'form'=>$form->createView()
            )
        );
    }
   public function listerMarchandiseAction()
    {
        $repository = $this->getDoctrine()->getManager ()->getRepository (ContratMarchandise::class);
        $liste = $repository->findAll ();
        return $this->render('StockBundle:Repres:listerMarchandise.html.twig',
            array(
                'liste'=>$liste
            ));
    }

    public function addCommercantAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(CommnercantFormType::class);
        StaticMathod::persistData($request, $form, $em);
        return $this->render('StockBundle:Repres:addCommercant.html.twig',
            array(
                'form'=>$form->createView()
            )
        );

    }
	
}