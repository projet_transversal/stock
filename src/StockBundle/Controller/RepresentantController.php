<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/07/17
 * Time: 11:18
 */

namespace StockBundle\Controller;

use StockBundle\Entity\ContratMarchandise;
use StockBundle\Form\RepresentantFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use StockBundle\Repository\StaticMathod;
use StockBundle\Entity\Representant;
use Symfony\Component\HttpFoundation\Request;

class RepresentantController extends SocieteController
{

    public function addRepresentantAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository (Representant::class);
        $form = $this->createForm(RepresentantFormType::class);
        $message = StaticMathod::persistData($request, $form, $em);
        $representant = $form->getData ();
        if ($message == "ok") {
            if ($repository->verifiData($representant->getNom(), $representant->getLogin(),
                $representant->getEmail(), $representant->getTelephone())){
                $em->flush ();

            }else{
                $this->addFlash ("error", "objet deja existant");
            }
            return $this->redirectToRoute ("lister_representant");
        }
        return $this->render('StockBundle:Representant:addRepresentant.html.twig',
            array(
                'form' => $form->createView()
            )
        );

    }
    public function listerAction()
    {
        $repository = $this->getDoctrine()->getManager ()->getRepository (Representant::class);
        $liste = $repository->findAll ();
        return $this->render('StockBundle:Representant:listerRepresentant.html.twig',
            array(
                'liste'=>$liste
            )
        );
    }

    public function editerRepresentantAction(Request $request, Representant $representant)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(RepresentantFormType::class, $representant);
        $message = StaticMathod::persistData($request, $form, $em);
        if ($message == "ok"){
            return $this->redirectToRoute("lister_representant");
        }else {
            return $this->render('StockBundle:Representant:addRepresentant.html.twig',
                array(
                    'form' => $form->createView()
                )
            );
        }
    }

    public function deleteRepresentantAction(Representant $representant)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($representant);
        if ($em->getRepository(ContratMarchandise::class)->existe($representant->getId (), "representant") == null){
            $em->flush ();
        }else{
            $this->addFlash ("warning", "ce represnetant est lié à un(des) contrat(s)");
        }
        return $this->redirectToRoute("lister_representant");

    }
}