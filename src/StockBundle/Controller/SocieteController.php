<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/07/17
 * Time: 10:53
 */

namespace StockBundle\Controller;


use Doctrine\ORM\EntityManager;
use StockBundle\Repository\SocieteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

class SocieteController extends Controller
{
    public function envoieMail($mes, $to){
        $message = \Swift_Message::newInstance()
            ->setSubject('contact site web aceylanfr')
            ->setFrom(''."bambalimahine@gmail.com")
            ->setTo($to)
            ->setBody(
                $mes
            )
        ;
        $mailer = $this->get('mailer');
        $mailer->send($message);
    }
}