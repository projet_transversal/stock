<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/07/17
 * Time: 11:06
 */

namespace StockBundle\Controller;

use StockBundle\Repository\StaticMathod;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use StockBundle\Form\TraderFormType;
use Symfony\Component\HttpFoundation\Request;

class TraderController extends SocieteController
{

    public function addTraderAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(TraderFormType::class);
        StaticMathod::persistData($request, $form, $em);
        return $this->render('StockBundle:Trader:addTrader.html.twig',
            array(
                'form'=>$form->createView()
            )
        );

    }
}