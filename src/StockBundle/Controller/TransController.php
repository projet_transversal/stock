<?php


namespace StockBundle\Controller;

use StockBundle\Form\LivrerMarchandiseFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use StockBundle\Entity\ContratMarchandise;


class TransController extends Controller
{
    public function ajouterProformatAction()
    {
        $form = $this->createForm(LivrerMarchandiseFormType::class);
        return $this->render('StockBundle:Trans:ajouterProformat.html.twig',
            array(
                'form'=>$form->createView()
            )
        );
    }
   public function listerContratAction()
    {
        $repository = $this->getDoctrine()->getManager ()->getRepository (ContratMarchandise::class);
        $liste = $repository->findAll ();
        return $this->render('StockBundle:Trans:listerContrat.html.twig',
            array(
                'liste'=>$liste
            )
        );
    }

}