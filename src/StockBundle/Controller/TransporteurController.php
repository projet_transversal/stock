<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/07/17
 * Time: 11:11
 */

namespace StockBundle\Controller;

use StockBundle\Entity\Transporteur;
use StockBundle\Form\TransporteurFormType;
use StockBundle\Repository\StaticMathod;
use Symfony\Component\HttpFoundation\Request;
use StockBundle\Entity\ContratMarchandise;

class TransporteurController extends SocieteController
{

    public function addTransporteurAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository (Transporteur::class);
        $form = $this->createForm(TransporteurFormType::class);
        $message = StaticMathod::persistData($request, $form, $em);
        $transporteur = $form->getData ();
        if ($message == "ok") {
            if ($repository->verifiData($transporteur->getNom(), $transporteur->getLogin(),
                $transporteur->getEmail(), $transporteur->getTelephone())){
                $em->flush ();
                $mes = "Login = ".$transporteur->getLogin()." \n mot de passe = ".$transporteur->getPassword();
                $this->envoieMail ($mes, $transporteur->getEmail());
            }else{
                $this->addFlash ("error", "objet deja existant");
            }
            return $this->redirectToRoute ("lister_transporteur");
        }
        return $this->render('StockBundle:Transporteur:addTransporteur.html.twig',
            array(
                'form' => $form->createView()
            )
        );

    }
    public function listerAction()
    {
        $em = $this->getDoctrine()->getManager ();
        $repository = $em->getRepository (Transporteur::class);
        $liste = $repository->findAll ();
        return $this->render('StockBundle:Transporteur:listerTransporteur.html.twig',
            array(
                'liste'=>$liste
            )
        );
        //return new JsonResponse($liste);
    }

    public function editerTransporteurAction(Request $request, Transporteur $transporteur)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(TransporteurFormType::class, $transporteur);
        $message = StaticMathod::persistData($request, $form, $em);
        if ($message == "ok"){
            return $this->redirectToRoute("lister_transporteur");
        }else {
            return $this->render('StockBundle:Transporteur:addTransporteur.html.twig',
                array(
                    'form' => $form->createView()
                )
            );
        }
    }

    public function deleteTransporteurAction(Transporteur $transporteur)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($transporteur);
        if ($em->getRepository (ContratMarchandise::class)->existe($transporteur->getId (), "transporteur") == null){
            $em->flush ();
        }else{
            $this->addFlash ("warning", "ce transport est lie à un(des) contrat(s)");
        }

        return $this->redirectToRoute("lister_transporteur");

    }
}