<?php

namespace StockBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContratMarchandise
 *
 * @ORM\Table(name="contrat")
 * @ORM\Entity(repositoryClass="StockBundle\Repository\ContratMarchandiseRepository")
 */
class ContratMarchandise
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="text")
     */
    private $libelle;

    /**
     * @var boolean
     *
     * @ORM\Column(name="valide", type="boolean")
     */
    private $valide = false;

    /**
     * @var int
     *
     * @ORM\Column(name="quantite", type="integer")
     */
    private $quantite;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDepart", type="datetime")
     */
    private $dateDepart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateArrivee", type="datetime")
     */
    private $dateArrivee;

    /**
     * @ORM\ManyToOne(targetEntity="StockBundle\Entity\Transporteur")
     */
    protected $transporteur;

    /**
     * @ORM\ManyToOne(targetEntity="StockBundle\Entity\Representant")
     */
    private $representant;

    /**
     * @ORM\ManyToOne(targetEntity="StockBundle\Entity\Marchandise")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $marchandise;
    /**
     * @var int
     *
     * @ORM\Column(name="noteDebit", type="integer")
     */
    private $noteDebit = 0;

    /**
     * @return int
     */
    public function getNoteDebit()
    {
        return $this->noteDebit;
    }

    /**
     * @param int $noteDebit
     */
    public function setNoteDebit($noteDebit)
    {
        $this->noteDebit = $noteDebit;
    }

    /**
     * @return bool
     */
    public function isValide()
    {
        return $this->valide;
    }

    /**
     * @param bool $valide
     */
    public function setValide($valide)
    {
        $this->valide = $valide;
    }

    /**
     * @return int
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * @param int $quantite
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return ContratMarchandise
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @return mixed
     */
    public function getRepresentant()
    {
        return $this->representant;
    }

    /**
     * @param mixed $representant
     */
    public function setRepresentant($representant)
    {
        $this->representant = $representant;
    }

    /**
     * @return mixed
     */
    public function getMarchandise()
    {
        return $this->marchandise;
    }

    /**
     * @param mixed $marchandise
     */
    public function setMarchandise($marchandise)
    {
        $this->marchandise = $marchandise;
    }

    /**
     * @return mixed
     */
    public function getTransporteur()
    {
        return $this->transporteur;
    }

    /**
     * @param mixed $transporteur
     */
    public function setTransporteur($transporteur)
    {
        $this->transporteur = $transporteur;
    }


    /**
     * Set dateDepart
     *
     * @param \DateTime $dateDepart
     *
     * @return ContratMarchandise
     */
    public function setDateDepart($dateDepart)
    {
        $this->dateDepart = $dateDepart;

        return $this;
    }

    /**
     * Get dateDepart
     *
     * @return \DateTime
     */
    public function getDateDepart()
    {
        return $this->dateDepart;
    }

    /**
     * Set dateArrivee
     *
     * @param \DateTime $dateArrivee
     *
     * @return ContratMarchandise
     */
    public function setDateArrivee($dateArrivee)
    {
        $this->dateArrivee = $dateArrivee;

        return $this;
    }

    /**
     * Get dateArrivee
     *
     * @return \DateTime
     */
    public function getDateArrivee()
    {
        return $this->dateArrivee;
    }


}

