<?php

namespace StockBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OdreLivraison
 *
 * @ORM\Table(name="odre_livraison")
 * @ORM\Entity(repositoryClass="StockBundle\Repository\OdreLivraisonRepository")
 */
class OdreLivraison
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="text")
     */
    private $libelle;

    /**
     * @var int
     *
     * @ORM\Column(name="quante", type="integer")
     */
    private $quante;

    /**
     * @var bool
     *
     * @ORM\Column(name="valider", type="boolean")
     */
    private $valider = false;

    /**
     * @var string
     *
     * @ORM\Column(name="unite", type="string", length=20)
     */
    private $unite;

    /**
     * @ORM\ManyToOne(targetEntity="StockBundle\Entity\Commercant")
     */
    private $commercant;

    /**
     * @ORM\ManyToOne(targetEntity="StockBundle\Entity\ContratMarchandise")
     */
    private $contrat;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return OdreLivraison
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set quante
     *
     * @param integer $quante
     *
     * @return OdreLivraison
     */
    public function setQuante($quante)
    {
        $this->quante = $quante;

        return $this;
    }

    /**
     * Get quante
     *
     * @return int
     */
    public function getQuante()
    {
        return $this->quante;
    }

    /**
     * Set unite
     *
     * @param string $unite
     *
     * @return OdreLivraison
     */
    public function setUnite($unite)
    {
        $this->unite = $unite;

        return $this;
    }

    /**
     * Get unite
     *
     * @return string
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * @return mixed
     */
    public function getCommercant()
    {
        return $this->commercant;
    }

    /**
     * @param mixed $commercant
     */
    public function setCommercant($commercant)
    {
        $this->commercant = $commercant;
    }

    /**
     * @return bool
     */
    public function isValider()
    {
        return $this->valider;
    }

    /**
     * @param bool $valider
     */
    public function setValider($valider)
    {
        $this->valider = $valider;
    }

    /**
     * @return mixed
     */
    public function getContrat()
    {
        return $this->contrat;
    }

    /**
     * @param mixed $contrat
     */
    public function setContrat($contrat)
    {
        $this->contrat = $contrat;
    }


}

