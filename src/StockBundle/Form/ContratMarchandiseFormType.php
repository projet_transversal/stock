<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 09/07/2017
 * Time: 22:47
 */

namespace StockBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use StockBundle\Entity\ContratMarchandise;
use StockBundle\Entity\Marchandise;
use StockBundle\Entity\Representant;
use StockBundle\Entity\Transporteur;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ContratMarchandiseFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle', TextType::class)
            ->add('Date_Depart', DateType::class)
            ->add('Date_Arrivee', DateType::class)
            ->add('transporteur', EntityType::class,
                [
                    'class'=> Transporteur::class,
                    'choice_label' => 'login'
                ])
            ->add('representant', EntityType::class,
                [
                    'class'=> Representant::class,
                    'choice_label' => 'login'
                ])
            ->add('marchandise', EntityType::class,
                [
                    'class'=> Marchandise::class,
                    'choice_label' => 'nom'
                ])
            ->add ('quantite', TextType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'=>ContratMarchandise::class
            ]
        );
    }

}