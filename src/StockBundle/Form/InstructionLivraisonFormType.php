<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/07/17
 * Time: 10:47
 */

namespace StockBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use StockBundle\Entity\Marchandise;

class InstructionLivraisonFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle', TextType::class)
            ->add('marchandise', EntityType::class,
                [
                    'class'=> Marchandise::class,
                    'choice_label' => 'nom'
                ])
            ->add('quantite', IntegerType::class)
            ->add('unite', TextType::class)
        ;
    }
}