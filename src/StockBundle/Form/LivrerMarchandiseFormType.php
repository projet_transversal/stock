<?php


namespace StockBundle\Form;


use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class LivrerMarchandiseFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
			->add('commercant', TextType::class)
            ->add('produit', TextType::class)
            ->add('quantite', NumberType::class)
            ->add('unite', ChoiceType::class, [
                'choices'=>array(
                    'Tonne'=>'t',
                    'Kilogramme'=>'kg'
                )
            ])
            ->add('categorie', ChoiceType::class, [
                'choices'=>array(
                    'céréales'=>'cereales',
                    'electronique'=>'electronique'

                )
            ])
			
            ;
    }
}