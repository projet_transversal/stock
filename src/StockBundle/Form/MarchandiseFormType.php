<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 04/07/17
 * Time: 09:25
 */

namespace StockBundle\Form;


use StockBundle\Entity\Marchandise;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MarchandiseFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class)
            ->add('categorie', ChoiceType::class, [
                'choices'=>array(
                    'céréales'=>'cereales',
                    'electronique'=>'electronique',
                    'alcool'=>'alcool'
                )
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults([
            "data_class"=>Marchandise::class
        ]);
    }
}