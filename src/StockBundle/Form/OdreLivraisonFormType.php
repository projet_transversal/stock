<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 02/08/17
 * Time: 01:13
 */

namespace StockBundle\Entity;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OdreLivraisonFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('libelle', TextType::class)
            ->add('quantite', NumberType::class)
            ->add('unite', TextType::class)
            ->add('commercant', EntityType::class,
                [
                    'class'=> Commercant::class,
                    'choice_label' => 'nom'
                ]
                )
            ->add('commercant', EntityType::class,
                [
                    'class'=> ContratMarchandise::class,
                    'choice_label' => 'nom'
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'=>OdreLivraison::class
            ]
        );
    }
}