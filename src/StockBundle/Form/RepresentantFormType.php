<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/07/17
 * Time: 10:49
 */

namespace StockBundle\Form;


use StockBundle\Entity\Representant;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RepresentantFormType extends SocieteFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults([
            "data_class"=>Representant::class
        ]);
    }


}