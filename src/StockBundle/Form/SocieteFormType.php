<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/07/17
 * Time: 10:39
 */

namespace StockBundle\Form;


use StockBundle\Entity\Societe;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SocieteFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class)
            ->add('telephone', TextType::class)
            ->add('adresse', TextType::class)
            ->add('email', EmailType::class)
            ->add('login', TextType::class)
            ->add('passWord', PasswordType::class, array(
                'label'=>'Mot de pass'
            ))

            ;
    }


}