<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/07/17
 * Time: 10:50
 */

namespace StockBundle\Form;


use StockBundle\Entity\Transporteur;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TransporteurFormType extends SocieteFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults([
            "data_class"=>Transporteur::class
        ]);
    }

}