<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 08/07/17
 * Time: 01:04
 */

namespace StockBundle\Repository;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Repository\RepositoryFactory;
use StockBundle\Entity\Utilisateur;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

class StaticMathod extends EntityRepository
{
    /**
     * @param Request $request
     * @param Form $form
     * @param EntityManager $manager
     */
    public static function persistData(Request $request, Form $form, EntityManager $manager){
        $form->handleRequest($request);
        if ($form->isValid()){
            $objet = $form->getData();
            $manager->persist($objet);
            $manager->flush();
            return 'ok';
        }
        return 'error';
    }

    public static function verifiData($nom, $login, $email, $telephone, EntityManager $manager){
        $query = $manager->createQueryBuilder ("s")
            ->where ('s.nom = ? or s.email = ? or s.telephone = ? or s.login = ?')
            ->setParameter (1, $nom)
            ->setParameter (2, $email)
            ->setParameter (3, $telephone)
            ->setParameter (4, $login);

        if ($query->getQuery () == null){
            return true;
        }
        return false;
    }
}