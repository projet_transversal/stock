<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevDebugProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ($pathinfo === '/_profiler/open') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

        }

        // _twig_error_test
        if (preg_match('#^/(?P<_locale>en|fr|de|es|cs|nl|ru|uk|ro|pt_BR|pl|it|ja|id|ca|sl)/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',  '_locale' => 'en',));
        }

        // admin_index
        if (preg_match('#^/(?P<_locale>en|fr|de|es|cs|nl|ru|uk|ro|pt_BR|pl|it|ja|id|ca|sl)/admin/post/?$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_admin_index;
            }

            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_index');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_index')), array (  '_controller' => 'AppBundle\\Controller\\Admin\\BlogController::indexAction',  '_locale' => 'en',));
        }
        not_admin_index:

        // admin_post_index
        if (preg_match('#^/(?P<_locale>en|fr|de|es|cs|nl|ru|uk|ro|pt_BR|pl|it|ja|id|ca|sl)/admin/post/?$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_admin_post_index;
            }

            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_post_index');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_post_index')), array (  '_controller' => 'AppBundle\\Controller\\Admin\\BlogController::indexAction',  '_locale' => 'en',));
        }
        not_admin_post_index:

        // admin_post_new
        if (preg_match('#^/(?P<_locale>en|fr|de|es|cs|nl|ru|uk|ro|pt_BR|pl|it|ja|id|ca|sl)/admin/post/new$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_admin_post_new;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_post_new')), array (  '_controller' => 'AppBundle\\Controller\\Admin\\BlogController::newAction',  '_locale' => 'en',));
        }
        not_admin_post_new:

        // admin_post_show
        if (preg_match('#^/(?P<_locale>en|fr|de|es|cs|nl|ru|uk|ro|pt_BR|pl|it|ja|id|ca|sl)/admin/post/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_admin_post_show;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_post_show')), array (  '_controller' => 'AppBundle\\Controller\\Admin\\BlogController::showAction',  '_locale' => 'en',));
        }
        not_admin_post_show:

        // admin_post_edit
        if (preg_match('#^/(?P<_locale>en|fr|de|es|cs|nl|ru|uk|ro|pt_BR|pl|it|ja|id|ca|sl)/admin/post/(?P<id>\\d+)/edit$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_admin_post_edit;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_post_edit')), array (  '_controller' => 'AppBundle\\Controller\\Admin\\BlogController::editAction',  '_locale' => 'en',));
        }
        not_admin_post_edit:

        // admin_post_delete
        if (preg_match('#^/(?P<_locale>en|fr|de|es|cs|nl|ru|uk|ro|pt_BR|pl|it|ja|id|ca|sl)/admin/post/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_post_delete;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_post_delete')), array (  '_controller' => 'AppBundle\\Controller\\Admin\\BlogController::deleteAction',  '_locale' => 'en',));
        }
        not_admin_post_delete:

        // blog_index
        if (preg_match('#^/(?P<_locale>en|fr|de|es|cs|nl|ru|uk|ro|pt_BR|pl|it|ja|id|ca|sl)/blog/?$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_blog_index;
            }

            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'blog_index');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'blog_index')), array (  'page' => '1',  '_format' => 'html',  '_controller' => 'AppBundle\\Controller\\BlogController::indexAction',  '_locale' => 'en',));
        }
        not_blog_index:

        // blog_rss
        if (preg_match('#^/(?P<_locale>en|fr|de|es|cs|nl|ru|uk|ro|pt_BR|pl|it|ja|id|ca|sl)/blog/rss\\.xml$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_blog_rss;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'blog_rss')), array (  'page' => '1',  '_format' => 'xml',  '_controller' => 'AppBundle\\Controller\\BlogController::indexAction',  '_locale' => 'en',));
        }
        not_blog_rss:

        // blog_index_paginated
        if (preg_match('#^/(?P<_locale>en|fr|de|es|cs|nl|ru|uk|ro|pt_BR|pl|it|ja|id|ca|sl)/blog/page/(?P<page>[1-9]\\d*)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_blog_index_paginated;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'blog_index_paginated')), array (  '_format' => 'html',  '_controller' => 'AppBundle\\Controller\\BlogController::indexAction',  '_locale' => 'en',));
        }
        not_blog_index_paginated:

        // blog_post
        if (preg_match('#^/(?P<_locale>en|fr|de|es|cs|nl|ru|uk|ro|pt_BR|pl|it|ja|id|ca|sl)/blog/posts/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_blog_post;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'blog_post')), array (  '_controller' => 'AppBundle\\Controller\\BlogController::postShowAction',  '_locale' => 'en',));
        }
        not_blog_post:

        // comment_new
        if (preg_match('#^/(?P<_locale>en|fr|de|es|cs|nl|ru|uk|ro|pt_BR|pl|it|ja|id|ca|sl)/blog/comment/(?P<postSlug>[^/]++)/new$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_comment_new;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'comment_new')), array (  '_controller' => 'AppBundle\\Controller\\BlogController::commentNewAction',  '_locale' => 'en',));
        }
        not_comment_new:

        // security_login
        if (preg_match('#^/(?P<_locale>en|fr|de|es|cs|nl|ru|uk|ro|pt_BR|pl|it|ja|id|ca|sl)/login$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'security_login')), array (  '_controller' => 'AppBundle\\Controller\\SecurityController::loginAction',  '_locale' => 'en',));
        }

        // security_logout
        if (preg_match('#^/(?P<_locale>en|fr|de|es|cs|nl|ru|uk|ro|pt_BR|pl|it|ja|id|ca|sl)/logout$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'security_logout')), array (  '_controller' => 'AppBundle\\Controller\\SecurityController::logoutAction',  '_locale' => 'en',));
        }

        // login
        if ($pathinfo === '/login') {
            return array (  '_controller' => 'StockBundle\\Controller\\DefaultController::utilisateurAction',  '_locale' => 'en',  '_route' => 'login',);
        }

        // accueil
        if ($pathinfo === '/traderAccueil') {
            return array (  '_controller' => 'StockBundle\\Controller\\DefaultController::indexAction',  '_route' => 'accueil',);
        }

        if (0 === strpos($pathinfo, '/lister')) {
            if (0 === strpos($pathinfo, '/listerProduit')) {
                // lister_produit
                if ($pathinfo === '/listerProduit') {
                    return array (  '_controller' => 'StockBundle\\Controller\\MarchandiseController::listerAction',  '_route' => 'lister_produit',);
                }

                // lister_produits
                if ($pathinfo === '/listerProduits') {
                    return array (  '_controller' => 'StockBundle\\Controller\\MarchandiseController::getAction',  '_route' => 'lister_produits',);
                }

            }

            // lister_representant
            if ($pathinfo === '/listerRepresentant') {
                return array (  '_controller' => 'StockBundle\\Controller\\RepresentantController::listerAction',  '_route' => 'lister_representant',);
            }

            // lister_transporteur
            if ($pathinfo === '/listerTransporteur') {
                return array (  '_controller' => 'StockBundle\\Controller\\TransporteurController::listerAction',  '_route' => 'lister_transporteur',);
            }

            // lister_contrat
            if ($pathinfo === '/listerContrat') {
                return array (  '_controller' => 'StockBundle\\Controller\\ContratMarchandiseController::listerAction',  '_route' => 'lister_contrat',);
            }

            // lister_instruction
            if ($pathinfo === '/listerInstruction') {
                return array (  '_controller' => 'StockBundle\\Controller\\InstructionLivraisonController::listerAction',  '_route' => 'lister_instruction',);
            }

        }

        if (0 === strpos($pathinfo, '/a')) {
            // ajouter_instruction
            if ($pathinfo === '/ajouterInstruction') {
                return array (  '_controller' => 'StockBundle\\Controller\\InstructionLivraisonController::addInstructionLivraisonAction',  '_route' => 'ajouter_instruction',);
            }

            if (0 === strpos($pathinfo, '/add')) {
                // ajouter_produit
                if ($pathinfo === '/addProduit') {
                    return array (  '_controller' => 'StockBundle\\Controller\\MarchandiseController::addProduitAction',  '_route' => 'ajouter_produit',);
                }

                // ajouter_representant
                if ($pathinfo === '/addRepresentant') {
                    return array (  '_controller' => 'StockBundle\\Controller\\RepresentantController::addRepresentantAction',  '_route' => 'ajouter_representant',);
                }

                if (0 === strpos($pathinfo, '/addTra')) {
                    // ajouter_trader
                    if ($pathinfo === '/addTrader') {
                        return array (  '_controller' => 'StockBundle\\Controller\\TraderController::addTraderAction',  '_route' => 'ajouter_trader',);
                    }

                    // ajouter_transporteur
                    if ($pathinfo === '/addTransporteur') {
                        return array (  '_controller' => 'StockBundle\\Controller\\TransporteurController::addTransporteurAction',  '_route' => 'ajouter_transporteur',);
                    }

                }

                // ajouter_contratMarchandise
                if ($pathinfo === '/addContratMarchandise') {
                    return array (  '_controller' => 'StockBundle\\Controller\\ContratMarchandiseController::addContratMarchandiseAction',  '_route' => 'ajouter_contratMarchandise',);
                }

            }

        }

        // delete_transporteur
        if (preg_match('#^/(?P<id>[^/]++)/deleteTransporteur$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'delete_transporteur')), array (  '_controller' => 'StockBundle\\Controller\\TransporteurController::deleteTransporteurAction',));
        }

        // delete_contrat
        if (preg_match('#^/(?P<id>[^/]++)/deleteContrat$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'delete_contrat')), array (  '_controller' => 'StockBundle\\Controller\\ContratMarchandiseController::deleteContratAction',));
        }

        // delete_representant
        if (preg_match('#^/(?P<id>[^/]++)/deleteRepresentant$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'delete_representant')), array (  '_controller' => 'StockBundle\\Controller\\RepresentantController::deleteRepresentantAction',));
        }

        // delete_marchandise
        if (preg_match('#^/(?P<id>[^/]++)/deleteMarchandise$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'delete_marchandise')), array (  '_controller' => 'StockBundle\\Controller\\MarchandiseController::deleteMarchandiseAction',));
        }

        // editer_marchandise
        if (preg_match('#^/(?P<id>[^/]++)/editerMarchandise$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'editer_marchandise')), array (  '_controller' => 'StockBundle\\Controller\\MarchandiseController::editerMarchandiseAction',));
        }

        // editer_transporteur
        if (preg_match('#^/(?P<id>[^/]++)/editerTransporteur$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'editer_transporteur')), array (  '_controller' => 'StockBundle\\Controller\\TransporteurController::editerTransporteurAction',));
        }

        // editer_representant
        if (preg_match('#^/(?P<id>[^/]++)/editerRepresentant$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'editer_representant')), array (  '_controller' => 'StockBundle\\Controller\\RepresentantController::editerRepresentantAction',));
        }

        // editer_contrat
        if (preg_match('#^/(?P<id>[^/]++)/editerContrat$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'editer_contrat')), array (  '_controller' => 'StockBundle\\Controller\\ContratMarchandiseController::editerContratAction',));
        }

        // visualiser_contrat
        if (preg_match('#^/(?P<id>[^/]++)/visualiserContrat$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'visualiser_contrat')), array (  '_controller' => 'StockBundle\\Controller\\ContratMarchandiseController::visualiserContratAction',));
        }

        // editer_instruction
        if (preg_match('#^/(?P<id>[^/]++)/editerInstruction$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'editer_instruction')), array (  '_controller' => 'StockBundle\\Controller\\InstructionLivraisonController::editerInstructionAction',));
        }

        // delete_instruction
        if (preg_match('#^/(?P<id>[^/]++)/deleteInstruction$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'delete_instruction')), array (  '_controller' => 'StockBundle\\Controller\\InstructionLivraisonController::deleteInstructionAction',));
        }

        if (0 === strpos($pathinfo, '/representant')) {
            if (0 === strpos($pathinfo, '/representant/li')) {
                // representant_livrer_marchandise
                if ($pathinfo === '/representant/livrerMarchandise') {
                    return array (  '_controller' => 'StockBundle\\Controller\\RepresController::livrerMarchandiseAction',  '_route' => 'representant_livrer_marchandise',);
                }

                // representant_lister_marchandise
                if ($pathinfo === '/representant/listerMarchandise') {
                    return array (  '_controller' => 'StockBundle\\Controller\\RepresController::listerMarchandiseAction',  '_route' => 'representant_lister_marchandise',);
                }

            }

            // representant_ajouter_comm
            if ($pathinfo === '/representant/addCommercant') {
                return array (  '_controller' => 'StockBundle\\Controller\\RepresController::addCommercantAction',  '_route' => 'representant_ajouter_comm',);
            }

        }

        if (0 === strpos($pathinfo, '/transporteur')) {
            // transporteur_ajouter_proformat
            if ($pathinfo === '/transporteur/addProformat') {
                return array (  '_controller' => 'StockBundle\\Controller\\TransController::ajouterProformatAction',  '_route' => 'transporteur_ajouter_proformat',);
            }

            // transporteur_listerContrat
            if ($pathinfo === '/transporteur/listerContrat') {
                return array (  '_controller' => 'StockBundle\\Controller\\TransController::listerContratAction',  '_route' => 'transporteur_listerContrat',);
            }

        }

        // valider_contrat
        if (preg_match('#^/(?P<id>[^/]++)/valideContrat$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'valider_contrat')), array (  '_controller' => 'StockBundle\\Controller\\ContratMarchandiseController::valideContratAction',));
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
