<?php

/* StockBundle:ContratMarchandise:listerContratMarchandise.html.twig */
class __TwigTemplate_7a509d9745b9ad741a2ef882b32cdf43f25057b36329676837c3d76c0bea0c11 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:ContratMarchandise:listerContratMarchandise.html.twig", 1);
        $this->blocks = array(
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9bbd10c3ca402361fb7b60c8fd650138c292d85f7c7db138a631a46a9a16a1e5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9bbd10c3ca402361fb7b60c8fd650138c292d85f7c7db138a631a46a9a16a1e5->enter($__internal_9bbd10c3ca402361fb7b60c8fd650138c292d85f7c7db138a631a46a9a16a1e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:ContratMarchandise:listerContratMarchandise.html.twig"));

        $__internal_4e33060f3838c65cf0c67887e54d5a3273b511c933eeba14c93b566b4520a330 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4e33060f3838c65cf0c67887e54d5a3273b511c933eeba14c93b566b4520a330->enter($__internal_4e33060f3838c65cf0c67887e54d5a3273b511c933eeba14c93b566b4520a330_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:ContratMarchandise:listerContratMarchandise.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9bbd10c3ca402361fb7b60c8fd650138c292d85f7c7db138a631a46a9a16a1e5->leave($__internal_9bbd10c3ca402361fb7b60c8fd650138c292d85f7c7db138a631a46a9a16a1e5_prof);

        
        $__internal_4e33060f3838c65cf0c67887e54d5a3273b511c933eeba14c93b566b4520a330->leave($__internal_4e33060f3838c65cf0c67887e54d5a3273b511c933eeba14c93b566b4520a330_prof);

    }

    // line 2
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_dc7b0b16c413933717771455c1bdda6429a95570e7f3a79271d74d2f1acc309f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dc7b0b16c413933717771455c1bdda6429a95570e7f3a79271d74d2f1acc309f->enter($__internal_dc7b0b16c413933717771455c1bdda6429a95570e7f3a79271d74d2f1acc309f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_35a369333528c5e8d3a4b211822b95c9831eae9ad4927a645901d951613323ad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_35a369333528c5e8d3a4b211822b95c9831eae9ad4927a645901d951613323ad->enter($__internal_35a369333528c5e8d3a4b211822b95c9831eae9ad4927a645901d951613323ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        echo "<!--le block du milieu de la page-->
    ";
        // line 3
        $this->displayBlock('debutPage', $context, $blocks);
        // line 11
        echo "    ";
        $this->displayBlock('modules', $context, $blocks);
        
        $__internal_35a369333528c5e8d3a4b211822b95c9831eae9ad4927a645901d951613323ad->leave($__internal_35a369333528c5e8d3a4b211822b95c9831eae9ad4927a645901d951613323ad_prof);

        
        $__internal_dc7b0b16c413933717771455c1bdda6429a95570e7f3a79271d74d2f1acc309f->leave($__internal_dc7b0b16c413933717771455c1bdda6429a95570e7f3a79271d74d2f1acc309f_prof);

    }

    // line 3
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_b0762d7ced5b1b4e71272c9ea9663fcf04fa5cf49e691d2396fb9c41a31a8b5c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b0762d7ced5b1b4e71272c9ea9663fcf04fa5cf49e691d2396fb9c41a31a8b5c->enter($__internal_b0762d7ced5b1b4e71272c9ea9663fcf04fa5cf49e691d2396fb9c41a31a8b5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_854a5a3f942405120e09f7d9de496d0cb140c7e45c86f12aa0fa061bbec458f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_854a5a3f942405120e09f7d9de496d0cb140c7e45c86f12aa0fa061bbec458f6->enter($__internal_854a5a3f942405120e09f7d9de496d0cb140c7e45c86f12aa0fa061bbec458f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 4
        echo "        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-45px;\">
                <h3 style=\"color: white;margin-top:4%\"> <center>Liste des Contrats</center></h3>
            </div>
            <br><br>
        </div>
    ";
        
        $__internal_854a5a3f942405120e09f7d9de496d0cb140c7e45c86f12aa0fa061bbec458f6->leave($__internal_854a5a3f942405120e09f7d9de496d0cb140c7e45c86f12aa0fa061bbec458f6_prof);

        
        $__internal_b0762d7ced5b1b4e71272c9ea9663fcf04fa5cf49e691d2396fb9c41a31a8b5c->leave($__internal_b0762d7ced5b1b4e71272c9ea9663fcf04fa5cf49e691d2396fb9c41a31a8b5c_prof);

    }

    // line 11
    public function block_modules($context, array $blocks = array())
    {
        $__internal_4a8d9f1d91ca3d983c927642bbef972185f2dad934e00c998f779fbd966b916a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a8d9f1d91ca3d983c927642bbef972185f2dad934e00c998f779fbd966b916a->enter($__internal_4a8d9f1d91ca3d983c927642bbef972185f2dad934e00c998f779fbd966b916a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_4e21e3f9f447aa36c6118910c305eefaa553ee8576d9aa3fdb90b137cb0e2861 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4e21e3f9f447aa36c6118910c305eefaa553ee8576d9aa3fdb90b137cb0e2861->enter($__internal_4e21e3f9f447aa36c6118910c305eefaa553ee8576d9aa3fdb90b137cb0e2861_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        echo "<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"margin-left: -12%\">
              `
                    <div class=\"\">
                        <table   class=\"display table table-striped table-bordered\" cellspacing=\"0\" width=\"100%\" id=\"table\" style=\"margin-left:-20%;margin-top:-11%\">
                            <thead>
                                <tr class=\"jumbotron\">
                                    <th id=\"id\">Numero</th>
                                    <th id=\"libelle\" >Libelle</th>
                                    <th id=\"quantite\" >Quantite</th>
                                    <th id=\"transporteurs\" >Transporteur</th>
                                    <th id=\"representant\">Representant</th>
                                    <th id=\"dateDepart\" >Date Depart</th>
                                    <th id=\"dateArrivee\">Date Arrivee</th>
                                    <th id=\"noteDebit\">Note de Debit</th>
                                    <th id=\"modifier\">Modifier</th>
                                    <th id=\"supprimer\" >Supprimer</th>
                                    <th id=\"visualiser\" >Visualiser</th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 33
        $context["id"] = 1;
        // line 34
        echo "                                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["liste"] ?? $this->getContext($context, "liste")));
        foreach ($context['_seq'] as $context["_key"] => $context["l"]) {
            // line 35
            echo "                                    <tr>
                                        <td>";
            // line 36
            echo twig_escape_filter($this->env, ($context["id"] ?? $this->getContext($context, "id")), "html", null, true);
            echo "</td>
                                        <td>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "libelle", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "quantite", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["l"], "transporteur", array()), "login", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["l"], "representant", array()), "login", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 41
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["l"], "getDateDepart", array(), "method"), "d-m-Y"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 42
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["l"], "getDateArrivee", array(), "method"), "d-m-Y"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "noteDebit", array()), "html", null, true);
            echo "</td>
                                        <td><a href=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("editer_contrat", array("id" => $this->getAttribute($context["l"], "id", array()))), "html", null, true);
            echo "\" class=\"font-icon font-icon-pen\" style=\"color: green\"></a></td>
                                        <td><a href=\"";
            // line 45
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("delete_contrat", array("id" => $this->getAttribute($context["l"], "id", array()))), "html", null, true);
            echo "\" class=\"font-icon font-icon-del\" style=\"color: red\"></a></td>
                                        <td><a href=\"";
            // line 46
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("visualiser_contrat", array("id" => $this->getAttribute($context["l"], "id", array()))), "html", null, true);
            echo "\" class=\"font-icon font-icon-doc\" style=\"color: red\"></a></td>
                                    </tr>
                                    ";
            // line 48
            $context["id"] = (($context["id"] ?? $this->getContext($context, "id")) + 1);
            // line 49
            echo "                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['l'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "                            </tbody>
                        </table>
                    </div>


            </div><!--.container-fluid-->
        </div><!--.page-content-->
    ";
        
        $__internal_4e21e3f9f447aa36c6118910c305eefaa553ee8576d9aa3fdb90b137cb0e2861->leave($__internal_4e21e3f9f447aa36c6118910c305eefaa553ee8576d9aa3fdb90b137cb0e2861_prof);

        
        $__internal_4a8d9f1d91ca3d983c927642bbef972185f2dad934e00c998f779fbd966b916a->leave($__internal_4a8d9f1d91ca3d983c927642bbef972185f2dad934e00c998f779fbd966b916a_prof);

    }

    // line 59
    public function block_script($context, array $blocks = array())
    {
        $__internal_48631931a30271df3004bfe58496016cf681799a9a093bdf4ac87f7ea5ce9d66 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_48631931a30271df3004bfe58496016cf681799a9a093bdf4ac87f7ea5ce9d66->enter($__internal_48631931a30271df3004bfe58496016cf681799a9a093bdf4ac87f7ea5ce9d66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        $__internal_c4a760da54a2238c74030fd282c00a9c2ff80b6ff3650bac080d961915a7cddf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c4a760da54a2238c74030fd282c00a9c2ff80b6ff3650bac080d961915a7cddf->enter($__internal_c4a760da54a2238c74030fd282c00a9c2ff80b6ff3650bac080d961915a7cddf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        // line 60
        echo "    ";
        $this->displayParentBlock("script", $context, $blocks);
        echo "
    <script src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/jquery.tablednd.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/listerContrat.js"), "html", null, true);
        echo "\"></script>

";
        
        $__internal_c4a760da54a2238c74030fd282c00a9c2ff80b6ff3650bac080d961915a7cddf->leave($__internal_c4a760da54a2238c74030fd282c00a9c2ff80b6ff3650bac080d961915a7cddf_prof);

        
        $__internal_48631931a30271df3004bfe58496016cf681799a9a093bdf4ac87f7ea5ce9d66->leave($__internal_48631931a30271df3004bfe58496016cf681799a9a093bdf4ac87f7ea5ce9d66_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:ContratMarchandise:listerContratMarchandise.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  230 => 64,  226 => 63,  222 => 62,  218 => 61,  213 => 60,  204 => 59,  187 => 50,  181 => 49,  179 => 48,  174 => 46,  170 => 45,  166 => 44,  162 => 43,  158 => 42,  154 => 41,  150 => 40,  146 => 39,  142 => 38,  138 => 37,  134 => 36,  131 => 35,  126 => 34,  124 => 33,  92 => 11,  76 => 4,  67 => 3,  56 => 11,  54 => 3,  43 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}
{% block contenuDuMilieu %}<!--le block du milieu de la page-->
    {% block debutPage %}
        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-45px;\">
                <h3 style=\"color: white;margin-top:4%\"> <center>Liste des Contrats</center></h3>
            </div>
            <br><br>
        </div>
    {% endblock %}
    {% block modules %}<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"margin-left: -12%\">
              `
                    <div class=\"\">
                        <table   class=\"display table table-striped table-bordered\" cellspacing=\"0\" width=\"100%\" id=\"table\" style=\"margin-left:-20%;margin-top:-11%\">
                            <thead>
                                <tr class=\"jumbotron\">
                                    <th id=\"id\">Numero</th>
                                    <th id=\"libelle\" >Libelle</th>
                                    <th id=\"quantite\" >Quantite</th>
                                    <th id=\"transporteurs\" >Transporteur</th>
                                    <th id=\"representant\">Representant</th>
                                    <th id=\"dateDepart\" >Date Depart</th>
                                    <th id=\"dateArrivee\">Date Arrivee</th>
                                    <th id=\"noteDebit\">Note de Debit</th>
                                    <th id=\"modifier\">Modifier</th>
                                    <th id=\"supprimer\" >Supprimer</th>
                                    <th id=\"visualiser\" >Visualiser</th>
                                </tr>
                            </thead>
                            <tbody>
                                {% set id = 1 %}
                                {% for l in liste %}
                                    <tr>
                                        <td>{{ id }}</td>
                                        <td>{{ l.libelle }}</td>
                                        <td>{{ l.quantite }}</td>
                                        <td>{{ l.transporteur.login }}</td>
                                        <td>{{ l.representant.login }}</td>
                                        <td>{{ l.getDateDepart()|date('d-m-Y') }}</td>
                                        <td>{{ l.getDateArrivee()|date('d-m-Y') }}</td>
                                        <td>{{ l.noteDebit }}</td>
                                        <td><a href=\"{{ path(\"editer_contrat\", {'id':l.id}) }}\" class=\"font-icon font-icon-pen\" style=\"color: green\"></a></td>
                                        <td><a href=\"{{ path(\"delete_contrat\", {'id':l.id}) }}\" class=\"font-icon font-icon-del\" style=\"color: red\"></a></td>
                                        <td><a href=\"{{ path(\"visualiser_contrat\", {'id':l.id}) }}\" class=\"font-icon font-icon-doc\" style=\"color: red\"></a></td>
                                    </tr>
                                    {% set id = id +1 %}
                                {% endfor %}
                            </tbody>
                        </table>
                    </div>


            </div><!--.container-fluid-->
        </div><!--.page-content-->
    {% endblock %}
{% endblock %}
{% block script %}
    {{ parent() }}
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/jquery.tablednd.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/listerContrat.js') }}\"></script>

{% endblock %}", "StockBundle:ContratMarchandise:listerContratMarchandise.html.twig", "C:\\wamp64\\www\\stock\\src\\StockBundle/Resources/views/ContratMarchandise/listerContratMarchandise.html.twig");
    }
}
