<?php

/* StockBundle:Marchandise:listerMarchandise.html.twig */
class __TwigTemplate_d2637a0c9ac0d4511d56c88782dd1c583bdfa061da81195bcd0d7c2c313a7b06 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:Marchandise:listerMarchandise.html.twig", 1);
        $this->blocks = array(
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e1ff8cc1a52826f32d05e05c1c9b319afba5544a06dc98c947253eea4713c82b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e1ff8cc1a52826f32d05e05c1c9b319afba5544a06dc98c947253eea4713c82b->enter($__internal_e1ff8cc1a52826f32d05e05c1c9b319afba5544a06dc98c947253eea4713c82b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Marchandise:listerMarchandise.html.twig"));

        $__internal_6275894c5b0c1211ea3a4fc6b92f6d56f79bb983e749f1d1d5f15361a805b0b8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6275894c5b0c1211ea3a4fc6b92f6d56f79bb983e749f1d1d5f15361a805b0b8->enter($__internal_6275894c5b0c1211ea3a4fc6b92f6d56f79bb983e749f1d1d5f15361a805b0b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Marchandise:listerMarchandise.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e1ff8cc1a52826f32d05e05c1c9b319afba5544a06dc98c947253eea4713c82b->leave($__internal_e1ff8cc1a52826f32d05e05c1c9b319afba5544a06dc98c947253eea4713c82b_prof);

        
        $__internal_6275894c5b0c1211ea3a4fc6b92f6d56f79bb983e749f1d1d5f15361a805b0b8->leave($__internal_6275894c5b0c1211ea3a4fc6b92f6d56f79bb983e749f1d1d5f15361a805b0b8_prof);

    }

    // line 2
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_42d797e6e7e4b9016657f342184542ca45bf39fa121e8596da0c19440eeca4f1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_42d797e6e7e4b9016657f342184542ca45bf39fa121e8596da0c19440eeca4f1->enter($__internal_42d797e6e7e4b9016657f342184542ca45bf39fa121e8596da0c19440eeca4f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_c2b1980510a4ba687daed12ecabd0896bbfab4d588784ebfde29e0536eb224df = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c2b1980510a4ba687daed12ecabd0896bbfab4d588784ebfde29e0536eb224df->enter($__internal_c2b1980510a4ba687daed12ecabd0896bbfab4d588784ebfde29e0536eb224df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        echo "<!--le block du milieu de la page-->
    ";
        // line 3
        $this->displayBlock('debutPage', $context, $blocks);
        // line 11
        echo "    ";
        $this->displayBlock('modules', $context, $blocks);
        
        $__internal_c2b1980510a4ba687daed12ecabd0896bbfab4d588784ebfde29e0536eb224df->leave($__internal_c2b1980510a4ba687daed12ecabd0896bbfab4d588784ebfde29e0536eb224df_prof);

        
        $__internal_42d797e6e7e4b9016657f342184542ca45bf39fa121e8596da0c19440eeca4f1->leave($__internal_42d797e6e7e4b9016657f342184542ca45bf39fa121e8596da0c19440eeca4f1_prof);

    }

    // line 3
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_124301bdfbd412bf467905b5b20528149ff412b03010b3b5c26d85e73f164d8e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_124301bdfbd412bf467905b5b20528149ff412b03010b3b5c26d85e73f164d8e->enter($__internal_124301bdfbd412bf467905b5b20528149ff412b03010b3b5c26d85e73f164d8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_d42e545bb102487caba12adf28f172c59836ccf102c3393ac481254ad3f3cb05 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d42e545bb102487caba12adf28f172c59836ccf102c3393ac481254ad3f3cb05->enter($__internal_d42e545bb102487caba12adf28f172c59836ccf102c3393ac481254ad3f3cb05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 4
        echo "        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                <h3 style=\"color: white;margin-top:4%\"> <center>Liste des Marchandises</center></h3>
            </div>
            <br><br>
        </div>
    ";
        
        $__internal_d42e545bb102487caba12adf28f172c59836ccf102c3393ac481254ad3f3cb05->leave($__internal_d42e545bb102487caba12adf28f172c59836ccf102c3393ac481254ad3f3cb05_prof);

        
        $__internal_124301bdfbd412bf467905b5b20528149ff412b03010b3b5c26d85e73f164d8e->leave($__internal_124301bdfbd412bf467905b5b20528149ff412b03010b3b5c26d85e73f164d8e_prof);

    }

    // line 11
    public function block_modules($context, array $blocks = array())
    {
        $__internal_d1943248e8072a1ade62c0ab3651df754e6d225a83d4cfa7bbd482c7697fcc62 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d1943248e8072a1ade62c0ab3651df754e6d225a83d4cfa7bbd482c7697fcc62->enter($__internal_d1943248e8072a1ade62c0ab3651df754e6d225a83d4cfa7bbd482c7697fcc62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_cf5439f1d461c97977f0f73e721a0ad6c1cd8cc8068b85e4d3c0dd5d2d23e3d3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cf5439f1d461c97977f0f73e721a0ad6c1cd8cc8068b85e4d3c0dd5d2d23e3d3->enter($__internal_cf5439f1d461c97977f0f73e721a0ad6c1cd8cc8068b85e4d3c0dd5d2d23e3d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        echo "<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"margin-left: -12%\">

                    <div class=\"\">
                        <table  class=\"display table table-striped table-bordered\" cellspacing=\"0\" width=\"100%\" id=\"table\" style=\"margin-left:1%;margin-top: -10%\">
                            <thead>
                                <tr class=\"jumbotron\">
                                    <th data-field=\"id\">ID</th>
                                    <th data-field=\"libelle\">Libelle</th>
                                    <th data-field=\"categorie\">Categorie</th>
                                    <th data-field=\"Modifier\">Modifier</th>
                                    <th data-field=\"Supprimer\">Supprimer</th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 27
        $context["j"] = 1;
        // line 28
        echo "                                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["liste"] ?? $this->getContext($context, "liste")));
        foreach ($context['_seq'] as $context["_key"] => $context["l"]) {
            // line 29
            echo "                                    <tr>
                                        <td>";
            // line 30
            echo twig_escape_filter($this->env, ($context["j"] ?? $this->getContext($context, "j")), "html", null, true);
            echo "</td>
                                        <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "nom", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "categorie", array()), "html", null, true);
            echo "</td>
                                        <td><a href=\"";
            // line 33
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("editer_marchandise", array("id" => $this->getAttribute($context["l"], "id", array()))), "html", null, true);
            echo "\" class=\"font-icon font-icon-pen\" style=\"color: green\"></a></td>
                                        <td><a href=\"";
            // line 34
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("delete_marchandise", array("id" => $this->getAttribute($context["l"], "id", array()))), "html", null, true);
            echo "\" class=\"font-icon font-icon-del\" style=\"color: red\"></a></td>
                                    </tr>
                                    ";
            // line 36
            $context["j"] = (($context["j"] ?? $this->getContext($context, "j")) + 1);
            // line 37
            echo "                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['l'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "                            </tbody>
                        </table>
                    </div>


            </div><!--.container-fluid-->
        </div><!--.page-content-->
    ";
        
        $__internal_cf5439f1d461c97977f0f73e721a0ad6c1cd8cc8068b85e4d3c0dd5d2d23e3d3->leave($__internal_cf5439f1d461c97977f0f73e721a0ad6c1cd8cc8068b85e4d3c0dd5d2d23e3d3_prof);

        
        $__internal_d1943248e8072a1ade62c0ab3651df754e6d225a83d4cfa7bbd482c7697fcc62->leave($__internal_d1943248e8072a1ade62c0ab3651df754e6d225a83d4cfa7bbd482c7697fcc62_prof);

    }

    // line 47
    public function block_script($context, array $blocks = array())
    {
        $__internal_56cd710ae1c60aa44993c179456fb8d65724bb5cd6525bf397829e6e4b176027 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_56cd710ae1c60aa44993c179456fb8d65724bb5cd6525bf397829e6e4b176027->enter($__internal_56cd710ae1c60aa44993c179456fb8d65724bb5cd6525bf397829e6e4b176027_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        $__internal_193e702c09f9c1304930fe71efc2a2b25b88e7fa972edfada47a69fc649b0e55 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_193e702c09f9c1304930fe71efc2a2b25b88e7fa972edfada47a69fc649b0e55->enter($__internal_193e702c09f9c1304930fe71efc2a2b25b88e7fa972edfada47a69fc649b0e55_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        // line 48
        echo "    ";
        $this->displayParentBlock("script", $context, $blocks);
        echo "
    <script src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/jquery.tablednd.js"), "html", null, true);
        echo "\"></script>
    ";
        
        $__internal_193e702c09f9c1304930fe71efc2a2b25b88e7fa972edfada47a69fc649b0e55->leave($__internal_193e702c09f9c1304930fe71efc2a2b25b88e7fa972edfada47a69fc649b0e55_prof);

        
        $__internal_56cd710ae1c60aa44993c179456fb8d65724bb5cd6525bf397829e6e4b176027->leave($__internal_56cd710ae1c60aa44993c179456fb8d65724bb5cd6525bf397829e6e4b176027_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Marchandise:listerMarchandise.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  196 => 51,  192 => 50,  188 => 49,  183 => 48,  174 => 47,  157 => 38,  151 => 37,  149 => 36,  144 => 34,  140 => 33,  136 => 32,  132 => 31,  128 => 30,  125 => 29,  120 => 28,  118 => 27,  92 => 11,  76 => 4,  67 => 3,  56 => 11,  54 => 3,  43 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}
{% block contenuDuMilieu %}<!--le block du milieu de la page-->
    {% block debutPage %}
        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                <h3 style=\"color: white;margin-top:4%\"> <center>Liste des Marchandises</center></h3>
            </div>
            <br><br>
        </div>
    {% endblock %}
    {% block modules %}<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"margin-left: -12%\">

                    <div class=\"\">
                        <table  class=\"display table table-striped table-bordered\" cellspacing=\"0\" width=\"100%\" id=\"table\" style=\"margin-left:1%;margin-top: -10%\">
                            <thead>
                                <tr class=\"jumbotron\">
                                    <th data-field=\"id\">ID</th>
                                    <th data-field=\"libelle\">Libelle</th>
                                    <th data-field=\"categorie\">Categorie</th>
                                    <th data-field=\"Modifier\">Modifier</th>
                                    <th data-field=\"Supprimer\">Supprimer</th>
                                </tr>
                            </thead>
                            <tbody>
                                {% set j=1 %}
                                {% for l in liste %}
                                    <tr>
                                        <td>{{ j }}</td>
                                        <td>{{ l.nom }}</td>
                                        <td>{{ l.categorie }}</td>
                                        <td><a href=\"{{ path(\"editer_marchandise\", {'id':l.id}) }}\" class=\"font-icon font-icon-pen\" style=\"color: green\"></a></td>
                                        <td><a href=\"{{ path(\"delete_marchandise\", {'id':l.id}) }}\" class=\"font-icon font-icon-del\" style=\"color: red\"></a></td>
                                    </tr>
                                    {% set j=j+1 %}
                                {% endfor %}
                            </tbody>
                        </table>
                    </div>


            </div><!--.container-fluid-->
        </div><!--.page-content-->
    {% endblock %}
{% endblock %}
{% block script %}
    {{ parent() }}
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/jquery.tablednd.js') }}\"></script>
    {#<script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table-reorder-rows-init.js') }}\"></script>#}
{% endblock %}", "StockBundle:Marchandise:listerMarchandise.html.twig", "C:\\wamp64\\www\\stock\\src\\StockBundle/Resources/views/Marchandise/listerMarchandise.html.twig");
    }
}
