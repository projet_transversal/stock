<?php

/* StockBundle:Representant:listerRepresentant.html.twig */
class __TwigTemplate_6e2c5244e61d81557028ff3d47d44b212cddfda22b033607566c4925eec46391 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:Representant:listerRepresentant.html.twig", 1);
        $this->blocks = array(
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1fe649c7121bee13bcc6c00d39d27f4a2782dc6c30756f50bbaac35661f75951 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1fe649c7121bee13bcc6c00d39d27f4a2782dc6c30756f50bbaac35661f75951->enter($__internal_1fe649c7121bee13bcc6c00d39d27f4a2782dc6c30756f50bbaac35661f75951_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Representant:listerRepresentant.html.twig"));

        $__internal_c58975a8d6ec8358567b57e1aa37d58dab3cfa3b25d7f362d463ab33a5cfbf70 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c58975a8d6ec8358567b57e1aa37d58dab3cfa3b25d7f362d463ab33a5cfbf70->enter($__internal_c58975a8d6ec8358567b57e1aa37d58dab3cfa3b25d7f362d463ab33a5cfbf70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Representant:listerRepresentant.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1fe649c7121bee13bcc6c00d39d27f4a2782dc6c30756f50bbaac35661f75951->leave($__internal_1fe649c7121bee13bcc6c00d39d27f4a2782dc6c30756f50bbaac35661f75951_prof);

        
        $__internal_c58975a8d6ec8358567b57e1aa37d58dab3cfa3b25d7f362d463ab33a5cfbf70->leave($__internal_c58975a8d6ec8358567b57e1aa37d58dab3cfa3b25d7f362d463ab33a5cfbf70_prof);

    }

    // line 2
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_8efb3806293103f3b9b67e0c8ed2c0fb48eeacd21c68fea49a454a9c76d6b8c4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8efb3806293103f3b9b67e0c8ed2c0fb48eeacd21c68fea49a454a9c76d6b8c4->enter($__internal_8efb3806293103f3b9b67e0c8ed2c0fb48eeacd21c68fea49a454a9c76d6b8c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_66a14c59b44cc4a13d5bf7b1db3fec7e35ac63ce005e86c8a42487401bf234d5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_66a14c59b44cc4a13d5bf7b1db3fec7e35ac63ce005e86c8a42487401bf234d5->enter($__internal_66a14c59b44cc4a13d5bf7b1db3fec7e35ac63ce005e86c8a42487401bf234d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        echo "<!--le block du milieu de la page-->
    ";
        // line 3
        $this->displayBlock('debutPage', $context, $blocks);
        // line 11
        echo "    ";
        $this->displayBlock('modules', $context, $blocks);
        
        $__internal_66a14c59b44cc4a13d5bf7b1db3fec7e35ac63ce005e86c8a42487401bf234d5->leave($__internal_66a14c59b44cc4a13d5bf7b1db3fec7e35ac63ce005e86c8a42487401bf234d5_prof);

        
        $__internal_8efb3806293103f3b9b67e0c8ed2c0fb48eeacd21c68fea49a454a9c76d6b8c4->leave($__internal_8efb3806293103f3b9b67e0c8ed2c0fb48eeacd21c68fea49a454a9c76d6b8c4_prof);

    }

    // line 3
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_664e01e926b04635c6679ca0892c3cd33205d9e608bb9b642f73f4041c48d3ad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_664e01e926b04635c6679ca0892c3cd33205d9e608bb9b642f73f4041c48d3ad->enter($__internal_664e01e926b04635c6679ca0892c3cd33205d9e608bb9b642f73f4041c48d3ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_4cc96ef56d50cb4278bc1d2449afccb8247ab7078b04c1e34d7f97f9230102d9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4cc96ef56d50cb4278bc1d2449afccb8247ab7078b04c1e34d7f97f9230102d9->enter($__internal_4cc96ef56d50cb4278bc1d2449afccb8247ab7078b04c1e34d7f97f9230102d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 4
        echo "        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-26px;\">
                <h2 style=\"color: white;margin-top:4%\"> <center>Liste des Marchandises</center></h2>
            </div>
            <br><br>
        </div>
    ";
        
        $__internal_4cc96ef56d50cb4278bc1d2449afccb8247ab7078b04c1e34d7f97f9230102d9->leave($__internal_4cc96ef56d50cb4278bc1d2449afccb8247ab7078b04c1e34d7f97f9230102d9_prof);

        
        $__internal_664e01e926b04635c6679ca0892c3cd33205d9e608bb9b642f73f4041c48d3ad->leave($__internal_664e01e926b04635c6679ca0892c3cd33205d9e608bb9b642f73f4041c48d3ad_prof);

    }

    // line 11
    public function block_modules($context, array $blocks = array())
    {
        $__internal_3033b930789639650b55e7c5ad4cb7600da8b7389497b1d00b097a135b178988 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3033b930789639650b55e7c5ad4cb7600da8b7389497b1d00b097a135b178988->enter($__internal_3033b930789639650b55e7c5ad4cb7600da8b7389497b1d00b097a135b178988_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_b5f084321ed13cde7e0fa7538b14b1e5fa21dd3ac39c90b0cb5e3e3a7d8bc35d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b5f084321ed13cde7e0fa7538b14b1e5fa21dd3ac39c90b0cb5e3e3a7d8bc35d->enter($__internal_b5f084321ed13cde7e0fa7538b14b1e5fa21dd3ac39c90b0cb5e3e3a7d8bc35d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        echo "<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"margin-left: -12%\">
                <section class=\"box-typical\">
                    <div id=\"toolbar\">
                        <h1 style=\"font-weight: bold\">Liste des Contrats</h1>
                    </div>
                    <div class=\"table-responsive\">
                        <table id=\"table\"
                               data-pagination=\"true\"
                               data-search=\"true\"
                               data-use-row-attr-func=\"true\"
                               data-reorderable-rows=\"true\"
                               data-toolbar=\"#toolbar\">
                            <thead>
                            <tr >
                                <th data-field=\"id\">ID</th>
                                <th data-field=\"name\" >Nom</th>
                                <th data-field=\"prenom\" >Prenom</th>
                                <th data-field=\"pays\" >Pays</th>
                                <th data-field=\"Modifier\" >Modifier</th>
                                <th data-field=\"Supprimer\" >Supprimer</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </section><!--.box-typical-->

            </div><!--.container-fluid-->
        </div><!--.page-content-->
    ";
        
        $__internal_b5f084321ed13cde7e0fa7538b14b1e5fa21dd3ac39c90b0cb5e3e3a7d8bc35d->leave($__internal_b5f084321ed13cde7e0fa7538b14b1e5fa21dd3ac39c90b0cb5e3e3a7d8bc35d_prof);

        
        $__internal_3033b930789639650b55e7c5ad4cb7600da8b7389497b1d00b097a135b178988->leave($__internal_3033b930789639650b55e7c5ad4cb7600da8b7389497b1d00b097a135b178988_prof);

    }

    // line 43
    public function block_script($context, array $blocks = array())
    {
        $__internal_504048c1f1bf6560da73b8871c9a65c9b43f713789b0dc30e6903304b0a61b48 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_504048c1f1bf6560da73b8871c9a65c9b43f713789b0dc30e6903304b0a61b48->enter($__internal_504048c1f1bf6560da73b8871c9a65c9b43f713789b0dc30e6903304b0a61b48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        $__internal_9e0ce18a2617b005ccf5650deccd76dd9b131fc15e7b63f0703bb1159cee048c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9e0ce18a2617b005ccf5650deccd76dd9b131fc15e7b63f0703bb1159cee048c->enter($__internal_9e0ce18a2617b005ccf5650deccd76dd9b131fc15e7b63f0703bb1159cee048c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        // line 44
        echo "    ";
        $this->displayParentBlock("script", $context, $blocks);
        echo "
    <script src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/jquery.tablednd.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/listerRepresentant.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_9e0ce18a2617b005ccf5650deccd76dd9b131fc15e7b63f0703bb1159cee048c->leave($__internal_9e0ce18a2617b005ccf5650deccd76dd9b131fc15e7b63f0703bb1159cee048c_prof);

        
        $__internal_504048c1f1bf6560da73b8871c9a65c9b43f713789b0dc30e6903304b0a61b48->leave($__internal_504048c1f1bf6560da73b8871c9a65c9b43f713789b0dc30e6903304b0a61b48_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Representant:listerRepresentant.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 48,  162 => 47,  158 => 46,  154 => 45,  149 => 44,  140 => 43,  92 => 11,  76 => 4,  67 => 3,  56 => 11,  54 => 3,  43 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}
{% block contenuDuMilieu %}<!--le block du milieu de la page-->
    {% block debutPage %}
        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-26px;\">
                <h2 style=\"color: white;margin-top:4%\"> <center>Liste des Marchandises</center></h2>
            </div>
            <br><br>
        </div>
    {% endblock %}
    {% block modules %}<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"margin-left: -12%\">
                <section class=\"box-typical\">
                    <div id=\"toolbar\">
                        <h1 style=\"font-weight: bold\">Liste des Contrats</h1>
                    </div>
                    <div class=\"table-responsive\">
                        <table id=\"table\"
                               data-pagination=\"true\"
                               data-search=\"true\"
                               data-use-row-attr-func=\"true\"
                               data-reorderable-rows=\"true\"
                               data-toolbar=\"#toolbar\">
                            <thead>
                            <tr >
                                <th data-field=\"id\">ID</th>
                                <th data-field=\"name\" >Nom</th>
                                <th data-field=\"prenom\" >Prenom</th>
                                <th data-field=\"pays\" >Pays</th>
                                <th data-field=\"Modifier\" >Modifier</th>
                                <th data-field=\"Supprimer\" >Supprimer</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </section><!--.box-typical-->

            </div><!--.container-fluid-->
        </div><!--.page-content-->
    {% endblock %}
{% endblock %}
{% block script %}
    {{ parent() }}
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/jquery.tablednd.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/listerRepresentant.js') }}\"></script>
{% endblock %}", "StockBundle:Representant:listerRepresentant.html.twig", "C:\\wamp\\www\\symfony\\stock\\src\\StockBundle/Resources/views/Representant/listerRepresentant.html.twig");
    }
}
