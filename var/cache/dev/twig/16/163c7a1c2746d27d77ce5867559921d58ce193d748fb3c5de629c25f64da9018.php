<?php

/* StockBundle:Marchandise:addMarchandise.html.twig */
class __TwigTemplate_a6878f77e12b7be5bb756c83676a7e816957bdb23a497039c89f4229fcf5e916 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:Marchandise:addMarchandise.html.twig", 1);
        $this->blocks = array(
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8a3ed5e6e3191736f9b89ffeafba920e0ce5888689f1e1080ced16937c46910e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8a3ed5e6e3191736f9b89ffeafba920e0ce5888689f1e1080ced16937c46910e->enter($__internal_8a3ed5e6e3191736f9b89ffeafba920e0ce5888689f1e1080ced16937c46910e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Marchandise:addMarchandise.html.twig"));

        $__internal_43e91f047a87e21ff7af961bacb9793b6fcd67e909e663e4b4a06e921abe44d4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_43e91f047a87e21ff7af961bacb9793b6fcd67e909e663e4b4a06e921abe44d4->enter($__internal_43e91f047a87e21ff7af961bacb9793b6fcd67e909e663e4b4a06e921abe44d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Marchandise:addMarchandise.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8a3ed5e6e3191736f9b89ffeafba920e0ce5888689f1e1080ced16937c46910e->leave($__internal_8a3ed5e6e3191736f9b89ffeafba920e0ce5888689f1e1080ced16937c46910e_prof);

        
        $__internal_43e91f047a87e21ff7af961bacb9793b6fcd67e909e663e4b4a06e921abe44d4->leave($__internal_43e91f047a87e21ff7af961bacb9793b6fcd67e909e663e4b4a06e921abe44d4_prof);

    }

    // line 3
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_5f8b317d21061f3f25a425307ba59950c317b4181861e290bd19304dc61e31e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5f8b317d21061f3f25a425307ba59950c317b4181861e290bd19304dc61e31e3->enter($__internal_5f8b317d21061f3f25a425307ba59950c317b4181861e290bd19304dc61e31e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_bafe569ff61bffdce260e0771c81fa2e815375c3b46155a610ca3ef27c139646 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bafe569ff61bffdce260e0771c81fa2e815375c3b46155a610ca3ef27c139646->enter($__internal_bafe569ff61bffdce260e0771c81fa2e815375c3b46155a610ca3ef27c139646_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        // line 4
        echo "        ";
        $this->displayBlock('debutPage', $context, $blocks);
        // line 13
        echo "        ";
        $this->displayBlock('modules', $context, $blocks);
        // line 18
        echo "    ";
        
        $__internal_bafe569ff61bffdce260e0771c81fa2e815375c3b46155a610ca3ef27c139646->leave($__internal_bafe569ff61bffdce260e0771c81fa2e815375c3b46155a610ca3ef27c139646_prof);

        
        $__internal_5f8b317d21061f3f25a425307ba59950c317b4181861e290bd19304dc61e31e3->leave($__internal_5f8b317d21061f3f25a425307ba59950c317b4181861e290bd19304dc61e31e3_prof);

    }

    // line 4
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_fe531fe8898521201ff63a8382d3a4e9eaaeb4f5c613200fbb78bb44d5037f0f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fe531fe8898521201ff63a8382d3a4e9eaaeb4f5c613200fbb78bb44d5037f0f->enter($__internal_fe531fe8898521201ff63a8382d3a4e9eaaeb4f5c613200fbb78bb44d5037f0f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_1bb963e9ad5fe2ec1693674a4e0614afcd6cb27e70d0bab2921c6a53a9703ca0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1bb963e9ad5fe2ec1693674a4e0614afcd6cb27e70d0bab2921c6a53a9703ca0->enter($__internal_1bb963e9ad5fe2ec1693674a4e0614afcd6cb27e70d0bab2921c6a53a9703ca0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 5
        echo "            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                    <h3 style=\"color: white;margin-top:4%\"> <center>Ajouter Instruction de livraison</center></h3>
                </div>
                <br><br><br><br><br><br>
            </div>

        ";
        
        $__internal_1bb963e9ad5fe2ec1693674a4e0614afcd6cb27e70d0bab2921c6a53a9703ca0->leave($__internal_1bb963e9ad5fe2ec1693674a4e0614afcd6cb27e70d0bab2921c6a53a9703ca0_prof);

        
        $__internal_fe531fe8898521201ff63a8382d3a4e9eaaeb4f5c613200fbb78bb44d5037f0f->leave($__internal_fe531fe8898521201ff63a8382d3a4e9eaaeb4f5c613200fbb78bb44d5037f0f_prof);

    }

    // line 13
    public function block_modules($context, array $blocks = array())
    {
        $__internal_26626aa203d20bf8961a79422f751fd7963973e9c5ac7c43e7a196cdac322da2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_26626aa203d20bf8961a79422f751fd7963973e9c5ac7c43e7a196cdac322da2->enter($__internal_26626aa203d20bf8961a79422f751fd7963973e9c5ac7c43e7a196cdac322da2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_9ecdbcb6a3170aeb8f542ec4b0c2c3a78312269bd5b0973b732234851108923d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9ecdbcb6a3170aeb8f542ec4b0c2c3a78312269bd5b0973b732234851108923d->enter($__internal_9ecdbcb6a3170aeb8f542ec4b0c2c3a78312269bd5b0973b732234851108923d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        // line 14
        echo "            <div class=\"col-xl-4\" style=\"margin-left: 25%;\" >
                ";
        // line 15
        $this->loadTemplate("::formulaire.html.twig", "StockBundle:Marchandise:addMarchandise.html.twig", 15)->display($context);
        // line 16
        echo "            </div>
        ";
        
        $__internal_9ecdbcb6a3170aeb8f542ec4b0c2c3a78312269bd5b0973b732234851108923d->leave($__internal_9ecdbcb6a3170aeb8f542ec4b0c2c3a78312269bd5b0973b732234851108923d_prof);

        
        $__internal_26626aa203d20bf8961a79422f751fd7963973e9c5ac7c43e7a196cdac322da2->leave($__internal_26626aa203d20bf8961a79422f751fd7963973e9c5ac7c43e7a196cdac322da2_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Marchandise:addMarchandise.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 16,  105 => 15,  102 => 14,  93 => 13,  76 => 5,  67 => 4,  57 => 18,  54 => 13,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}

    {% block contenuDuMilieu %}
        {% block debutPage %}
            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                    <h3 style=\"color: white;margin-top:4%\"> <center>Ajouter Instruction de livraison</center></h3>
                </div>
                <br><br><br><br><br><br>
            </div>

        {% endblock %}
        {% block modules %}
            <div class=\"col-xl-4\" style=\"margin-left: 25%;\" >
                {% include '::formulaire.html.twig' %}
            </div>
        {% endblock %}
    {% endblock %}", "StockBundle:Marchandise:addMarchandise.html.twig", "C:\\wamp64\\www\\stock\\src\\StockBundle/Resources/views/Marchandise/addMarchandise.html.twig");
    }
}
