<?php

/* StockBundle:ContratMarchandise:listerContratMarchandise.html.twig */
class __TwigTemplate_4b5516e551b2c5865501ca1ec7aef47eacfcb7e3534ea9ec2eeb25a96a13008d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:ContratMarchandise:listerContratMarchandise.html.twig", 1);
        $this->blocks = array(
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_adbaa3ad74c7debd55c31b5702ee03254f20c16df904e6a664205f72e3361604 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_adbaa3ad74c7debd55c31b5702ee03254f20c16df904e6a664205f72e3361604->enter($__internal_adbaa3ad74c7debd55c31b5702ee03254f20c16df904e6a664205f72e3361604_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:ContratMarchandise:listerContratMarchandise.html.twig"));

        $__internal_0c2dfdcb94240e4f30ad28a15489e95fdc64949fd0f3ca42d21dc0500ba7bbb0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0c2dfdcb94240e4f30ad28a15489e95fdc64949fd0f3ca42d21dc0500ba7bbb0->enter($__internal_0c2dfdcb94240e4f30ad28a15489e95fdc64949fd0f3ca42d21dc0500ba7bbb0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:ContratMarchandise:listerContratMarchandise.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_adbaa3ad74c7debd55c31b5702ee03254f20c16df904e6a664205f72e3361604->leave($__internal_adbaa3ad74c7debd55c31b5702ee03254f20c16df904e6a664205f72e3361604_prof);

        
        $__internal_0c2dfdcb94240e4f30ad28a15489e95fdc64949fd0f3ca42d21dc0500ba7bbb0->leave($__internal_0c2dfdcb94240e4f30ad28a15489e95fdc64949fd0f3ca42d21dc0500ba7bbb0_prof);

    }

    // line 2
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_c84f376b350465731f677dca3bfb338cb79127b35759eee6860b4950dabdb737 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c84f376b350465731f677dca3bfb338cb79127b35759eee6860b4950dabdb737->enter($__internal_c84f376b350465731f677dca3bfb338cb79127b35759eee6860b4950dabdb737_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_f3b94cc95fa3c14a8e4855ddb7d55cb1d50c1d2a64bdf9da765d886c72a28a79 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f3b94cc95fa3c14a8e4855ddb7d55cb1d50c1d2a64bdf9da765d886c72a28a79->enter($__internal_f3b94cc95fa3c14a8e4855ddb7d55cb1d50c1d2a64bdf9da765d886c72a28a79_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        echo "<!--le block du milieu de la page-->
    ";
        // line 3
        $this->displayBlock('debutPage', $context, $blocks);
        // line 11
        echo "    ";
        $this->displayBlock('modules', $context, $blocks);
        
        $__internal_f3b94cc95fa3c14a8e4855ddb7d55cb1d50c1d2a64bdf9da765d886c72a28a79->leave($__internal_f3b94cc95fa3c14a8e4855ddb7d55cb1d50c1d2a64bdf9da765d886c72a28a79_prof);

        
        $__internal_c84f376b350465731f677dca3bfb338cb79127b35759eee6860b4950dabdb737->leave($__internal_c84f376b350465731f677dca3bfb338cb79127b35759eee6860b4950dabdb737_prof);

    }

    // line 3
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_195f3782603bfba8ec9330cb0bbcba931da10e46074551db99ffd32a122ccdf2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_195f3782603bfba8ec9330cb0bbcba931da10e46074551db99ffd32a122ccdf2->enter($__internal_195f3782603bfba8ec9330cb0bbcba931da10e46074551db99ffd32a122ccdf2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_00cfc7b42ab1cfa8740f978e7989a654e4c88a948425eed2fdbe74a17b163f2d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_00cfc7b42ab1cfa8740f978e7989a654e4c88a948425eed2fdbe74a17b163f2d->enter($__internal_00cfc7b42ab1cfa8740f978e7989a654e4c88a948425eed2fdbe74a17b163f2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 4
        echo "        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-26px;\">
                <h2 style=\"color: white;margin-top:4%\"> <center>Liste des Contrats</center></h2>
            </div>
            <br><br>
        </div>
    ";
        
        $__internal_00cfc7b42ab1cfa8740f978e7989a654e4c88a948425eed2fdbe74a17b163f2d->leave($__internal_00cfc7b42ab1cfa8740f978e7989a654e4c88a948425eed2fdbe74a17b163f2d_prof);

        
        $__internal_195f3782603bfba8ec9330cb0bbcba931da10e46074551db99ffd32a122ccdf2->leave($__internal_195f3782603bfba8ec9330cb0bbcba931da10e46074551db99ffd32a122ccdf2_prof);

    }

    // line 11
    public function block_modules($context, array $blocks = array())
    {
        $__internal_388d4bf0beb574eeabf985297de098816062bca11d675d7e9824c29eae46215a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_388d4bf0beb574eeabf985297de098816062bca11d675d7e9824c29eae46215a->enter($__internal_388d4bf0beb574eeabf985297de098816062bca11d675d7e9824c29eae46215a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_adb87c3fa5eac54e39a2638b9dbe7c2a5d2d45b56f8ee2b3f511e1815cb87b0c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_adb87c3fa5eac54e39a2638b9dbe7c2a5d2d45b56f8ee2b3f511e1815cb87b0c->enter($__internal_adb87c3fa5eac54e39a2638b9dbe7c2a5d2d45b56f8ee2b3f511e1815cb87b0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        echo "<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"margin-left: -12%\">
                <section class=\"box-typical\">
                    <div id=\"toolbar\">
                        <h1 style=\"font-weight: bold\">Liste des Contrats</h1>
                    </div>
                    <div class=\"table-responsive\">
                        <table id=\"table\"
                               data-pagination=\"true\"
                               data-search=\"true\"
                               data-use-row-attr-func=\"true\"
                               data-reorderable-rows=\"true\"
                               data-toolbar=\"#toolbar\" border=\"yes\">
                            <thead>
                                <tr class=\"jumbotron\">
                                    <th id=\"id\">ID</th>
                                    <th id=\"libelle\" >Libelle</th>
                                    <th id=\"quantite\" >Quantite</th>
                                    <th id=\"transporteur\" >Transporteur</th>
                                    <th id=\"representant\">Representant</th>
                                    <th id=\"dateDepart\" >Date Depart</th>
                                    <th id=\"dateArrivee\">Date Arrivee</th>
                                    <th id=\"valider\">Valider</th>
                                    <th id=\"modifier\">Modifier</th>
                                    <th id=\"supprimer\" >Supprimer</th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["liste"] ?? $this->getContext($context, "liste")));
        foreach ($context['_seq'] as $context["_key"] => $context["l"]) {
            // line 41
            echo "                                    <tr>
                                        <td id=\"id\">";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "id", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "libelle", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "quantite", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["l"], "transporteur", array()), "nom", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["l"], "representant", array()), "nom", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 47
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["l"], "getDateDepart", array(), "method"), "d-m-Y"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 48
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["l"], "getDateArrivee", array(), "method"), "d-m-Y"), "html", null, true);
            echo "</td>
                                        <td>
                                            ";
            // line 50
            if (($this->getAttribute($context["l"], "valide", array()) == 1)) {
                // line 51
                echo "                                                ";
                echo "Oui";
                echo "
                                            ";
            } else {
                // line 53
                echo "                                                ";
                echo "Non";
                echo "
                                            ";
            }
            // line 55
            echo "                                        </td>
                                        <td><i class=\"font-icon font-icon-pen\" style=\"color: green\"></i></td>
                                        <td><i class=\"font-icon font-icon-del\" style=\"color: red\"></i></td>
                                    </tr>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['l'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "                            </tbody>
                        </table>
                    </div>
                </section><!--.box-typical-->

            </div><!--.container-fluid-->
        </div><!--.page-content-->
    ";
        
        $__internal_adb87c3fa5eac54e39a2638b9dbe7c2a5d2d45b56f8ee2b3f511e1815cb87b0c->leave($__internal_adb87c3fa5eac54e39a2638b9dbe7c2a5d2d45b56f8ee2b3f511e1815cb87b0c_prof);

        
        $__internal_388d4bf0beb574eeabf985297de098816062bca11d675d7e9824c29eae46215a->leave($__internal_388d4bf0beb574eeabf985297de098816062bca11d675d7e9824c29eae46215a_prof);

    }

    // line 69
    public function block_script($context, array $blocks = array())
    {
        $__internal_70aa33b1d58aa737fdbb0eccf3adb836749bfd29536a28d1da1727780c3d178e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_70aa33b1d58aa737fdbb0eccf3adb836749bfd29536a28d1da1727780c3d178e->enter($__internal_70aa33b1d58aa737fdbb0eccf3adb836749bfd29536a28d1da1727780c3d178e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        $__internal_1d3f0a8f3483d94ac19cb6413dc8b69a96ab8ff03a64f8c6df2663d920572ea9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1d3f0a8f3483d94ac19cb6413dc8b69a96ab8ff03a64f8c6df2663d920572ea9->enter($__internal_1d3f0a8f3483d94ac19cb6413dc8b69a96ab8ff03a64f8c6df2663d920572ea9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        // line 70
        echo "    ";
        $this->displayParentBlock("script", $context, $blocks);
        echo "
    <script src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/jquery.tablednd.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/listerContrat.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_1d3f0a8f3483d94ac19cb6413dc8b69a96ab8ff03a64f8c6df2663d920572ea9->leave($__internal_1d3f0a8f3483d94ac19cb6413dc8b69a96ab8ff03a64f8c6df2663d920572ea9_prof);

        
        $__internal_70aa33b1d58aa737fdbb0eccf3adb836749bfd29536a28d1da1727780c3d178e->leave($__internal_70aa33b1d58aa737fdbb0eccf3adb836749bfd29536a28d1da1727780c3d178e_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:ContratMarchandise:listerContratMarchandise.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  234 => 74,  230 => 73,  226 => 72,  222 => 71,  217 => 70,  208 => 69,  191 => 60,  181 => 55,  175 => 53,  169 => 51,  167 => 50,  162 => 48,  158 => 47,  154 => 46,  150 => 45,  146 => 44,  142 => 43,  138 => 42,  135 => 41,  131 => 40,  92 => 11,  76 => 4,  67 => 3,  56 => 11,  54 => 3,  43 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}
{% block contenuDuMilieu %}<!--le block du milieu de la page-->
    {% block debutPage %}
        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-26px;\">
                <h2 style=\"color: white;margin-top:4%\"> <center>Liste des Contrats</center></h2>
            </div>
            <br><br>
        </div>
    {% endblock %}
    {% block modules %}<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"margin-left: -12%\">
                <section class=\"box-typical\">
                    <div id=\"toolbar\">
                        <h1 style=\"font-weight: bold\">Liste des Contrats</h1>
                    </div>
                    <div class=\"table-responsive\">
                        <table id=\"table\"
                               data-pagination=\"true\"
                               data-search=\"true\"
                               data-use-row-attr-func=\"true\"
                               data-reorderable-rows=\"true\"
                               data-toolbar=\"#toolbar\" border=\"yes\">
                            <thead>
                                <tr class=\"jumbotron\">
                                    <th id=\"id\">ID</th>
                                    <th id=\"libelle\" >Libelle</th>
                                    <th id=\"quantite\" >Quantite</th>
                                    <th id=\"transporteur\" >Transporteur</th>
                                    <th id=\"representant\">Representant</th>
                                    <th id=\"dateDepart\" >Date Depart</th>
                                    <th id=\"dateArrivee\">Date Arrivee</th>
                                    <th id=\"valider\">Valider</th>
                                    <th id=\"modifier\">Modifier</th>
                                    <th id=\"supprimer\" >Supprimer</th>
                                </tr>
                            </thead>
                            <tbody>
                                {% for l in liste %}
                                    <tr>
                                        <td id=\"id\">{{ l.id }}</td>
                                        <td>{{ l.libelle }}</td>
                                        <td>{{ l.quantite }}</td>
                                        <td>{{ l.transporteur.nom }}</td>
                                        <td>{{ l.representant.nom }}</td>
                                        <td>{{ l.getDateDepart()|date('d-m-Y') }}</td>
                                        <td>{{ l.getDateArrivee()|date('d-m-Y') }}</td>
                                        <td>
                                            {% if l.valide==1 %}
                                                {{ \"Oui\" }}
                                            {% else %}
                                                {{ \"Non\" }}
                                            {% endif %}
                                        </td>
                                        <td><i class=\"font-icon font-icon-pen\" style=\"color: green\"></i></td>
                                        <td><i class=\"font-icon font-icon-del\" style=\"color: red\"></i></td>
                                    </tr>
                                {% endfor %}
                            </tbody>
                        </table>
                    </div>
                </section><!--.box-typical-->

            </div><!--.container-fluid-->
        </div><!--.page-content-->
    {% endblock %}
{% endblock %}
{% block script %}
    {{ parent() }}
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/jquery.tablednd.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/listerContrat.js') }}\"></script>
{% endblock %}", "StockBundle:ContratMarchandise:listerContratMarchandise.html.twig", "C:\\wamp64\\www\\stock\\src\\StockBundle/Resources/views/ContratMarchandise/listerContratMarchandise.html.twig");
    }
}
