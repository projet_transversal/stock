<?php

/* @Twig/Exception/traces.txt.twig */
class __TwigTemplate_bce00ceaa777dfefff5212eb08fefa8c8908bc4a70f9b6268382d8aa1936ef9a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d15cf3c53d5a77f7a08a8101401a19650df3517cd8ee26dbb6b190eae76dfbbd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d15cf3c53d5a77f7a08a8101401a19650df3517cd8ee26dbb6b190eae76dfbbd->enter($__internal_d15cf3c53d5a77f7a08a8101401a19650df3517cd8ee26dbb6b190eae76dfbbd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/traces.txt.twig"));

        $__internal_b72ce5d7eaa6741103cf7356f27f5c725e975f36693e2ef228067b348e830f54 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b72ce5d7eaa6741103cf7356f27f5c725e975f36693e2ef228067b348e830f54->enter($__internal_b72ce5d7eaa6741103cf7356f27f5c725e975f36693e2ef228067b348e830f54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/traces.txt.twig"));

        // line 1
        if (twig_length_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "trace", array()))) {
            // line 2
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "trace", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
                // line 3
                $this->loadTemplate("@Twig/Exception/trace.txt.twig", "@Twig/Exception/traces.txt.twig", 3)->display(array("trace" => $context["trace"]));
                // line 4
                echo "
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        
        $__internal_d15cf3c53d5a77f7a08a8101401a19650df3517cd8ee26dbb6b190eae76dfbbd->leave($__internal_d15cf3c53d5a77f7a08a8101401a19650df3517cd8ee26dbb6b190eae76dfbbd_prof);

        
        $__internal_b72ce5d7eaa6741103cf7356f27f5c725e975f36693e2ef228067b348e830f54->leave($__internal_b72ce5d7eaa6741103cf7356f27f5c725e975f36693e2ef228067b348e830f54_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/traces.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  31 => 3,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if exception.trace|length %}
{% for trace in exception.trace %}
{% include '@Twig/Exception/trace.txt.twig' with { 'trace': trace } only %}

{% endfor %}
{% endif %}
", "@Twig/Exception/traces.txt.twig", "C:\\wamp64\\www\\stock\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\traces.txt.twig");
    }
}
