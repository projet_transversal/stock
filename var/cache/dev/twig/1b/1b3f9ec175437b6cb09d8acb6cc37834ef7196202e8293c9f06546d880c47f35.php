<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_59e7ce6086a050ee114c9f5e488ac036b5a7812adb18e352146dc288470abe29 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5bc7861d68b960704135f5718d47b6c1e4e60170737d07b13f124de69fb81b1d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5bc7861d68b960704135f5718d47b6c1e4e60170737d07b13f124de69fb81b1d->enter($__internal_5bc7861d68b960704135f5718d47b6c1e4e60170737d07b13f124de69fb81b1d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_7a32a8ffb8a7fb184a8ccdaeb7f67c6dfcc69039842d032a44469ec14cde3016 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7a32a8ffb8a7fb184a8ccdaeb7f67c6dfcc69039842d032a44469ec14cde3016->enter($__internal_7a32a8ffb8a7fb184a8ccdaeb7f67c6dfcc69039842d032a44469ec14cde3016_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5bc7861d68b960704135f5718d47b6c1e4e60170737d07b13f124de69fb81b1d->leave($__internal_5bc7861d68b960704135f5718d47b6c1e4e60170737d07b13f124de69fb81b1d_prof);

        
        $__internal_7a32a8ffb8a7fb184a8ccdaeb7f67c6dfcc69039842d032a44469ec14cde3016->leave($__internal_7a32a8ffb8a7fb184a8ccdaeb7f67c6dfcc69039842d032a44469ec14cde3016_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_917449005cab1295ac2d33749fcdf145144b741366e561eaea700923542264b9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_917449005cab1295ac2d33749fcdf145144b741366e561eaea700923542264b9->enter($__internal_917449005cab1295ac2d33749fcdf145144b741366e561eaea700923542264b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_e80c0cb686747d12e9299d690bace0f6229aea0a91e1c1fd923eb4f50781412a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e80c0cb686747d12e9299d690bace0f6229aea0a91e1c1fd923eb4f50781412a->enter($__internal_e80c0cb686747d12e9299d690bace0f6229aea0a91e1c1fd923eb4f50781412a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_e80c0cb686747d12e9299d690bace0f6229aea0a91e1c1fd923eb4f50781412a->leave($__internal_e80c0cb686747d12e9299d690bace0f6229aea0a91e1c1fd923eb4f50781412a_prof);

        
        $__internal_917449005cab1295ac2d33749fcdf145144b741366e561eaea700923542264b9->leave($__internal_917449005cab1295ac2d33749fcdf145144b741366e561eaea700923542264b9_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_6a6868b5bf64fc46afbc219306e5f10c7521867f51ea7a6c9522c94c82ab8c55 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6a6868b5bf64fc46afbc219306e5f10c7521867f51ea7a6c9522c94c82ab8c55->enter($__internal_6a6868b5bf64fc46afbc219306e5f10c7521867f51ea7a6c9522c94c82ab8c55_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_79b2916579e33ffec5be73f27fc1deab4f2ce0d45a86cccd29aacb708f5f2cce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_79b2916579e33ffec5be73f27fc1deab4f2ce0d45a86cccd29aacb708f5f2cce->enter($__internal_79b2916579e33ffec5be73f27fc1deab4f2ce0d45a86cccd29aacb708f5f2cce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_79b2916579e33ffec5be73f27fc1deab4f2ce0d45a86cccd29aacb708f5f2cce->leave($__internal_79b2916579e33ffec5be73f27fc1deab4f2ce0d45a86cccd29aacb708f5f2cce_prof);

        
        $__internal_6a6868b5bf64fc46afbc219306e5f10c7521867f51ea7a6c9522c94c82ab8c55->leave($__internal_6a6868b5bf64fc46afbc219306e5f10c7521867f51ea7a6c9522c94c82ab8c55_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_d9707e9efd633cc4bad4640175dad5e357997fa2cc0d9134830b21bf0d2554a1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d9707e9efd633cc4bad4640175dad5e357997fa2cc0d9134830b21bf0d2554a1->enter($__internal_d9707e9efd633cc4bad4640175dad5e357997fa2cc0d9134830b21bf0d2554a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_014492fc9d58ca8cca53824735378bf57e51bef2bc1bd6f5dce0c8d2da2fbbb9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_014492fc9d58ca8cca53824735378bf57e51bef2bc1bd6f5dce0c8d2da2fbbb9->enter($__internal_014492fc9d58ca8cca53824735378bf57e51bef2bc1bd6f5dce0c8d2da2fbbb9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_014492fc9d58ca8cca53824735378bf57e51bef2bc1bd6f5dce0c8d2da2fbbb9->leave($__internal_014492fc9d58ca8cca53824735378bf57e51bef2bc1bd6f5dce0c8d2da2fbbb9_prof);

        
        $__internal_d9707e9efd633cc4bad4640175dad5e357997fa2cc0d9134830b21bf0d2554a1->leave($__internal_d9707e9efd633cc4bad4640175dad5e357997fa2cc0d9134830b21bf0d2554a1_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "C:\\wamp\\www\\symfony\\stock\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception_full.html.twig");
    }
}
