<?php

/* security/login.html.twig */
class __TwigTemplate_e48a4b54608a87fb9d9fec36aa8a0b5e76a1c8877a3c7cc89a100acabc50856b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "security/login.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2ca90bcc5d8ec6b34c2088ec889a4ab305d4da0115249d831527950ddeeb28a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2ca90bcc5d8ec6b34c2088ec889a4ab305d4da0115249d831527950ddeeb28a5->enter($__internal_2ca90bcc5d8ec6b34c2088ec889a4ab305d4da0115249d831527950ddeeb28a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        $__internal_7b08563966df6a8ad0e0a1b8b2b32f3851656c11cf8d74ff1cbb7281f42043d3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7b08563966df6a8ad0e0a1b8b2b32f3851656c11cf8d74ff1cbb7281f42043d3->enter($__internal_7b08563966df6a8ad0e0a1b8b2b32f3851656c11cf8d74ff1cbb7281f42043d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2ca90bcc5d8ec6b34c2088ec889a4ab305d4da0115249d831527950ddeeb28a5->leave($__internal_2ca90bcc5d8ec6b34c2088ec889a4ab305d4da0115249d831527950ddeeb28a5_prof);

        
        $__internal_7b08563966df6a8ad0e0a1b8b2b32f3851656c11cf8d74ff1cbb7281f42043d3->leave($__internal_7b08563966df6a8ad0e0a1b8b2b32f3851656c11cf8d74ff1cbb7281f42043d3_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_18767cea11ccb035dfd76577f3e973950a7cd138fa09a1cac8ecc0f4c2fe4a56 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_18767cea11ccb035dfd76577f3e973950a7cd138fa09a1cac8ecc0f4c2fe4a56->enter($__internal_18767cea11ccb035dfd76577f3e973950a7cd138fa09a1cac8ecc0f4c2fe4a56_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_bdaf75821ec9f9bfc9bef5e894456e9cd4b835c32f9d9b290cb418e0d383fb4d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bdaf75821ec9f9bfc9bef5e894456e9cd4b835c32f9d9b290cb418e0d383fb4d->enter($__internal_bdaf75821ec9f9bfc9bef5e894456e9cd4b835c32f9d9b290cb418e0d383fb4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "login";
        
        $__internal_bdaf75821ec9f9bfc9bef5e894456e9cd4b835c32f9d9b290cb418e0d383fb4d->leave($__internal_bdaf75821ec9f9bfc9bef5e894456e9cd4b835c32f9d9b290cb418e0d383fb4d_prof);

        
        $__internal_18767cea11ccb035dfd76577f3e973950a7cd138fa09a1cac8ecc0f4c2fe4a56->leave($__internal_18767cea11ccb035dfd76577f3e973950a7cd138fa09a1cac8ecc0f4c2fe4a56_prof);

    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        $__internal_709585c49d35009b2d59910341ae68da13ead82d0e1240cd32ce40abdf4a1984 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_709585c49d35009b2d59910341ae68da13ead82d0e1240cd32ce40abdf4a1984->enter($__internal_709585c49d35009b2d59910341ae68da13ead82d0e1240cd32ce40abdf4a1984_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_85cd61713c9e1a06ed9cd4e72c0caee32d32e27a49f9c0ff5373a11626e1f7f9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_85cd61713c9e1a06ed9cd4e72c0caee32d32e27a49f9c0ff5373a11626e1f7f9->enter($__internal_85cd61713c9e1a06ed9cd4e72c0caee32d32e27a49f9c0ff5373a11626e1f7f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 6
        echo "    ";
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            // line 7
            echo "        <div class=\"alert alert-danger\">
            ";
            // line 8
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "
        </div>
    ";
        }
        // line 11
        echo "
    <div class=\"row\">
        <div class=\"col-sm-5\">
            <div class=\"well\">
                <form action=\"";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("security_login");
        echo "\" method=\"post\">
                    <fieldset>
                        <legend><i class=\"fa fa-lock\" aria-hidden=\"true\"></i> ";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("title.login"), "html", null, true);
        echo "</legend>
                        <div class=\"form-group\">
                            <label for=\"username\">";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.username"), "html", null, true);
        echo "</label>
                            <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 20
        echo twig_escape_filter($this->env, ($context["last_username"] ?? $this->getContext($context, "last_username")), "html", null, true);
        echo "\" class=\"form-control\"/>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"password\">";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.password"), "html", null, true);
        echo "</label>
                            <input type=\"password\" id=\"password\" name=\"_password\" class=\"form-control\" />
                        </div>
                        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderCsrfToken("authenticate"), "html", null, true);
        echo "\"/>
                        <button type=\"submit\" class=\"btn btn-primary\">
                            <i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i> ";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("action.sign_in"), "html", null, true);
        echo "
                        </button>
                    </fieldset>
                </form>
            </div>
        </div>

        <div id=\"login-help\" class=\"col-sm-7\">
            <h3>
                <i class=\"fa fa-long-arrow-left\" aria-hidden=\"true\"></i>
                ";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("help.login_users"), "html", null, true);
        echo "
            </h3>

            <table class=\"table table-striped table-bordered table-hover\">
                <thead>
                    <tr>
                        <th scope=\"col\">";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.username"), "html", null, true);
        echo "</th>
                        <th scope=\"col\">";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.password"), "html", null, true);
        echo "</th>
                        <th scope=\"col\">";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.role"), "html", null, true);
        echo "</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>john_user</td>
                        <td>kitten</td>
                        <td><code>ROLE_USER</code> (";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("help.role_user"), "html", null, true);
        echo ")</td>
                    </tr>
                    <tr>
                        <td>anna_admin</td>
                        <td>kitten</td>
                        <td><code>ROLE_ADMIN</code> (";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("help.role_admin"), "html", null, true);
        echo ")</td>
                    </tr>
                </tbody>
            </table>

            <div id=\"login-users-help\" class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <p>
                        <span class=\"label label-success\">";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("note"), "html", null, true);
        echo "</span>
                        ";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("help.reload_fixtures"), "html", null, true);
        echo "<br/>

                        <code class=\"console\">\$ php bin/console doctrine:fixtures:load</code>
                    </p>

                    <p>
                        <span class=\"label label-success\">";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("tip"), "html", null, true);
        echo "</span>
                        ";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("help.add_user"), "html", null, true);
        echo "<br/>

                        <code class=\"console\">\$ php bin/console app:add-user</code>
                    </p>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_85cd61713c9e1a06ed9cd4e72c0caee32d32e27a49f9c0ff5373a11626e1f7f9->leave($__internal_85cd61713c9e1a06ed9cd4e72c0caee32d32e27a49f9c0ff5373a11626e1f7f9_prof);

        
        $__internal_709585c49d35009b2d59910341ae68da13ead82d0e1240cd32ce40abdf4a1984->leave($__internal_709585c49d35009b2d59910341ae68da13ead82d0e1240cd32ce40abdf4a1984_prof);

    }

    // line 84
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_4a37ee4950371ec129057da2a4e77e8b93e5f81f84841bd0adf7620b2b0319ec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a37ee4950371ec129057da2a4e77e8b93e5f81f84841bd0adf7620b2b0319ec->enter($__internal_4a37ee4950371ec129057da2a4e77e8b93e5f81f84841bd0adf7620b2b0319ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_43af9e5a26d50e002537ab0af1399b195ab4c5a657ff2994f6ea82633af2d4ce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_43af9e5a26d50e002537ab0af1399b195ab4c5a657ff2994f6ea82633af2d4ce->enter($__internal_43af9e5a26d50e002537ab0af1399b195ab4c5a657ff2994f6ea82633af2d4ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 85
        echo "    ";
        $this->displayParentBlock("sidebar", $context, $blocks);
        echo "

    ";
        // line 87
        echo $this->env->getExtension('CodeExplorerBundle\Twig\SourceCodeExtension')->showSourceCode($this->env, $this);
        echo "
";
        
        $__internal_43af9e5a26d50e002537ab0af1399b195ab4c5a657ff2994f6ea82633af2d4ce->leave($__internal_43af9e5a26d50e002537ab0af1399b195ab4c5a657ff2994f6ea82633af2d4ce_prof);

        
        $__internal_4a37ee4950371ec129057da2a4e77e8b93e5f81f84841bd0adf7620b2b0319ec->leave($__internal_4a37ee4950371ec129057da2a4e77e8b93e5f81f84841bd0adf7620b2b0319ec_prof);

    }

    // line 90
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_f23a39975957ba2adbb0d51d665281d525505f382afaed8a2deb1bf79d49b244 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f23a39975957ba2adbb0d51d665281d525505f382afaed8a2deb1bf79d49b244->enter($__internal_f23a39975957ba2adbb0d51d665281d525505f382afaed8a2deb1bf79d49b244_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_bd28a8502290be85a54bd9a78863a0bc4ab25321544ad28092f4d54b09443777 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bd28a8502290be85a54bd9a78863a0bc4ab25321544ad28092f4d54b09443777->enter($__internal_bd28a8502290be85a54bd9a78863a0bc4ab25321544ad28092f4d54b09443777_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 91
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

    <script>
        \$(document).ready(function() {
            var usernameEl = \$('#username');
            var passwordEl = \$('#password');
            // in a real application, hardcoding the user/password would be idiotic
            // but for the demo application it's very convenient to do so
            if (!usernameEl.val() && !passwordEl.val()) {
                usernameEl.val('anna_admin');
                passwordEl.val('kitten');
            }
        });
    </script>
";
        
        $__internal_bd28a8502290be85a54bd9a78863a0bc4ab25321544ad28092f4d54b09443777->leave($__internal_bd28a8502290be85a54bd9a78863a0bc4ab25321544ad28092f4d54b09443777_prof);

        
        $__internal_f23a39975957ba2adbb0d51d665281d525505f382afaed8a2deb1bf79d49b244->leave($__internal_f23a39975957ba2adbb0d51d665281d525505f382afaed8a2deb1bf79d49b244_prof);

    }

    public function getTemplateName()
    {
        return "security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  250 => 91,  241 => 90,  229 => 87,  223 => 85,  214 => 84,  195 => 74,  191 => 73,  182 => 67,  178 => 66,  167 => 58,  159 => 53,  149 => 46,  145 => 45,  141 => 44,  132 => 38,  119 => 28,  114 => 26,  108 => 23,  102 => 20,  98 => 19,  93 => 17,  88 => 15,  82 => 11,  76 => 8,  73 => 7,  70 => 6,  61 => 5,  43 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body_id 'login' %}

{% block main %}
    {% if error %}
        <div class=\"alert alert-danger\">
            {{ error.messageKey|trans(error.messageData, 'security') }}
        </div>
    {% endif %}

    <div class=\"row\">
        <div class=\"col-sm-5\">
            <div class=\"well\">
                <form action=\"{{ path('security_login') }}\" method=\"post\">
                    <fieldset>
                        <legend><i class=\"fa fa-lock\" aria-hidden=\"true\"></i> {{ 'title.login'|trans }}</legend>
                        <div class=\"form-group\">
                            <label for=\"username\">{{ 'label.username'|trans }}</label>
                            <input type=\"text\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" class=\"form-control\"/>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"password\">{{ 'label.password'|trans }}</label>
                            <input type=\"password\" id=\"password\" name=\"_password\" class=\"form-control\" />
                        </div>
                        <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token('authenticate') }}\"/>
                        <button type=\"submit\" class=\"btn btn-primary\">
                            <i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i> {{ 'action.sign_in'|trans }}
                        </button>
                    </fieldset>
                </form>
            </div>
        </div>

        <div id=\"login-help\" class=\"col-sm-7\">
            <h3>
                <i class=\"fa fa-long-arrow-left\" aria-hidden=\"true\"></i>
                {{ 'help.login_users'|trans }}
            </h3>

            <table class=\"table table-striped table-bordered table-hover\">
                <thead>
                    <tr>
                        <th scope=\"col\">{{ 'label.username'|trans }}</th>
                        <th scope=\"col\">{{ 'label.password'|trans }}</th>
                        <th scope=\"col\">{{ 'label.role'|trans }}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>john_user</td>
                        <td>kitten</td>
                        <td><code>ROLE_USER</code> ({{ 'help.role_user'|trans }})</td>
                    </tr>
                    <tr>
                        <td>anna_admin</td>
                        <td>kitten</td>
                        <td><code>ROLE_ADMIN</code> ({{ 'help.role_admin'|trans }})</td>
                    </tr>
                </tbody>
            </table>

            <div id=\"login-users-help\" class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <p>
                        <span class=\"label label-success\">{{ 'note'|trans }}</span>
                        {{ 'help.reload_fixtures'|trans }}<br/>

                        <code class=\"console\">\$ php bin/console doctrine:fixtures:load</code>
                    </p>

                    <p>
                        <span class=\"label label-success\">{{ 'tip'|trans }}</span>
                        {{ 'help.add_user'|trans }}<br/>

                        <code class=\"console\">\$ php bin/console app:add-user</code>
                    </p>
                </div>
            </div>
        </div>
    </div>
{% endblock %}

{% block sidebar %}
    {{ parent() }}

    {{ show_source_code(_self) }}
{% endblock %}

{% block javascripts %}
    {{ parent() }}

    <script>
        \$(document).ready(function() {
            var usernameEl = \$('#username');
            var passwordEl = \$('#password');
            // in a real application, hardcoding the user/password would be idiotic
            // but for the demo application it's very convenient to do so
            if (!usernameEl.val() && !passwordEl.val()) {
                usernameEl.val('anna_admin');
                passwordEl.val('kitten');
            }
        });
    </script>
{% endblock %}
", "security/login.html.twig", "C:\\wamp64\\www\\stock\\app\\Resources\\views\\security\\login.html.twig");
    }
}
