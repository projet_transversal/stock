<?php

/* StockBundle:Marchandise:listerContratMarchandise.html.twig */
class __TwigTemplate_5ddb12bf7651457cc4db5d7666d8309f6a2fdf7a6495f4341498cb19fdf2f692 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:Marchandise:listerContratMarchandise.html.twig", 1);
        $this->blocks = array(
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_531ba1d16574096e5c60d4b8e6015b5409d61ef54dda289bf89cc4868820db91 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_531ba1d16574096e5c60d4b8e6015b5409d61ef54dda289bf89cc4868820db91->enter($__internal_531ba1d16574096e5c60d4b8e6015b5409d61ef54dda289bf89cc4868820db91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Marchandise:listerContratMarchandise.html.twig"));

        $__internal_8a8869cc785d239e50698ca8b1e41d652c36a0327b2313cb093bf0f80664453c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a8869cc785d239e50698ca8b1e41d652c36a0327b2313cb093bf0f80664453c->enter($__internal_8a8869cc785d239e50698ca8b1e41d652c36a0327b2313cb093bf0f80664453c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Marchandise:listerContratMarchandise.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_531ba1d16574096e5c60d4b8e6015b5409d61ef54dda289bf89cc4868820db91->leave($__internal_531ba1d16574096e5c60d4b8e6015b5409d61ef54dda289bf89cc4868820db91_prof);

        
        $__internal_8a8869cc785d239e50698ca8b1e41d652c36a0327b2313cb093bf0f80664453c->leave($__internal_8a8869cc785d239e50698ca8b1e41d652c36a0327b2313cb093bf0f80664453c_prof);

    }

    // line 2
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_87f0654083fb2367459ec2b3d7013e166087f419658cba5f2773bd8f19fd0410 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_87f0654083fb2367459ec2b3d7013e166087f419658cba5f2773bd8f19fd0410->enter($__internal_87f0654083fb2367459ec2b3d7013e166087f419658cba5f2773bd8f19fd0410_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_c3351133e4ce4e293c8084355bb73bd544e7d3294c5fc67ff7cf1c2d95b0fd5d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c3351133e4ce4e293c8084355bb73bd544e7d3294c5fc67ff7cf1c2d95b0fd5d->enter($__internal_c3351133e4ce4e293c8084355bb73bd544e7d3294c5fc67ff7cf1c2d95b0fd5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        echo "<!--le block du milieu de la page-->
    ";
        // line 3
        $this->displayBlock('debutPage', $context, $blocks);
        // line 11
        echo "    ";
        $this->displayBlock('modules', $context, $blocks);
        
        $__internal_c3351133e4ce4e293c8084355bb73bd544e7d3294c5fc67ff7cf1c2d95b0fd5d->leave($__internal_c3351133e4ce4e293c8084355bb73bd544e7d3294c5fc67ff7cf1c2d95b0fd5d_prof);

        
        $__internal_87f0654083fb2367459ec2b3d7013e166087f419658cba5f2773bd8f19fd0410->leave($__internal_87f0654083fb2367459ec2b3d7013e166087f419658cba5f2773bd8f19fd0410_prof);

    }

    // line 3
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_a82a5946c87d29c9c3bfa603cc0fd1b8ce147e10987868473bef6ea7584ab8bc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a82a5946c87d29c9c3bfa603cc0fd1b8ce147e10987868473bef6ea7584ab8bc->enter($__internal_a82a5946c87d29c9c3bfa603cc0fd1b8ce147e10987868473bef6ea7584ab8bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_84e181f406d53b6b2af1fadbbcc2b376c0738d9200272bc745f78220e29bbcff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_84e181f406d53b6b2af1fadbbcc2b376c0738d9200272bc745f78220e29bbcff->enter($__internal_84e181f406d53b6b2af1fadbbcc2b376c0738d9200272bc745f78220e29bbcff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 4
        echo "        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-26px;\">
                <h2 style=\"color: white;margin-top:4%\"> <center>Liste des Marchandises</center></h2>
            </div>
            <br><br>
        </div>
    ";
        
        $__internal_84e181f406d53b6b2af1fadbbcc2b376c0738d9200272bc745f78220e29bbcff->leave($__internal_84e181f406d53b6b2af1fadbbcc2b376c0738d9200272bc745f78220e29bbcff_prof);

        
        $__internal_a82a5946c87d29c9c3bfa603cc0fd1b8ce147e10987868473bef6ea7584ab8bc->leave($__internal_a82a5946c87d29c9c3bfa603cc0fd1b8ce147e10987868473bef6ea7584ab8bc_prof);

    }

    // line 11
    public function block_modules($context, array $blocks = array())
    {
        $__internal_5662937bd9d5616f5aa078a5459ab87bf1f213904e72c06aaa701a282a98acd5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5662937bd9d5616f5aa078a5459ab87bf1f213904e72c06aaa701a282a98acd5->enter($__internal_5662937bd9d5616f5aa078a5459ab87bf1f213904e72c06aaa701a282a98acd5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_3bb96f0145ff6a253cf428c7aeaab6c9c9ab90a064cccb0e6d83f0afbbdcf369 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3bb96f0145ff6a253cf428c7aeaab6c9c9ab90a064cccb0e6d83f0afbbdcf369->enter($__internal_3bb96f0145ff6a253cf428c7aeaab6c9c9ab90a064cccb0e6d83f0afbbdcf369_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        echo "<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"margin-left: -12%\">
                <section class=\"box-typical\">
                    <div id=\"toolbar\">
                        <h1 style=\"font-weight: bold\">Liste des Marchandises</h1>
                    </div>
                    <div class=\"table-responsive\">
                        <table id=\"table\"
                               data-pagination=\"true\"
                               data-search=\"true\"
                               data-use-row-attr-func=\"true\"
                               data-reorderable-rows=\"true\"
                               data-toolbar=\"#toolbar\">
                            <thead>
                            <tr >
                                <th data-field=\"id\">ID</th>
                                <th data-field=\"name\" >Libelle</th>
                                <th data-field=\"price\" >Categorie</th>
                                <th data-field=\"Modifier\" >Modifier</th>
                                <th data-field=\"Supprimer\" >Supprimer</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </section><!--.box-typical-->

            </div><!--.container-fluid-->
        </div><!--.page-content-->
    ";
        
        $__internal_3bb96f0145ff6a253cf428c7aeaab6c9c9ab90a064cccb0e6d83f0afbbdcf369->leave($__internal_3bb96f0145ff6a253cf428c7aeaab6c9c9ab90a064cccb0e6d83f0afbbdcf369_prof);

        
        $__internal_5662937bd9d5616f5aa078a5459ab87bf1f213904e72c06aaa701a282a98acd5->leave($__internal_5662937bd9d5616f5aa078a5459ab87bf1f213904e72c06aaa701a282a98acd5_prof);

    }

    // line 42
    public function block_script($context, array $blocks = array())
    {
        $__internal_235db4a30d9311499e8d12814202079617c942fccc389b73ce6856d910d91d3c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_235db4a30d9311499e8d12814202079617c942fccc389b73ce6856d910d91d3c->enter($__internal_235db4a30d9311499e8d12814202079617c942fccc389b73ce6856d910d91d3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        $__internal_eb1994214703112caf499aedfb39c78791f6eb6b67ae0bb0b699ef11e93157eb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eb1994214703112caf499aedfb39c78791f6eb6b67ae0bb0b699ef11e93157eb->enter($__internal_eb1994214703112caf499aedfb39c78791f6eb6b67ae0bb0b699ef11e93157eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        // line 43
        echo "    ";
        $this->displayParentBlock("script", $context, $blocks);
        echo "
    <script src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/jquery.tablednd.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table-reorder-rows-init.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_eb1994214703112caf499aedfb39c78791f6eb6b67ae0bb0b699ef11e93157eb->leave($__internal_eb1994214703112caf499aedfb39c78791f6eb6b67ae0bb0b699ef11e93157eb_prof);

        
        $__internal_235db4a30d9311499e8d12814202079617c942fccc389b73ce6856d910d91d3c->leave($__internal_235db4a30d9311499e8d12814202079617c942fccc389b73ce6856d910d91d3c_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Marchandise:listerContratMarchandise.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  165 => 47,  161 => 46,  157 => 45,  153 => 44,  148 => 43,  139 => 42,  92 => 11,  76 => 4,  67 => 3,  56 => 11,  54 => 3,  43 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}
{% block contenuDuMilieu %}<!--le block du milieu de la page-->
    {% block debutPage %}
        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-26px;\">
                <h2 style=\"color: white;margin-top:4%\"> <center>Liste des Marchandises</center></h2>
            </div>
            <br><br>
        </div>
    {% endblock %}
    {% block modules %}<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"margin-left: -12%\">
                <section class=\"box-typical\">
                    <div id=\"toolbar\">
                        <h1 style=\"font-weight: bold\">Liste des Marchandises</h1>
                    </div>
                    <div class=\"table-responsive\">
                        <table id=\"table\"
                               data-pagination=\"true\"
                               data-search=\"true\"
                               data-use-row-attr-func=\"true\"
                               data-reorderable-rows=\"true\"
                               data-toolbar=\"#toolbar\">
                            <thead>
                            <tr >
                                <th data-field=\"id\">ID</th>
                                <th data-field=\"name\" >Libelle</th>
                                <th data-field=\"price\" >Categorie</th>
                                <th data-field=\"Modifier\" >Modifier</th>
                                <th data-field=\"Supprimer\" >Supprimer</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </section><!--.box-typical-->

            </div><!--.container-fluid-->
        </div><!--.page-content-->
    {% endblock %}
{% endblock %}
{% block script %}
    {{ parent() }}
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/jquery.tablednd.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table-reorder-rows-init.js') }}\"></script>
{% endblock %}", "StockBundle:Marchandise:listerContratMarchandise.html.twig", "C:\\wamp\\www\\symfony\\stock\\src\\StockBundle/Resources/views/Marchandise/listerContratMarchandise.html.twig");
    }
}
