<?php

/* default/homepage.html.twig */
class __TwigTemplate_63204c786e9cdb7ffa5923e450e3e8c26cd064b71a86d69690e6f1083d7ce22c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/homepage.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'header' => array($this, 'block_header'),
            'footer' => array($this, 'block_footer'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_66a4c4d40eb5cd2695db353d10de704d55147a84c4ad202b35333827d0aaa1e8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_66a4c4d40eb5cd2695db353d10de704d55147a84c4ad202b35333827d0aaa1e8->enter($__internal_66a4c4d40eb5cd2695db353d10de704d55147a84c4ad202b35333827d0aaa1e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/homepage.html.twig"));

        $__internal_f453ccd2d0f488a3c5b593f24959f452117b65c23c11485a1493ab36f17145f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f453ccd2d0f488a3c5b593f24959f452117b65c23c11485a1493ab36f17145f6->enter($__internal_f453ccd2d0f488a3c5b593f24959f452117b65c23c11485a1493ab36f17145f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/homepage.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_66a4c4d40eb5cd2695db353d10de704d55147a84c4ad202b35333827d0aaa1e8->leave($__internal_66a4c4d40eb5cd2695db353d10de704d55147a84c4ad202b35333827d0aaa1e8_prof);

        
        $__internal_f453ccd2d0f488a3c5b593f24959f452117b65c23c11485a1493ab36f17145f6->leave($__internal_f453ccd2d0f488a3c5b593f24959f452117b65c23c11485a1493ab36f17145f6_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_4f8e9e39a8553d7f0e07ca536892a42078a3724c6476c9a8d60fc9268cb74b6b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4f8e9e39a8553d7f0e07ca536892a42078a3724c6476c9a8d60fc9268cb74b6b->enter($__internal_4f8e9e39a8553d7f0e07ca536892a42078a3724c6476c9a8d60fc9268cb74b6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_d3f376cbd4bf852e462bb54e0689b71edcdf28be3bcfc267d0ba528e02416a63 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d3f376cbd4bf852e462bb54e0689b71edcdf28be3bcfc267d0ba528e02416a63->enter($__internal_d3f376cbd4bf852e462bb54e0689b71edcdf28be3bcfc267d0ba528e02416a63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "homepage";
        
        $__internal_d3f376cbd4bf852e462bb54e0689b71edcdf28be3bcfc267d0ba528e02416a63->leave($__internal_d3f376cbd4bf852e462bb54e0689b71edcdf28be3bcfc267d0ba528e02416a63_prof);

        
        $__internal_4f8e9e39a8553d7f0e07ca536892a42078a3724c6476c9a8d60fc9268cb74b6b->leave($__internal_4f8e9e39a8553d7f0e07ca536892a42078a3724c6476c9a8d60fc9268cb74b6b_prof);

    }

    // line 9
    public function block_header($context, array $blocks = array())
    {
        $__internal_2093efa8030cfb344927d5ad706feab76b334be5d2305cad8b1fd62ddb13112d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2093efa8030cfb344927d5ad706feab76b334be5d2305cad8b1fd62ddb13112d->enter($__internal_2093efa8030cfb344927d5ad706feab76b334be5d2305cad8b1fd62ddb13112d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_ffd938e7c7ed5c31b30db08e47e3936527ed9450dd4f78fe2fc01dd6f5546220 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ffd938e7c7ed5c31b30db08e47e3936527ed9450dd4f78fe2fc01dd6f5546220->enter($__internal_ffd938e7c7ed5c31b30db08e47e3936527ed9450dd4f78fe2fc01dd6f5546220_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        
        $__internal_ffd938e7c7ed5c31b30db08e47e3936527ed9450dd4f78fe2fc01dd6f5546220->leave($__internal_ffd938e7c7ed5c31b30db08e47e3936527ed9450dd4f78fe2fc01dd6f5546220_prof);

        
        $__internal_2093efa8030cfb344927d5ad706feab76b334be5d2305cad8b1fd62ddb13112d->leave($__internal_2093efa8030cfb344927d5ad706feab76b334be5d2305cad8b1fd62ddb13112d_prof);

    }

    // line 10
    public function block_footer($context, array $blocks = array())
    {
        $__internal_46134960ef2358e06d627fec0ed9bcbfad7f0af28ca9abf371b94239339f3a45 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_46134960ef2358e06d627fec0ed9bcbfad7f0af28ca9abf371b94239339f3a45->enter($__internal_46134960ef2358e06d627fec0ed9bcbfad7f0af28ca9abf371b94239339f3a45_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        $__internal_fc6052453b420e302cf5c453c9e79e65627118b5c15e252efef41a5fd30d06c4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fc6052453b420e302cf5c453c9e79e65627118b5c15e252efef41a5fd30d06c4->enter($__internal_fc6052453b420e302cf5c453c9e79e65627118b5c15e252efef41a5fd30d06c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        
        $__internal_fc6052453b420e302cf5c453c9e79e65627118b5c15e252efef41a5fd30d06c4->leave($__internal_fc6052453b420e302cf5c453c9e79e65627118b5c15e252efef41a5fd30d06c4_prof);

        
        $__internal_46134960ef2358e06d627fec0ed9bcbfad7f0af28ca9abf371b94239339f3a45->leave($__internal_46134960ef2358e06d627fec0ed9bcbfad7f0af28ca9abf371b94239339f3a45_prof);

    }

    // line 12
    public function block_body($context, array $blocks = array())
    {
        $__internal_17616c4b7fd0e330e15862b94673e3537649cdc3bac1c7e45186f54c368b490a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_17616c4b7fd0e330e15862b94673e3537649cdc3bac1c7e45186f54c368b490a->enter($__internal_17616c4b7fd0e330e15862b94673e3537649cdc3bac1c7e45186f54c368b490a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_6a13cf41a77890c1a74d0a38ade4d92ab196c7fd66488d1bb9403df9edadb4a1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6a13cf41a77890c1a74d0a38ade4d92ab196c7fd66488d1bb9403df9edadb4a1->enter($__internal_6a13cf41a77890c1a74d0a38ade4d92ab196c7fd66488d1bb9403df9edadb4a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 13
        echo "    <div class=\"page-header\">
        <h1>";
        // line 14
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("title.homepage");
        echo "</h1>
    </div>

    <div class=\"row\">
        <div class=\"col-sm-6\">
            <div class=\"jumbotron\">
                <p>
                    ";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("help.browse_app");
        echo "
                </p>
                <p>
                    <a class=\"btn btn-primary btn-lg\" href=\"";
        // line 24
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_index");
        echo "\">
                        <i class=\"fa fa-users\" aria-hidden=\"true\"></i> ";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("action.browse_app"), "html", null, true);
        echo "
                    </a>
                </p>
            </div>
        </div>

        <div class=\"col-sm-6\">
            <div class=\"jumbotron\">
                <p>
                    ";
        // line 34
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("help.browse_admin");
        echo "
                </p>
                <p>
                    <a class=\"btn btn-primary btn-lg\" href=\"";
        // line 37
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_index");
        echo "\">
                        <i class=\"fa fa-lock\" aria-hidden=\"true\"></i> ";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("action.browse_admin"), "html", null, true);
        echo "
                    </a>
                </p>
            </div>
        </div>
    </div>
";
        
        $__internal_6a13cf41a77890c1a74d0a38ade4d92ab196c7fd66488d1bb9403df9edadb4a1->leave($__internal_6a13cf41a77890c1a74d0a38ade4d92ab196c7fd66488d1bb9403df9edadb4a1_prof);

        
        $__internal_17616c4b7fd0e330e15862b94673e3537649cdc3bac1c7e45186f54c368b490a->leave($__internal_17616c4b7fd0e330e15862b94673e3537649cdc3bac1c7e45186f54c368b490a_prof);

    }

    public function getTemplateName()
    {
        return "default/homepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 38,  145 => 37,  139 => 34,  127 => 25,  123 => 24,  117 => 21,  107 => 14,  104 => 13,  95 => 12,  78 => 10,  61 => 9,  43 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body_id 'homepage' %}

{#
    the homepage is a special page which displays neither a header nor a footer.
    this is done with the 'trick' of defining empty Twig blocks without any content
#}
{% block header %}{% endblock %}
{% block footer %}{% endblock %}

{% block body %}
    <div class=\"page-header\">
        <h1>{{ 'title.homepage'|trans|raw }}</h1>
    </div>

    <div class=\"row\">
        <div class=\"col-sm-6\">
            <div class=\"jumbotron\">
                <p>
                    {{ 'help.browse_app'|trans|raw }}
                </p>
                <p>
                    <a class=\"btn btn-primary btn-lg\" href=\"{{ path('blog_index') }}\">
                        <i class=\"fa fa-users\" aria-hidden=\"true\"></i> {{ 'action.browse_app'|trans }}
                    </a>
                </p>
            </div>
        </div>

        <div class=\"col-sm-6\">
            <div class=\"jumbotron\">
                <p>
                    {{ 'help.browse_admin'|trans|raw }}
                </p>
                <p>
                    <a class=\"btn btn-primary btn-lg\" href=\"{{ path('admin_index') }}\">
                        <i class=\"fa fa-lock\" aria-hidden=\"true\"></i> {{ 'action.browse_admin'|trans }}
                    </a>
                </p>
            </div>
        </div>
    </div>
{% endblock %}
", "default/homepage.html.twig", "C:\\wamp64\\www\\stock\\app\\Resources\\views\\default\\homepage.html.twig");
    }
}
