<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_e85f2a370c14cd3dcadc416324108b420eeb85c44573f5d2dc8e13d82e36a68f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8e61ca6d142f2c2e204b566068c9ddbb9e83ccddf839230c4c3b0eebe727d881 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8e61ca6d142f2c2e204b566068c9ddbb9e83ccddf839230c4c3b0eebe727d881->enter($__internal_8e61ca6d142f2c2e204b566068c9ddbb9e83ccddf839230c4c3b0eebe727d881_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_094d672bf43f0b4459e44d077f4f8e33811d835c95aa2b6740808a691b07adbd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_094d672bf43f0b4459e44d077f4f8e33811d835c95aa2b6740808a691b07adbd->enter($__internal_094d672bf43f0b4459e44d077f4f8e33811d835c95aa2b6740808a691b07adbd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8e61ca6d142f2c2e204b566068c9ddbb9e83ccddf839230c4c3b0eebe727d881->leave($__internal_8e61ca6d142f2c2e204b566068c9ddbb9e83ccddf839230c4c3b0eebe727d881_prof);

        
        $__internal_094d672bf43f0b4459e44d077f4f8e33811d835c95aa2b6740808a691b07adbd->leave($__internal_094d672bf43f0b4459e44d077f4f8e33811d835c95aa2b6740808a691b07adbd_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_a8a4c01a36a89f1aca6e7789782b21c63c70c74567d9fe0fb0ec5c10dd6c01c4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a8a4c01a36a89f1aca6e7789782b21c63c70c74567d9fe0fb0ec5c10dd6c01c4->enter($__internal_a8a4c01a36a89f1aca6e7789782b21c63c70c74567d9fe0fb0ec5c10dd6c01c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_d697c2ec1561a637dc64388c6243a6d64a6fe1c78823d43eb8b246b9b9271418 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d697c2ec1561a637dc64388c6243a6d64a6fe1c78823d43eb8b246b9b9271418->enter($__internal_d697c2ec1561a637dc64388c6243a6d64a6fe1c78823d43eb8b246b9b9271418_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_d697c2ec1561a637dc64388c6243a6d64a6fe1c78823d43eb8b246b9b9271418->leave($__internal_d697c2ec1561a637dc64388c6243a6d64a6fe1c78823d43eb8b246b9b9271418_prof);

        
        $__internal_a8a4c01a36a89f1aca6e7789782b21c63c70c74567d9fe0fb0ec5c10dd6c01c4->leave($__internal_a8a4c01a36a89f1aca6e7789782b21c63c70c74567d9fe0fb0ec5c10dd6c01c4_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_1e20bc3ee82d1713bfd53f066ac080a1b6388c6be4068266f1a5d7d8d2e21664 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1e20bc3ee82d1713bfd53f066ac080a1b6388c6be4068266f1a5d7d8d2e21664->enter($__internal_1e20bc3ee82d1713bfd53f066ac080a1b6388c6be4068266f1a5d7d8d2e21664_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_3d15fb739588f53d44991dce24e3caa26d07d7209e37213261b1637e8587dd05 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3d15fb739588f53d44991dce24e3caa26d07d7209e37213261b1637e8587dd05->enter($__internal_3d15fb739588f53d44991dce24e3caa26d07d7209e37213261b1637e8587dd05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_3d15fb739588f53d44991dce24e3caa26d07d7209e37213261b1637e8587dd05->leave($__internal_3d15fb739588f53d44991dce24e3caa26d07d7209e37213261b1637e8587dd05_prof);

        
        $__internal_1e20bc3ee82d1713bfd53f066ac080a1b6388c6be4068266f1a5d7d8d2e21664->leave($__internal_1e20bc3ee82d1713bfd53f066ac080a1b6388c6be4068266f1a5d7d8d2e21664_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_62f7b68e0cdbf07ae641e33622d5b4913150c3a08c6d3d552f37fa89b206d103 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_62f7b68e0cdbf07ae641e33622d5b4913150c3a08c6d3d552f37fa89b206d103->enter($__internal_62f7b68e0cdbf07ae641e33622d5b4913150c3a08c6d3d552f37fa89b206d103_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_b4b9871f791b0ce62fdf6700a767b26e9f89c9eede2791dccda6656250ff02c0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b4b9871f791b0ce62fdf6700a767b26e9f89c9eede2791dccda6656250ff02c0->enter($__internal_b4b9871f791b0ce62fdf6700a767b26e9f89c9eede2791dccda6656250ff02c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_b4b9871f791b0ce62fdf6700a767b26e9f89c9eede2791dccda6656250ff02c0->leave($__internal_b4b9871f791b0ce62fdf6700a767b26e9f89c9eede2791dccda6656250ff02c0_prof);

        
        $__internal_62f7b68e0cdbf07ae641e33622d5b4913150c3a08c6d3d552f37fa89b206d103->leave($__internal_62f7b68e0cdbf07ae641e33622d5b4913150c3a08c6d3d552f37fa89b206d103_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "C:\\wamp64\\www\\stock\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception_full.html.twig");
    }
}
