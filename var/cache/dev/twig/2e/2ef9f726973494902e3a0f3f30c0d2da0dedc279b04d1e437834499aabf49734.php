<?php

/* StockBundle:InstructionLivraison:listerInstructionLivraison.html.twig */
class __TwigTemplate_70f694fb11f25d2f2e2a617225277b236d89af1e33efb62d5c735d8081ebe299 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:InstructionLivraison:listerInstructionLivraison.html.twig", 1);
        $this->blocks = array(
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_069b93c5efe049d32f976c244317e6a72621260a79c7321260e285f5966a9a0e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_069b93c5efe049d32f976c244317e6a72621260a79c7321260e285f5966a9a0e->enter($__internal_069b93c5efe049d32f976c244317e6a72621260a79c7321260e285f5966a9a0e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:InstructionLivraison:listerInstructionLivraison.html.twig"));

        $__internal_3d20c9d7c457cc4b7f9edd17d155221aca96082aa03c8cf6fef0c255831c3cf9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3d20c9d7c457cc4b7f9edd17d155221aca96082aa03c8cf6fef0c255831c3cf9->enter($__internal_3d20c9d7c457cc4b7f9edd17d155221aca96082aa03c8cf6fef0c255831c3cf9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:InstructionLivraison:listerInstructionLivraison.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_069b93c5efe049d32f976c244317e6a72621260a79c7321260e285f5966a9a0e->leave($__internal_069b93c5efe049d32f976c244317e6a72621260a79c7321260e285f5966a9a0e_prof);

        
        $__internal_3d20c9d7c457cc4b7f9edd17d155221aca96082aa03c8cf6fef0c255831c3cf9->leave($__internal_3d20c9d7c457cc4b7f9edd17d155221aca96082aa03c8cf6fef0c255831c3cf9_prof);

    }

    // line 2
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_efabfa1a0bd6f01d0762c8727108b16150e0e9b3acba6efe0ca4699a311fec3a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_efabfa1a0bd6f01d0762c8727108b16150e0e9b3acba6efe0ca4699a311fec3a->enter($__internal_efabfa1a0bd6f01d0762c8727108b16150e0e9b3acba6efe0ca4699a311fec3a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_db9fce2ed624d7e0c5a4742e347b81984b985b0d68ededd8ec09d019425e736b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_db9fce2ed624d7e0c5a4742e347b81984b985b0d68ededd8ec09d019425e736b->enter($__internal_db9fce2ed624d7e0c5a4742e347b81984b985b0d68ededd8ec09d019425e736b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        echo "<!--le block du milieu de la page-->
    ";
        // line 3
        $this->displayBlock('debutPage', $context, $blocks);
        // line 11
        echo "    ";
        $this->displayBlock('modules', $context, $blocks);
        
        $__internal_db9fce2ed624d7e0c5a4742e347b81984b985b0d68ededd8ec09d019425e736b->leave($__internal_db9fce2ed624d7e0c5a4742e347b81984b985b0d68ededd8ec09d019425e736b_prof);

        
        $__internal_efabfa1a0bd6f01d0762c8727108b16150e0e9b3acba6efe0ca4699a311fec3a->leave($__internal_efabfa1a0bd6f01d0762c8727108b16150e0e9b3acba6efe0ca4699a311fec3a_prof);

    }

    // line 3
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_eb08fd4eb5c20137cb464bceaf73fb10b7dc65f2a8ec3e908bc9a898b7e31893 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eb08fd4eb5c20137cb464bceaf73fb10b7dc65f2a8ec3e908bc9a898b7e31893->enter($__internal_eb08fd4eb5c20137cb464bceaf73fb10b7dc65f2a8ec3e908bc9a898b7e31893_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_11402b878f33823956137072fd81687ceb243894af0474355f77208d7949af18 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_11402b878f33823956137072fd81687ceb243894af0474355f77208d7949af18->enter($__internal_11402b878f33823956137072fd81687ceb243894af0474355f77208d7949af18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 4
        echo "        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                <h3 style=\"color: white;margin-top:4%\"> <center>Liste des Marchandises</center></h3>
            </div>
            <br><br>
        </div>
    ";
        
        $__internal_11402b878f33823956137072fd81687ceb243894af0474355f77208d7949af18->leave($__internal_11402b878f33823956137072fd81687ceb243894af0474355f77208d7949af18_prof);

        
        $__internal_eb08fd4eb5c20137cb464bceaf73fb10b7dc65f2a8ec3e908bc9a898b7e31893->leave($__internal_eb08fd4eb5c20137cb464bceaf73fb10b7dc65f2a8ec3e908bc9a898b7e31893_prof);

    }

    // line 11
    public function block_modules($context, array $blocks = array())
    {
        $__internal_8e415f505f7967af4048aef662ee13060b0b0cff0cb154ceb6bd904bb759ef73 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8e415f505f7967af4048aef662ee13060b0b0cff0cb154ceb6bd904bb759ef73->enter($__internal_8e415f505f7967af4048aef662ee13060b0b0cff0cb154ceb6bd904bb759ef73_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_fa644e3a1b30cff4b9e812432eb4e8cc4c17070fa468ed9685342b7e1c1939b4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fa644e3a1b30cff4b9e812432eb4e8cc4c17070fa468ed9685342b7e1c1939b4->enter($__internal_fa644e3a1b30cff4b9e812432eb4e8cc4c17070fa468ed9685342b7e1c1939b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        echo "<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"margin-left: -12%\">

                <div class=\"\">
                    <table  class=\"display table table-striped table-bordered\" cellspacing=\"0\" width=\"100%\" id=\"table\" style=\"margin-left:1%;margin-top: -10%\">
                        <thead>
                        <tr class=\"jumbotron\">
                            <th data-field=\"id\">ID</th>
                            <th data-field=\"libelle\">Libelle</th>
                            <th data-field=\"marchandise\">Marchandise</th>
                            <th data-field=\"quantite\">Quantite</th>
                            <th data-field=\"unite\">Unite</th>
                            <th data-field=\"Modifier\">Modifier</th>
                            <th data-field=\"Supprimer\">Supprimer</th>
                        </tr>
                        </thead>
                        <tbody>
                        ";
        // line 29
        $context["j"] = 1;
        // line 30
        echo "                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["liste"] ?? $this->getContext($context, "liste")));
        foreach ($context['_seq'] as $context["_key"] => $context["l"]) {
            // line 31
            echo "                            <tr>
                                <td>";
            // line 32
            echo twig_escape_filter($this->env, ($context["j"] ?? $this->getContext($context, "j")), "html", null, true);
            echo "</td>
                                <td>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "libelle", array()), "html", null, true);
            echo "</td>
                                <td>";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["l"], "marchandise", array()), "nom", array()), "html", null, true);
            echo "</td>
                                <td>";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "quantite", array()), "html", null, true);
            echo "</td>
                                <td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "unite", array()), "html", null, true);
            echo "</td>
                                <td><a href=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("editer_instruction", array("id" => $this->getAttribute($context["l"], "id", array()))), "html", null, true);
            echo "\" class=\"font-icon font-icon-pen\" style=\"color: green\"></a></td>
                                <td><a href=\"";
            // line 38
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("delete_instruction", array("id" => $this->getAttribute($context["l"], "id", array()))), "html", null, true);
            echo "\" class=\"font-icon font-icon-del\" style=\"color: red\"></a></td>
                            </tr>
                            ";
            // line 40
            $context["j"] = (($context["j"] ?? $this->getContext($context, "j")) + 1);
            // line 41
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['l'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "                        </tbody>
                    </table>
                </div>


            </div><!--.container-fluid-->
        </div><!--.page-content-->
    ";
        
        $__internal_fa644e3a1b30cff4b9e812432eb4e8cc4c17070fa468ed9685342b7e1c1939b4->leave($__internal_fa644e3a1b30cff4b9e812432eb4e8cc4c17070fa468ed9685342b7e1c1939b4_prof);

        
        $__internal_8e415f505f7967af4048aef662ee13060b0b0cff0cb154ceb6bd904bb759ef73->leave($__internal_8e415f505f7967af4048aef662ee13060b0b0cff0cb154ceb6bd904bb759ef73_prof);

    }

    // line 51
    public function block_script($context, array $blocks = array())
    {
        $__internal_c4298ea8b71ff15634002305412c5fff2103b2eaa677c5ef92bdf76ddd84b3e6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c4298ea8b71ff15634002305412c5fff2103b2eaa677c5ef92bdf76ddd84b3e6->enter($__internal_c4298ea8b71ff15634002305412c5fff2103b2eaa677c5ef92bdf76ddd84b3e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        $__internal_f0485990857112d5d84b4ad082f4af44302c1764347acadac9d20c9f6845b3d6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f0485990857112d5d84b4ad082f4af44302c1764347acadac9d20c9f6845b3d6->enter($__internal_f0485990857112d5d84b4ad082f4af44302c1764347acadac9d20c9f6845b3d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        // line 52
        echo "    ";
        $this->displayParentBlock("script", $context, $blocks);
        echo "
    <script src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/jquery.tablednd.js"), "html", null, true);
        echo "\"></script>
    ";
        
        $__internal_f0485990857112d5d84b4ad082f4af44302c1764347acadac9d20c9f6845b3d6->leave($__internal_f0485990857112d5d84b4ad082f4af44302c1764347acadac9d20c9f6845b3d6_prof);

        
        $__internal_c4298ea8b71ff15634002305412c5fff2103b2eaa677c5ef92bdf76ddd84b3e6->leave($__internal_c4298ea8b71ff15634002305412c5fff2103b2eaa677c5ef92bdf76ddd84b3e6_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:InstructionLivraison:listerInstructionLivraison.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  206 => 55,  202 => 54,  198 => 53,  193 => 52,  184 => 51,  167 => 42,  161 => 41,  159 => 40,  154 => 38,  150 => 37,  146 => 36,  142 => 35,  138 => 34,  134 => 33,  130 => 32,  127 => 31,  122 => 30,  120 => 29,  92 => 11,  76 => 4,  67 => 3,  56 => 11,  54 => 3,  43 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}
{% block contenuDuMilieu %}<!--le block du milieu de la page-->
    {% block debutPage %}
        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                <h3 style=\"color: white;margin-top:4%\"> <center>Liste des Marchandises</center></h3>
            </div>
            <br><br>
        </div>
    {% endblock %}
    {% block modules %}<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"margin-left: -12%\">

                <div class=\"\">
                    <table  class=\"display table table-striped table-bordered\" cellspacing=\"0\" width=\"100%\" id=\"table\" style=\"margin-left:1%;margin-top: -10%\">
                        <thead>
                        <tr class=\"jumbotron\">
                            <th data-field=\"id\">ID</th>
                            <th data-field=\"libelle\">Libelle</th>
                            <th data-field=\"marchandise\">Marchandise</th>
                            <th data-field=\"quantite\">Quantite</th>
                            <th data-field=\"unite\">Unite</th>
                            <th data-field=\"Modifier\">Modifier</th>
                            <th data-field=\"Supprimer\">Supprimer</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% set j=1 %}
                        {% for l in liste %}
                            <tr>
                                <td>{{ j }}</td>
                                <td>{{ l.libelle }}</td>
                                <td>{{ l.marchandise.nom }}</td>
                                <td>{{ l.quantite }}</td>
                                <td>{{ l.unite }}</td>
                                <td><a href=\"{{ path(\"editer_instruction\", {'id':l.id}) }}\" class=\"font-icon font-icon-pen\" style=\"color: green\"></a></td>
                                <td><a href=\"{{ path(\"delete_instruction\", {'id':l.id}) }}\" class=\"font-icon font-icon-del\" style=\"color: red\"></a></td>
                            </tr>
                            {% set j=j+1 %}
                        {% endfor %}
                        </tbody>
                    </table>
                </div>


            </div><!--.container-fluid-->
        </div><!--.page-content-->
    {% endblock %}
{% endblock %}
{% block script %}
    {{ parent() }}
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/jquery.tablednd.js') }}\"></script>
    {#<script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table-reorder-rows-init.js') }}\"></script>#}
{% endblock %}", "StockBundle:InstructionLivraison:listerInstructionLivraison.html.twig", "C:\\wamp64\\www\\stock\\src\\StockBundle/Resources/views/InstructionLivraison/listerInstructionLivraison.html.twig");
    }
}
