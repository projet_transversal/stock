<?php

/* StockBundle:Trans:addCommercant.html.twig */
class __TwigTemplate_f3e085e7cdb926dbbedc93610a760c8b45d59574749fa2261dac55c52aa39124 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:Trans:addCommercant.html.twig", 1);
        $this->blocks = array(
            'menu' => array($this, 'block_menu'),
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f8f33a4a6dccdefd0a3fa50898cf6dd5e3d7da27a8d56c08d43412e0a8e4216c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f8f33a4a6dccdefd0a3fa50898cf6dd5e3d7da27a8d56c08d43412e0a8e4216c->enter($__internal_f8f33a4a6dccdefd0a3fa50898cf6dd5e3d7da27a8d56c08d43412e0a8e4216c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Trans:addCommercant.html.twig"));

        $__internal_810a98568a7f319da41658cb2eccc176c9e5455c80754eef34c1947253c249d9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_810a98568a7f319da41658cb2eccc176c9e5455c80754eef34c1947253c249d9->enter($__internal_810a98568a7f319da41658cb2eccc176c9e5455c80754eef34c1947253c249d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Trans:addCommercant.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f8f33a4a6dccdefd0a3fa50898cf6dd5e3d7da27a8d56c08d43412e0a8e4216c->leave($__internal_f8f33a4a6dccdefd0a3fa50898cf6dd5e3d7da27a8d56c08d43412e0a8e4216c_prof);

        
        $__internal_810a98568a7f319da41658cb2eccc176c9e5455c80754eef34c1947253c249d9->leave($__internal_810a98568a7f319da41658cb2eccc176c9e5455c80754eef34c1947253c249d9_prof);

    }

    // line 3
    public function block_menu($context, array $blocks = array())
    {
        $__internal_ee65b15fbd645f7f9b6896558535cec77cf4bcdca2ae8a20cb67c81c55321210 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ee65b15fbd645f7f9b6896558535cec77cf4bcdca2ae8a20cb67c81c55321210->enter($__internal_ee65b15fbd645f7f9b6896558535cec77cf4bcdca2ae8a20cb67c81c55321210_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_0362a2bc6225f9491873c77106d66833ba1f520769786fdce532ca8a34f78c40 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0362a2bc6225f9491873c77106d66833ba1f520769786fdce532ca8a34f78c40->enter($__internal_0362a2bc6225f9491873c77106d66833ba1f520769786fdce532ca8a34f78c40_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 4
        echo "\t\t<nav class=\"side-menu side-menu-big-icon\" style=\"background: #3299CC;overflow: hidden\">
            <nav class=\"sidebar-header\">
                <img src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/man.png"), "html", null, true);
        echo "\" style=\"margin-left:35%;margin-top:-15%;\"><br><br>
                <h5 style=\"text-align:center\">User Name</h5>
                <h5 style=\"text-align:center\">Profil</h5>
            </nav>
            <hr style=\"margin-left:20%;margin-right: 20%;background: red;height:1px\">
\t\t<ul class=\"side-menu-list\">
                <li class=\"opened\">
                    <span>
                    \t<a href=\"";
        // line 14
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("livrer_marchandise");
        echo "\" >
\t                        <i class=\"glyphicon glyphicon-inbox\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Livrer marchandise</span>
                        </a>
                    </span>
                </li>
\t\t\t\t<li class=\"opened\">
                    <span>
                    \t<a href=\"";
        // line 22
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_marchandises");
        echo "\" >
\t                        <i class=\"glyphicon glyphicon-th-list\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Lister marchandise</span>
                        </a>
                    </span>
                </li>
\t\t\t\t<li class=\"opened\">
                    <span>
                    \t<a href=\"";
        // line 30
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_comm");
        echo "\" >
\t                        <i class=\"glyphicon glyphicon-plus\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Ajouter commerçant</span>
                        </a>
                    </span>
                </li>
\t\t</ul>
\t  </nav>
\t ";
        
        $__internal_0362a2bc6225f9491873c77106d66833ba1f520769786fdce532ca8a34f78c40->leave($__internal_0362a2bc6225f9491873c77106d66833ba1f520769786fdce532ca8a34f78c40_prof);

        
        $__internal_ee65b15fbd645f7f9b6896558535cec77cf4bcdca2ae8a20cb67c81c55321210->leave($__internal_ee65b15fbd645f7f9b6896558535cec77cf4bcdca2ae8a20cb67c81c55321210_prof);

    }

    // line 39
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_c970ad497a9033ad61b8f7e08da75dff3cb410084f1042f4d4cb7f83d0a848ec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c970ad497a9033ad61b8f7e08da75dff3cb410084f1042f4d4cb7f83d0a848ec->enter($__internal_c970ad497a9033ad61b8f7e08da75dff3cb410084f1042f4d4cb7f83d0a848ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_52abc419a456ab6e585716615013eaa0a23ebe1114020ffcfbbd644738efd128 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_52abc419a456ab6e585716615013eaa0a23ebe1114020ffcfbbd644738efd128->enter($__internal_52abc419a456ab6e585716615013eaa0a23ebe1114020ffcfbbd644738efd128_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        // line 40
        echo "        ";
        $this->displayBlock('debutPage', $context, $blocks);
        // line 49
        echo "        ";
        $this->displayBlock('modules', $context, $blocks);
        // line 60
        echo "    ";
        
        $__internal_52abc419a456ab6e585716615013eaa0a23ebe1114020ffcfbbd644738efd128->leave($__internal_52abc419a456ab6e585716615013eaa0a23ebe1114020ffcfbbd644738efd128_prof);

        
        $__internal_c970ad497a9033ad61b8f7e08da75dff3cb410084f1042f4d4cb7f83d0a848ec->leave($__internal_c970ad497a9033ad61b8f7e08da75dff3cb410084f1042f4d4cb7f83d0a848ec_prof);

    }

    // line 40
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_dfd57ad30956eba63c2265a0f688613afc3da7760ae370eb3541e9be9dc56e66 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dfd57ad30956eba63c2265a0f688613afc3da7760ae370eb3541e9be9dc56e66->enter($__internal_dfd57ad30956eba63c2265a0f688613afc3da7760ae370eb3541e9be9dc56e66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_d80937d1da43dea37a771ba90300e3fc35148927cb79493161b8f597e57e3c03 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d80937d1da43dea37a771ba90300e3fc35148927cb79493161b8f597e57e3c03->enter($__internal_d80937d1da43dea37a771ba90300e3fc35148927cb79493161b8f597e57e3c03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 41
        echo "            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-20px;\">
                    <h2 style=\"color: white;margin-top:4%\"> <center>Ajouter commerçant</center></h2>
                </div>
                <br><br><br><br><br><br>
            </div>

        ";
        
        $__internal_d80937d1da43dea37a771ba90300e3fc35148927cb79493161b8f597e57e3c03->leave($__internal_d80937d1da43dea37a771ba90300e3fc35148927cb79493161b8f597e57e3c03_prof);

        
        $__internal_dfd57ad30956eba63c2265a0f688613afc3da7760ae370eb3541e9be9dc56e66->leave($__internal_dfd57ad30956eba63c2265a0f688613afc3da7760ae370eb3541e9be9dc56e66_prof);

    }

    // line 49
    public function block_modules($context, array $blocks = array())
    {
        $__internal_83c50abad72f91f5517e7e0503f78b5a0419fd80468beb15da81733c718a4cf5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_83c50abad72f91f5517e7e0503f78b5a0419fd80468beb15da81733c718a4cf5->enter($__internal_83c50abad72f91f5517e7e0503f78b5a0419fd80468beb15da81733c718a4cf5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_abf05b3c658f53567c5afb823e7f4ccedd84e2ae2428ab08baa991492f330250 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_abf05b3c658f53567c5afb823e7f4ccedd84e2ae2428ab08baa991492f330250->enter($__internal_abf05b3c658f53567c5afb823e7f4ccedd84e2ae2428ab08baa991492f330250_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        // line 50
        echo "            <div class=\"col-xl-6\" style=\"margin-left: 27%\">
                ";
        // line 51
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
                    ";
        // line 52
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
                    <div class=\"form-group\" align=\"center\">
                        <input type=\"submit\" name=\"addCommercant\" class=\"btn btn-success\" value=\"Ajouter\"/>
                        <input type=\"reset\" name=\"resetCommercant\" class=\"btn btn-danger\" value=\"Annuler\"/>
                    </div>
                ";
        // line 57
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
            </div>
        ";
        
        $__internal_abf05b3c658f53567c5afb823e7f4ccedd84e2ae2428ab08baa991492f330250->leave($__internal_abf05b3c658f53567c5afb823e7f4ccedd84e2ae2428ab08baa991492f330250_prof);

        
        $__internal_83c50abad72f91f5517e7e0503f78b5a0419fd80468beb15da81733c718a4cf5->leave($__internal_83c50abad72f91f5517e7e0503f78b5a0419fd80468beb15da81733c718a4cf5_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Trans:addCommercant.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  183 => 57,  175 => 52,  171 => 51,  168 => 50,  159 => 49,  142 => 41,  133 => 40,  123 => 60,  120 => 49,  117 => 40,  108 => 39,  89 => 30,  78 => 22,  67 => 14,  56 => 6,  52 => 4,  43 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}

\t{% block menu %}
\t\t<nav class=\"side-menu side-menu-big-icon\" style=\"background: #3299CC;overflow: hidden\">
            <nav class=\"sidebar-header\">
                <img src=\"{{asset('img/man.png')}}\" style=\"margin-left:35%;margin-top:-15%;\"><br><br>
                <h5 style=\"text-align:center\">User Name</h5>
                <h5 style=\"text-align:center\">Profil</h5>
            </nav>
            <hr style=\"margin-left:20%;margin-right: 20%;background: red;height:1px\">
\t\t<ul class=\"side-menu-list\">
                <li class=\"opened\">
                    <span>
                    \t<a href=\"{{ path('livrer_marchandise') }}\" >
\t                        <i class=\"glyphicon glyphicon-inbox\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Livrer marchandise</span>
                        </a>
                    </span>
                </li>
\t\t\t\t<li class=\"opened\">
                    <span>
                    \t<a href=\"{{ path('lister_marchandises') }}\" >
\t                        <i class=\"glyphicon glyphicon-th-list\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Lister marchandise</span>
                        </a>
                    </span>
                </li>
\t\t\t\t<li class=\"opened\">
                    <span>
                    \t<a href=\"{{ path('ajouter_comm') }}\" >
\t                        <i class=\"glyphicon glyphicon-plus\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Ajouter commerçant</span>
                        </a>
                    </span>
                </li>
\t\t</ul>
\t  </nav>
\t {% endblock %}
    {% block contenuDuMilieu %}
        {% block debutPage %}
            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-20px;\">
                    <h2 style=\"color: white;margin-top:4%\"> <center>Ajouter commerçant</center></h2>
                </div>
                <br><br><br><br><br><br>
            </div>

        {% endblock %}
        {% block modules %}
            <div class=\"col-xl-6\" style=\"margin-left: 27%\">
                {{ form_start(form) }}
                    {{ form_widget(form) }}
                    <div class=\"form-group\" align=\"center\">
                        <input type=\"submit\" name=\"addCommercant\" class=\"btn btn-success\" value=\"Ajouter\"/>
                        <input type=\"reset\" name=\"resetCommercant\" class=\"btn btn-danger\" value=\"Annuler\"/>
                    </div>
                {{ form_end(form) }}
            </div>
        {% endblock %}
    {% endblock %}", "StockBundle:Trans:addCommercant.html.twig", "C:\\wamp\\www\\symfony\\stock\\src\\StockBundle/Resources/views/Trans/addCommercant.html.twig");
    }
}
