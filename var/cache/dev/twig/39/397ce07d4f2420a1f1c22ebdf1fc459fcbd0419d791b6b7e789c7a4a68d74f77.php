<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_8dfa32f164722abfe0224532199144c0c98f71a4e2771ff50fa5981c69a9bcab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_16e4b582250f4a56d93a3f8f08269fb6cc564b83ef91482b4f4aa7c0eb3c66aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_16e4b582250f4a56d93a3f8f08269fb6cc564b83ef91482b4f4aa7c0eb3c66aa->enter($__internal_16e4b582250f4a56d93a3f8f08269fb6cc564b83ef91482b4f4aa7c0eb3c66aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_6938eb2d6fca23d3c1f2d4792f845a57b495317506911c0fd1b96d47122a00a0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6938eb2d6fca23d3c1f2d4792f845a57b495317506911c0fd1b96d47122a00a0->enter($__internal_6938eb2d6fca23d3c1f2d4792f845a57b495317506911c0fd1b96d47122a00a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_16e4b582250f4a56d93a3f8f08269fb6cc564b83ef91482b4f4aa7c0eb3c66aa->leave($__internal_16e4b582250f4a56d93a3f8f08269fb6cc564b83ef91482b4f4aa7c0eb3c66aa_prof);

        
        $__internal_6938eb2d6fca23d3c1f2d4792f845a57b495317506911c0fd1b96d47122a00a0->leave($__internal_6938eb2d6fca23d3c1f2d4792f845a57b495317506911c0fd1b96d47122a00a0_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_14957390c6c1fa969ed3bd49fc2b9e4760710bdeaafe4b1dd90d46e7baef4b3f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_14957390c6c1fa969ed3bd49fc2b9e4760710bdeaafe4b1dd90d46e7baef4b3f->enter($__internal_14957390c6c1fa969ed3bd49fc2b9e4760710bdeaafe4b1dd90d46e7baef4b3f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_d0191950bb9b28130230052477ce1531ed35b6fe53b9f106caff5b42a02382b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d0191950bb9b28130230052477ce1531ed35b6fe53b9f106caff5b42a02382b9->enter($__internal_d0191950bb9b28130230052477ce1531ed35b6fe53b9f106caff5b42a02382b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_d0191950bb9b28130230052477ce1531ed35b6fe53b9f106caff5b42a02382b9->leave($__internal_d0191950bb9b28130230052477ce1531ed35b6fe53b9f106caff5b42a02382b9_prof);

        
        $__internal_14957390c6c1fa969ed3bd49fc2b9e4760710bdeaafe4b1dd90d46e7baef4b3f->leave($__internal_14957390c6c1fa969ed3bd49fc2b9e4760710bdeaafe4b1dd90d46e7baef4b3f_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_e5245ba676b7dc93b27fed2a2d2c869401b0d47ae60f4dc564057fec97884abe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e5245ba676b7dc93b27fed2a2d2c869401b0d47ae60f4dc564057fec97884abe->enter($__internal_e5245ba676b7dc93b27fed2a2d2c869401b0d47ae60f4dc564057fec97884abe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_0b80856d07aaa14b042ecaf07ef5699f239092ce0775ef84cabf8476061e9b9d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b80856d07aaa14b042ecaf07ef5699f239092ce0775ef84cabf8476061e9b9d->enter($__internal_0b80856d07aaa14b042ecaf07ef5699f239092ce0775ef84cabf8476061e9b9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_0b80856d07aaa14b042ecaf07ef5699f239092ce0775ef84cabf8476061e9b9d->leave($__internal_0b80856d07aaa14b042ecaf07ef5699f239092ce0775ef84cabf8476061e9b9d_prof);

        
        $__internal_e5245ba676b7dc93b27fed2a2d2c869401b0d47ae60f4dc564057fec97884abe->leave($__internal_e5245ba676b7dc93b27fed2a2d2c869401b0d47ae60f4dc564057fec97884abe_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_e7da4cdc398619cccdb9c3762df327682307238a82948e911b4877a087490295 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e7da4cdc398619cccdb9c3762df327682307238a82948e911b4877a087490295->enter($__internal_e7da4cdc398619cccdb9c3762df327682307238a82948e911b4877a087490295_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_170a9ebe7138c32fd2bcbfe668fb1bdff9e7a1775abb53285c2416e30cb76187 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_170a9ebe7138c32fd2bcbfe668fb1bdff9e7a1775abb53285c2416e30cb76187->enter($__internal_170a9ebe7138c32fd2bcbfe668fb1bdff9e7a1775abb53285c2416e30cb76187_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_170a9ebe7138c32fd2bcbfe668fb1bdff9e7a1775abb53285c2416e30cb76187->leave($__internal_170a9ebe7138c32fd2bcbfe668fb1bdff9e7a1775abb53285c2416e30cb76187_prof);

        
        $__internal_e7da4cdc398619cccdb9c3762df327682307238a82948e911b4877a087490295->leave($__internal_e7da4cdc398619cccdb9c3762df327682307238a82948e911b4877a087490295_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "C:\\wamp64\\www\\stock\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
