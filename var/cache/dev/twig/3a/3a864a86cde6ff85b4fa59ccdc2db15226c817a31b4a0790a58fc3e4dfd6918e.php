<?php

/* ::transporteur.html.twig */
class __TwigTemplate_6e4f24491d32e513139193507a1dbe59d7af73b09caa1fa4bd0c57a35354e9ef extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "::transporteur.html.twig", 1);
        $this->blocks = array(
            'menu' => array($this, 'block_menu'),
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_abb24553663f09f4c49521a0d2645cb95c63ee92512ef122cbad6476e1e51a95 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_abb24553663f09f4c49521a0d2645cb95c63ee92512ef122cbad6476e1e51a95->enter($__internal_abb24553663f09f4c49521a0d2645cb95c63ee92512ef122cbad6476e1e51a95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::transporteur.html.twig"));

        $__internal_781de8ba11c42a42c9022ab49b6648946dfcbe4dfddb575df28c95fccbe977ab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_781de8ba11c42a42c9022ab49b6648946dfcbe4dfddb575df28c95fccbe977ab->enter($__internal_781de8ba11c42a42c9022ab49b6648946dfcbe4dfddb575df28c95fccbe977ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::transporteur.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_abb24553663f09f4c49521a0d2645cb95c63ee92512ef122cbad6476e1e51a95->leave($__internal_abb24553663f09f4c49521a0d2645cb95c63ee92512ef122cbad6476e1e51a95_prof);

        
        $__internal_781de8ba11c42a42c9022ab49b6648946dfcbe4dfddb575df28c95fccbe977ab->leave($__internal_781de8ba11c42a42c9022ab49b6648946dfcbe4dfddb575df28c95fccbe977ab_prof);

    }

    // line 3
    public function block_menu($context, array $blocks = array())
    {
        $__internal_175e743db90dbb2ca0cabd3c422d6d7a6d5b3555f6022e31433fa83cd3715731 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_175e743db90dbb2ca0cabd3c422d6d7a6d5b3555f6022e31433fa83cd3715731->enter($__internal_175e743db90dbb2ca0cabd3c422d6d7a6d5b3555f6022e31433fa83cd3715731_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_40760ea84254c4807d6c79d82384ca99e9fc258b9f86e587a67df9593398e075 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_40760ea84254c4807d6c79d82384ca99e9fc258b9f86e587a67df9593398e075->enter($__internal_40760ea84254c4807d6c79d82384ca99e9fc258b9f86e587a67df9593398e075_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 4
        echo "        <nav class=\"side-menu side-menu-big-icon\" style=\"background: #3299CC;overflow: hidden\">
            <nav class=\"sidebar-header\">
                <img src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/man.png"), "html", null, true);
        echo "\" style=\"margin-left:35%;margin-top:-15%;\"><br><br>
                <h5 style=\"text-align:center\">User Name</h5>
                <h5 style=\"text-align:center\">Profil</h5>
            </nav>
            <hr style=\"margin-left:20%;margin-right: 20%;background: red;height:1px\">
            <ul class=\"side-menu-list\">
                <li class=\"opened\">
                    <span id=\"instr\">
                    \t<a href=\"";
        // line 14
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("representant_livrer_marchandise");
        echo "\" >
\t                        <i class=\"glyphicon glyphicon-inbox\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Ordre de livraison</span>
                        </a>
                    </span>
                </li>
                <li class=\"opened\">
                    <span id=\"prod\">
                    \t<a href=\"";
        // line 22
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("representant_lister_marchandise");
        echo "\" >
\t                        <i class=\"glyphicon glyphicon-th-list\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Lister marchandises</span>
                        </a>
                    </span>
                </li>
                
            </ul>
        </nav>
    ";
        
        $__internal_40760ea84254c4807d6c79d82384ca99e9fc258b9f86e587a67df9593398e075->leave($__internal_40760ea84254c4807d6c79d82384ca99e9fc258b9f86e587a67df9593398e075_prof);

        
        $__internal_175e743db90dbb2ca0cabd3c422d6d7a6d5b3555f6022e31433fa83cd3715731->leave($__internal_175e743db90dbb2ca0cabd3c422d6d7a6d5b3555f6022e31433fa83cd3715731_prof);

    }

    // line 32
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_865499820483ee2deb43881a47244f50d6cf92691a35fe8ef6ebe0fa905d8dd9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_865499820483ee2deb43881a47244f50d6cf92691a35fe8ef6ebe0fa905d8dd9->enter($__internal_865499820483ee2deb43881a47244f50d6cf92691a35fe8ef6ebe0fa905d8dd9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_494bee370b64515df37107c2bfff2336e05ecf99012a7873f94b402244fb6283 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_494bee370b64515df37107c2bfff2336e05ecf99012a7873f94b402244fb6283->enter($__internal_494bee370b64515df37107c2bfff2336e05ecf99012a7873f94b402244fb6283_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        // line 33
        echo "        ";
        $this->displayBlock('debutPage', $context, $blocks);
        // line 42
        echo "        ";
        $this->displayBlock('modules', $context, $blocks);
        // line 47
        echo "    ";
        
        $__internal_494bee370b64515df37107c2bfff2336e05ecf99012a7873f94b402244fb6283->leave($__internal_494bee370b64515df37107c2bfff2336e05ecf99012a7873f94b402244fb6283_prof);

        
        $__internal_865499820483ee2deb43881a47244f50d6cf92691a35fe8ef6ebe0fa905d8dd9->leave($__internal_865499820483ee2deb43881a47244f50d6cf92691a35fe8ef6ebe0fa905d8dd9_prof);

    }

    // line 33
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_58a0a3b6b97bdd57a14778d0fe041ceb43b0b2f9531df07cb48d70ad7fb4617b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_58a0a3b6b97bdd57a14778d0fe041ceb43b0b2f9531df07cb48d70ad7fb4617b->enter($__internal_58a0a3b6b97bdd57a14778d0fe041ceb43b0b2f9531df07cb48d70ad7fb4617b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_e963c497256e34654f0393f71bbaa0e52e3d9c92aef022014c913ff719321329 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e963c497256e34654f0393f71bbaa0e52e3d9c92aef022014c913ff719321329->enter($__internal_e963c497256e34654f0393f71bbaa0e52e3d9c92aef022014c913ff719321329_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 34
        echo "            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-20px;\">
                    <h2 style=\"color: white;margin-top:4%\"> <center>Liste des contrats</center></h2>
                </div>
                <br><br><br><br><br><br>
            </div>

        ";
        
        $__internal_e963c497256e34654f0393f71bbaa0e52e3d9c92aef022014c913ff719321329->leave($__internal_e963c497256e34654f0393f71bbaa0e52e3d9c92aef022014c913ff719321329_prof);

        
        $__internal_58a0a3b6b97bdd57a14778d0fe041ceb43b0b2f9531df07cb48d70ad7fb4617b->leave($__internal_58a0a3b6b97bdd57a14778d0fe041ceb43b0b2f9531df07cb48d70ad7fb4617b_prof);

    }

    // line 42
    public function block_modules($context, array $blocks = array())
    {
        $__internal_85e529386ae08adbcac0f3807ff5fa5831ba0ee2c6b0455d0b2d2b1246569344 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_85e529386ae08adbcac0f3807ff5fa5831ba0ee2c6b0455d0b2d2b1246569344->enter($__internal_85e529386ae08adbcac0f3807ff5fa5831ba0ee2c6b0455d0b2d2b1246569344_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_2ccf36ad9fcd35e3f4b8cb50a6f7badbbc3244d7603ea482db3eae330f01dfbf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2ccf36ad9fcd35e3f4b8cb50a6f7badbbc3244d7603ea482db3eae330f01dfbf->enter($__internal_2ccf36ad9fcd35e3f4b8cb50a6f7badbbc3244d7603ea482db3eae330f01dfbf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        // line 43
        echo "            <div class=\"col-xl-6\" style=\"margin-left: 27%\">
                ";
        // line 44
        $this->loadTemplate("formulaire.html.twig", "::transporteur.html.twig", 44)->display($context);
        // line 45
        echo "            </div>
        ";
        
        $__internal_2ccf36ad9fcd35e3f4b8cb50a6f7badbbc3244d7603ea482db3eae330f01dfbf->leave($__internal_2ccf36ad9fcd35e3f4b8cb50a6f7badbbc3244d7603ea482db3eae330f01dfbf_prof);

        
        $__internal_85e529386ae08adbcac0f3807ff5fa5831ba0ee2c6b0455d0b2d2b1246569344->leave($__internal_85e529386ae08adbcac0f3807ff5fa5831ba0ee2c6b0455d0b2d2b1246569344_prof);

    }

    public function getTemplateName()
    {
        return "::transporteur.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 45,  161 => 44,  158 => 43,  149 => 42,  132 => 34,  123 => 33,  113 => 47,  110 => 42,  107 => 33,  98 => 32,  78 => 22,  67 => 14,  56 => 6,  52 => 4,  43 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

\t{% block menu %}
        <nav class=\"side-menu side-menu-big-icon\" style=\"background: #3299CC;overflow: hidden\">
            <nav class=\"sidebar-header\">
                <img src=\"{{asset('img/man.png')}}\" style=\"margin-left:35%;margin-top:-15%;\"><br><br>
                <h5 style=\"text-align:center\">User Name</h5>
                <h5 style=\"text-align:center\">Profil</h5>
            </nav>
            <hr style=\"margin-left:20%;margin-right: 20%;background: red;height:1px\">
            <ul class=\"side-menu-list\">
                <li class=\"opened\">
                    <span id=\"instr\">
                    \t<a href=\"{{ path('representant_livrer_marchandise') }}\" >
\t                        <i class=\"glyphicon glyphicon-inbox\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Ordre de livraison</span>
                        </a>
                    </span>
                </li>
                <li class=\"opened\">
                    <span id=\"prod\">
                    \t<a href=\"{{ path('representant_lister_marchandise') }}\" >
\t                        <i class=\"glyphicon glyphicon-th-list\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Lister marchandises</span>
                        </a>
                    </span>
                </li>
                
            </ul>
        </nav>
    {% endblock %}
    {% block contenuDuMilieu %}
        {% block debutPage %}
            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-20px;\">
                    <h2 style=\"color: white;margin-top:4%\"> <center>Liste des contrats</center></h2>
                </div>
                <br><br><br><br><br><br>
            </div>

        {% endblock %}
        {% block modules %}
            <div class=\"col-xl-6\" style=\"margin-left: 27%\">
                {% include 'formulaire.html.twig' %}
            </div>
        {% endblock %}
    {% endblock %}", "::transporteur.html.twig", "C:\\wamp\\www\\symfony\\stock\\app/Resources\\views/transporteur.html.twig");
    }
}
