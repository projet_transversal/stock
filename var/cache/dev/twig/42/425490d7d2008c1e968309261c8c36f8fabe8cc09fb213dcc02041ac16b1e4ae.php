<?php

/* StockBundle:Repres:listerMarchandise.html.twig */
class __TwigTemplate_8f43bb8e4221606010c2603590ab49a43520b31e011ee7d7c71007286b6a6383 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::representant.html.twig", "StockBundle:Repres:listerMarchandise.html.twig", 1);
        $this->blocks = array(
            'menu' => array($this, 'block_menu'),
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::representant.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4d806628818dca63773a08650b9f964b3b827092da4f5c734c96020776824f93 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4d806628818dca63773a08650b9f964b3b827092da4f5c734c96020776824f93->enter($__internal_4d806628818dca63773a08650b9f964b3b827092da4f5c734c96020776824f93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Repres:listerMarchandise.html.twig"));

        $__internal_63b181566db1a309a111ab084d8fa2ad028a18dd7a9301f79d5f613ee8092c61 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_63b181566db1a309a111ab084d8fa2ad028a18dd7a9301f79d5f613ee8092c61->enter($__internal_63b181566db1a309a111ab084d8fa2ad028a18dd7a9301f79d5f613ee8092c61_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Repres:listerMarchandise.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4d806628818dca63773a08650b9f964b3b827092da4f5c734c96020776824f93->leave($__internal_4d806628818dca63773a08650b9f964b3b827092da4f5c734c96020776824f93_prof);

        
        $__internal_63b181566db1a309a111ab084d8fa2ad028a18dd7a9301f79d5f613ee8092c61->leave($__internal_63b181566db1a309a111ab084d8fa2ad028a18dd7a9301f79d5f613ee8092c61_prof);

    }

    // line 2
    public function block_menu($context, array $blocks = array())
    {
        $__internal_52cf6250d92bf6d162f6f04de6b8b745a33227bd6a086aa263f0bf7b510d0046 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_52cf6250d92bf6d162f6f04de6b8b745a33227bd6a086aa263f0bf7b510d0046->enter($__internal_52cf6250d92bf6d162f6f04de6b8b745a33227bd6a086aa263f0bf7b510d0046_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_1c77175fe67ddf96c0dc27f59b92ac9093470b5e8d18affc7615223ffa27c74b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c77175fe67ddf96c0dc27f59b92ac9093470b5e8d18affc7615223ffa27c74b->enter($__internal_1c77175fe67ddf96c0dc27f59b92ac9093470b5e8d18affc7615223ffa27c74b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 3
        echo "    ";
        $this->displayParentBlock("menu", $context, $blocks);
        echo "
";
        
        $__internal_1c77175fe67ddf96c0dc27f59b92ac9093470b5e8d18affc7615223ffa27c74b->leave($__internal_1c77175fe67ddf96c0dc27f59b92ac9093470b5e8d18affc7615223ffa27c74b_prof);

        
        $__internal_52cf6250d92bf6d162f6f04de6b8b745a33227bd6a086aa263f0bf7b510d0046->leave($__internal_52cf6250d92bf6d162f6f04de6b8b745a33227bd6a086aa263f0bf7b510d0046_prof);

    }

    // line 5
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_c450af680c4c985f21e1c931db4edca3cd7fb34740f7677fd686f288f9815e9e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c450af680c4c985f21e1c931db4edca3cd7fb34740f7677fd686f288f9815e9e->enter($__internal_c450af680c4c985f21e1c931db4edca3cd7fb34740f7677fd686f288f9815e9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_0adf8a511eccc2f71d1207635d1761203b387f1e8d54d6e516787373859690ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0adf8a511eccc2f71d1207635d1761203b387f1e8d54d6e516787373859690ff->enter($__internal_0adf8a511eccc2f71d1207635d1761203b387f1e8d54d6e516787373859690ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        echo "<!--le block du milieu de la page-->
    ";
        // line 6
        $this->displayBlock('debutPage', $context, $blocks);
        // line 14
        echo "    ";
        $this->displayBlock('modules', $context, $blocks);
        
        $__internal_0adf8a511eccc2f71d1207635d1761203b387f1e8d54d6e516787373859690ff->leave($__internal_0adf8a511eccc2f71d1207635d1761203b387f1e8d54d6e516787373859690ff_prof);

        
        $__internal_c450af680c4c985f21e1c931db4edca3cd7fb34740f7677fd686f288f9815e9e->leave($__internal_c450af680c4c985f21e1c931db4edca3cd7fb34740f7677fd686f288f9815e9e_prof);

    }

    // line 6
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_06e9e079579a18054ebb65fdc4c2590b26291adf22a33591635b942a7366bc30 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_06e9e079579a18054ebb65fdc4c2590b26291adf22a33591635b942a7366bc30->enter($__internal_06e9e079579a18054ebb65fdc4c2590b26291adf22a33591635b942a7366bc30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_dea8c1b0d4967d8622da323205d55037b88f8c66b8c4cea309d18b2ed55ef8ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dea8c1b0d4967d8622da323205d55037b88f8c66b8c4cea309d18b2ed55ef8ff->enter($__internal_dea8c1b0d4967d8622da323205d55037b88f8c66b8c4cea309d18b2ed55ef8ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 7
        echo "        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-26px;\">
                <h2 style=\"color: white;margin-top:4%\"> <center>Liste des marchandises</center></h2>
            </div>
            <br><br>
        </div>
    ";
        
        $__internal_dea8c1b0d4967d8622da323205d55037b88f8c66b8c4cea309d18b2ed55ef8ff->leave($__internal_dea8c1b0d4967d8622da323205d55037b88f8c66b8c4cea309d18b2ed55ef8ff_prof);

        
        $__internal_06e9e079579a18054ebb65fdc4c2590b26291adf22a33591635b942a7366bc30->leave($__internal_06e9e079579a18054ebb65fdc4c2590b26291adf22a33591635b942a7366bc30_prof);

    }

    // line 14
    public function block_modules($context, array $blocks = array())
    {
        $__internal_d31f5333775079f740d85839dc7dbcf793522d342411dab8ee0f385294447638 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d31f5333775079f740d85839dc7dbcf793522d342411dab8ee0f385294447638->enter($__internal_d31f5333775079f740d85839dc7dbcf793522d342411dab8ee0f385294447638_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_ea3492cfdcf04da5da262e4c4052567861f3d138084ef5f15d046502422228e4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ea3492cfdcf04da5da262e4c4052567861f3d138084ef5f15d046502422228e4->enter($__internal_ea3492cfdcf04da5da262e4c4052567861f3d138084ef5f15d046502422228e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        echo "<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"\">
                <table   class=\"display table table-striped table-bordered\" cellspacing=\"0\" width=\"100%\" id=\"table\" style=\"margin-left:-35%;margin-top:-11%\">
                    <thead>
                    <tr class=\"jumbotron\">
                        <th id=\"libelle\" >Libelle</th>
                        <th id=\"quantite\" >Quantite</th>
                        ";
        // line 23
        echo "                    </tr>
                    </thead>
                    <tbody>
                    ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["liste"] ?? $this->getContext($context, "liste")));
        foreach ($context['_seq'] as $context["_key"] => $context["l"]) {
            // line 27
            echo "                        <tr>
                            <td>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["l"], "marchandise", array()), "nom", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "quantite", array()), "html", null, true);
            echo "</td>
                            ";
            // line 31
            echo "                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['l'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "                    </tbody>
                </table>
            </div><!--.container-fluid-->
        </div><!--.page-content-->
    ";
        
        $__internal_ea3492cfdcf04da5da262e4c4052567861f3d138084ef5f15d046502422228e4->leave($__internal_ea3492cfdcf04da5da262e4c4052567861f3d138084ef5f15d046502422228e4_prof);

        
        $__internal_d31f5333775079f740d85839dc7dbcf793522d342411dab8ee0f385294447638->leave($__internal_d31f5333775079f740d85839dc7dbcf793522d342411dab8ee0f385294447638_prof);

    }

    // line 39
    public function block_script($context, array $blocks = array())
    {
        $__internal_76e2b17422c4d6285646ef54c88de047bb91be6a082c65547bb3e21b31568a69 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_76e2b17422c4d6285646ef54c88de047bb91be6a082c65547bb3e21b31568a69->enter($__internal_76e2b17422c4d6285646ef54c88de047bb91be6a082c65547bb3e21b31568a69_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        $__internal_c6474311d663f835c7dcb46f07be31dd9f139c52a663dd5fd35171b1ac49c783 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c6474311d663f835c7dcb46f07be31dd9f139c52a663dd5fd35171b1ac49c783->enter($__internal_c6474311d663f835c7dcb46f07be31dd9f139c52a663dd5fd35171b1ac49c783_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        // line 40
        echo "    ";
        $this->displayParentBlock("script", $context, $blocks);
        echo "
    <script src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/jquery.tablednd.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table-reorder-rows-init.js"), "html", null, true);
        echo "\"></script>
    ";
        
        $__internal_c6474311d663f835c7dcb46f07be31dd9f139c52a663dd5fd35171b1ac49c783->leave($__internal_c6474311d663f835c7dcb46f07be31dd9f139c52a663dd5fd35171b1ac49c783_prof);

        
        $__internal_76e2b17422c4d6285646ef54c88de047bb91be6a082c65547bb3e21b31568a69->leave($__internal_76e2b17422c4d6285646ef54c88de047bb91be6a082c65547bb3e21b31568a69_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Repres:listerMarchandise.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  200 => 44,  196 => 43,  192 => 42,  188 => 41,  183 => 40,  174 => 39,  160 => 33,  153 => 31,  149 => 29,  145 => 28,  142 => 27,  138 => 26,  133 => 23,  115 => 14,  99 => 7,  90 => 6,  79 => 14,  77 => 6,  66 => 5,  53 => 3,  44 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::representant.html.twig' %}
{% block menu %}
    {{ parent() }}
{% endblock %}
{% block contenuDuMilieu %}<!--le block du milieu de la page-->
    {% block debutPage %}
        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-26px;\">
                <h2 style=\"color: white;margin-top:4%\"> <center>Liste des marchandises</center></h2>
            </div>
            <br><br>
        </div>
    {% endblock %}
    {% block modules %}<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"\">
                <table   class=\"display table table-striped table-bordered\" cellspacing=\"0\" width=\"100%\" id=\"table\" style=\"margin-left:-35%;margin-top:-11%\">
                    <thead>
                    <tr class=\"jumbotron\">
                        <th id=\"libelle\" >Libelle</th>
                        <th id=\"quantite\" >Quantite</th>
                        {#<th id=\"unite\" >Unite</th>#}
                    </tr>
                    </thead>
                    <tbody>
                    {% for l in liste %}
                        <tr>
                            <td>{{ l.marchandise.nom }}</td>
                            <td>{{ l.quantite }}</td>
                            {#<td>{{ l.unite }}</td>#}
                        </tr>
                    {% endfor %}
                    </tbody>
                </table>
            </div><!--.container-fluid-->
        </div><!--.page-content-->
    {% endblock %}
{% endblock %}
{% block script %}
    {{ parent() }}
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/jquery.tablednd.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table-reorder-rows-init.js') }}\"></script>
    {#<script type=\"text/javascript\">{{ liste(liste) }}</script>#}
{% endblock %}", "StockBundle:Repres:listerMarchandise.html.twig", "C:\\wamp64\\www\\stock\\src\\StockBundle/Resources/views/Repres/listerMarchandise.html.twig");
    }
}
