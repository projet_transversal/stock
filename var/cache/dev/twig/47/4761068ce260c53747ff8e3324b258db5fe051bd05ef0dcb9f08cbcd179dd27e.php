<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_56f3cd2195f563e29e7460459819da8437d3f0b099165782d08850e065550ac3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_445bcf3ca09aca9e87966e5943e457cf693e8aa5d402093d830752236f80a27e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_445bcf3ca09aca9e87966e5943e457cf693e8aa5d402093d830752236f80a27e->enter($__internal_445bcf3ca09aca9e87966e5943e457cf693e8aa5d402093d830752236f80a27e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_8861cf6b1f946fff567b4f3cb62fd93fcce8949484aeac35f291c3697aaa8f87 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8861cf6b1f946fff567b4f3cb62fd93fcce8949484aeac35f291c3697aaa8f87->enter($__internal_8861cf6b1f946fff567b4f3cb62fd93fcce8949484aeac35f291c3697aaa8f87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_445bcf3ca09aca9e87966e5943e457cf693e8aa5d402093d830752236f80a27e->leave($__internal_445bcf3ca09aca9e87966e5943e457cf693e8aa5d402093d830752236f80a27e_prof);

        
        $__internal_8861cf6b1f946fff567b4f3cb62fd93fcce8949484aeac35f291c3697aaa8f87->leave($__internal_8861cf6b1f946fff567b4f3cb62fd93fcce8949484aeac35f291c3697aaa8f87_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_e5931f247d013ecc27a11b8153867fd67446f7d3d6cb45248e07e16b554e6e9a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e5931f247d013ecc27a11b8153867fd67446f7d3d6cb45248e07e16b554e6e9a->enter($__internal_e5931f247d013ecc27a11b8153867fd67446f7d3d6cb45248e07e16b554e6e9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_6b89fd697781b35e56261f6eb10831cd8bdba0d0f59ecc8b6f2d0deb3f7e4e97 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6b89fd697781b35e56261f6eb10831cd8bdba0d0f59ecc8b6f2d0deb3f7e4e97->enter($__internal_6b89fd697781b35e56261f6eb10831cd8bdba0d0f59ecc8b6f2d0deb3f7e4e97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_6b89fd697781b35e56261f6eb10831cd8bdba0d0f59ecc8b6f2d0deb3f7e4e97->leave($__internal_6b89fd697781b35e56261f6eb10831cd8bdba0d0f59ecc8b6f2d0deb3f7e4e97_prof);

        
        $__internal_e5931f247d013ecc27a11b8153867fd67446f7d3d6cb45248e07e16b554e6e9a->leave($__internal_e5931f247d013ecc27a11b8153867fd67446f7d3d6cb45248e07e16b554e6e9a_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_018f88369157fc71c70a2b4a657d668b8e2584e2def754629bd8402ca30c4e0c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_018f88369157fc71c70a2b4a657d668b8e2584e2def754629bd8402ca30c4e0c->enter($__internal_018f88369157fc71c70a2b4a657d668b8e2584e2def754629bd8402ca30c4e0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_960b854df4a72c79a0ae5fe8f62561bdbdf17b2d2753c08da2e6cca358944915 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_960b854df4a72c79a0ae5fe8f62561bdbdf17b2d2753c08da2e6cca358944915->enter($__internal_960b854df4a72c79a0ae5fe8f62561bdbdf17b2d2753c08da2e6cca358944915_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_960b854df4a72c79a0ae5fe8f62561bdbdf17b2d2753c08da2e6cca358944915->leave($__internal_960b854df4a72c79a0ae5fe8f62561bdbdf17b2d2753c08da2e6cca358944915_prof);

        
        $__internal_018f88369157fc71c70a2b4a657d668b8e2584e2def754629bd8402ca30c4e0c->leave($__internal_018f88369157fc71c70a2b4a657d668b8e2584e2def754629bd8402ca30c4e0c_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_a7e5a222a770ff9cb68bd2aa43ee20950b024a3356bee7d1c697a78bcf655005 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a7e5a222a770ff9cb68bd2aa43ee20950b024a3356bee7d1c697a78bcf655005->enter($__internal_a7e5a222a770ff9cb68bd2aa43ee20950b024a3356bee7d1c697a78bcf655005_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_cc4bb72fe20247d3c6472e959b6567cfcf7145adf78fecf23c59d90953c9af13 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cc4bb72fe20247d3c6472e959b6567cfcf7145adf78fecf23c59d90953c9af13->enter($__internal_cc4bb72fe20247d3c6472e959b6567cfcf7145adf78fecf23c59d90953c9af13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_cc4bb72fe20247d3c6472e959b6567cfcf7145adf78fecf23c59d90953c9af13->leave($__internal_cc4bb72fe20247d3c6472e959b6567cfcf7145adf78fecf23c59d90953c9af13_prof);

        
        $__internal_a7e5a222a770ff9cb68bd2aa43ee20950b024a3356bee7d1c697a78bcf655005->leave($__internal_a7e5a222a770ff9cb68bd2aa43ee20950b024a3356bee7d1c697a78bcf655005_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "C:\\wamp\\www\\symfony\\stock\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\exception.html.twig");
    }
}
