<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_b11224d64d4f4a49035f13d7ebd69d7bdb1657e3952df26f313f0fa60b013e58 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_978d4f4c938badfd1f5500009b5ea5b3c41ceccf604c8a15bba2bda5df6b1174 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_978d4f4c938badfd1f5500009b5ea5b3c41ceccf604c8a15bba2bda5df6b1174->enter($__internal_978d4f4c938badfd1f5500009b5ea5b3c41ceccf604c8a15bba2bda5df6b1174_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_1b8934fb38aa3294c6db2514fca955f6b4c548389981df71ca94f7e3a3b8686b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1b8934fb38aa3294c6db2514fca955f6b4c548389981df71ca94f7e3a3b8686b->enter($__internal_1b8934fb38aa3294c6db2514fca955f6b4c548389981df71ca94f7e3a3b8686b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_978d4f4c938badfd1f5500009b5ea5b3c41ceccf604c8a15bba2bda5df6b1174->leave($__internal_978d4f4c938badfd1f5500009b5ea5b3c41ceccf604c8a15bba2bda5df6b1174_prof);

        
        $__internal_1b8934fb38aa3294c6db2514fca955f6b4c548389981df71ca94f7e3a3b8686b->leave($__internal_1b8934fb38aa3294c6db2514fca955f6b4c548389981df71ca94f7e3a3b8686b_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_2654f6762cbe7f286fdaa378eb6ab0f92990682f064ac5a52b9568c486bbd917 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2654f6762cbe7f286fdaa378eb6ab0f92990682f064ac5a52b9568c486bbd917->enter($__internal_2654f6762cbe7f286fdaa378eb6ab0f92990682f064ac5a52b9568c486bbd917_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_77b1a61e013defa8fddb3db5d1cac6914097acecd64f8e1aa48fec05eecbfac9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_77b1a61e013defa8fddb3db5d1cac6914097acecd64f8e1aa48fec05eecbfac9->enter($__internal_77b1a61e013defa8fddb3db5d1cac6914097acecd64f8e1aa48fec05eecbfac9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_77b1a61e013defa8fddb3db5d1cac6914097acecd64f8e1aa48fec05eecbfac9->leave($__internal_77b1a61e013defa8fddb3db5d1cac6914097acecd64f8e1aa48fec05eecbfac9_prof);

        
        $__internal_2654f6762cbe7f286fdaa378eb6ab0f92990682f064ac5a52b9568c486bbd917->leave($__internal_2654f6762cbe7f286fdaa378eb6ab0f92990682f064ac5a52b9568c486bbd917_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_ffb41a8adaa64b12d85a12f68f47b804f3f7a788165f23939298a2becf9a1c8c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ffb41a8adaa64b12d85a12f68f47b804f3f7a788165f23939298a2becf9a1c8c->enter($__internal_ffb41a8adaa64b12d85a12f68f47b804f3f7a788165f23939298a2becf9a1c8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_6924562436df7eef205d0a29e174a91fe541a5760c7a78a478b18e0fdb676dda = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6924562436df7eef205d0a29e174a91fe541a5760c7a78a478b18e0fdb676dda->enter($__internal_6924562436df7eef205d0a29e174a91fe541a5760c7a78a478b18e0fdb676dda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_6924562436df7eef205d0a29e174a91fe541a5760c7a78a478b18e0fdb676dda->leave($__internal_6924562436df7eef205d0a29e174a91fe541a5760c7a78a478b18e0fdb676dda_prof);

        
        $__internal_ffb41a8adaa64b12d85a12f68f47b804f3f7a788165f23939298a2becf9a1c8c->leave($__internal_ffb41a8adaa64b12d85a12f68f47b804f3f7a788165f23939298a2becf9a1c8c_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_8360d725ca4b11e911e1571d58ed5478b3edb26fafa8e4d2be0e7f9513b4233d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8360d725ca4b11e911e1571d58ed5478b3edb26fafa8e4d2be0e7f9513b4233d->enter($__internal_8360d725ca4b11e911e1571d58ed5478b3edb26fafa8e4d2be0e7f9513b4233d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_9a4e7d2bcb2aaa004c2e49c187e492828e411bcbe77fc7b8cc502fefc0e3b554 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9a4e7d2bcb2aaa004c2e49c187e492828e411bcbe77fc7b8cc502fefc0e3b554->enter($__internal_9a4e7d2bcb2aaa004c2e49c187e492828e411bcbe77fc7b8cc502fefc0e3b554_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_9a4e7d2bcb2aaa004c2e49c187e492828e411bcbe77fc7b8cc502fefc0e3b554->leave($__internal_9a4e7d2bcb2aaa004c2e49c187e492828e411bcbe77fc7b8cc502fefc0e3b554_prof);

        
        $__internal_8360d725ca4b11e911e1571d58ed5478b3edb26fafa8e4d2be0e7f9513b4233d->leave($__internal_8360d725ca4b11e911e1571d58ed5478b3edb26fafa8e4d2be0e7f9513b4233d_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "C:\\wamp\\www\\symfony\\stock\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
