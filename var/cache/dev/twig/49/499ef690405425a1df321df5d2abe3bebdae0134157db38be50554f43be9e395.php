<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_88216b2742e1132d2d5e9e4b1ea54238b82ee165cf3c36bc34e95e9a49168490 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_80a96d5f29d86b690723a5e7f0a6ce4f620ba30ee838a262edb3fbf10821f67b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_80a96d5f29d86b690723a5e7f0a6ce4f620ba30ee838a262edb3fbf10821f67b->enter($__internal_80a96d5f29d86b690723a5e7f0a6ce4f620ba30ee838a262edb3fbf10821f67b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_45161ccc57128773a4d0de0ed5ec73138db6e7ae4d7a1115cd6e45784a720da7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_45161ccc57128773a4d0de0ed5ec73138db6e7ae4d7a1115cd6e45784a720da7->enter($__internal_45161ccc57128773a4d0de0ed5ec73138db6e7ae4d7a1115cd6e45784a720da7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_80a96d5f29d86b690723a5e7f0a6ce4f620ba30ee838a262edb3fbf10821f67b->leave($__internal_80a96d5f29d86b690723a5e7f0a6ce4f620ba30ee838a262edb3fbf10821f67b_prof);

        
        $__internal_45161ccc57128773a4d0de0ed5ec73138db6e7ae4d7a1115cd6e45784a720da7->leave($__internal_45161ccc57128773a4d0de0ed5ec73138db6e7ae4d7a1115cd6e45784a720da7_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_cac7db27752af69d6ab76622adb1840c3ebc22ffaceed87f0cff5468acb05bdf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cac7db27752af69d6ab76622adb1840c3ebc22ffaceed87f0cff5468acb05bdf->enter($__internal_cac7db27752af69d6ab76622adb1840c3ebc22ffaceed87f0cff5468acb05bdf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_e682d6834d61cfdf3e5df3faadcee38ae38676ef2b7b01ca22ef46d76c29aba7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e682d6834d61cfdf3e5df3faadcee38ae38676ef2b7b01ca22ef46d76c29aba7->enter($__internal_e682d6834d61cfdf3e5df3faadcee38ae38676ef2b7b01ca22ef46d76c29aba7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_e682d6834d61cfdf3e5df3faadcee38ae38676ef2b7b01ca22ef46d76c29aba7->leave($__internal_e682d6834d61cfdf3e5df3faadcee38ae38676ef2b7b01ca22ef46d76c29aba7_prof);

        
        $__internal_cac7db27752af69d6ab76622adb1840c3ebc22ffaceed87f0cff5468acb05bdf->leave($__internal_cac7db27752af69d6ab76622adb1840c3ebc22ffaceed87f0cff5468acb05bdf_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_5d5827dc44e7453ac7f2ba0159c2279ea6709cb64d555e9c6221478802ef81e5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d5827dc44e7453ac7f2ba0159c2279ea6709cb64d555e9c6221478802ef81e5->enter($__internal_5d5827dc44e7453ac7f2ba0159c2279ea6709cb64d555e9c6221478802ef81e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_8de0f4fc7881b1cc5a519d1677f912aab3df35b38505782de42d8664730d9fd7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8de0f4fc7881b1cc5a519d1677f912aab3df35b38505782de42d8664730d9fd7->enter($__internal_8de0f4fc7881b1cc5a519d1677f912aab3df35b38505782de42d8664730d9fd7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_8de0f4fc7881b1cc5a519d1677f912aab3df35b38505782de42d8664730d9fd7->leave($__internal_8de0f4fc7881b1cc5a519d1677f912aab3df35b38505782de42d8664730d9fd7_prof);

        
        $__internal_5d5827dc44e7453ac7f2ba0159c2279ea6709cb64d555e9c6221478802ef81e5->leave($__internal_5d5827dc44e7453ac7f2ba0159c2279ea6709cb64d555e9c6221478802ef81e5_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_80f746c67c661c01f3a72393c525e25d35819325dd85e50293453b19cd82f1e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_80f746c67c661c01f3a72393c525e25d35819325dd85e50293453b19cd82f1e4->enter($__internal_80f746c67c661c01f3a72393c525e25d35819325dd85e50293453b19cd82f1e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_b4b041982f36c4d502cc8dae4c2031fab01b7a19bc642dd7496479c3dd779710 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b4b041982f36c4d502cc8dae4c2031fab01b7a19bc642dd7496479c3dd779710->enter($__internal_b4b041982f36c4d502cc8dae4c2031fab01b7a19bc642dd7496479c3dd779710_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_b4b041982f36c4d502cc8dae4c2031fab01b7a19bc642dd7496479c3dd779710->leave($__internal_b4b041982f36c4d502cc8dae4c2031fab01b7a19bc642dd7496479c3dd779710_prof);

        
        $__internal_80f746c67c661c01f3a72393c525e25d35819325dd85e50293453b19cd82f1e4->leave($__internal_80f746c67c661c01f3a72393c525e25d35819325dd85e50293453b19cd82f1e4_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "C:\\wamp64\\www\\stock\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\exception.html.twig");
    }
}
