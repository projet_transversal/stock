<?php

/* default/homepage.html.twig */
class __TwigTemplate_7ac7da08d5a3f00c686a695849a4a41ffb2267a3947df0c09e6bcdf1a081152c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/homepage.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'header' => array($this, 'block_header'),
            'footer' => array($this, 'block_footer'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_35f5e9de84a2a9e5aa018954ded7d63b3a7245142d1f96a2ea702b1a57d266c4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_35f5e9de84a2a9e5aa018954ded7d63b3a7245142d1f96a2ea702b1a57d266c4->enter($__internal_35f5e9de84a2a9e5aa018954ded7d63b3a7245142d1f96a2ea702b1a57d266c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/homepage.html.twig"));

        $__internal_048758507cbaac36a64f3b17a759b4aea8cbca8c62afb0d49c10fa5cfd34dfc3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_048758507cbaac36a64f3b17a759b4aea8cbca8c62afb0d49c10fa5cfd34dfc3->enter($__internal_048758507cbaac36a64f3b17a759b4aea8cbca8c62afb0d49c10fa5cfd34dfc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/homepage.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_35f5e9de84a2a9e5aa018954ded7d63b3a7245142d1f96a2ea702b1a57d266c4->leave($__internal_35f5e9de84a2a9e5aa018954ded7d63b3a7245142d1f96a2ea702b1a57d266c4_prof);

        
        $__internal_048758507cbaac36a64f3b17a759b4aea8cbca8c62afb0d49c10fa5cfd34dfc3->leave($__internal_048758507cbaac36a64f3b17a759b4aea8cbca8c62afb0d49c10fa5cfd34dfc3_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_f0dc1f6a7dd352f05ac8196d19f24c084a7764ecde76abe83268e21d275ddb14 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f0dc1f6a7dd352f05ac8196d19f24c084a7764ecde76abe83268e21d275ddb14->enter($__internal_f0dc1f6a7dd352f05ac8196d19f24c084a7764ecde76abe83268e21d275ddb14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_497e7c746de8b3d5d21d766dc09ba9f769f86826e9510faadb3e413af283a43c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_497e7c746de8b3d5d21d766dc09ba9f769f86826e9510faadb3e413af283a43c->enter($__internal_497e7c746de8b3d5d21d766dc09ba9f769f86826e9510faadb3e413af283a43c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "homepage";
        
        $__internal_497e7c746de8b3d5d21d766dc09ba9f769f86826e9510faadb3e413af283a43c->leave($__internal_497e7c746de8b3d5d21d766dc09ba9f769f86826e9510faadb3e413af283a43c_prof);

        
        $__internal_f0dc1f6a7dd352f05ac8196d19f24c084a7764ecde76abe83268e21d275ddb14->leave($__internal_f0dc1f6a7dd352f05ac8196d19f24c084a7764ecde76abe83268e21d275ddb14_prof);

    }

    // line 9
    public function block_header($context, array $blocks = array())
    {
        $__internal_d2eb81bbb47421953401df17618b26709cc82cf44cf51aeb5396a4f4955f23bb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d2eb81bbb47421953401df17618b26709cc82cf44cf51aeb5396a4f4955f23bb->enter($__internal_d2eb81bbb47421953401df17618b26709cc82cf44cf51aeb5396a4f4955f23bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_98aec4cf281f64827e1ac0d5d0223d792292231901fd223b02de1c3a544e64d8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_98aec4cf281f64827e1ac0d5d0223d792292231901fd223b02de1c3a544e64d8->enter($__internal_98aec4cf281f64827e1ac0d5d0223d792292231901fd223b02de1c3a544e64d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        
        $__internal_98aec4cf281f64827e1ac0d5d0223d792292231901fd223b02de1c3a544e64d8->leave($__internal_98aec4cf281f64827e1ac0d5d0223d792292231901fd223b02de1c3a544e64d8_prof);

        
        $__internal_d2eb81bbb47421953401df17618b26709cc82cf44cf51aeb5396a4f4955f23bb->leave($__internal_d2eb81bbb47421953401df17618b26709cc82cf44cf51aeb5396a4f4955f23bb_prof);

    }

    // line 10
    public function block_footer($context, array $blocks = array())
    {
        $__internal_c49ffb2a66326c51956f445a42e606a60530c8170bd9fa9e541eb9ada1a42069 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c49ffb2a66326c51956f445a42e606a60530c8170bd9fa9e541eb9ada1a42069->enter($__internal_c49ffb2a66326c51956f445a42e606a60530c8170bd9fa9e541eb9ada1a42069_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        $__internal_7923dd2e48e787759e09238bb8b5adeb3fd534b570d31c861305a3f5063b895d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7923dd2e48e787759e09238bb8b5adeb3fd534b570d31c861305a3f5063b895d->enter($__internal_7923dd2e48e787759e09238bb8b5adeb3fd534b570d31c861305a3f5063b895d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        
        $__internal_7923dd2e48e787759e09238bb8b5adeb3fd534b570d31c861305a3f5063b895d->leave($__internal_7923dd2e48e787759e09238bb8b5adeb3fd534b570d31c861305a3f5063b895d_prof);

        
        $__internal_c49ffb2a66326c51956f445a42e606a60530c8170bd9fa9e541eb9ada1a42069->leave($__internal_c49ffb2a66326c51956f445a42e606a60530c8170bd9fa9e541eb9ada1a42069_prof);

    }

    // line 12
    public function block_body($context, array $blocks = array())
    {
        $__internal_168298701d2cb3bb242479fb5ce428bf6c5041d492507df8762625982e5a4037 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_168298701d2cb3bb242479fb5ce428bf6c5041d492507df8762625982e5a4037->enter($__internal_168298701d2cb3bb242479fb5ce428bf6c5041d492507df8762625982e5a4037_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_2fef2bee1b473649a74dcbaf18b9c256fe4f4ad6d4df67638e2fe614b2320cd1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2fef2bee1b473649a74dcbaf18b9c256fe4f4ad6d4df67638e2fe614b2320cd1->enter($__internal_2fef2bee1b473649a74dcbaf18b9c256fe4f4ad6d4df67638e2fe614b2320cd1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 13
        echo "    <div class=\"page-header\">
        <h1>";
        // line 14
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("title.homepage");
        echo "</h1>
    </div>

    <div class=\"row\">
        <div class=\"col-sm-6\">
            <div class=\"jumbotron\">
                <p>
                    ";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("help.browse_app");
        echo "
                </p>
                <p>
                    <a class=\"btn btn-primary btn-lg\" href=\"";
        // line 24
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_index");
        echo "\">
                        <i class=\"fa fa-users\" aria-hidden=\"true\"></i> ";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("action.browse_app"), "html", null, true);
        echo "
                    </a>
                </p>
            </div>
        </div>

        <div class=\"col-sm-6\">
            <div class=\"jumbotron\">
                <p>
                    ";
        // line 34
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("help.browse_admin");
        echo "
                </p>
                <p>
                    <a class=\"btn btn-primary btn-lg\" href=\"";
        // line 37
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_index");
        echo "\">
                        <i class=\"fa fa-lock\" aria-hidden=\"true\"></i> ";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("action.browse_admin"), "html", null, true);
        echo "
                    </a>
                </p>
            </div>
        </div>
    </div>
";
        
        $__internal_2fef2bee1b473649a74dcbaf18b9c256fe4f4ad6d4df67638e2fe614b2320cd1->leave($__internal_2fef2bee1b473649a74dcbaf18b9c256fe4f4ad6d4df67638e2fe614b2320cd1_prof);

        
        $__internal_168298701d2cb3bb242479fb5ce428bf6c5041d492507df8762625982e5a4037->leave($__internal_168298701d2cb3bb242479fb5ce428bf6c5041d492507df8762625982e5a4037_prof);

    }

    public function getTemplateName()
    {
        return "default/homepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 38,  145 => 37,  139 => 34,  127 => 25,  123 => 24,  117 => 21,  107 => 14,  104 => 13,  95 => 12,  78 => 10,  61 => 9,  43 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body_id 'homepage' %}

{#
    the homepage is a special page which displays neither a header nor a footer.
    this is done with the 'trick' of defining empty Twig blocks without any content
#}
{% block header %}{% endblock %}
{% block footer %}{% endblock %}

{% block body %}
    <div class=\"page-header\">
        <h1>{{ 'title.homepage'|trans|raw }}</h1>
    </div>

    <div class=\"row\">
        <div class=\"col-sm-6\">
            <div class=\"jumbotron\">
                <p>
                    {{ 'help.browse_app'|trans|raw }}
                </p>
                <p>
                    <a class=\"btn btn-primary btn-lg\" href=\"{{ path('blog_index') }}\">
                        <i class=\"fa fa-users\" aria-hidden=\"true\"></i> {{ 'action.browse_app'|trans }}
                    </a>
                </p>
            </div>
        </div>

        <div class=\"col-sm-6\">
            <div class=\"jumbotron\">
                <p>
                    {{ 'help.browse_admin'|trans|raw }}
                </p>
                <p>
                    <a class=\"btn btn-primary btn-lg\" href=\"{{ path('admin_index') }}\">
                        <i class=\"fa fa-lock\" aria-hidden=\"true\"></i> {{ 'action.browse_admin'|trans }}
                    </a>
                </p>
            </div>
        </div>
    </div>
{% endblock %}
", "default/homepage.html.twig", "C:\\wamp\\www\\symfony\\stock\\app\\Resources\\views\\default\\homepage.html.twig");
    }
}
