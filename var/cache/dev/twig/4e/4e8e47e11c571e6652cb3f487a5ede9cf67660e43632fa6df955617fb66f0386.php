<?php

/* formulaire.html.twig */
class __TwigTemplate_cdc0995a83e0b5ff7f583dd5b456789bad44357721c7ee3e40b69aa0af4f2bf5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1c698e56c19bc10d3ef4659cc7980cab3322e05ce3d4c7a51c95f37ab3ab5c13 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1c698e56c19bc10d3ef4659cc7980cab3322e05ce3d4c7a51c95f37ab3ab5c13->enter($__internal_1c698e56c19bc10d3ef4659cc7980cab3322e05ce3d4c7a51c95f37ab3ab5c13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "formulaire.html.twig"));

        $__internal_57e9987e4947d5c088c9ca3989a3370862ab0942fbb90f5325227e0934a5f2b7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_57e9987e4947d5c088c9ca3989a3370862ab0942fbb90f5325227e0934a5f2b7->enter($__internal_57e9987e4947d5c088c9ca3989a3370862ab0942fbb90f5325227e0934a5f2b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "formulaire.html.twig"));

        // line 1
        echo "<!Doctype>

<html>
    <head>
        ";
        // line 6
        echo "

        ";
        // line 9
        echo "            ";
        // line 10
        echo "            ";
        // line 11
        echo "                ";
        // line 12
        echo "                ";
        // line 13
        echo "            ";
        // line 14
        echo "        ";
        // line 15
        echo "    </head>
    <body>
        <form method=\"post\" class=\"form-horizontal\" >
            ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
            ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["form"] ?? $this->getContext($context, "form")));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 20
            echo "                ";
            if (($context["item"] == $this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "_token", array()))) {
                // line 21
                echo "                ";
            } else {
                // line 22
                echo "                    <div class=\"form-group\">
                        ";
                // line 23
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["item"], 'label');
                echo "
                        <div class=\"controls\" style=\"border-bottom: 1px  solid  silver;\">
                            ";
                // line 25
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["item"], 'widget');
                echo "
                        </div>
                    </div>
                ";
            }
            // line 29
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "            ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'rest');
        echo "
            <div class=\"controls\" align=\"center\">
                <input type=\"reset\" class=\"btn btn-danger\" value=\"Annuler\"/>
                <input type=\"submit\" class=\"btn btn-success\" value=\"Ajouter\"/>
            </div>
            ";
        // line 35
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
        </form>
    </body>
</html>";
        
        $__internal_1c698e56c19bc10d3ef4659cc7980cab3322e05ce3d4c7a51c95f37ab3ab5c13->leave($__internal_1c698e56c19bc10d3ef4659cc7980cab3322e05ce3d4c7a51c95f37ab3ab5c13_prof);

        
        $__internal_57e9987e4947d5c088c9ca3989a3370862ab0942fbb90f5325227e0934a5f2b7->leave($__internal_57e9987e4947d5c088c9ca3989a3370862ab0942fbb90f5325227e0934a5f2b7_prof);

    }

    public function getTemplateName()
    {
        return "formulaire.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 35,  87 => 30,  81 => 29,  74 => 25,  69 => 23,  66 => 22,  63 => 21,  60 => 20,  56 => 19,  52 => 18,  47 => 15,  45 => 14,  43 => 13,  41 => 12,  39 => 11,  37 => 10,  35 => 9,  31 => 6,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!Doctype>

<html>
    <head>
        {#{% form_theme form _self %}#}


        {#{% block form_widget_simple %}#}
            {#{{ dump(form.vars.value) }}#}
            {#{% spaceless %}#}
                {#{% set type = type|default('text') %}#}
                {#<input type=\"{{ type }}\" {{ block('widget_attributes') }}/>#}
            {#{% endspaceless %}#}
        {#{% endblock form_widget_simple %}#}
    </head>
    <body>
        <form method=\"post\" class=\"form-horizontal\" >
            {{ form_start(form) }}
            {% for item in form %}
                {% if item == form._token %}
                {% else %}
                    <div class=\"form-group\">
                        {{ form_label(item) }}
                        <div class=\"controls\" style=\"border-bottom: 1px  solid  silver;\">
                            {{form_widget(item) }}
                        </div>
                    </div>
                {% endif %}
            {% endfor %}
            {{ form_rest(form) }}
            <div class=\"controls\" align=\"center\">
                <input type=\"reset\" class=\"btn btn-danger\" value=\"Annuler\"/>
                <input type=\"submit\" class=\"btn btn-success\" value=\"Ajouter\"/>
            </div>
            {{ form_end(form) }}
        </form>
    </body>
</html>", "formulaire.html.twig", "C:\\wamp64\\www\\stock\\app\\Resources\\views\\formulaire.html.twig");
    }
}
