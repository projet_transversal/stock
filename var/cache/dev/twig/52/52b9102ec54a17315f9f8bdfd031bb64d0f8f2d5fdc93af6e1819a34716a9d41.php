<?php

/* form/layout.html.twig */
class __TwigTemplate_eba796cb3e6972d26caae6f23d292096ad8ca5a1ad146dd8b8b821f59bcdb0ad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("bootstrap_3_layout.html.twig", "form/layout.html.twig", 1);
        $this->blocks = array(
            'form_errors' => array($this, 'block_form_errors'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "bootstrap_3_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c649089de8ef2c6f60fcc91f853312c082b2fee1d0382250ab6ecdc7035a66d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c649089de8ef2c6f60fcc91f853312c082b2fee1d0382250ab6ecdc7035a66d4->enter($__internal_c649089de8ef2c6f60fcc91f853312c082b2fee1d0382250ab6ecdc7035a66d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/layout.html.twig"));

        $__internal_c9d481f10bae3cb87814cf657a3ab55c7c30bb7ad4c4bc5c11a1c24cbe33d038 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c9d481f10bae3cb87814cf657a3ab55c7c30bb7ad4c4bc5c11a1c24cbe33d038->enter($__internal_c9d481f10bae3cb87814cf657a3ab55c7c30bb7ad4c4bc5c11a1c24cbe33d038_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c649089de8ef2c6f60fcc91f853312c082b2fee1d0382250ab6ecdc7035a66d4->leave($__internal_c649089de8ef2c6f60fcc91f853312c082b2fee1d0382250ab6ecdc7035a66d4_prof);

        
        $__internal_c9d481f10bae3cb87814cf657a3ab55c7c30bb7ad4c4bc5c11a1c24cbe33d038->leave($__internal_c9d481f10bae3cb87814cf657a3ab55c7c30bb7ad4c4bc5c11a1c24cbe33d038_prof);

    }

    // line 5
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_13de2d9b2261db4083027604b749433b25094687cd1824d710e901a92286bf4f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_13de2d9b2261db4083027604b749433b25094687cd1824d710e901a92286bf4f->enter($__internal_13de2d9b2261db4083027604b749433b25094687cd1824d710e901a92286bf4f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        $__internal_e6f3e5e107196906836eccb602d9370f562d1a1cc2ebf4644789cef15c184571 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e6f3e5e107196906836eccb602d9370f562d1a1cc2ebf4644789cef15c184571->enter($__internal_e6f3e5e107196906836eccb602d9370f562d1a1cc2ebf4644789cef15c184571_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 6
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
            // line 7
            if ($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array())) {
                echo "<span class=\"help-block\">";
            } else {
                echo "<div class=\"alert alert-danger\">";
            }
            // line 8
            echo "        <ul class=\"list-unstyled\">";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 11
                echo "            <li><span class=\"fa fa-exclamation-triangle\"></span> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "</li>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 13
            echo "</ul>
        ";
            // line 14
            if ($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array())) {
                echo "</span>";
            } else {
                echo "</div>";
            }
        }
        
        $__internal_e6f3e5e107196906836eccb602d9370f562d1a1cc2ebf4644789cef15c184571->leave($__internal_e6f3e5e107196906836eccb602d9370f562d1a1cc2ebf4644789cef15c184571_prof);

        
        $__internal_13de2d9b2261db4083027604b749433b25094687cd1824d710e901a92286bf4f->leave($__internal_13de2d9b2261db4083027604b749433b25094687cd1824d710e901a92286bf4f_prof);

    }

    public function getTemplateName()
    {
        return "form/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 14,  71 => 13,  63 => 11,  59 => 9,  57 => 8,  51 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'bootstrap_3_layout.html.twig' %}

{# Errors #}

{% block form_errors -%}
    {% if errors|length > 0 -%}
        {% if form.parent %}<span class=\"help-block\">{% else %}<div class=\"alert alert-danger\">{% endif %}
        <ul class=\"list-unstyled\">
        {%- for error in errors -%}
            {# use font-awesome icon library #}
            <li><span class=\"fa fa-exclamation-triangle\"></span> {{ error.message }}</li>
        {%- endfor -%}
        </ul>
        {% if form.parent %}</span>{% else %}</div>{% endif %}
    {%- endif %}
{%- endblock form_errors %}
", "form/layout.html.twig", "C:\\wamp\\www\\symfony\\stock\\app\\Resources\\views\\form\\layout.html.twig");
    }
}
