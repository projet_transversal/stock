<?php

/* form/fields.html.twig */
class __TwigTemplate_f6b2b1b42958791a64dc42b043703c4793d3df3def80f1a7c54c36c2197c7c42 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'date_time_picker_widget' => array($this, 'block_date_time_picker_widget'),
            'tags_input_widget' => array($this, 'block_tags_input_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_75204161949501027c8032d0f7b08357c5e8b200812b179e563d1f4e18445aea = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_75204161949501027c8032d0f7b08357c5e8b200812b179e563d1f4e18445aea->enter($__internal_75204161949501027c8032d0f7b08357c5e8b200812b179e563d1f4e18445aea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/fields.html.twig"));

        $__internal_2b25e1bbc6cba32f842de49198ed4598d0de51d9d9299c93f759ffafecada9c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2b25e1bbc6cba32f842de49198ed4598d0de51d9d9299c93f759ffafecada9c5->enter($__internal_2b25e1bbc6cba32f842de49198ed4598d0de51d9d9299c93f759ffafecada9c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/fields.html.twig"));

        // line 9
        echo "
";
        // line 10
        $this->displayBlock('date_time_picker_widget', $context, $blocks);
        // line 18
        echo "
";
        // line 19
        $this->displayBlock('tags_input_widget', $context, $blocks);
        
        $__internal_75204161949501027c8032d0f7b08357c5e8b200812b179e563d1f4e18445aea->leave($__internal_75204161949501027c8032d0f7b08357c5e8b200812b179e563d1f4e18445aea_prof);

        
        $__internal_2b25e1bbc6cba32f842de49198ed4598d0de51d9d9299c93f759ffafecada9c5->leave($__internal_2b25e1bbc6cba32f842de49198ed4598d0de51d9d9299c93f759ffafecada9c5_prof);

    }

    // line 10
    public function block_date_time_picker_widget($context, array $blocks = array())
    {
        $__internal_10cd597279bd923e1e6459ef39978065d0fd94c755b833c5fb83f7a7b7917923 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_10cd597279bd923e1e6459ef39978065d0fd94c755b833c5fb83f7a7b7917923->enter($__internal_10cd597279bd923e1e6459ef39978065d0fd94c755b833c5fb83f7a7b7917923_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_time_picker_widget"));

        $__internal_c187da1dcb5f250c2ac27058f25f77d510c786f2e45c52332df1bb5eccbd9dde = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c187da1dcb5f250c2ac27058f25f77d510c786f2e45c52332df1bb5eccbd9dde->enter($__internal_c187da1dcb5f250c2ac27058f25f77d510c786f2e45c52332df1bb5eccbd9dde_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_time_picker_widget"));

        // line 11
        echo "    <div class=\"input-group date\" data-toggle=\"datetimepicker\">
        ";
        // line 12
        $this->displayBlock("datetime_widget", $context, $blocks);
        echo "
        <span class=\"input-group-addon\">
            <span class=\"fa fa-calendar\" aria-hidden=\"true\"></span>
        </span>
    </div>
";
        
        $__internal_c187da1dcb5f250c2ac27058f25f77d510c786f2e45c52332df1bb5eccbd9dde->leave($__internal_c187da1dcb5f250c2ac27058f25f77d510c786f2e45c52332df1bb5eccbd9dde_prof);

        
        $__internal_10cd597279bd923e1e6459ef39978065d0fd94c755b833c5fb83f7a7b7917923->leave($__internal_10cd597279bd923e1e6459ef39978065d0fd94c755b833c5fb83f7a7b7917923_prof);

    }

    // line 19
    public function block_tags_input_widget($context, array $blocks = array())
    {
        $__internal_c2b5969f36e22b4601b8c2c78e831fedd9b82ad262c59a6c41540441a7a9b77c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c2b5969f36e22b4601b8c2c78e831fedd9b82ad262c59a6c41540441a7a9b77c->enter($__internal_c2b5969f36e22b4601b8c2c78e831fedd9b82ad262c59a6c41540441a7a9b77c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tags_input_widget"));

        $__internal_1c015e53679b523ca339247ec13decc35c2b4d78c2b762d5bc8a688b886ce5b2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c015e53679b523ca339247ec13decc35c2b4d78c2b762d5bc8a688b886ce5b2->enter($__internal_1c015e53679b523ca339247ec13decc35c2b4d78c2b762d5bc8a688b886ce5b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tags_input_widget"));

        // line 20
        echo "    <div class=\"input-group\">
        ";
        // line 21
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget', array("attr" => array("data-toggle" => "tagsinput", "data-tags" => twig_jsonencode_filter(($context["tags"] ?? $this->getContext($context, "tags"))))));
        echo "
        <span class=\"input-group-addon\">
            <span class=\"fa fa-tags\" aria-hidden=\"true\"></span>
        </span>
    </div>
";
        
        $__internal_1c015e53679b523ca339247ec13decc35c2b4d78c2b762d5bc8a688b886ce5b2->leave($__internal_1c015e53679b523ca339247ec13decc35c2b4d78c2b762d5bc8a688b886ce5b2_prof);

        
        $__internal_c2b5969f36e22b4601b8c2c78e831fedd9b82ad262c59a6c41540441a7a9b77c->leave($__internal_c2b5969f36e22b4601b8c2c78e831fedd9b82ad262c59a6c41540441a7a9b77c_prof);

    }

    public function getTemplateName()
    {
        return "form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 21,  82 => 20,  73 => 19,  57 => 12,  54 => 11,  45 => 10,  35 => 19,  32 => 18,  30 => 10,  27 => 9,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
   Each field type is rendered by a template fragment, which is determined
   by the name of your form type class (DateTimePickerType -> date_time_picker)
   and the suffix \"_widget\". This can be controlled by overriding getBlockPrefix()
   in DateTimePickerType.

   See http://symfony.com/doc/current/cookbook/form/create_custom_field_type.html#creating-a-template-for-the-field
#}

{% block date_time_picker_widget %}
    <div class=\"input-group date\" data-toggle=\"datetimepicker\">
        {{ block('datetime_widget') }}
        <span class=\"input-group-addon\">
            <span class=\"fa fa-calendar\" aria-hidden=\"true\"></span>
        </span>
    </div>
{% endblock %}

{% block tags_input_widget %}
    <div class=\"input-group\">
        {{ form_widget(form, {'attr': {'data-toggle': 'tagsinput', 'data-tags': tags|json_encode}}) }}
        <span class=\"input-group-addon\">
            <span class=\"fa fa-tags\" aria-hidden=\"true\"></span>
        </span>
    </div>
{% endblock %}
", "form/fields.html.twig", "C:\\wamp64\\www\\stock\\app\\Resources\\views\\form\\fields.html.twig");
    }
}
