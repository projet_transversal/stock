<?php

/* bootstrap_3_layout.html.twig */
class __TwigTemplate_62e8902eca8bdb61646043f63c76f629760212703f5e2854144759d7c5c34d79 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("form_div_layout.html.twig", "bootstrap_3_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."form_div_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_widget_simple' => array($this, 'block_form_widget_simple'),
                'textarea_widget' => array($this, 'block_textarea_widget'),
                'button_widget' => array($this, 'block_button_widget'),
                'money_widget' => array($this, 'block_money_widget'),
                'percent_widget' => array($this, 'block_percent_widget'),
                'datetime_widget' => array($this, 'block_datetime_widget'),
                'date_widget' => array($this, 'block_date_widget'),
                'time_widget' => array($this, 'block_time_widget'),
                'dateinterval_widget' => array($this, 'block_dateinterval_widget'),
                'choice_widget_collapsed' => array($this, 'block_choice_widget_collapsed'),
                'choice_widget_expanded' => array($this, 'block_choice_widget_expanded'),
                'checkbox_widget' => array($this, 'block_checkbox_widget'),
                'radio_widget' => array($this, 'block_radio_widget'),
                'form_label' => array($this, 'block_form_label'),
                'choice_label' => array($this, 'block_choice_label'),
                'checkbox_label' => array($this, 'block_checkbox_label'),
                'radio_label' => array($this, 'block_radio_label'),
                'checkbox_radio_label' => array($this, 'block_checkbox_radio_label'),
                'form_row' => array($this, 'block_form_row'),
                'button_row' => array($this, 'block_button_row'),
                'choice_row' => array($this, 'block_choice_row'),
                'date_row' => array($this, 'block_date_row'),
                'time_row' => array($this, 'block_time_row'),
                'datetime_row' => array($this, 'block_datetime_row'),
                'checkbox_row' => array($this, 'block_checkbox_row'),
                'radio_row' => array($this, 'block_radio_row'),
                'form_errors' => array($this, 'block_form_errors'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_56ed8f4974752c94642ffb87270959aaab433f1f2eb50764b6e87614f06c3c87 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_56ed8f4974752c94642ffb87270959aaab433f1f2eb50764b6e87614f06c3c87->enter($__internal_56ed8f4974752c94642ffb87270959aaab433f1f2eb50764b6e87614f06c3c87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_3_layout.html.twig"));

        $__internal_3a6092e89fdbc06816b2fcba6a3852ec2ae954f4d426491ac4105ad065afbce8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3a6092e89fdbc06816b2fcba6a3852ec2ae954f4d426491ac4105ad065afbce8->enter($__internal_3a6092e89fdbc06816b2fcba6a3852ec2ae954f4d426491ac4105ad065afbce8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_3_layout.html.twig"));

        // line 2
        echo "
";
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('form_widget_simple', $context, $blocks);
        // line 11
        echo "
";
        // line 12
        $this->displayBlock('textarea_widget', $context, $blocks);
        // line 16
        echo "
";
        // line 17
        $this->displayBlock('button_widget', $context, $blocks);
        // line 21
        echo "
";
        // line 22
        $this->displayBlock('money_widget', $context, $blocks);
        // line 34
        echo "
";
        // line 35
        $this->displayBlock('percent_widget', $context, $blocks);
        // line 41
        echo "
";
        // line 42
        $this->displayBlock('datetime_widget', $context, $blocks);
        // line 55
        echo "
";
        // line 56
        $this->displayBlock('date_widget', $context, $blocks);
        // line 74
        echo "
";
        // line 75
        $this->displayBlock('time_widget', $context, $blocks);
        // line 90
        $this->displayBlock('dateinterval_widget', $context, $blocks);
        // line 109
        $this->displayBlock('choice_widget_collapsed', $context, $blocks);
        // line 113
        echo "
";
        // line 114
        $this->displayBlock('choice_widget_expanded', $context, $blocks);
        // line 133
        echo "
";
        // line 134
        $this->displayBlock('checkbox_widget', $context, $blocks);
        // line 144
        echo "
";
        // line 145
        $this->displayBlock('radio_widget', $context, $blocks);
        // line 155
        echo "
";
        // line 157
        echo "
";
        // line 158
        $this->displayBlock('form_label', $context, $blocks);
        // line 162
        echo "
";
        // line 163
        $this->displayBlock('choice_label', $context, $blocks);
        // line 168
        echo "
";
        // line 169
        $this->displayBlock('checkbox_label', $context, $blocks);
        // line 172
        echo "
";
        // line 173
        $this->displayBlock('radio_label', $context, $blocks);
        // line 176
        echo "
";
        // line 177
        $this->displayBlock('checkbox_radio_label', $context, $blocks);
        // line 201
        echo "
";
        // line 203
        echo "
";
        // line 204
        $this->displayBlock('form_row', $context, $blocks);
        // line 211
        echo "
";
        // line 212
        $this->displayBlock('button_row', $context, $blocks);
        // line 217
        echo "
";
        // line 218
        $this->displayBlock('choice_row', $context, $blocks);
        // line 222
        echo "
";
        // line 223
        $this->displayBlock('date_row', $context, $blocks);
        // line 227
        echo "
";
        // line 228
        $this->displayBlock('time_row', $context, $blocks);
        // line 232
        echo "
";
        // line 233
        $this->displayBlock('datetime_row', $context, $blocks);
        // line 237
        echo "
";
        // line 238
        $this->displayBlock('checkbox_row', $context, $blocks);
        // line 244
        echo "
";
        // line 245
        $this->displayBlock('radio_row', $context, $blocks);
        // line 251
        echo "
";
        // line 253
        echo "
";
        // line 254
        $this->displayBlock('form_errors', $context, $blocks);
        
        $__internal_56ed8f4974752c94642ffb87270959aaab433f1f2eb50764b6e87614f06c3c87->leave($__internal_56ed8f4974752c94642ffb87270959aaab433f1f2eb50764b6e87614f06c3c87_prof);

        
        $__internal_3a6092e89fdbc06816b2fcba6a3852ec2ae954f4d426491ac4105ad065afbce8->leave($__internal_3a6092e89fdbc06816b2fcba6a3852ec2ae954f4d426491ac4105ad065afbce8_prof);

    }

    // line 5
    public function block_form_widget_simple($context, array $blocks = array())
    {
        $__internal_db0fdd126a932f79e9c61923a69c6510abee6de52c0bc425eb7ed125e42b333f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_db0fdd126a932f79e9c61923a69c6510abee6de52c0bc425eb7ed125e42b333f->enter($__internal_db0fdd126a932f79e9c61923a69c6510abee6de52c0bc425eb7ed125e42b333f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        $__internal_61e76b70c632c127e470e3d134a1974c495404762b0d71984ab20d1fb08005e3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_61e76b70c632c127e470e3d134a1974c495404762b0d71984ab20d1fb08005e3->enter($__internal_61e76b70c632c127e470e3d134a1974c495404762b0d71984ab20d1fb08005e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        // line 6
        if (( !array_key_exists("type", $context) || !twig_in_filter(($context["type"] ?? $this->getContext($context, "type")), array(0 => "file", 1 => "hidden")))) {
            // line 7
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control"))));
        }
        // line 9
        $this->displayParentBlock("form_widget_simple", $context, $blocks);
        
        $__internal_61e76b70c632c127e470e3d134a1974c495404762b0d71984ab20d1fb08005e3->leave($__internal_61e76b70c632c127e470e3d134a1974c495404762b0d71984ab20d1fb08005e3_prof);

        
        $__internal_db0fdd126a932f79e9c61923a69c6510abee6de52c0bc425eb7ed125e42b333f->leave($__internal_db0fdd126a932f79e9c61923a69c6510abee6de52c0bc425eb7ed125e42b333f_prof);

    }

    // line 12
    public function block_textarea_widget($context, array $blocks = array())
    {
        $__internal_c6c9ea337449a0ca8a703a701972ff6f9a8635a1a862eefb7fa32b3bdf045062 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c6c9ea337449a0ca8a703a701972ff6f9a8635a1a862eefb7fa32b3bdf045062->enter($__internal_c6c9ea337449a0ca8a703a701972ff6f9a8635a1a862eefb7fa32b3bdf045062_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "textarea_widget"));

        $__internal_d84edbd530ac94ac9cf249c3d8a12d1b6cb1274da79460dc368e94176e846c1f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d84edbd530ac94ac9cf249c3d8a12d1b6cb1274da79460dc368e94176e846c1f->enter($__internal_d84edbd530ac94ac9cf249c3d8a12d1b6cb1274da79460dc368e94176e846c1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "textarea_widget"));

        // line 13
        $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control"))));
        // line 14
        $this->displayParentBlock("textarea_widget", $context, $blocks);
        
        $__internal_d84edbd530ac94ac9cf249c3d8a12d1b6cb1274da79460dc368e94176e846c1f->leave($__internal_d84edbd530ac94ac9cf249c3d8a12d1b6cb1274da79460dc368e94176e846c1f_prof);

        
        $__internal_c6c9ea337449a0ca8a703a701972ff6f9a8635a1a862eefb7fa32b3bdf045062->leave($__internal_c6c9ea337449a0ca8a703a701972ff6f9a8635a1a862eefb7fa32b3bdf045062_prof);

    }

    // line 17
    public function block_button_widget($context, array $blocks = array())
    {
        $__internal_614eb9e3319ac92cec879e61bb6c4402ac68e9575aca806394e13a8b391d0af9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_614eb9e3319ac92cec879e61bb6c4402ac68e9575aca806394e13a8b391d0af9->enter($__internal_614eb9e3319ac92cec879e61bb6c4402ac68e9575aca806394e13a8b391d0af9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        $__internal_1c2638c78a1091501c49053f4a9108500cbad5aa2c7075b24c93c88cb03b1d74 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c2638c78a1091501c49053f4a9108500cbad5aa2c7075b24c93c88cb03b1d74->enter($__internal_1c2638c78a1091501c49053f4a9108500cbad5aa2c7075b24c93c88cb03b1d74_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        // line 18
        $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "btn-default")) : ("btn-default")) . " btn"))));
        // line 19
        $this->displayParentBlock("button_widget", $context, $blocks);
        
        $__internal_1c2638c78a1091501c49053f4a9108500cbad5aa2c7075b24c93c88cb03b1d74->leave($__internal_1c2638c78a1091501c49053f4a9108500cbad5aa2c7075b24c93c88cb03b1d74_prof);

        
        $__internal_614eb9e3319ac92cec879e61bb6c4402ac68e9575aca806394e13a8b391d0af9->leave($__internal_614eb9e3319ac92cec879e61bb6c4402ac68e9575aca806394e13a8b391d0af9_prof);

    }

    // line 22
    public function block_money_widget($context, array $blocks = array())
    {
        $__internal_52e4dc37df93888a313684b9bdde17c83df9a511b14e10c068c87dbfa247fe4a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_52e4dc37df93888a313684b9bdde17c83df9a511b14e10c068c87dbfa247fe4a->enter($__internal_52e4dc37df93888a313684b9bdde17c83df9a511b14e10c068c87dbfa247fe4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        $__internal_aeb67eb589cd6c0d2d8c32947f6b34851fc989f1e697bc6a6dca182fef3b70a6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aeb67eb589cd6c0d2d8c32947f6b34851fc989f1e697bc6a6dca182fef3b70a6->enter($__internal_aeb67eb589cd6c0d2d8c32947f6b34851fc989f1e697bc6a6dca182fef3b70a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        // line 23
        echo "<div class=\"input-group\">
        ";
        // line 24
        $context["append"] = (is_string($__internal_b7afc8a09d86395b24400ac1392bd391e3044ac4c362279f8bf617a37d3df6de = ($context["money_pattern"] ?? $this->getContext($context, "money_pattern"))) && is_string($__internal_9b08f9d5f697c5f0701fb63fa43122fc900486f67cd006f5e6abaf4fd7a60d97 = "{{") && ('' === $__internal_9b08f9d5f697c5f0701fb63fa43122fc900486f67cd006f5e6abaf4fd7a60d97 || 0 === strpos($__internal_b7afc8a09d86395b24400ac1392bd391e3044ac4c362279f8bf617a37d3df6de, $__internal_9b08f9d5f697c5f0701fb63fa43122fc900486f67cd006f5e6abaf4fd7a60d97)));
        // line 25
        echo "        ";
        if ( !($context["append"] ?? $this->getContext($context, "append"))) {
            // line 26
            echo "            <span class=\"input-group-addon\">";
            echo twig_escape_filter($this->env, twig_replace_filter(($context["money_pattern"] ?? $this->getContext($context, "money_pattern")), array("{{ widget }}" => "")), "html", null, true);
            echo "</span>
        ";
        }
        // line 28
        $this->displayBlock("form_widget_simple", $context, $blocks);
        // line 29
        if (($context["append"] ?? $this->getContext($context, "append"))) {
            // line 30
            echo "            <span class=\"input-group-addon\">";
            echo twig_escape_filter($this->env, twig_replace_filter(($context["money_pattern"] ?? $this->getContext($context, "money_pattern")), array("{{ widget }}" => "")), "html", null, true);
            echo "</span>
        ";
        }
        // line 32
        echo "    </div>";
        
        $__internal_aeb67eb589cd6c0d2d8c32947f6b34851fc989f1e697bc6a6dca182fef3b70a6->leave($__internal_aeb67eb589cd6c0d2d8c32947f6b34851fc989f1e697bc6a6dca182fef3b70a6_prof);

        
        $__internal_52e4dc37df93888a313684b9bdde17c83df9a511b14e10c068c87dbfa247fe4a->leave($__internal_52e4dc37df93888a313684b9bdde17c83df9a511b14e10c068c87dbfa247fe4a_prof);

    }

    // line 35
    public function block_percent_widget($context, array $blocks = array())
    {
        $__internal_2b3175ecefb5103362dd1b9ecd68ad9c07161e1b9ccdb2bacdb3f71d33bad2c5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2b3175ecefb5103362dd1b9ecd68ad9c07161e1b9ccdb2bacdb3f71d33bad2c5->enter($__internal_2b3175ecefb5103362dd1b9ecd68ad9c07161e1b9ccdb2bacdb3f71d33bad2c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        $__internal_1d5a0df688cec2453d8fe24c342057b2b94aa856a3e6ac17a5da12d71516e0b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1d5a0df688cec2453d8fe24c342057b2b94aa856a3e6ac17a5da12d71516e0b9->enter($__internal_1d5a0df688cec2453d8fe24c342057b2b94aa856a3e6ac17a5da12d71516e0b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        // line 36
        echo "<div class=\"input-group\">";
        // line 37
        $this->displayBlock("form_widget_simple", $context, $blocks);
        // line 38
        echo "<span class=\"input-group-addon\">%</span>
    </div>";
        
        $__internal_1d5a0df688cec2453d8fe24c342057b2b94aa856a3e6ac17a5da12d71516e0b9->leave($__internal_1d5a0df688cec2453d8fe24c342057b2b94aa856a3e6ac17a5da12d71516e0b9_prof);

        
        $__internal_2b3175ecefb5103362dd1b9ecd68ad9c07161e1b9ccdb2bacdb3f71d33bad2c5->leave($__internal_2b3175ecefb5103362dd1b9ecd68ad9c07161e1b9ccdb2bacdb3f71d33bad2c5_prof);

    }

    // line 42
    public function block_datetime_widget($context, array $blocks = array())
    {
        $__internal_0c039cbdbf71aae1775a645f719ab80c49a7ea9c966e5aa911eed3e3d0b906ff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0c039cbdbf71aae1775a645f719ab80c49a7ea9c966e5aa911eed3e3d0b906ff->enter($__internal_0c039cbdbf71aae1775a645f719ab80c49a7ea9c966e5aa911eed3e3d0b906ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        $__internal_be6b5dd98568771015acb4f953e4db5bb5f5cd8b52552cfcb70dee06b8897a8b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_be6b5dd98568771015acb4f953e4db5bb5f5cd8b52552cfcb70dee06b8897a8b->enter($__internal_be6b5dd98568771015acb4f953e4db5bb5f5cd8b52552cfcb70dee06b8897a8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        // line 43
        if ((($context["widget"] ?? $this->getContext($context, "widget")) == "single_text")) {
            // line 44
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 46
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-inline"))));
            // line 47
            echo "<div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">";
            // line 48
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "date", array()), 'errors');
            // line 49
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "time", array()), 'errors');
            // line 50
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "date", array()), 'widget', array("datetime" => true));
            // line 51
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "time", array()), 'widget', array("datetime" => true));
            // line 52
            echo "</div>";
        }
        
        $__internal_be6b5dd98568771015acb4f953e4db5bb5f5cd8b52552cfcb70dee06b8897a8b->leave($__internal_be6b5dd98568771015acb4f953e4db5bb5f5cd8b52552cfcb70dee06b8897a8b_prof);

        
        $__internal_0c039cbdbf71aae1775a645f719ab80c49a7ea9c966e5aa911eed3e3d0b906ff->leave($__internal_0c039cbdbf71aae1775a645f719ab80c49a7ea9c966e5aa911eed3e3d0b906ff_prof);

    }

    // line 56
    public function block_date_widget($context, array $blocks = array())
    {
        $__internal_54679184c1f3fd75de715866c952e4856e5f01f265bdc001a63f32759a685315 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_54679184c1f3fd75de715866c952e4856e5f01f265bdc001a63f32759a685315->enter($__internal_54679184c1f3fd75de715866c952e4856e5f01f265bdc001a63f32759a685315_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        $__internal_6172b74d3e6b357c158b11fa559512f284022535acf31fa4adddd54c1a615fb4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6172b74d3e6b357c158b11fa559512f284022535acf31fa4adddd54c1a615fb4->enter($__internal_6172b74d3e6b357c158b11fa559512f284022535acf31fa4adddd54c1a615fb4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        // line 57
        if ((($context["widget"] ?? $this->getContext($context, "widget")) == "single_text")) {
            // line 58
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 60
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-inline"))));
            // line 61
            if (( !array_key_exists("datetime", $context) ||  !($context["datetime"] ?? $this->getContext($context, "datetime")))) {
                // line 62
                echo "<div ";
                $this->displayBlock("widget_container_attributes", $context, $blocks);
                echo ">";
            }
            // line 64
            echo twig_replace_filter(($context["date_pattern"] ?? $this->getContext($context, "date_pattern")), array("{{ year }}" =>             // line 65
$this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "year", array()), 'widget'), "{{ month }}" =>             // line 66
$this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "month", array()), 'widget'), "{{ day }}" =>             // line 67
$this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "day", array()), 'widget')));
            // line 69
            if (( !array_key_exists("datetime", $context) ||  !($context["datetime"] ?? $this->getContext($context, "datetime")))) {
                // line 70
                echo "</div>";
            }
        }
        
        $__internal_6172b74d3e6b357c158b11fa559512f284022535acf31fa4adddd54c1a615fb4->leave($__internal_6172b74d3e6b357c158b11fa559512f284022535acf31fa4adddd54c1a615fb4_prof);

        
        $__internal_54679184c1f3fd75de715866c952e4856e5f01f265bdc001a63f32759a685315->leave($__internal_54679184c1f3fd75de715866c952e4856e5f01f265bdc001a63f32759a685315_prof);

    }

    // line 75
    public function block_time_widget($context, array $blocks = array())
    {
        $__internal_e5298939c27f33cc1a12aa639a9049d6a5712e274bb697601392e93c5fae2b0a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e5298939c27f33cc1a12aa639a9049d6a5712e274bb697601392e93c5fae2b0a->enter($__internal_e5298939c27f33cc1a12aa639a9049d6a5712e274bb697601392e93c5fae2b0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        $__internal_380a600ab30bcaa8ec91dc15c2948c28d9edb7069c9cc2c00645229a1eef39f2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_380a600ab30bcaa8ec91dc15c2948c28d9edb7069c9cc2c00645229a1eef39f2->enter($__internal_380a600ab30bcaa8ec91dc15c2948c28d9edb7069c9cc2c00645229a1eef39f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        // line 76
        if ((($context["widget"] ?? $this->getContext($context, "widget")) == "single_text")) {
            // line 77
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 79
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-inline"))));
            // line 80
            if (( !array_key_exists("datetime", $context) || (false == ($context["datetime"] ?? $this->getContext($context, "datetime"))))) {
                // line 81
                echo "<div ";
                $this->displayBlock("widget_container_attributes", $context, $blocks);
                echo ">";
            }
            // line 83
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "hour", array()), 'widget');
            if (($context["with_minutes"] ?? $this->getContext($context, "with_minutes"))) {
                echo ":";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "minute", array()), 'widget');
            }
            if (($context["with_seconds"] ?? $this->getContext($context, "with_seconds"))) {
                echo ":";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "second", array()), 'widget');
            }
            // line 84
            echo "        ";
            if (( !array_key_exists("datetime", $context) || (false == ($context["datetime"] ?? $this->getContext($context, "datetime"))))) {
                // line 85
                echo "</div>";
            }
        }
        
        $__internal_380a600ab30bcaa8ec91dc15c2948c28d9edb7069c9cc2c00645229a1eef39f2->leave($__internal_380a600ab30bcaa8ec91dc15c2948c28d9edb7069c9cc2c00645229a1eef39f2_prof);

        
        $__internal_e5298939c27f33cc1a12aa639a9049d6a5712e274bb697601392e93c5fae2b0a->leave($__internal_e5298939c27f33cc1a12aa639a9049d6a5712e274bb697601392e93c5fae2b0a_prof);

    }

    // line 90
    public function block_dateinterval_widget($context, array $blocks = array())
    {
        $__internal_abe47a8be19681764e1f8b328cf681b9bbd827ad25a35269a19d0fb8dbf3bc57 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_abe47a8be19681764e1f8b328cf681b9bbd827ad25a35269a19d0fb8dbf3bc57->enter($__internal_abe47a8be19681764e1f8b328cf681b9bbd827ad25a35269a19d0fb8dbf3bc57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "dateinterval_widget"));

        $__internal_4242cbda796502357f00481d23f10f0578e0425f2ab880e9da2b5bb70c4281e7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4242cbda796502357f00481d23f10f0578e0425f2ab880e9da2b5bb70c4281e7->enter($__internal_4242cbda796502357f00481d23f10f0578e0425f2ab880e9da2b5bb70c4281e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "dateinterval_widget"));

        // line 91
        if ((($context["widget"] ?? $this->getContext($context, "widget")) == "single_text")) {
            // line 92
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 94
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-inline"))));
            // line 95
            echo "<div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">";
            // line 96
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
            // line 97
            if (($context["with_years"] ?? $this->getContext($context, "with_years"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "years", array()), 'widget');
            }
            // line 98
            if (($context["with_months"] ?? $this->getContext($context, "with_months"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "months", array()), 'widget');
            }
            // line 99
            if (($context["with_weeks"] ?? $this->getContext($context, "with_weeks"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "weeks", array()), 'widget');
            }
            // line 100
            if (($context["with_days"] ?? $this->getContext($context, "with_days"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "days", array()), 'widget');
            }
            // line 101
            if (($context["with_hours"] ?? $this->getContext($context, "with_hours"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "hours", array()), 'widget');
            }
            // line 102
            if (($context["with_minutes"] ?? $this->getContext($context, "with_minutes"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "minutes", array()), 'widget');
            }
            // line 103
            if (($context["with_seconds"] ?? $this->getContext($context, "with_seconds"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "seconds", array()), 'widget');
            }
            // line 104
            if (($context["with_invert"] ?? $this->getContext($context, "with_invert"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "invert", array()), 'widget');
            }
            // line 105
            echo "</div>";
        }
        
        $__internal_4242cbda796502357f00481d23f10f0578e0425f2ab880e9da2b5bb70c4281e7->leave($__internal_4242cbda796502357f00481d23f10f0578e0425f2ab880e9da2b5bb70c4281e7_prof);

        
        $__internal_abe47a8be19681764e1f8b328cf681b9bbd827ad25a35269a19d0fb8dbf3bc57->leave($__internal_abe47a8be19681764e1f8b328cf681b9bbd827ad25a35269a19d0fb8dbf3bc57_prof);

    }

    // line 109
    public function block_choice_widget_collapsed($context, array $blocks = array())
    {
        $__internal_b964c4a5269e0f6b452b43d9aa9ae30925c0802e210e35552fa681918bd3f38c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b964c4a5269e0f6b452b43d9aa9ae30925c0802e210e35552fa681918bd3f38c->enter($__internal_b964c4a5269e0f6b452b43d9aa9ae30925c0802e210e35552fa681918bd3f38c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_collapsed"));

        $__internal_f35042dc722deb019788a8c0be30760157a51dca0c8dd58dd90b4acca4539e44 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f35042dc722deb019788a8c0be30760157a51dca0c8dd58dd90b4acca4539e44->enter($__internal_f35042dc722deb019788a8c0be30760157a51dca0c8dd58dd90b4acca4539e44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_collapsed"));

        // line 110
        $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control"))));
        // line 111
        $this->displayParentBlock("choice_widget_collapsed", $context, $blocks);
        
        $__internal_f35042dc722deb019788a8c0be30760157a51dca0c8dd58dd90b4acca4539e44->leave($__internal_f35042dc722deb019788a8c0be30760157a51dca0c8dd58dd90b4acca4539e44_prof);

        
        $__internal_b964c4a5269e0f6b452b43d9aa9ae30925c0802e210e35552fa681918bd3f38c->leave($__internal_b964c4a5269e0f6b452b43d9aa9ae30925c0802e210e35552fa681918bd3f38c_prof);

    }

    // line 114
    public function block_choice_widget_expanded($context, array $blocks = array())
    {
        $__internal_b02c79019b05e2ea1ab054b7bd0f16d27d06808139ae6b083fa828194593bbed = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b02c79019b05e2ea1ab054b7bd0f16d27d06808139ae6b083fa828194593bbed->enter($__internal_b02c79019b05e2ea1ab054b7bd0f16d27d06808139ae6b083fa828194593bbed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        $__internal_1353309076eb439e49046e264ca35fb1887a25711c11ce88232dc2b449165393 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1353309076eb439e49046e264ca35fb1887a25711c11ce88232dc2b449165393->enter($__internal_1353309076eb439e49046e264ca35fb1887a25711c11ce88232dc2b449165393_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        // line 115
        if (twig_in_filter("-inline", (($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")))) {
            // line 116
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 117
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 118
($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")), "translation_domain" =>                 // line 119
($context["choice_translation_domain"] ?? $this->getContext($context, "choice_translation_domain"))));
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 123
            echo "<div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">";
            // line 124
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 125
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 126
($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")), "translation_domain" =>                 // line 127
($context["choice_translation_domain"] ?? $this->getContext($context, "choice_translation_domain"))));
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 130
            echo "</div>";
        }
        
        $__internal_1353309076eb439e49046e264ca35fb1887a25711c11ce88232dc2b449165393->leave($__internal_1353309076eb439e49046e264ca35fb1887a25711c11ce88232dc2b449165393_prof);

        
        $__internal_b02c79019b05e2ea1ab054b7bd0f16d27d06808139ae6b083fa828194593bbed->leave($__internal_b02c79019b05e2ea1ab054b7bd0f16d27d06808139ae6b083fa828194593bbed_prof);

    }

    // line 134
    public function block_checkbox_widget($context, array $blocks = array())
    {
        $__internal_8f5b73cd9e7613300b977363c4bc744e7caa35a59c9aed4801ca453329696c8e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8f5b73cd9e7613300b977363c4bc744e7caa35a59c9aed4801ca453329696c8e->enter($__internal_8f5b73cd9e7613300b977363c4bc744e7caa35a59c9aed4801ca453329696c8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        $__internal_669ca4c1b889dc5153d0e8792c6fe3f516d608bd5ab962ce583f9765d41c0c04 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_669ca4c1b889dc5153d0e8792c6fe3f516d608bd5ab962ce583f9765d41c0c04->enter($__internal_669ca4c1b889dc5153d0e8792c6fe3f516d608bd5ab962ce583f9765d41c0c04_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        // line 135
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter(($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")), (($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")))) : ((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : (""))));
        // line 136
        if (twig_in_filter("checkbox-inline", ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")))) {
            // line 137
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
        } else {
            // line 139
            echo "<div class=\"checkbox\">";
            // line 140
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
            // line 141
            echo "</div>";
        }
        
        $__internal_669ca4c1b889dc5153d0e8792c6fe3f516d608bd5ab962ce583f9765d41c0c04->leave($__internal_669ca4c1b889dc5153d0e8792c6fe3f516d608bd5ab962ce583f9765d41c0c04_prof);

        
        $__internal_8f5b73cd9e7613300b977363c4bc744e7caa35a59c9aed4801ca453329696c8e->leave($__internal_8f5b73cd9e7613300b977363c4bc744e7caa35a59c9aed4801ca453329696c8e_prof);

    }

    // line 145
    public function block_radio_widget($context, array $blocks = array())
    {
        $__internal_14ad5c54f8975fc02e1b8f489fd89f66e947a1ecfa6d825c128f81b67cf2cc70 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_14ad5c54f8975fc02e1b8f489fd89f66e947a1ecfa6d825c128f81b67cf2cc70->enter($__internal_14ad5c54f8975fc02e1b8f489fd89f66e947a1ecfa6d825c128f81b67cf2cc70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        $__internal_90f9e4daaf44532ef9482d4d6432c3c2f4d314181f3d85d22d7f3faad0b44a65 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_90f9e4daaf44532ef9482d4d6432c3c2f4d314181f3d85d22d7f3faad0b44a65->enter($__internal_90f9e4daaf44532ef9482d4d6432c3c2f4d314181f3d85d22d7f3faad0b44a65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        // line 146
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter(($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")), (($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")))) : ((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : (""))));
        // line 147
        if (twig_in_filter("radio-inline", ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")))) {
            // line 148
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
        } else {
            // line 150
            echo "<div class=\"radio\">";
            // line 151
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
            // line 152
            echo "</div>";
        }
        
        $__internal_90f9e4daaf44532ef9482d4d6432c3c2f4d314181f3d85d22d7f3faad0b44a65->leave($__internal_90f9e4daaf44532ef9482d4d6432c3c2f4d314181f3d85d22d7f3faad0b44a65_prof);

        
        $__internal_14ad5c54f8975fc02e1b8f489fd89f66e947a1ecfa6d825c128f81b67cf2cc70->leave($__internal_14ad5c54f8975fc02e1b8f489fd89f66e947a1ecfa6d825c128f81b67cf2cc70_prof);

    }

    // line 158
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_0860ec92e580f56a7d5c834d7e83fe02cc19520e4b266dcdcb14859af62f49a7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0860ec92e580f56a7d5c834d7e83fe02cc19520e4b266dcdcb14859af62f49a7->enter($__internal_0860ec92e580f56a7d5c834d7e83fe02cc19520e4b266dcdcb14859af62f49a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_0cc47f96583f86ab474ce879370378d719b5e7283bdc48ebdfaaef645f0cd130 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0cc47f96583f86ab474ce879370378d719b5e7283bdc48ebdfaaef645f0cd130->enter($__internal_0cc47f96583f86ab474ce879370378d719b5e7283bdc48ebdfaaef645f0cd130_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 159
        $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => trim(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " control-label"))));
        // line 160
        $this->displayParentBlock("form_label", $context, $blocks);
        
        $__internal_0cc47f96583f86ab474ce879370378d719b5e7283bdc48ebdfaaef645f0cd130->leave($__internal_0cc47f96583f86ab474ce879370378d719b5e7283bdc48ebdfaaef645f0cd130_prof);

        
        $__internal_0860ec92e580f56a7d5c834d7e83fe02cc19520e4b266dcdcb14859af62f49a7->leave($__internal_0860ec92e580f56a7d5c834d7e83fe02cc19520e4b266dcdcb14859af62f49a7_prof);

    }

    // line 163
    public function block_choice_label($context, array $blocks = array())
    {
        $__internal_4f98fce34a991a0c47c08912653fc7c51a6717c1b8660ddf5927ac2e7d938ae7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4f98fce34a991a0c47c08912653fc7c51a6717c1b8660ddf5927ac2e7d938ae7->enter($__internal_4f98fce34a991a0c47c08912653fc7c51a6717c1b8660ddf5927ac2e7d938ae7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_label"));

        $__internal_e546eff52b1071ef0a1df452b791c9b932528dac301dad83837b00dbbc905bb4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e546eff52b1071ef0a1df452b791c9b932528dac301dad83837b00dbbc905bb4->enter($__internal_e546eff52b1071ef0a1df452b791c9b932528dac301dad83837b00dbbc905bb4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_label"));

        // line 165
        $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => trim(twig_replace_filter((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")), array("checkbox-inline" => "", "radio-inline" => "")))));
        // line 166
        $this->displayBlock("form_label", $context, $blocks);
        
        $__internal_e546eff52b1071ef0a1df452b791c9b932528dac301dad83837b00dbbc905bb4->leave($__internal_e546eff52b1071ef0a1df452b791c9b932528dac301dad83837b00dbbc905bb4_prof);

        
        $__internal_4f98fce34a991a0c47c08912653fc7c51a6717c1b8660ddf5927ac2e7d938ae7->leave($__internal_4f98fce34a991a0c47c08912653fc7c51a6717c1b8660ddf5927ac2e7d938ae7_prof);

    }

    // line 169
    public function block_checkbox_label($context, array $blocks = array())
    {
        $__internal_35305c52f6ca2eb8186642a945d6abb1671200aa20475b6ea0aefcd560e6d2be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_35305c52f6ca2eb8186642a945d6abb1671200aa20475b6ea0aefcd560e6d2be->enter($__internal_35305c52f6ca2eb8186642a945d6abb1671200aa20475b6ea0aefcd560e6d2be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_label"));

        $__internal_3389f43feb883ca7718a8a2287b89f0f5c0214b2af146c5537df40b9f2675dff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3389f43feb883ca7718a8a2287b89f0f5c0214b2af146c5537df40b9f2675dff->enter($__internal_3389f43feb883ca7718a8a2287b89f0f5c0214b2af146c5537df40b9f2675dff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_label"));

        // line 170
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_3389f43feb883ca7718a8a2287b89f0f5c0214b2af146c5537df40b9f2675dff->leave($__internal_3389f43feb883ca7718a8a2287b89f0f5c0214b2af146c5537df40b9f2675dff_prof);

        
        $__internal_35305c52f6ca2eb8186642a945d6abb1671200aa20475b6ea0aefcd560e6d2be->leave($__internal_35305c52f6ca2eb8186642a945d6abb1671200aa20475b6ea0aefcd560e6d2be_prof);

    }

    // line 173
    public function block_radio_label($context, array $blocks = array())
    {
        $__internal_92c4a39257872581afb823b34b80a51965d18e857b0e86a4c221944388094f73 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_92c4a39257872581afb823b34b80a51965d18e857b0e86a4c221944388094f73->enter($__internal_92c4a39257872581afb823b34b80a51965d18e857b0e86a4c221944388094f73_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_label"));

        $__internal_ced9997eddaf41c8513b412cf8b2debe08de083033dc50f6a826b5fe264c9449 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ced9997eddaf41c8513b412cf8b2debe08de083033dc50f6a826b5fe264c9449->enter($__internal_ced9997eddaf41c8513b412cf8b2debe08de083033dc50f6a826b5fe264c9449_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_label"));

        // line 174
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_ced9997eddaf41c8513b412cf8b2debe08de083033dc50f6a826b5fe264c9449->leave($__internal_ced9997eddaf41c8513b412cf8b2debe08de083033dc50f6a826b5fe264c9449_prof);

        
        $__internal_92c4a39257872581afb823b34b80a51965d18e857b0e86a4c221944388094f73->leave($__internal_92c4a39257872581afb823b34b80a51965d18e857b0e86a4c221944388094f73_prof);

    }

    // line 177
    public function block_checkbox_radio_label($context, array $blocks = array())
    {
        $__internal_e7fa4f40941a76eb55c861cc28fd0e1f1b2f87dce23a3f4cd865c49bf7f3cf63 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e7fa4f40941a76eb55c861cc28fd0e1f1b2f87dce23a3f4cd865c49bf7f3cf63->enter($__internal_e7fa4f40941a76eb55c861cc28fd0e1f1b2f87dce23a3f4cd865c49bf7f3cf63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        $__internal_af7d9c1c0f87249d77c26c31759bc622a7159463c3b0fca676ae52b6c6e6d4d1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_af7d9c1c0f87249d77c26c31759bc622a7159463c3b0fca676ae52b6c6e6d4d1->enter($__internal_af7d9c1c0f87249d77c26c31759bc622a7159463c3b0fca676ae52b6c6e6d4d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        // line 178
        echo "    ";
        // line 179
        echo "    ";
        if (array_key_exists("widget", $context)) {
            // line 180
            echo "        ";
            if (($context["required"] ?? $this->getContext($context, "required"))) {
                // line 181
                echo "            ";
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => trim(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " required"))));
                // line 182
                echo "        ";
            }
            // line 183
            echo "        ";
            if (array_key_exists("parent_label_class", $context)) {
                // line 184
                echo "            ";
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => trim((((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " ") . ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class"))))));
                // line 185
                echo "        ";
            }
            // line 186
            echo "        ";
            if (( !(($context["label"] ?? $this->getContext($context, "label")) === false) && twig_test_empty(($context["label"] ?? $this->getContext($context, "label"))))) {
                // line 187
                if ( !twig_test_empty(($context["label_format"] ?? $this->getContext($context, "label_format")))) {
                    // line 188
                    $context["label"] = twig_replace_filter(($context["label_format"] ?? $this->getContext($context, "label_format")), array("%name%" =>                     // line 189
($context["name"] ?? $this->getContext($context, "name")), "%id%" =>                     // line 190
($context["id"] ?? $this->getContext($context, "id"))));
                } else {
                    // line 193
                    $context["label"] = $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->humanize(($context["name"] ?? $this->getContext($context, "name")));
                }
            }
            // line 196
            echo "        <label";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["label_attr"] ?? $this->getContext($context, "label_attr")));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">";
            // line 197
            echo ($context["widget"] ?? $this->getContext($context, "widget"));
            echo " ";
            echo twig_escape_filter($this->env, (( !(($context["label"] ?? $this->getContext($context, "label")) === false)) ? ((((($context["translation_domain"] ?? $this->getContext($context, "translation_domain")) === false)) ? (($context["label"] ?? $this->getContext($context, "label"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(($context["label"] ?? $this->getContext($context, "label")), array(), ($context["translation_domain"] ?? $this->getContext($context, "translation_domain")))))) : ("")), "html", null, true);
            // line 198
            echo "</label>
    ";
        }
        
        $__internal_af7d9c1c0f87249d77c26c31759bc622a7159463c3b0fca676ae52b6c6e6d4d1->leave($__internal_af7d9c1c0f87249d77c26c31759bc622a7159463c3b0fca676ae52b6c6e6d4d1_prof);

        
        $__internal_e7fa4f40941a76eb55c861cc28fd0e1f1b2f87dce23a3f4cd865c49bf7f3cf63->leave($__internal_e7fa4f40941a76eb55c861cc28fd0e1f1b2f87dce23a3f4cd865c49bf7f3cf63_prof);

    }

    // line 204
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_b71185d6f16ef21f98f960e607b8eb75ea94c8b8b655d8576d081d225f5e80b7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b71185d6f16ef21f98f960e607b8eb75ea94c8b8b655d8576d081d225f5e80b7->enter($__internal_b71185d6f16ef21f98f960e607b8eb75ea94c8b8b655d8576d081d225f5e80b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_d5febe908463151709eabf62ae78a0f5fd807a305a7db13fda8fcb272cef0249 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d5febe908463151709eabf62ae78a0f5fd807a305a7db13fda8fcb272cef0249->enter($__internal_d5febe908463151709eabf62ae78a0f5fd807a305a7db13fda8fcb272cef0249_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 205
        echo "<div class=\"form-group";
        if ((( !($context["compound"] ?? $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter(($context["force_error"] ?? $this->getContext($context, "force_error")), false)) : (false))) &&  !($context["valid"] ?? $this->getContext($context, "valid")))) {
            echo " has-error";
        }
        echo "\">";
        // line 206
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label');
        // line 207
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 208
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 209
        echo "</div>";
        
        $__internal_d5febe908463151709eabf62ae78a0f5fd807a305a7db13fda8fcb272cef0249->leave($__internal_d5febe908463151709eabf62ae78a0f5fd807a305a7db13fda8fcb272cef0249_prof);

        
        $__internal_b71185d6f16ef21f98f960e607b8eb75ea94c8b8b655d8576d081d225f5e80b7->leave($__internal_b71185d6f16ef21f98f960e607b8eb75ea94c8b8b655d8576d081d225f5e80b7_prof);

    }

    // line 212
    public function block_button_row($context, array $blocks = array())
    {
        $__internal_27dd84c071eb027c37c295a19a56da89bfa3221636b9c2ebc3f202fe9c4e3df4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_27dd84c071eb027c37c295a19a56da89bfa3221636b9c2ebc3f202fe9c4e3df4->enter($__internal_27dd84c071eb027c37c295a19a56da89bfa3221636b9c2ebc3f202fe9c4e3df4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        $__internal_e800e075d27209e7f58674cbe6abf2830c62bb45406775966e1961a692d3663f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e800e075d27209e7f58674cbe6abf2830c62bb45406775966e1961a692d3663f->enter($__internal_e800e075d27209e7f58674cbe6abf2830c62bb45406775966e1961a692d3663f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        // line 213
        echo "<div class=\"form-group\">";
        // line 214
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 215
        echo "</div>";
        
        $__internal_e800e075d27209e7f58674cbe6abf2830c62bb45406775966e1961a692d3663f->leave($__internal_e800e075d27209e7f58674cbe6abf2830c62bb45406775966e1961a692d3663f_prof);

        
        $__internal_27dd84c071eb027c37c295a19a56da89bfa3221636b9c2ebc3f202fe9c4e3df4->leave($__internal_27dd84c071eb027c37c295a19a56da89bfa3221636b9c2ebc3f202fe9c4e3df4_prof);

    }

    // line 218
    public function block_choice_row($context, array $blocks = array())
    {
        $__internal_6d393714b76ae8e7c77cfd687caa21bbd1df8108c020bf4b31e6b1ba95289a80 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6d393714b76ae8e7c77cfd687caa21bbd1df8108c020bf4b31e6b1ba95289a80->enter($__internal_6d393714b76ae8e7c77cfd687caa21bbd1df8108c020bf4b31e6b1ba95289a80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_row"));

        $__internal_a9a50fb8d8964bca1ef9073202a9c90a0267b2faedc7d9adc22a0744f906bc70 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a9a50fb8d8964bca1ef9073202a9c90a0267b2faedc7d9adc22a0744f906bc70->enter($__internal_a9a50fb8d8964bca1ef9073202a9c90a0267b2faedc7d9adc22a0744f906bc70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_row"));

        // line 219
        $context["force_error"] = true;
        // line 220
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_a9a50fb8d8964bca1ef9073202a9c90a0267b2faedc7d9adc22a0744f906bc70->leave($__internal_a9a50fb8d8964bca1ef9073202a9c90a0267b2faedc7d9adc22a0744f906bc70_prof);

        
        $__internal_6d393714b76ae8e7c77cfd687caa21bbd1df8108c020bf4b31e6b1ba95289a80->leave($__internal_6d393714b76ae8e7c77cfd687caa21bbd1df8108c020bf4b31e6b1ba95289a80_prof);

    }

    // line 223
    public function block_date_row($context, array $blocks = array())
    {
        $__internal_ab16a12b893f9a42a8aaa652f77f43587639349fbf663a85a736019b0b3cef6c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ab16a12b893f9a42a8aaa652f77f43587639349fbf663a85a736019b0b3cef6c->enter($__internal_ab16a12b893f9a42a8aaa652f77f43587639349fbf663a85a736019b0b3cef6c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_row"));

        $__internal_6438b50d105367013b53cf341ee75db5877afbc9ce0a03332b0cd4b2fdb16213 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6438b50d105367013b53cf341ee75db5877afbc9ce0a03332b0cd4b2fdb16213->enter($__internal_6438b50d105367013b53cf341ee75db5877afbc9ce0a03332b0cd4b2fdb16213_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_row"));

        // line 224
        $context["force_error"] = true;
        // line 225
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_6438b50d105367013b53cf341ee75db5877afbc9ce0a03332b0cd4b2fdb16213->leave($__internal_6438b50d105367013b53cf341ee75db5877afbc9ce0a03332b0cd4b2fdb16213_prof);

        
        $__internal_ab16a12b893f9a42a8aaa652f77f43587639349fbf663a85a736019b0b3cef6c->leave($__internal_ab16a12b893f9a42a8aaa652f77f43587639349fbf663a85a736019b0b3cef6c_prof);

    }

    // line 228
    public function block_time_row($context, array $blocks = array())
    {
        $__internal_71d4473a1551201821c72e5d0066d50ded0a7b64d53748b056156f69d3eb77bf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_71d4473a1551201821c72e5d0066d50ded0a7b64d53748b056156f69d3eb77bf->enter($__internal_71d4473a1551201821c72e5d0066d50ded0a7b64d53748b056156f69d3eb77bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_row"));

        $__internal_a663c74ea8a7a29c4bc992386703e223646b4665b5a6bd9d9510483654406575 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a663c74ea8a7a29c4bc992386703e223646b4665b5a6bd9d9510483654406575->enter($__internal_a663c74ea8a7a29c4bc992386703e223646b4665b5a6bd9d9510483654406575_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_row"));

        // line 229
        $context["force_error"] = true;
        // line 230
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_a663c74ea8a7a29c4bc992386703e223646b4665b5a6bd9d9510483654406575->leave($__internal_a663c74ea8a7a29c4bc992386703e223646b4665b5a6bd9d9510483654406575_prof);

        
        $__internal_71d4473a1551201821c72e5d0066d50ded0a7b64d53748b056156f69d3eb77bf->leave($__internal_71d4473a1551201821c72e5d0066d50ded0a7b64d53748b056156f69d3eb77bf_prof);

    }

    // line 233
    public function block_datetime_row($context, array $blocks = array())
    {
        $__internal_5aae4065339d55d9911da7377753e5a5868fd37afc9dc05de2572a177e200776 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5aae4065339d55d9911da7377753e5a5868fd37afc9dc05de2572a177e200776->enter($__internal_5aae4065339d55d9911da7377753e5a5868fd37afc9dc05de2572a177e200776_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_row"));

        $__internal_6f220629f137efe4ffb6cb67d85b2291171496fde4038f83ce6ddf2c069ae02f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6f220629f137efe4ffb6cb67d85b2291171496fde4038f83ce6ddf2c069ae02f->enter($__internal_6f220629f137efe4ffb6cb67d85b2291171496fde4038f83ce6ddf2c069ae02f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_row"));

        // line 234
        $context["force_error"] = true;
        // line 235
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_6f220629f137efe4ffb6cb67d85b2291171496fde4038f83ce6ddf2c069ae02f->leave($__internal_6f220629f137efe4ffb6cb67d85b2291171496fde4038f83ce6ddf2c069ae02f_prof);

        
        $__internal_5aae4065339d55d9911da7377753e5a5868fd37afc9dc05de2572a177e200776->leave($__internal_5aae4065339d55d9911da7377753e5a5868fd37afc9dc05de2572a177e200776_prof);

    }

    // line 238
    public function block_checkbox_row($context, array $blocks = array())
    {
        $__internal_c473b486af0de9a4ea52c7b91c68a86c514d5d893f247e6669fe1bab689c285f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c473b486af0de9a4ea52c7b91c68a86c514d5d893f247e6669fe1bab689c285f->enter($__internal_c473b486af0de9a4ea52c7b91c68a86c514d5d893f247e6669fe1bab689c285f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        $__internal_b42aff47321919ef236e5dfa55b6005acd4371608de6441640d776f6f0801a73 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b42aff47321919ef236e5dfa55b6005acd4371608de6441640d776f6f0801a73->enter($__internal_b42aff47321919ef236e5dfa55b6005acd4371608de6441640d776f6f0801a73_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        // line 239
        echo "<div class=\"form-group";
        if ( !($context["valid"] ?? $this->getContext($context, "valid"))) {
            echo " has-error";
        }
        echo "\">";
        // line 240
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 241
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 242
        echo "</div>";
        
        $__internal_b42aff47321919ef236e5dfa55b6005acd4371608de6441640d776f6f0801a73->leave($__internal_b42aff47321919ef236e5dfa55b6005acd4371608de6441640d776f6f0801a73_prof);

        
        $__internal_c473b486af0de9a4ea52c7b91c68a86c514d5d893f247e6669fe1bab689c285f->leave($__internal_c473b486af0de9a4ea52c7b91c68a86c514d5d893f247e6669fe1bab689c285f_prof);

    }

    // line 245
    public function block_radio_row($context, array $blocks = array())
    {
        $__internal_a15ad4bbdab867ecee96504f1a1306358d55bd6dbe887f619bfb5c28c2890098 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a15ad4bbdab867ecee96504f1a1306358d55bd6dbe887f619bfb5c28c2890098->enter($__internal_a15ad4bbdab867ecee96504f1a1306358d55bd6dbe887f619bfb5c28c2890098_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        $__internal_6c92abd81251a7a8d6ad16b9feb77ea624bbd26ddff8ddc9f72fb488bcc32264 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6c92abd81251a7a8d6ad16b9feb77ea624bbd26ddff8ddc9f72fb488bcc32264->enter($__internal_6c92abd81251a7a8d6ad16b9feb77ea624bbd26ddff8ddc9f72fb488bcc32264_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        // line 246
        echo "<div class=\"form-group";
        if ( !($context["valid"] ?? $this->getContext($context, "valid"))) {
            echo " has-error";
        }
        echo "\">";
        // line 247
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 248
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 249
        echo "</div>";
        
        $__internal_6c92abd81251a7a8d6ad16b9feb77ea624bbd26ddff8ddc9f72fb488bcc32264->leave($__internal_6c92abd81251a7a8d6ad16b9feb77ea624bbd26ddff8ddc9f72fb488bcc32264_prof);

        
        $__internal_a15ad4bbdab867ecee96504f1a1306358d55bd6dbe887f619bfb5c28c2890098->leave($__internal_a15ad4bbdab867ecee96504f1a1306358d55bd6dbe887f619bfb5c28c2890098_prof);

    }

    // line 254
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_809f65743f662a4333bf5ac4b926d276038b0894def127861f9315823de85e58 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_809f65743f662a4333bf5ac4b926d276038b0894def127861f9315823de85e58->enter($__internal_809f65743f662a4333bf5ac4b926d276038b0894def127861f9315823de85e58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        $__internal_bf42d3f86819907b329e10fab4489565e64f73045478695f32b66ba4b0b8baaf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bf42d3f86819907b329e10fab4489565e64f73045478695f32b66ba4b0b8baaf->enter($__internal_bf42d3f86819907b329e10fab4489565e64f73045478695f32b66ba4b0b8baaf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 255
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 256
            if ($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "parent", array())) {
                echo "<span class=\"help-block\">";
            } else {
                echo "<div class=\"alert alert-danger\">";
            }
            // line 257
            echo "    <ul class=\"list-unstyled\">";
            // line 258
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? $this->getContext($context, "errors")));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 259
                echo "<li><span class=\"glyphicon glyphicon-exclamation-sign\"></span> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "</li>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 261
            echo "</ul>
    ";
            // line 262
            if ($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "parent", array())) {
                echo "</span>";
            } else {
                echo "</div>";
            }
        }
        
        $__internal_bf42d3f86819907b329e10fab4489565e64f73045478695f32b66ba4b0b8baaf->leave($__internal_bf42d3f86819907b329e10fab4489565e64f73045478695f32b66ba4b0b8baaf_prof);

        
        $__internal_809f65743f662a4333bf5ac4b926d276038b0894def127861f9315823de85e58->leave($__internal_809f65743f662a4333bf5ac4b926d276038b0894def127861f9315823de85e58_prof);

    }

    public function getTemplateName()
    {
        return "bootstrap_3_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  1061 => 262,  1058 => 261,  1050 => 259,  1046 => 258,  1044 => 257,  1038 => 256,  1036 => 255,  1027 => 254,  1017 => 249,  1015 => 248,  1013 => 247,  1007 => 246,  998 => 245,  988 => 242,  986 => 241,  984 => 240,  978 => 239,  969 => 238,  959 => 235,  957 => 234,  948 => 233,  938 => 230,  936 => 229,  927 => 228,  917 => 225,  915 => 224,  906 => 223,  896 => 220,  894 => 219,  885 => 218,  875 => 215,  873 => 214,  871 => 213,  862 => 212,  852 => 209,  850 => 208,  848 => 207,  846 => 206,  840 => 205,  831 => 204,  819 => 198,  815 => 197,  800 => 196,  796 => 193,  793 => 190,  792 => 189,  791 => 188,  789 => 187,  786 => 186,  783 => 185,  780 => 184,  777 => 183,  774 => 182,  771 => 181,  768 => 180,  765 => 179,  763 => 178,  754 => 177,  744 => 174,  735 => 173,  725 => 170,  716 => 169,  706 => 166,  704 => 165,  695 => 163,  685 => 160,  683 => 159,  674 => 158,  663 => 152,  661 => 151,  659 => 150,  656 => 148,  654 => 147,  652 => 146,  643 => 145,  632 => 141,  630 => 140,  628 => 139,  625 => 137,  623 => 136,  621 => 135,  612 => 134,  601 => 130,  595 => 127,  594 => 126,  593 => 125,  589 => 124,  585 => 123,  578 => 119,  577 => 118,  576 => 117,  572 => 116,  570 => 115,  561 => 114,  551 => 111,  549 => 110,  540 => 109,  529 => 105,  525 => 104,  521 => 103,  517 => 102,  513 => 101,  509 => 100,  505 => 99,  501 => 98,  497 => 97,  495 => 96,  491 => 95,  489 => 94,  486 => 92,  484 => 91,  475 => 90,  463 => 85,  460 => 84,  450 => 83,  445 => 81,  443 => 80,  441 => 79,  438 => 77,  436 => 76,  427 => 75,  415 => 70,  413 => 69,  411 => 67,  410 => 66,  409 => 65,  408 => 64,  403 => 62,  401 => 61,  399 => 60,  396 => 58,  394 => 57,  385 => 56,  374 => 52,  372 => 51,  370 => 50,  368 => 49,  366 => 48,  362 => 47,  360 => 46,  357 => 44,  355 => 43,  346 => 42,  335 => 38,  333 => 37,  331 => 36,  322 => 35,  312 => 32,  306 => 30,  304 => 29,  302 => 28,  296 => 26,  293 => 25,  291 => 24,  288 => 23,  279 => 22,  269 => 19,  267 => 18,  258 => 17,  248 => 14,  246 => 13,  237 => 12,  227 => 9,  224 => 7,  222 => 6,  213 => 5,  203 => 254,  200 => 253,  197 => 251,  195 => 245,  192 => 244,  190 => 238,  187 => 237,  185 => 233,  182 => 232,  180 => 228,  177 => 227,  175 => 223,  172 => 222,  170 => 218,  167 => 217,  165 => 212,  162 => 211,  160 => 204,  157 => 203,  154 => 201,  152 => 177,  149 => 176,  147 => 173,  144 => 172,  142 => 169,  139 => 168,  137 => 163,  134 => 162,  132 => 158,  129 => 157,  126 => 155,  124 => 145,  121 => 144,  119 => 134,  116 => 133,  114 => 114,  111 => 113,  109 => 109,  107 => 90,  105 => 75,  102 => 74,  100 => 56,  97 => 55,  95 => 42,  92 => 41,  90 => 35,  87 => 34,  85 => 22,  82 => 21,  80 => 17,  77 => 16,  75 => 12,  72 => 11,  70 => 5,  67 => 4,  64 => 2,  14 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"form_div_layout.html.twig\" %}

{# Widgets #}

{% block form_widget_simple -%}
    {% if type is not defined or type not in ['file', 'hidden'] %}
        {%- set attr = attr|merge({class: (attr.class|default('') ~ ' form-control')|trim}) -%}
    {% endif %}
    {{- parent() -}}
{%- endblock form_widget_simple %}

{% block textarea_widget -%}
    {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-control')|trim}) %}
    {{- parent() -}}
{%- endblock textarea_widget %}

{% block button_widget -%}
    {% set attr = attr|merge({class: (attr.class|default('btn-default') ~ ' btn')|trim}) %}
    {{- parent() -}}
{%- endblock %}

{% block money_widget -%}
    <div class=\"input-group\">
        {% set append = money_pattern starts with '{{' %}
        {% if not append %}
            <span class=\"input-group-addon\">{{ money_pattern|replace({ '{{ widget }}':''}) }}</span>
        {% endif %}
        {{- block('form_widget_simple') -}}
        {% if append %}
            <span class=\"input-group-addon\">{{ money_pattern|replace({ '{{ widget }}':''}) }}</span>
        {% endif %}
    </div>
{%- endblock money_widget %}

{% block percent_widget -%}
    <div class=\"input-group\">
        {{- block('form_widget_simple') -}}
        <span class=\"input-group-addon\">%</span>
    </div>
{%- endblock percent_widget %}

{% block datetime_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-inline')|trim}) -%}
        <div {{ block('widget_container_attributes') }}>
            {{- form_errors(form.date) -}}
            {{- form_errors(form.time) -}}
            {{- form_widget(form.date, { datetime: true } ) -}}
            {{- form_widget(form.time, { datetime: true } ) -}}
        </div>
    {%- endif %}
{%- endblock datetime_widget %}

{% block date_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-inline')|trim}) -%}
        {% if datetime is not defined or not datetime -%}
            <div {{ block('widget_container_attributes') -}}>
        {%- endif %}
            {{- date_pattern|replace({
                '{{ year }}': form_widget(form.year),
                '{{ month }}': form_widget(form.month),
                '{{ day }}': form_widget(form.day),
            })|raw -}}
        {% if datetime is not defined or not datetime -%}
            </div>
        {%- endif -%}
    {% endif %}
{%- endblock date_widget %}

{% block time_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-inline')|trim}) -%}
        {% if datetime is not defined or false == datetime -%}
            <div {{ block('widget_container_attributes') -}}>
        {%- endif -%}
        {{- form_widget(form.hour) }}{% if with_minutes %}:{{ form_widget(form.minute) }}{% endif %}{% if with_seconds %}:{{ form_widget(form.second) }}{% endif %}
        {% if datetime is not defined or false == datetime -%}
            </div>
        {%- endif -%}
    {% endif %}
{%- endblock time_widget %}

{%- block dateinterval_widget -%}
    {%- if widget == 'single_text' -%}
        {{- block('form_widget_simple') -}}
    {%- else -%}
        {%- set attr = attr|merge({class: (attr.class|default('') ~ ' form-inline')|trim}) -%}
        <div {{ block('widget_container_attributes') }}>
            {{- form_errors(form) -}}
            {%- if with_years %}{{ form_widget(form.years) }}{% endif -%}
            {%- if with_months %}{{ form_widget(form.months) }}{% endif -%}
            {%- if with_weeks %}{{ form_widget(form.weeks) }}{% endif -%}
            {%- if with_days %}{{ form_widget(form.days) }}{% endif -%}
            {%- if with_hours %}{{ form_widget(form.hours) }}{% endif -%}
            {%- if with_minutes %}{{ form_widget(form.minutes) }}{% endif -%}
            {%- if with_seconds %}{{ form_widget(form.seconds) }}{% endif -%}
            {%- if with_invert %}{{ form_widget(form.invert) }}{% endif -%}
        </div>
    {%- endif -%}
{%- endblock dateinterval_widget -%}

{% block choice_widget_collapsed -%}
    {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-control')|trim}) %}
    {{- parent() -}}
{%- endblock %}

{% block choice_widget_expanded -%}
    {% if '-inline' in label_attr.class|default('') -%}
        {%- for child in form %}
            {{- form_widget(child, {
                parent_label_class: label_attr.class|default(''),
                translation_domain: choice_translation_domain,
            }) -}}
        {% endfor -%}
    {%- else -%}
        <div {{ block('widget_container_attributes') }}>
            {%- for child in form %}
                {{- form_widget(child, {
                    parent_label_class: label_attr.class|default(''),
                    translation_domain: choice_translation_domain,
                }) -}}
            {% endfor -%}
        </div>
    {%- endif %}
{%- endblock choice_widget_expanded %}

{% block checkbox_widget -%}
    {%- set parent_label_class = parent_label_class|default(label_attr.class|default('')) -%}
    {% if 'checkbox-inline' in parent_label_class %}
        {{- form_label(form, null, { widget: parent() }) -}}
    {% else -%}
        <div class=\"checkbox\">
            {{- form_label(form, null, { widget: parent() }) -}}
        </div>
    {%- endif %}
{%- endblock checkbox_widget %}

{% block radio_widget -%}
    {%- set parent_label_class = parent_label_class|default(label_attr.class|default('')) -%}
    {% if 'radio-inline' in parent_label_class %}
        {{- form_label(form, null, { widget: parent() }) -}}
    {% else -%}
        <div class=\"radio\">
            {{- form_label(form, null, { widget: parent() }) -}}
        </div>
    {%- endif %}
{%- endblock radio_widget %}

{# Labels #}

{% block form_label -%}
    {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' control-label')|trim}) -%}
    {{- parent() -}}
{%- endblock form_label %}

{% block choice_label -%}
    {# remove the checkbox-inline and radio-inline class, it's only useful for embed labels #}
    {%- set label_attr = label_attr|merge({class: label_attr.class|default('')|replace({'checkbox-inline': '', 'radio-inline': ''})|trim}) -%}
    {{- block('form_label') -}}
{% endblock %}

{% block checkbox_label -%}
    {{- block('checkbox_radio_label') -}}
{%- endblock checkbox_label %}

{% block radio_label -%}
    {{- block('checkbox_radio_label') -}}
{%- endblock radio_label %}

{% block checkbox_radio_label %}
    {# Do not display the label if widget is not defined in order to prevent double label rendering #}
    {% if widget is defined %}
        {% if required %}
            {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' required')|trim}) %}
        {% endif %}
        {% if parent_label_class is defined %}
            {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' ' ~ parent_label_class)|trim}) %}
        {% endif %}
        {% if label is not same as(false) and label is empty %}
            {%- if label_format is not empty -%}
                {% set label = label_format|replace({
                    '%name%': name,
                    '%id%': id,
                }) %}
            {%- else -%}
                {% set label = name|humanize %}
            {%- endif -%}
        {% endif %}
        <label{% for attrname, attrvalue in label_attr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}>
            {{- widget|raw }} {{ label is not same as(false) ? (translation_domain is same as(false) ? label : label|trans({}, translation_domain)) -}}
        </label>
    {% endif %}
{% endblock checkbox_radio_label %}

{# Rows #}

{% block form_row -%}
    <div class=\"form-group{% if (not compound or force_error|default(false)) and not valid %} has-error{% endif %}\">
        {{- form_label(form) -}}
        {{- form_widget(form) -}}
        {{- form_errors(form) -}}
    </div>
{%- endblock form_row %}

{% block button_row -%}
    <div class=\"form-group\">
        {{- form_widget(form) -}}
    </div>
{%- endblock button_row %}

{% block choice_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock choice_row %}

{% block date_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock date_row %}

{% block time_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock time_row %}

{% block datetime_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock datetime_row %}

{% block checkbox_row -%}
    <div class=\"form-group{% if not valid %} has-error{% endif %}\">
        {{- form_widget(form) -}}
        {{- form_errors(form) -}}
    </div>
{%- endblock checkbox_row %}

{% block radio_row -%}
    <div class=\"form-group{% if not valid %} has-error{% endif %}\">
        {{- form_widget(form) -}}
        {{- form_errors(form) -}}
    </div>
{%- endblock radio_row %}

{# Errors #}

{% block form_errors -%}
    {% if errors|length > 0 -%}
    {% if form.parent %}<span class=\"help-block\">{% else %}<div class=\"alert alert-danger\">{% endif %}
    <ul class=\"list-unstyled\">
        {%- for error in errors -%}
            <li><span class=\"glyphicon glyphicon-exclamation-sign\"></span> {{ error.message }}</li>
        {%- endfor -%}
    </ul>
    {% if form.parent %}</span>{% else %}</div>{% endif %}
    {%- endif %}
{%- endblock form_errors %}
", "bootstrap_3_layout.html.twig", "C:\\wamp64\\www\\stock\\vendor\\symfony\\symfony\\src\\Symfony\\Bridge\\Twig\\Resources\\views\\Form\\bootstrap_3_layout.html.twig");
    }
}
