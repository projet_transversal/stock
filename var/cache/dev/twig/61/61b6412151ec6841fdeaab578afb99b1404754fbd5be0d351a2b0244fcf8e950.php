<?php

/* StockBundle:Representant:listerRepresentant.html.twig */
class __TwigTemplate_82f3efaa9962d071f5cee2bbcac97d98d019596687690a245f14bb974bad3bdf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:Representant:listerRepresentant.html.twig", 1);
        $this->blocks = array(
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_988febfaf36dfe00df72e8ac038f10eec9add6b72aba4d8d5b9f8b8bb6c54a7f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_988febfaf36dfe00df72e8ac038f10eec9add6b72aba4d8d5b9f8b8bb6c54a7f->enter($__internal_988febfaf36dfe00df72e8ac038f10eec9add6b72aba4d8d5b9f8b8bb6c54a7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Representant:listerRepresentant.html.twig"));

        $__internal_81ffeebe917ae559dd81df21577d0d328e3089fa1abd20e6c0556d4d826c7e19 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_81ffeebe917ae559dd81df21577d0d328e3089fa1abd20e6c0556d4d826c7e19->enter($__internal_81ffeebe917ae559dd81df21577d0d328e3089fa1abd20e6c0556d4d826c7e19_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Representant:listerRepresentant.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_988febfaf36dfe00df72e8ac038f10eec9add6b72aba4d8d5b9f8b8bb6c54a7f->leave($__internal_988febfaf36dfe00df72e8ac038f10eec9add6b72aba4d8d5b9f8b8bb6c54a7f_prof);

        
        $__internal_81ffeebe917ae559dd81df21577d0d328e3089fa1abd20e6c0556d4d826c7e19->leave($__internal_81ffeebe917ae559dd81df21577d0d328e3089fa1abd20e6c0556d4d826c7e19_prof);

    }

    // line 2
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_4655ef182ad1bdac0e5727a3b8362a9e7409fe74897d43fdebb55978c5380454 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4655ef182ad1bdac0e5727a3b8362a9e7409fe74897d43fdebb55978c5380454->enter($__internal_4655ef182ad1bdac0e5727a3b8362a9e7409fe74897d43fdebb55978c5380454_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_43f184a10cf96920c4e69d1af44b70035d69df1d31436e21d899ead751820664 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_43f184a10cf96920c4e69d1af44b70035d69df1d31436e21d899ead751820664->enter($__internal_43f184a10cf96920c4e69d1af44b70035d69df1d31436e21d899ead751820664_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        echo "<!--le block du milieu de la page-->
    ";
        // line 3
        $this->displayBlock('debutPage', $context, $blocks);
        // line 11
        echo "    ";
        $this->displayBlock('modules', $context, $blocks);
        
        $__internal_43f184a10cf96920c4e69d1af44b70035d69df1d31436e21d899ead751820664->leave($__internal_43f184a10cf96920c4e69d1af44b70035d69df1d31436e21d899ead751820664_prof);

        
        $__internal_4655ef182ad1bdac0e5727a3b8362a9e7409fe74897d43fdebb55978c5380454->leave($__internal_4655ef182ad1bdac0e5727a3b8362a9e7409fe74897d43fdebb55978c5380454_prof);

    }

    // line 3
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_23803fcdd3b2f0a82abe053d347cbb6abf69fc135fde271653d2d8e1abc462aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_23803fcdd3b2f0a82abe053d347cbb6abf69fc135fde271653d2d8e1abc462aa->enter($__internal_23803fcdd3b2f0a82abe053d347cbb6abf69fc135fde271653d2d8e1abc462aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_9db7c88eb0e61da5296e5810b6fbfe410c524f27ed017627ca684490173c11d4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9db7c88eb0e61da5296e5810b6fbfe410c524f27ed017627ca684490173c11d4->enter($__internal_9db7c88eb0e61da5296e5810b6fbfe410c524f27ed017627ca684490173c11d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 4
        echo "        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                <h3 style=\"color: white;margin-top:4%\"> <center>Liste des Representants</center></h3>
            </div>
            <br><br>
        </div>
    ";
        
        $__internal_9db7c88eb0e61da5296e5810b6fbfe410c524f27ed017627ca684490173c11d4->leave($__internal_9db7c88eb0e61da5296e5810b6fbfe410c524f27ed017627ca684490173c11d4_prof);

        
        $__internal_23803fcdd3b2f0a82abe053d347cbb6abf69fc135fde271653d2d8e1abc462aa->leave($__internal_23803fcdd3b2f0a82abe053d347cbb6abf69fc135fde271653d2d8e1abc462aa_prof);

    }

    // line 11
    public function block_modules($context, array $blocks = array())
    {
        $__internal_a84ca667a1c0354a54f2d1cec84053d4ba28e5a85842e616f9d5631ebff054c1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a84ca667a1c0354a54f2d1cec84053d4ba28e5a85842e616f9d5631ebff054c1->enter($__internal_a84ca667a1c0354a54f2d1cec84053d4ba28e5a85842e616f9d5631ebff054c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_7e30b21e4bb6c846a26890eb9fb42c47a4eebcf15ab5eaca5a34921bccd39bd2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e30b21e4bb6c846a26890eb9fb42c47a4eebcf15ab5eaca5a34921bccd39bd2->enter($__internal_7e30b21e4bb6c846a26890eb9fb42c47a4eebcf15ab5eaca5a34921bccd39bd2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        echo "<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"margin-left: -12%\">

                    <div class=\"\">
                        <table id=\"table\" class=\"display table table-striped table-bordered\" cellspacing=\"0\" width=\"100%\" id=\"table\" style=\"margin-left:-11%;margin-top: -10%\">

                            <thead>
                            <tr class=\"jumbotron\">
                                <th data-field=\"id\">ID</th>
                                <th data-field=\"name\" >Nom</th>
                                <th data-field=\"telephone\" >Telephone</th>
                                <th data-field=\"adresse\" >Adresse</th>
                                <th data-field=\"mail\" >Courriel</th>
                                <th data-field=\"Modifier\" >Modifier</th>
                                <th data-field=\"Supprimer\" >Supprimer</th>
                            </tr>
                            </thead>
                            <tbody>
                            ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["liste"] ?? $this->getContext($context, "liste")));
        foreach ($context['_seq'] as $context["_key"] => $context["l"]) {
            // line 31
            echo "                                <tr>
                                    <td id=\"id\">";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "id", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "login", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "telephone", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "adresse", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "email", array()), "html", null, true);
            echo "</td>
                                    <td><i class=\"font-icon font-icon-pen\" style=\"color: green\"></i></td>
                                    <td><i class=\"font-icon font-icon-del\" style=\"color: red\"></i></td>
                                </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['l'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "                            </tbody>
                        </table>
                    </div>

            </div><!--.container-fluid-->
        </div><!--.page-content-->
    ";
        
        $__internal_7e30b21e4bb6c846a26890eb9fb42c47a4eebcf15ab5eaca5a34921bccd39bd2->leave($__internal_7e30b21e4bb6c846a26890eb9fb42c47a4eebcf15ab5eaca5a34921bccd39bd2_prof);

        
        $__internal_a84ca667a1c0354a54f2d1cec84053d4ba28e5a85842e616f9d5631ebff054c1->leave($__internal_a84ca667a1c0354a54f2d1cec84053d4ba28e5a85842e616f9d5631ebff054c1_prof);

    }

    // line 49
    public function block_script($context, array $blocks = array())
    {
        $__internal_3dfe1697a008a4f67c96e4876583e0099b20b27c6595abf5e43163beeb260363 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3dfe1697a008a4f67c96e4876583e0099b20b27c6595abf5e43163beeb260363->enter($__internal_3dfe1697a008a4f67c96e4876583e0099b20b27c6595abf5e43163beeb260363_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        $__internal_192a4ee9144a058dcf40dd756b9a54f2738555ae46719e8ca84850cc4ad0d879 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_192a4ee9144a058dcf40dd756b9a54f2738555ae46719e8ca84850cc4ad0d879->enter($__internal_192a4ee9144a058dcf40dd756b9a54f2738555ae46719e8ca84850cc4ad0d879_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        // line 50
        echo "    ";
        $this->displayParentBlock("script", $context, $blocks);
        echo "
    <script src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/jquery.tablednd.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/listerRepresentant.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_192a4ee9144a058dcf40dd756b9a54f2738555ae46719e8ca84850cc4ad0d879->leave($__internal_192a4ee9144a058dcf40dd756b9a54f2738555ae46719e8ca84850cc4ad0d879_prof);

        
        $__internal_3dfe1697a008a4f67c96e4876583e0099b20b27c6595abf5e43163beeb260363->leave($__internal_3dfe1697a008a4f67c96e4876583e0099b20b27c6595abf5e43163beeb260363_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Representant:listerRepresentant.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  197 => 54,  193 => 53,  189 => 52,  185 => 51,  180 => 50,  171 => 49,  155 => 41,  144 => 36,  140 => 35,  136 => 34,  132 => 33,  128 => 32,  125 => 31,  121 => 30,  92 => 11,  76 => 4,  67 => 3,  56 => 11,  54 => 3,  43 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}
{% block contenuDuMilieu %}<!--le block du milieu de la page-->
    {% block debutPage %}
        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                <h3 style=\"color: white;margin-top:4%\"> <center>Liste des Representants</center></h3>
            </div>
            <br><br>
        </div>
    {% endblock %}
    {% block modules %}<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"margin-left: -12%\">

                    <div class=\"\">
                        <table id=\"table\" class=\"display table table-striped table-bordered\" cellspacing=\"0\" width=\"100%\" id=\"table\" style=\"margin-left:-11%;margin-top: -10%\">

                            <thead>
                            <tr class=\"jumbotron\">
                                <th data-field=\"id\">ID</th>
                                <th data-field=\"name\" >Nom</th>
                                <th data-field=\"telephone\" >Telephone</th>
                                <th data-field=\"adresse\" >Adresse</th>
                                <th data-field=\"mail\" >Courriel</th>
                                <th data-field=\"Modifier\" >Modifier</th>
                                <th data-field=\"Supprimer\" >Supprimer</th>
                            </tr>
                            </thead>
                            <tbody>
                            {% for l in liste %}
                                <tr>
                                    <td id=\"id\">{{ l.id }}</td>
                                    <td>{{ l.login }}</td>
                                    <td>{{ l.telephone }}</td>
                                    <td>{{ l.adresse }}</td>
                                    <td>{{ l.email }}</td>
                                    <td><i class=\"font-icon font-icon-pen\" style=\"color: green\"></i></td>
                                    <td><i class=\"font-icon font-icon-del\" style=\"color: red\"></i></td>
                                </tr>
                            {% endfor %}
                            </tbody>
                        </table>
                    </div>

            </div><!--.container-fluid-->
        </div><!--.page-content-->
    {% endblock %}
{% endblock %}
{% block script %}
    {{ parent() }}
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/jquery.tablednd.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/listerRepresentant.js') }}\"></script>
{% endblock %}", "StockBundle:Representant:listerRepresentant.html.twig", "C:\\wamp64\\www\\stock\\src\\StockBundle/Resources/views/Representant/listerRepresentant.html.twig");
    }
}
