<?php

/* StockBundle:Trans:listerContratMarchandise.html.twig */
class __TwigTemplate_6618ce1df2ad525c70e87e48260c03bf797eedf4e569b2682f925b61e468949a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:Trans:listerContratMarchandise.html.twig", 1);
        $this->blocks = array(
            'menu' => array($this, 'block_menu'),
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4bfc1da879b57d1c68711076476f1c94c03b48dbdf591e4b00e49e42e5594e1b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4bfc1da879b57d1c68711076476f1c94c03b48dbdf591e4b00e49e42e5594e1b->enter($__internal_4bfc1da879b57d1c68711076476f1c94c03b48dbdf591e4b00e49e42e5594e1b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Trans:listerContratMarchandise.html.twig"));

        $__internal_93f404dcbcc422a610309a4ba8ea42fb0d694e29e4c962f5f15682de7f857ff4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_93f404dcbcc422a610309a4ba8ea42fb0d694e29e4c962f5f15682de7f857ff4->enter($__internal_93f404dcbcc422a610309a4ba8ea42fb0d694e29e4c962f5f15682de7f857ff4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Trans:listerContratMarchandise.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4bfc1da879b57d1c68711076476f1c94c03b48dbdf591e4b00e49e42e5594e1b->leave($__internal_4bfc1da879b57d1c68711076476f1c94c03b48dbdf591e4b00e49e42e5594e1b_prof);

        
        $__internal_93f404dcbcc422a610309a4ba8ea42fb0d694e29e4c962f5f15682de7f857ff4->leave($__internal_93f404dcbcc422a610309a4ba8ea42fb0d694e29e4c962f5f15682de7f857ff4_prof);

    }

    // line 4
    public function block_menu($context, array $blocks = array())
    {
        $__internal_c050e7b3cc608d46c10b7ac00e47864c38749228e57691938ff902c3797a899a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c050e7b3cc608d46c10b7ac00e47864c38749228e57691938ff902c3797a899a->enter($__internal_c050e7b3cc608d46c10b7ac00e47864c38749228e57691938ff902c3797a899a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_fe61ef5d3eebfcdafa7204298633e042c0386db3d70f24ae9437d3c5713db74a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fe61ef5d3eebfcdafa7204298633e042c0386db3d70f24ae9437d3c5713db74a->enter($__internal_fe61ef5d3eebfcdafa7204298633e042c0386db3d70f24ae9437d3c5713db74a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 5
        echo "\t\t<nav class=\"side-menu side-menu-big-icon\" style=\"background: #3299CC;overflow: hidden\">
            <nav class=\"sidebar-header\">
                <img src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/man.png"), "html", null, true);
        echo "\" style=\"margin-left:35%;margin-top:-15%;\"><br><br>
                <h5 style=\"text-align:center\">User Name</h5>
                <h5 style=\"text-align:center\">Profil</h5>
            </nav>
            <hr style=\"margin-left:20%;margin-right: 20%;background: red;height:1px\">
\t\t<ul class=\"side-menu-list\">
                <li class=\"opened\">
                    <span>
                    \t<a href=\"";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("livrer_marchandise");
        echo "\" >
\t                        <i class=\"glyphicon glyphicon-inbox\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Livrer marchandise</span>
                        </a>
                    </span>
                </li>
\t\t\t\t<li class=\"opened\">
                    <span>
                    \t<a href=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_marchandises");
        echo "\" >
\t                        <i class=\"glyphicon glyphicon-th-list\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Lister marchandise</span>
                        </a>
                    </span>
                </li>
\t\t\t\t<li class=\"opened\">
                    <span>
                    \t<a href=\"";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_comm");
        echo "\" >
\t                        <i class=\"glyphicon glyphicon-plus\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Ajouter commerçant</span>
                        </a>
                    </span>
                </li>
\t\t</ul>
\t  </nav>
\t ";
        
        $__internal_fe61ef5d3eebfcdafa7204298633e042c0386db3d70f24ae9437d3c5713db74a->leave($__internal_fe61ef5d3eebfcdafa7204298633e042c0386db3d70f24ae9437d3c5713db74a_prof);

        
        $__internal_c050e7b3cc608d46c10b7ac00e47864c38749228e57691938ff902c3797a899a->leave($__internal_c050e7b3cc608d46c10b7ac00e47864c38749228e57691938ff902c3797a899a_prof);

    }

    // line 40
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_409cff30f32367cb50ffde874d14f36ce8e72657e402bb0dcb2d927cd7e913dc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_409cff30f32367cb50ffde874d14f36ce8e72657e402bb0dcb2d927cd7e913dc->enter($__internal_409cff30f32367cb50ffde874d14f36ce8e72657e402bb0dcb2d927cd7e913dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_010df40b2ba8d8088fff1dc1bd2f47b9ebc239b679360642d3ff6d0d50a4d33b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_010df40b2ba8d8088fff1dc1bd2f47b9ebc239b679360642d3ff6d0d50a4d33b->enter($__internal_010df40b2ba8d8088fff1dc1bd2f47b9ebc239b679360642d3ff6d0d50a4d33b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        echo "<!--le block du milieu de la page-->
    ";
        // line 41
        $this->displayBlock('debutPage', $context, $blocks);
        // line 49
        echo "    ";
        $this->displayBlock('modules', $context, $blocks);
        
        $__internal_010df40b2ba8d8088fff1dc1bd2f47b9ebc239b679360642d3ff6d0d50a4d33b->leave($__internal_010df40b2ba8d8088fff1dc1bd2f47b9ebc239b679360642d3ff6d0d50a4d33b_prof);

        
        $__internal_409cff30f32367cb50ffde874d14f36ce8e72657e402bb0dcb2d927cd7e913dc->leave($__internal_409cff30f32367cb50ffde874d14f36ce8e72657e402bb0dcb2d927cd7e913dc_prof);

    }

    // line 41
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_3a340252fe1e4a70741ca7e1435b3b6e88b16772cae1bbc543669b94947d560e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3a340252fe1e4a70741ca7e1435b3b6e88b16772cae1bbc543669b94947d560e->enter($__internal_3a340252fe1e4a70741ca7e1435b3b6e88b16772cae1bbc543669b94947d560e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_abe5c4358e50dbafe2635e57dfdc0c607b26a3c80de66e3a268e4a0fe376ec2f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_abe5c4358e50dbafe2635e57dfdc0c607b26a3c80de66e3a268e4a0fe376ec2f->enter($__internal_abe5c4358e50dbafe2635e57dfdc0c607b26a3c80de66e3a268e4a0fe376ec2f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 42
        echo "        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-26px;\">
                <h2 style=\"color: white;margin-top:4%\"> <center>Liste des Marchandises</center></h2>
            </div>
            <br><br>
        </div>
    ";
        
        $__internal_abe5c4358e50dbafe2635e57dfdc0c607b26a3c80de66e3a268e4a0fe376ec2f->leave($__internal_abe5c4358e50dbafe2635e57dfdc0c607b26a3c80de66e3a268e4a0fe376ec2f_prof);

        
        $__internal_3a340252fe1e4a70741ca7e1435b3b6e88b16772cae1bbc543669b94947d560e->leave($__internal_3a340252fe1e4a70741ca7e1435b3b6e88b16772cae1bbc543669b94947d560e_prof);

    }

    // line 49
    public function block_modules($context, array $blocks = array())
    {
        $__internal_bf24124bb4936d8711110dd12671e53a463175f0cd84db48f6278d3f2555f371 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bf24124bb4936d8711110dd12671e53a463175f0cd84db48f6278d3f2555f371->enter($__internal_bf24124bb4936d8711110dd12671e53a463175f0cd84db48f6278d3f2555f371_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_34685a8b8aaa2508f11f170cbd0c3b07fe642ed1784bd0e76abe24caac7eeb30 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_34685a8b8aaa2508f11f170cbd0c3b07fe642ed1784bd0e76abe24caac7eeb30->enter($__internal_34685a8b8aaa2508f11f170cbd0c3b07fe642ed1784bd0e76abe24caac7eeb30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        echo "<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"margin-left: -12%\">
                <section class=\"box-typical\">
                    <div id=\"toolbar\">
                        <h1 style=\"font-weight: bold\">Liste des Marchandises</h1>
                    </div>
                    <div class=\"table-responsive\">
                        <table id=\"table\"
                               data-pagination=\"true\"
                               data-search=\"true\"
                               data-use-row-attr-func=\"true\"
                               data-reorderable-rows=\"true\"
                               data-toolbar=\"#toolbar\">
                            <thead>
                            <tr >
                                <th data-field=\"id\">ID</th>
                                <th data-field=\"name\" >Libelle</th>
                                <th data-field=\"price\" >Categorie</th>
                                <th data-field=\"Modifier\" >Modifier</th>
                                <th data-field=\"Supprimer\" >Supprimer</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </section><!--.box-typical-->

            </div><!--.container-fluid-->
        </div><!--.page-content-->
    ";
        
        $__internal_34685a8b8aaa2508f11f170cbd0c3b07fe642ed1784bd0e76abe24caac7eeb30->leave($__internal_34685a8b8aaa2508f11f170cbd0c3b07fe642ed1784bd0e76abe24caac7eeb30_prof);

        
        $__internal_bf24124bb4936d8711110dd12671e53a463175f0cd84db48f6278d3f2555f371->leave($__internal_bf24124bb4936d8711110dd12671e53a463175f0cd84db48f6278d3f2555f371_prof);

    }

    // line 80
    public function block_script($context, array $blocks = array())
    {
        $__internal_c73d6323b5c021a35464ecf41cfaa31650302623b702d7aeae0f0ad300a2f90c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c73d6323b5c021a35464ecf41cfaa31650302623b702d7aeae0f0ad300a2f90c->enter($__internal_c73d6323b5c021a35464ecf41cfaa31650302623b702d7aeae0f0ad300a2f90c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        $__internal_80eab2755e83ec18f7c6d6a06220a89c22a46fe302e42be98d451eb282a82049 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_80eab2755e83ec18f7c6d6a06220a89c22a46fe302e42be98d451eb282a82049->enter($__internal_80eab2755e83ec18f7c6d6a06220a89c22a46fe302e42be98d451eb282a82049_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        // line 81
        echo "    ";
        $this->displayParentBlock("script", $context, $blocks);
        echo "
    <script src=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/jquery.tablednd.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table-reorder-rows-init.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_80eab2755e83ec18f7c6d6a06220a89c22a46fe302e42be98d451eb282a82049->leave($__internal_80eab2755e83ec18f7c6d6a06220a89c22a46fe302e42be98d451eb282a82049_prof);

        
        $__internal_c73d6323b5c021a35464ecf41cfaa31650302623b702d7aeae0f0ad300a2f90c->leave($__internal_c73d6323b5c021a35464ecf41cfaa31650302623b702d7aeae0f0ad300a2f90c_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Trans:listerContratMarchandise.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  231 => 85,  227 => 84,  223 => 83,  219 => 82,  214 => 81,  205 => 80,  158 => 49,  142 => 42,  133 => 41,  122 => 49,  120 => 41,  109 => 40,  90 => 31,  79 => 23,  68 => 15,  57 => 7,  53 => 5,  44 => 4,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}


{% block menu %}
\t\t<nav class=\"side-menu side-menu-big-icon\" style=\"background: #3299CC;overflow: hidden\">
            <nav class=\"sidebar-header\">
                <img src=\"{{asset('img/man.png')}}\" style=\"margin-left:35%;margin-top:-15%;\"><br><br>
                <h5 style=\"text-align:center\">User Name</h5>
                <h5 style=\"text-align:center\">Profil</h5>
            </nav>
            <hr style=\"margin-left:20%;margin-right: 20%;background: red;height:1px\">
\t\t<ul class=\"side-menu-list\">
                <li class=\"opened\">
                    <span>
                    \t<a href=\"{{ path('livrer_marchandise') }}\" >
\t                        <i class=\"glyphicon glyphicon-inbox\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Livrer marchandise</span>
                        </a>
                    </span>
                </li>
\t\t\t\t<li class=\"opened\">
                    <span>
                    \t<a href=\"{{ path('lister_marchandises') }}\" >
\t                        <i class=\"glyphicon glyphicon-th-list\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Lister marchandise</span>
                        </a>
                    </span>
                </li>
\t\t\t\t<li class=\"opened\">
                    <span>
                    \t<a href=\"{{ path('ajouter_comm') }}\" >
\t                        <i class=\"glyphicon glyphicon-plus\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Ajouter commerçant</span>
                        </a>
                    </span>
                </li>
\t\t</ul>
\t  </nav>
\t {% endblock %}
{% block contenuDuMilieu %}<!--le block du milieu de la page-->
    {% block debutPage %}
        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-26px;\">
                <h2 style=\"color: white;margin-top:4%\"> <center>Liste des Marchandises</center></h2>
            </div>
            <br><br>
        </div>
    {% endblock %}
    {% block modules %}<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"margin-left: -12%\">
                <section class=\"box-typical\">
                    <div id=\"toolbar\">
                        <h1 style=\"font-weight: bold\">Liste des Marchandises</h1>
                    </div>
                    <div class=\"table-responsive\">
                        <table id=\"table\"
                               data-pagination=\"true\"
                               data-search=\"true\"
                               data-use-row-attr-func=\"true\"
                               data-reorderable-rows=\"true\"
                               data-toolbar=\"#toolbar\">
                            <thead>
                            <tr >
                                <th data-field=\"id\">ID</th>
                                <th data-field=\"name\" >Libelle</th>
                                <th data-field=\"price\" >Categorie</th>
                                <th data-field=\"Modifier\" >Modifier</th>
                                <th data-field=\"Supprimer\" >Supprimer</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </section><!--.box-typical-->

            </div><!--.container-fluid-->
        </div><!--.page-content-->
    {% endblock %}
{% endblock %}
{% block script %}
    {{ parent() }}
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/jquery.tablednd.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table-reorder-rows-init.js') }}\"></script>
{% endblock %}", "StockBundle:Trans:listerContratMarchandise.html.twig", "C:\\wamp\\www\\symfony\\stock\\src\\StockBundle/Resources/views/Trans/listerContratMarchandise.html.twig");
    }
}
