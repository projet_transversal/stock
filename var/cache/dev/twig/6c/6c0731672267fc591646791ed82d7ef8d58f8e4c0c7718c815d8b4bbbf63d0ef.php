<?php

/* StockBundle:Representant:addRepresentant.html.twig */
class __TwigTemplate_312d92321259e1fea10ba90a62d4e93d63d7928927237feef711e0b108a367fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:Representant:addRepresentant.html.twig", 1);
        $this->blocks = array(
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5638dac79a2722955ec0970b481794abcd5104991c8340d3b06bc0b8bcdcd2b9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5638dac79a2722955ec0970b481794abcd5104991c8340d3b06bc0b8bcdcd2b9->enter($__internal_5638dac79a2722955ec0970b481794abcd5104991c8340d3b06bc0b8bcdcd2b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Representant:addRepresentant.html.twig"));

        $__internal_4b59dd1966d26f042df50d0cf4508ec37350909261628601ed515b53bf25f108 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4b59dd1966d26f042df50d0cf4508ec37350909261628601ed515b53bf25f108->enter($__internal_4b59dd1966d26f042df50d0cf4508ec37350909261628601ed515b53bf25f108_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Representant:addRepresentant.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5638dac79a2722955ec0970b481794abcd5104991c8340d3b06bc0b8bcdcd2b9->leave($__internal_5638dac79a2722955ec0970b481794abcd5104991c8340d3b06bc0b8bcdcd2b9_prof);

        
        $__internal_4b59dd1966d26f042df50d0cf4508ec37350909261628601ed515b53bf25f108->leave($__internal_4b59dd1966d26f042df50d0cf4508ec37350909261628601ed515b53bf25f108_prof);

    }

    // line 3
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_a6b59b8b47bfc62041a6d1487469cd0fbd017071d145e8fd8e01993afeef85e0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a6b59b8b47bfc62041a6d1487469cd0fbd017071d145e8fd8e01993afeef85e0->enter($__internal_a6b59b8b47bfc62041a6d1487469cd0fbd017071d145e8fd8e01993afeef85e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_8fca84fbf34e83ed60a7739d88c06fa845447b22a2c2ba17f80f89f44ed1fc56 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8fca84fbf34e83ed60a7739d88c06fa845447b22a2c2ba17f80f89f44ed1fc56->enter($__internal_8fca84fbf34e83ed60a7739d88c06fa845447b22a2c2ba17f80f89f44ed1fc56_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        // line 4
        echo "        ";
        $this->displayBlock('debutPage', $context, $blocks);
        // line 13
        echo "        ";
        $this->displayBlock('modules', $context, $blocks);
        // line 24
        echo "    ";
        
        $__internal_8fca84fbf34e83ed60a7739d88c06fa845447b22a2c2ba17f80f89f44ed1fc56->leave($__internal_8fca84fbf34e83ed60a7739d88c06fa845447b22a2c2ba17f80f89f44ed1fc56_prof);

        
        $__internal_a6b59b8b47bfc62041a6d1487469cd0fbd017071d145e8fd8e01993afeef85e0->leave($__internal_a6b59b8b47bfc62041a6d1487469cd0fbd017071d145e8fd8e01993afeef85e0_prof);

    }

    // line 4
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_ceb210128bd930a851c4d704f9f773e07ab4bf610d0c5c8912436d72a6640e13 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ceb210128bd930a851c4d704f9f773e07ab4bf610d0c5c8912436d72a6640e13->enter($__internal_ceb210128bd930a851c4d704f9f773e07ab4bf610d0c5c8912436d72a6640e13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_2464f651fe4e5e7221183bb44438b154fe2b5d250283b04a2bec232d33c6483b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2464f651fe4e5e7221183bb44438b154fe2b5d250283b04a2bec232d33c6483b->enter($__internal_2464f651fe4e5e7221183bb44438b154fe2b5d250283b04a2bec232d33c6483b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 5
        echo "            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-20px;\">
                    <h2 style=\"color: white;margin-top:4%\"> <center>Ajouter Representant</center></h2>
                </div>
                <br><br><br><br><br><br>
            </div>

        ";
        
        $__internal_2464f651fe4e5e7221183bb44438b154fe2b5d250283b04a2bec232d33c6483b->leave($__internal_2464f651fe4e5e7221183bb44438b154fe2b5d250283b04a2bec232d33c6483b_prof);

        
        $__internal_ceb210128bd930a851c4d704f9f773e07ab4bf610d0c5c8912436d72a6640e13->leave($__internal_ceb210128bd930a851c4d704f9f773e07ab4bf610d0c5c8912436d72a6640e13_prof);

    }

    // line 13
    public function block_modules($context, array $blocks = array())
    {
        $__internal_00e00f515c6bdd8eb86722dc068e3651a070f215ff7580c626a45d74312e5e3d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_00e00f515c6bdd8eb86722dc068e3651a070f215ff7580c626a45d74312e5e3d->enter($__internal_00e00f515c6bdd8eb86722dc068e3651a070f215ff7580c626a45d74312e5e3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_6e93aa9991d00f300c995f8ee49a68443b84faaa917a8b7a4b3ae9c99915dcec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e93aa9991d00f300c995f8ee49a68443b84faaa917a8b7a4b3ae9c99915dcec->enter($__internal_6e93aa9991d00f300c995f8ee49a68443b84faaa917a8b7a4b3ae9c99915dcec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        // line 14
        echo "            <div class=\"col-xl-6\" style=\"margin-left: 27%\">
                ";
        // line 15
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
                    ";
        // line 16
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
                    <div class=\"form-group\" align=\"center\">
                        <input type=\"submit\" name=\"addRepresentant\" class=\"btn btn-success\" value=\"Ajouter\"/>
                        <input type=\"reset\" name=\"resetRepresentant\" class=\"btn btn-danger\" value=\"Annuler\"/>
                    </div>
                ";
        // line 21
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
            </div>
        ";
        
        $__internal_6e93aa9991d00f300c995f8ee49a68443b84faaa917a8b7a4b3ae9c99915dcec->leave($__internal_6e93aa9991d00f300c995f8ee49a68443b84faaa917a8b7a4b3ae9c99915dcec_prof);

        
        $__internal_00e00f515c6bdd8eb86722dc068e3651a070f215ff7580c626a45d74312e5e3d->leave($__internal_00e00f515c6bdd8eb86722dc068e3651a070f215ff7580c626a45d74312e5e3d_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Representant:addRepresentant.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 21,  109 => 16,  105 => 15,  102 => 14,  93 => 13,  76 => 5,  67 => 4,  57 => 24,  54 => 13,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}

    {% block contenuDuMilieu %}
        {% block debutPage %}
            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-20px;\">
                    <h2 style=\"color: white;margin-top:4%\"> <center>Ajouter Representant</center></h2>
                </div>
                <br><br><br><br><br><br>
            </div>

        {% endblock %}
        {% block modules %}
            <div class=\"col-xl-6\" style=\"margin-left: 27%\">
                {{ form_start(form) }}
                    {{ form_widget(form) }}
                    <div class=\"form-group\" align=\"center\">
                        <input type=\"submit\" name=\"addRepresentant\" class=\"btn btn-success\" value=\"Ajouter\"/>
                        <input type=\"reset\" name=\"resetRepresentant\" class=\"btn btn-danger\" value=\"Annuler\"/>
                    </div>
                {{ form_end(form) }}
            </div>
        {% endblock %}
    {% endblock %}", "StockBundle:Representant:addRepresentant.html.twig", "C:\\wamp\\www\\symfony\\stock\\src\\StockBundle/Resources/views/Representant/addRepresentant.html.twig");
    }
}
