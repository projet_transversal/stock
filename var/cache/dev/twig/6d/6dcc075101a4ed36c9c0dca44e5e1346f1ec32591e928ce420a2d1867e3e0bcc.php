<?php

/* ::formulaire.html.twig */
class __TwigTemplate_47b09f46d75610f24ec6de0bf4c319f32ab54b656425327e63990da53d566bf9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f09495259a4df57f7c0d4fa4d2032a3a147e0dca27c3ef389d6845cb962e9d1f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f09495259a4df57f7c0d4fa4d2032a3a147e0dca27c3ef389d6845cb962e9d1f->enter($__internal_f09495259a4df57f7c0d4fa4d2032a3a147e0dca27c3ef389d6845cb962e9d1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::formulaire.html.twig"));

        $__internal_641710025b6d5197a8accb9228a4fab1d4ca675d23694256f3105f5325ebc34a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_641710025b6d5197a8accb9228a4fab1d4ca675d23694256f3105f5325ebc34a->enter($__internal_641710025b6d5197a8accb9228a4fab1d4ca675d23694256f3105f5325ebc34a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::formulaire.html.twig"));

        // line 1
        echo "<!Doctype>

<html>
    <head>
        ";
        // line 6
        echo "

        ";
        // line 9
        echo "            ";
        // line 10
        echo "            ";
        // line 11
        echo "                ";
        // line 12
        echo "                ";
        // line 13
        echo "            ";
        // line 14
        echo "        ";
        // line 15
        echo "    </head>
    <body>
        <form method=\"post\" class=\"form-horizontal\" >
            ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
            ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["form"] ?? $this->getContext($context, "form")));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 20
            echo "                ";
            if (($context["item"] == $this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "_token", array()))) {
                // line 21
                echo "                ";
            } else {
                // line 22
                echo "                    <div class=\"form-group\">
                        ";
                // line 23
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["item"], 'label');
                echo "
                        <div class=\"controls\" style=\"border-bottom: 1px  solid  silver;\">
                            ";
                // line 25
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["item"], 'widget');
                echo "
                        </div>
                    </div>
                ";
            }
            // line 29
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "            ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'rest');
        echo "
            <div class=\"controls\" align=\"center\">
                <input type=\"reset\" class=\"btn btn-danger\" value=\"Annuler\"/>
                <input type=\"submit\" class=\"btn btn-success\" value=\"Ajouter\"/>
            </div>
            ";
        // line 35
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
        </form>
    </body>
</html>";
        
        $__internal_f09495259a4df57f7c0d4fa4d2032a3a147e0dca27c3ef389d6845cb962e9d1f->leave($__internal_f09495259a4df57f7c0d4fa4d2032a3a147e0dca27c3ef389d6845cb962e9d1f_prof);

        
        $__internal_641710025b6d5197a8accb9228a4fab1d4ca675d23694256f3105f5325ebc34a->leave($__internal_641710025b6d5197a8accb9228a4fab1d4ca675d23694256f3105f5325ebc34a_prof);

    }

    public function getTemplateName()
    {
        return "::formulaire.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 35,  87 => 30,  81 => 29,  74 => 25,  69 => 23,  66 => 22,  63 => 21,  60 => 20,  56 => 19,  52 => 18,  47 => 15,  45 => 14,  43 => 13,  41 => 12,  39 => 11,  37 => 10,  35 => 9,  31 => 6,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!Doctype>

<html>
    <head>
        {#{% form_theme form _self %}#}


        {#{% block form_widget_simple %}#}
            {#{{ dump(form.vars.value) }}#}
            {#{% spaceless %}#}
                {#{% set type = type|default('text') %}#}
                {#<input type=\"{{ type }}\" {{ block('widget_attributes') }}/>#}
            {#{% endspaceless %}#}
        {#{% endblock form_widget_simple %}#}
    </head>
    <body>
        <form method=\"post\" class=\"form-horizontal\" >
            {{ form_start(form) }}
            {% for item in form %}
                {% if item == form._token %}
                {% else %}
                    <div class=\"form-group\">
                        {{ form_label(item) }}
                        <div class=\"controls\" style=\"border-bottom: 1px  solid  silver;\">
                            {{form_widget(item) }}
                        </div>
                    </div>
                {% endif %}
            {% endfor %}
            {{ form_rest(form) }}
            <div class=\"controls\" align=\"center\">
                <input type=\"reset\" class=\"btn btn-danger\" value=\"Annuler\"/>
                <input type=\"submit\" class=\"btn btn-success\" value=\"Ajouter\"/>
            </div>
            {{ form_end(form) }}
        </form>
    </body>
</html>", "::formulaire.html.twig", "C:\\wamp64\\www\\stock\\app/Resources\\views/formulaire.html.twig");
    }
}
