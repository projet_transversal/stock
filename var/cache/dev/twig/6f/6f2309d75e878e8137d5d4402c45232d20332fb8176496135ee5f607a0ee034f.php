<?php

/* ::formulaire.html.twig */
class __TwigTemplate_0e3cc69efa6d43689010796e70149614fa6fc13c683e8a6084d803c694a1c16c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_86cbf05caa23a58cd3f9f71483933e92a694442df4eada42b9b11997ff5a0dba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_86cbf05caa23a58cd3f9f71483933e92a694442df4eada42b9b11997ff5a0dba->enter($__internal_86cbf05caa23a58cd3f9f71483933e92a694442df4eada42b9b11997ff5a0dba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::formulaire.html.twig"));

        $__internal_8a486cfa28c11d983574770b89cf0fe4a0c64826ce8dcaf41731fbc2a5fa7759 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a486cfa28c11d983574770b89cf0fe4a0c64826ce8dcaf41731fbc2a5fa7759->enter($__internal_8a486cfa28c11d983574770b89cf0fe4a0c64826ce8dcaf41731fbc2a5fa7759_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::formulaire.html.twig"));

        // line 1
        echo "<!Doctype>
<html>
    <head></head>
    <body>
        <form method=\"post\" class=\"form-horizontal\" >
            ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
            ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 8
            echo "                ";
            if (($context["item"] == $this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_token", array()))) {
                // line 9
                echo "                ";
            } else {
                // line 10
                echo "                    <div class=\"form-group\">
                        ";
                // line 11
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["item"], 'label');
                echo "
                        <div class=\"controls\" style=\"border-bottom: 1px  solid  silver;\">
                            ";
                // line 13
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["item"], 'widget');
                echo "
                        </div>
                    </div>
                ";
            }
            // line 17
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "            ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
            <div class=\"controls\" align=\"center\">
                <input type=\"reset\" class=\"btn btn-danger\" value=\"Annuler\"/>
                <input type=\"submit\" class=\"btn btn-success\" value=\"Ajouter\"/>
            </div>
            ";
        // line 23
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
        </form>
    </body>
</html>";
        
        $__internal_86cbf05caa23a58cd3f9f71483933e92a694442df4eada42b9b11997ff5a0dba->leave($__internal_86cbf05caa23a58cd3f9f71483933e92a694442df4eada42b9b11997ff5a0dba_prof);

        
        $__internal_8a486cfa28c11d983574770b89cf0fe4a0c64826ce8dcaf41731fbc2a5fa7759->leave($__internal_8a486cfa28c11d983574770b89cf0fe4a0c64826ce8dcaf41731fbc2a5fa7759_prof);

    }

    public function getTemplateName()
    {
        return "::formulaire.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 23,  67 => 18,  61 => 17,  54 => 13,  49 => 11,  46 => 10,  43 => 9,  40 => 8,  36 => 7,  32 => 6,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!Doctype>
<html>
    <head></head>
    <body>
        <form method=\"post\" class=\"form-horizontal\" >
            {{ form_start(form) }}
            {% for item in form %}
                {% if item == form._token %}
                {% else %}
                    <div class=\"form-group\">
                        {{ form_label(item) }}
                        <div class=\"controls\" style=\"border-bottom: 1px  solid  silver;\">
                            {{form_widget(item) }}
                        </div>
                    </div>
                {% endif %}
            {% endfor %}
            {{ form_rest(form) }}
            <div class=\"controls\" align=\"center\">
                <input type=\"reset\" class=\"btn btn-danger\" value=\"Annuler\"/>
                <input type=\"submit\" class=\"btn btn-success\" value=\"Ajouter\"/>
            </div>
            {{ form_end(form) }}
        </form>
    </body>
</html>", "::formulaire.html.twig", "C:\\wamp\\www\\symfony\\stock\\app/Resources\\views/formulaire.html.twig");
    }
}
