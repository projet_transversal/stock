<?php

/* StockBundle:ContratMarchandise:addContratMarchandise.html.twig */
class __TwigTemplate_ff42bdf411888b3991a16d866b8edd1cbe9a6f7e01342b7e193efcd2de7e3266 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:ContratMarchandise:addContratMarchandise.html.twig", 1);
        $this->blocks = array(
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d95dbb22fc0b011a16e0191a28026a7b37fd5d30ba6ce0bd397ba403df9ad925 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d95dbb22fc0b011a16e0191a28026a7b37fd5d30ba6ce0bd397ba403df9ad925->enter($__internal_d95dbb22fc0b011a16e0191a28026a7b37fd5d30ba6ce0bd397ba403df9ad925_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:ContratMarchandise:addContratMarchandise.html.twig"));

        $__internal_d0695b3f9887b90a5cd1be3269dc31c42241ac6f93d4a0b65c8c6dc19ec258bd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d0695b3f9887b90a5cd1be3269dc31c42241ac6f93d4a0b65c8c6dc19ec258bd->enter($__internal_d0695b3f9887b90a5cd1be3269dc31c42241ac6f93d4a0b65c8c6dc19ec258bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:ContratMarchandise:addContratMarchandise.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d95dbb22fc0b011a16e0191a28026a7b37fd5d30ba6ce0bd397ba403df9ad925->leave($__internal_d95dbb22fc0b011a16e0191a28026a7b37fd5d30ba6ce0bd397ba403df9ad925_prof);

        
        $__internal_d0695b3f9887b90a5cd1be3269dc31c42241ac6f93d4a0b65c8c6dc19ec258bd->leave($__internal_d0695b3f9887b90a5cd1be3269dc31c42241ac6f93d4a0b65c8c6dc19ec258bd_prof);

    }

    // line 3
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_ececbfd9fa7f54b7fc8838d6d739639d20303b0bd3b701da8622c0b3e75dc945 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ececbfd9fa7f54b7fc8838d6d739639d20303b0bd3b701da8622c0b3e75dc945->enter($__internal_ececbfd9fa7f54b7fc8838d6d739639d20303b0bd3b701da8622c0b3e75dc945_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_68ffc0bbb3e07e84b63fe94c455a1a6c5b367b96f735959878b9e7e4b174ac68 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_68ffc0bbb3e07e84b63fe94c455a1a6c5b367b96f735959878b9e7e4b174ac68->enter($__internal_68ffc0bbb3e07e84b63fe94c455a1a6c5b367b96f735959878b9e7e4b174ac68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        // line 4
        echo "        ";
        $this->displayBlock('debutPage', $context, $blocks);
        // line 13
        echo "        ";
        $this->displayBlock('modules', $context, $blocks);
        // line 18
        echo "    ";
        
        $__internal_68ffc0bbb3e07e84b63fe94c455a1a6c5b367b96f735959878b9e7e4b174ac68->leave($__internal_68ffc0bbb3e07e84b63fe94c455a1a6c5b367b96f735959878b9e7e4b174ac68_prof);

        
        $__internal_ececbfd9fa7f54b7fc8838d6d739639d20303b0bd3b701da8622c0b3e75dc945->leave($__internal_ececbfd9fa7f54b7fc8838d6d739639d20303b0bd3b701da8622c0b3e75dc945_prof);

    }

    // line 4
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_1d3daa627714e36f5d7dc76af834a6c0f124a01a9f05473a3b3ff35f65da0b19 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1d3daa627714e36f5d7dc76af834a6c0f124a01a9f05473a3b3ff35f65da0b19->enter($__internal_1d3daa627714e36f5d7dc76af834a6c0f124a01a9f05473a3b3ff35f65da0b19_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_48f960af3e8dc316f37ca1de0ddc0f96f10f190cee20ac21ca9999fbbc2a3d06 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_48f960af3e8dc316f37ca1de0ddc0f96f10f190cee20ac21ca9999fbbc2a3d06->enter($__internal_48f960af3e8dc316f37ca1de0ddc0f96f10f190cee20ac21ca9999fbbc2a3d06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 5
        echo "            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                    <h3 style=\"color: white;margin-top:4%\"> <center>Ajouter Contrat</center></h3>
                </div>
                <br><br><br><br><br><br>
            </div>

        ";
        
        $__internal_48f960af3e8dc316f37ca1de0ddc0f96f10f190cee20ac21ca9999fbbc2a3d06->leave($__internal_48f960af3e8dc316f37ca1de0ddc0f96f10f190cee20ac21ca9999fbbc2a3d06_prof);

        
        $__internal_1d3daa627714e36f5d7dc76af834a6c0f124a01a9f05473a3b3ff35f65da0b19->leave($__internal_1d3daa627714e36f5d7dc76af834a6c0f124a01a9f05473a3b3ff35f65da0b19_prof);

    }

    // line 13
    public function block_modules($context, array $blocks = array())
    {
        $__internal_01bf201e6294ba31cb2400ace3de2b0f35910d3ae7cd56e0334192f8b14c81fc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_01bf201e6294ba31cb2400ace3de2b0f35910d3ae7cd56e0334192f8b14c81fc->enter($__internal_01bf201e6294ba31cb2400ace3de2b0f35910d3ae7cd56e0334192f8b14c81fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_8b464d28516c0803d376fc27ec98a6e2f5249618efe911cc5860765ee7a300f1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b464d28516c0803d376fc27ec98a6e2f5249618efe911cc5860765ee7a300f1->enter($__internal_8b464d28516c0803d376fc27ec98a6e2f5249618efe911cc5860765ee7a300f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        // line 14
        echo "            <div class=\"col-xl-6\" style=\"margin-left: 27%\">
                ";
        // line 15
        $this->loadTemplate("::formulaire.html.twig", "StockBundle:ContratMarchandise:addContratMarchandise.html.twig", 15)->display($context);
        // line 16
        echo "            </div>
        ";
        
        $__internal_8b464d28516c0803d376fc27ec98a6e2f5249618efe911cc5860765ee7a300f1->leave($__internal_8b464d28516c0803d376fc27ec98a6e2f5249618efe911cc5860765ee7a300f1_prof);

        
        $__internal_01bf201e6294ba31cb2400ace3de2b0f35910d3ae7cd56e0334192f8b14c81fc->leave($__internal_01bf201e6294ba31cb2400ace3de2b0f35910d3ae7cd56e0334192f8b14c81fc_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:ContratMarchandise:addContratMarchandise.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 16,  105 => 15,  102 => 14,  93 => 13,  76 => 5,  67 => 4,  57 => 18,  54 => 13,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}

    {% block contenuDuMilieu %}
        {% block debutPage %}
            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                    <h3 style=\"color: white;margin-top:4%\"> <center>Ajouter Contrat</center></h3>
                </div>
                <br><br><br><br><br><br>
            </div>

        {% endblock %}
        {% block modules %}
            <div class=\"col-xl-6\" style=\"margin-left: 27%\">
                {% include '::formulaire.html.twig' %}
            </div>
        {% endblock %}
    {% endblock %}", "StockBundle:ContratMarchandise:addContratMarchandise.html.twig", "C:\\wamp64\\www\\stock\\src\\StockBundle/Resources/views/ContratMarchandise/addContratMarchandise.html.twig");
    }
}
