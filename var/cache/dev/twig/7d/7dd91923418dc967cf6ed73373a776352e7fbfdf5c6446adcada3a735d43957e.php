<?php

/* blog/about.html.twig */
class __TwigTemplate_a5dda6897e874c7a904db83e066a9ad6a3ee85654d2264379a095620109314a6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e1049d45e3a60d8d58d7d3cf6f861c00247385da2547683f8e71697465b2535d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e1049d45e3a60d8d58d7d3cf6f861c00247385da2547683f8e71697465b2535d->enter($__internal_e1049d45e3a60d8d58d7d3cf6f861c00247385da2547683f8e71697465b2535d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "blog/about.html.twig"));

        $__internal_bc5c3f25214839da2894a7c47b8334c2ed7973ad3382c44348f9d14450d88b82 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bc5c3f25214839da2894a7c47b8334c2ed7973ad3382c44348f9d14450d88b82->enter($__internal_bc5c3f25214839da2894a7c47b8334c2ed7973ad3382c44348f9d14450d88b82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "blog/about.html.twig"));

        // line 1
        echo "<div class=\"section about\">
    <div class=\"well well-lg\">
        <p>
            ";
        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("help.app_description");
        echo "
        </p>
        <p>
            ";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("help.more_information");
        echo "
        </p>
    </div>
</div>

";
        // line 15
        echo "<!-- Fragment rendered on ";
        echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, "now", "long", "long", null, "UTC"), "html", null, true);
        echo " -->
";
        
        $__internal_e1049d45e3a60d8d58d7d3cf6f861c00247385da2547683f8e71697465b2535d->leave($__internal_e1049d45e3a60d8d58d7d3cf6f861c00247385da2547683f8e71697465b2535d_prof);

        
        $__internal_bc5c3f25214839da2894a7c47b8334c2ed7973ad3382c44348f9d14450d88b82->leave($__internal_bc5c3f25214839da2894a7c47b8334c2ed7973ad3382c44348f9d14450d88b82_prof);

    }

    public function getTemplateName()
    {
        return "blog/about.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 15,  36 => 7,  30 => 4,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"section about\">
    <div class=\"well well-lg\">
        <p>
            {{ 'help.app_description'|trans|raw }}
        </p>
        <p>
            {{ 'help.more_information'|trans|raw }}
        </p>
    </div>
</div>

{# it's not mandatory to set the timezone in localizeddate(). This is done to
   avoid errors when the 'intl' PHP extension is not available and the application
   is forced to use the limited \"intl polyfill\", which only supports UTC and GMT #}
<!-- Fragment rendered on {{ 'now'|localizeddate('long', 'long', null, 'UTC') }} -->
", "blog/about.html.twig", "C:\\wamp64\\www\\stock\\app\\Resources\\views\\blog\\about.html.twig");
    }
}
