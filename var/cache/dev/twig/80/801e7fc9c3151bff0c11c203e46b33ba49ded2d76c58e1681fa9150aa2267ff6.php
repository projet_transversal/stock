<?php

/* StockBundle:Marchandise:listerContratMarchandise.html.twig */
class __TwigTemplate_af6a34df9ffc88228227ed152ba835ebee2eb592512b9be0696ad7fc691fd7d1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:Marchandise:listerContratMarchandise.html.twig", 1);
        $this->blocks = array(
            'menuGauche' => array($this, 'block_menuGauche'),
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_951c7915da4559f5d0a2460b02fcc10db03b8ab5e1b527455566771992912c8a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_951c7915da4559f5d0a2460b02fcc10db03b8ab5e1b527455566771992912c8a->enter($__internal_951c7915da4559f5d0a2460b02fcc10db03b8ab5e1b527455566771992912c8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Marchandise:listerContratMarchandise.html.twig"));

        $__internal_83381bd489bfbba44f88b11e00388e3111a2db6c389a7b47fc820318ea8329a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_83381bd489bfbba44f88b11e00388e3111a2db6c389a7b47fc820318ea8329a8->enter($__internal_83381bd489bfbba44f88b11e00388e3111a2db6c389a7b47fc820318ea8329a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Marchandise:listerContratMarchandise.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_951c7915da4559f5d0a2460b02fcc10db03b8ab5e1b527455566771992912c8a->leave($__internal_951c7915da4559f5d0a2460b02fcc10db03b8ab5e1b527455566771992912c8a_prof);

        
        $__internal_83381bd489bfbba44f88b11e00388e3111a2db6c389a7b47fc820318ea8329a8->leave($__internal_83381bd489bfbba44f88b11e00388e3111a2db6c389a7b47fc820318ea8329a8_prof);

    }

    // line 2
    public function block_menuGauche($context, array $blocks = array())
    {
        $__internal_d1382303c970a4f840fa499f9750c0961715c7834b574b7c0788654174018c2f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d1382303c970a4f840fa499f9750c0961715c7834b574b7c0788654174018c2f->enter($__internal_d1382303c970a4f840fa499f9750c0961715c7834b574b7c0788654174018c2f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menuGauche"));

        $__internal_07afbdf8cfff2ba078f235bec6dd5176ec766d7a8eba89f81e5ced1c6823c0f1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_07afbdf8cfff2ba078f235bec6dd5176ec766d7a8eba89f81e5ced1c6823c0f1->enter($__internal_07afbdf8cfff2ba078f235bec6dd5176ec766d7a8eba89f81e5ced1c6823c0f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menuGauche"));

        // line 3
        echo "    <nav class=\"side-menu\" style=\"background: #3299CC;overflow: hidden\">
        <nav class=\"sidebar-header\">
            <img src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/man.png"), "html", null, true);
        echo "\" style=\"margin-left:35%;margin-top:-15%;\"><br><br>
            <h5 style=\"text-align:center\">User Name</h5>
            <h5 style=\"text-align:center\">Profil</h5>
        </nav>
        <hr style=\"margin-left:20%;margin-right: 20%;background: red;height:1px\">
        <ul class=\"side-menu-list\">
            <li class=\"grey with-sub\">
                    <span>
                        <i class=\"font-icon font-icon-home\"  style=\"color: white\"></i>
                        <span class=\"lbl\"><a href=\"";
        // line 14
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("accueil");
        echo "\" style=\"color: white\">Accueil</a></span>
                    </span>
            </li>
            <li class=\"grey with-sub\">
                    <span>
                        <i class=\"font-icon font-icon-view-grid\"  style=\"color: white\"></i>
                        <span class=\"lbl\"><a href=\"";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_produit");
        echo "\" style=\"color: white\">Ajouter</a></span>
                    </span>
            </li>
            <li class=\"grey with-sub\">
                    <span>
                        <i class=\"font-icon font-icon-user\"  style=\"color: white\"></i>
                        <span class=\"lbl\"><a href=\"#\" style=\"color: white\">Modifier</a></span>
                    </span>
            </li>
            <li class=\"grey with-sub\">
                    <span>
                        <i class=\"font-icon font-icon-archive\"  style=\"color: white\"></i>
                        <span class=\"lbl\"><a href=\"#\" style=\"color: white\"> Lister</a></span>
                    </span>
            </li>
        </ul>
    </nav><!--.side-menu-->
";
        
        $__internal_07afbdf8cfff2ba078f235bec6dd5176ec766d7a8eba89f81e5ced1c6823c0f1->leave($__internal_07afbdf8cfff2ba078f235bec6dd5176ec766d7a8eba89f81e5ced1c6823c0f1_prof);

        
        $__internal_d1382303c970a4f840fa499f9750c0961715c7834b574b7c0788654174018c2f->leave($__internal_d1382303c970a4f840fa499f9750c0961715c7834b574b7c0788654174018c2f_prof);

    }

    // line 38
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_14573bd70c5816e59b8e213c2213d151c20733f7c60161b6d2df16b267aff64c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_14573bd70c5816e59b8e213c2213d151c20733f7c60161b6d2df16b267aff64c->enter($__internal_14573bd70c5816e59b8e213c2213d151c20733f7c60161b6d2df16b267aff64c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_d2b6053714304a45c9dd545ab15c0fffeae2271faf88d4920af08de8ed737d7c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d2b6053714304a45c9dd545ab15c0fffeae2271faf88d4920af08de8ed737d7c->enter($__internal_d2b6053714304a45c9dd545ab15c0fffeae2271faf88d4920af08de8ed737d7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        echo "<!--le block du milieu de la page-->
    ";
        // line 39
        $this->displayBlock('debutPage', $context, $blocks);
        // line 42
        echo "    ";
        $this->displayBlock('modules', $context, $blocks);
        
        $__internal_d2b6053714304a45c9dd545ab15c0fffeae2271faf88d4920af08de8ed737d7c->leave($__internal_d2b6053714304a45c9dd545ab15c0fffeae2271faf88d4920af08de8ed737d7c_prof);

        
        $__internal_14573bd70c5816e59b8e213c2213d151c20733f7c60161b6d2df16b267aff64c->leave($__internal_14573bd70c5816e59b8e213c2213d151c20733f7c60161b6d2df16b267aff64c_prof);

    }

    // line 39
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_6d84878db645d554a3cf86a3ae0b85909f4ae339e3a688bc96b1e1603d3da1c1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6d84878db645d554a3cf86a3ae0b85909f4ae339e3a688bc96b1e1603d3da1c1->enter($__internal_6d84878db645d554a3cf86a3ae0b85909f4ae339e3a688bc96b1e1603d3da1c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_f67fb08c61767538d0ca4fbe5162ddcf9b190ff135a26e0e4bcee7420d928caa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f67fb08c61767538d0ca4fbe5162ddcf9b190ff135a26e0e4bcee7420d928caa->enter($__internal_f67fb08c61767538d0ca4fbe5162ddcf9b190ff135a26e0e4bcee7420d928caa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 40
        echo "        ";
        $this->displayParentBlock("debutPage", $context, $blocks);
        echo "
    ";
        
        $__internal_f67fb08c61767538d0ca4fbe5162ddcf9b190ff135a26e0e4bcee7420d928caa->leave($__internal_f67fb08c61767538d0ca4fbe5162ddcf9b190ff135a26e0e4bcee7420d928caa_prof);

        
        $__internal_6d84878db645d554a3cf86a3ae0b85909f4ae339e3a688bc96b1e1603d3da1c1->leave($__internal_6d84878db645d554a3cf86a3ae0b85909f4ae339e3a688bc96b1e1603d3da1c1_prof);

    }

    // line 42
    public function block_modules($context, array $blocks = array())
    {
        $__internal_baf60b36e5687d1f4ce9a33f9d8e0c433ebddafa018469a03cb0bed01ac21039 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_baf60b36e5687d1f4ce9a33f9d8e0c433ebddafa018469a03cb0bed01ac21039->enter($__internal_baf60b36e5687d1f4ce9a33f9d8e0c433ebddafa018469a03cb0bed01ac21039_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_fee2ccc536f09c8d9965af32a1df54994fc6c175f091114cbf2f1afdcb2e4e72 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fee2ccc536f09c8d9965af32a1df54994fc6c175f091114cbf2f1afdcb2e4e72->enter($__internal_fee2ccc536f09c8d9965af32a1df54994fc6c175f091114cbf2f1afdcb2e4e72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        echo "<!--le block pour les differents modules de l'application-->
    ";
        
        $__internal_fee2ccc536f09c8d9965af32a1df54994fc6c175f091114cbf2f1afdcb2e4e72->leave($__internal_fee2ccc536f09c8d9965af32a1df54994fc6c175f091114cbf2f1afdcb2e4e72_prof);

        
        $__internal_baf60b36e5687d1f4ce9a33f9d8e0c433ebddafa018469a03cb0bed01ac21039->leave($__internal_baf60b36e5687d1f4ce9a33f9d8e0c433ebddafa018469a03cb0bed01ac21039_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Marchandise:listerContratMarchandise.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 42,  138 => 40,  129 => 39,  118 => 42,  116 => 39,  105 => 38,  77 => 20,  68 => 14,  56 => 5,  52 => 3,  43 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}
{% block menuGauche %}
    <nav class=\"side-menu\" style=\"background: #3299CC;overflow: hidden\">
        <nav class=\"sidebar-header\">
            <img src=\"{{asset('img/man.png')}}\" style=\"margin-left:35%;margin-top:-15%;\"><br><br>
            <h5 style=\"text-align:center\">User Name</h5>
            <h5 style=\"text-align:center\">Profil</h5>
        </nav>
        <hr style=\"margin-left:20%;margin-right: 20%;background: red;height:1px\">
        <ul class=\"side-menu-list\">
            <li class=\"grey with-sub\">
                    <span>
                        <i class=\"font-icon font-icon-home\"  style=\"color: white\"></i>
                        <span class=\"lbl\"><a href=\"{{ path('accueil') }}\" style=\"color: white\">Accueil</a></span>
                    </span>
            </li>
            <li class=\"grey with-sub\">
                    <span>
                        <i class=\"font-icon font-icon-view-grid\"  style=\"color: white\"></i>
                        <span class=\"lbl\"><a href=\"{{ path('ajouter_produit') }}\" style=\"color: white\">Ajouter</a></span>
                    </span>
            </li>
            <li class=\"grey with-sub\">
                    <span>
                        <i class=\"font-icon font-icon-user\"  style=\"color: white\"></i>
                        <span class=\"lbl\"><a href=\"#\" style=\"color: white\">Modifier</a></span>
                    </span>
            </li>
            <li class=\"grey with-sub\">
                    <span>
                        <i class=\"font-icon font-icon-archive\"  style=\"color: white\"></i>
                        <span class=\"lbl\"><a href=\"#\" style=\"color: white\"> Lister</a></span>
                    </span>
            </li>
        </ul>
    </nav><!--.side-menu-->
{% endblock %}
{% block contenuDuMilieu %}<!--le block du milieu de la page-->
    {% block debutPage %}
        {{ parent() }}
    {% endblock %}
    {% block modules %}<!--le block pour les differents modules de l'application-->
    {% endblock %}
{% endblock %}", "StockBundle:Marchandise:listerContratMarchandise.html.twig", "C:\\wamp64\\www\\stock\\src\\StockBundle/Resources/views/Marchandise/listerContratMarchandise.html.twig");
    }
}
