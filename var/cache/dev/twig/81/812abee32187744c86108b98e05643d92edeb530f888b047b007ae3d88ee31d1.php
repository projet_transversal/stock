<?php

/* form/fields.html.twig */
class __TwigTemplate_e3b5afee12eb256ea263fafec231d229374385cf973b9670910118e3c8584d5f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'date_time_picker_widget' => array($this, 'block_date_time_picker_widget'),
            'tags_input_widget' => array($this, 'block_tags_input_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b8962d18a1c3481a8e37dd9452fda2b25ffb7b67a554a25d86d7dc34d344f504 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b8962d18a1c3481a8e37dd9452fda2b25ffb7b67a554a25d86d7dc34d344f504->enter($__internal_b8962d18a1c3481a8e37dd9452fda2b25ffb7b67a554a25d86d7dc34d344f504_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/fields.html.twig"));

        $__internal_835c708cf406ab655af9ae5c14141013b73ff05d92f0ecf6a08b46857f2016fb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_835c708cf406ab655af9ae5c14141013b73ff05d92f0ecf6a08b46857f2016fb->enter($__internal_835c708cf406ab655af9ae5c14141013b73ff05d92f0ecf6a08b46857f2016fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/fields.html.twig"));

        // line 9
        echo "
";
        // line 10
        $this->displayBlock('date_time_picker_widget', $context, $blocks);
        // line 18
        echo "
";
        // line 19
        $this->displayBlock('tags_input_widget', $context, $blocks);
        
        $__internal_b8962d18a1c3481a8e37dd9452fda2b25ffb7b67a554a25d86d7dc34d344f504->leave($__internal_b8962d18a1c3481a8e37dd9452fda2b25ffb7b67a554a25d86d7dc34d344f504_prof);

        
        $__internal_835c708cf406ab655af9ae5c14141013b73ff05d92f0ecf6a08b46857f2016fb->leave($__internal_835c708cf406ab655af9ae5c14141013b73ff05d92f0ecf6a08b46857f2016fb_prof);

    }

    // line 10
    public function block_date_time_picker_widget($context, array $blocks = array())
    {
        $__internal_8bb7cf9cbf06a55a091e59c84fcdfc83cb9ec62d9ae2338721be037410d0ae13 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8bb7cf9cbf06a55a091e59c84fcdfc83cb9ec62d9ae2338721be037410d0ae13->enter($__internal_8bb7cf9cbf06a55a091e59c84fcdfc83cb9ec62d9ae2338721be037410d0ae13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_time_picker_widget"));

        $__internal_1c6740fb170e6be9d8ef4de42f93c4791ebc2423d4f4f61f6b79f6be99bec101 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c6740fb170e6be9d8ef4de42f93c4791ebc2423d4f4f61f6b79f6be99bec101->enter($__internal_1c6740fb170e6be9d8ef4de42f93c4791ebc2423d4f4f61f6b79f6be99bec101_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_time_picker_widget"));

        // line 11
        echo "    <div class=\"input-group date\" data-toggle=\"datetimepicker\">
        ";
        // line 12
        $this->displayBlock("datetime_widget", $context, $blocks);
        echo "
        <span class=\"input-group-addon\">
            <span class=\"fa fa-calendar\" aria-hidden=\"true\"></span>
        </span>
    </div>
";
        
        $__internal_1c6740fb170e6be9d8ef4de42f93c4791ebc2423d4f4f61f6b79f6be99bec101->leave($__internal_1c6740fb170e6be9d8ef4de42f93c4791ebc2423d4f4f61f6b79f6be99bec101_prof);

        
        $__internal_8bb7cf9cbf06a55a091e59c84fcdfc83cb9ec62d9ae2338721be037410d0ae13->leave($__internal_8bb7cf9cbf06a55a091e59c84fcdfc83cb9ec62d9ae2338721be037410d0ae13_prof);

    }

    // line 19
    public function block_tags_input_widget($context, array $blocks = array())
    {
        $__internal_a97d4eb162ebe190c88e6662924f1d00abf6501dcc8d9457de930ece4ffb3da3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a97d4eb162ebe190c88e6662924f1d00abf6501dcc8d9457de930ece4ffb3da3->enter($__internal_a97d4eb162ebe190c88e6662924f1d00abf6501dcc8d9457de930ece4ffb3da3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tags_input_widget"));

        $__internal_7ceb980a5a6f182c588cc73d80779599b4f710fbb9a07c4803ad57a104e31589 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7ceb980a5a6f182c588cc73d80779599b4f710fbb9a07c4803ad57a104e31589->enter($__internal_7ceb980a5a6f182c588cc73d80779599b4f710fbb9a07c4803ad57a104e31589_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tags_input_widget"));

        // line 20
        echo "    <div class=\"input-group\">
        ";
        // line 21
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget', array("attr" => array("data-toggle" => "tagsinput", "data-tags" => twig_jsonencode_filter((isset($context["tags"]) ? $context["tags"] : $this->getContext($context, "tags"))))));
        echo "
        <span class=\"input-group-addon\">
            <span class=\"fa fa-tags\" aria-hidden=\"true\"></span>
        </span>
    </div>
";
        
        $__internal_7ceb980a5a6f182c588cc73d80779599b4f710fbb9a07c4803ad57a104e31589->leave($__internal_7ceb980a5a6f182c588cc73d80779599b4f710fbb9a07c4803ad57a104e31589_prof);

        
        $__internal_a97d4eb162ebe190c88e6662924f1d00abf6501dcc8d9457de930ece4ffb3da3->leave($__internal_a97d4eb162ebe190c88e6662924f1d00abf6501dcc8d9457de930ece4ffb3da3_prof);

    }

    public function getTemplateName()
    {
        return "form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 21,  82 => 20,  73 => 19,  57 => 12,  54 => 11,  45 => 10,  35 => 19,  32 => 18,  30 => 10,  27 => 9,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
   Each field type is rendered by a template fragment, which is determined
   by the name of your form type class (DateTimePickerType -> date_time_picker)
   and the suffix \"_widget\". This can be controlled by overriding getBlockPrefix()
   in DateTimePickerType.

   See http://symfony.com/doc/current/cookbook/form/create_custom_field_type.html#creating-a-template-for-the-field
#}

{% block date_time_picker_widget %}
    <div class=\"input-group date\" data-toggle=\"datetimepicker\">
        {{ block('datetime_widget') }}
        <span class=\"input-group-addon\">
            <span class=\"fa fa-calendar\" aria-hidden=\"true\"></span>
        </span>
    </div>
{% endblock %}

{% block tags_input_widget %}
    <div class=\"input-group\">
        {{ form_widget(form, {'attr': {'data-toggle': 'tagsinput', 'data-tags': tags|json_encode}}) }}
        <span class=\"input-group-addon\">
            <span class=\"fa fa-tags\" aria-hidden=\"true\"></span>
        </span>
    </div>
{% endblock %}
", "form/fields.html.twig", "C:\\wamp\\www\\symfony\\stock\\app\\Resources\\views\\form\\fields.html.twig");
    }
}
