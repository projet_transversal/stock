<?php

/* StockBundle:Repres:livrerMarchandise.html.twig */
class __TwigTemplate_460446d42dc2d08605c43aff0408d1193551450e3d9ea4cfc4b5c25ab0dbf394 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::representant.html.twig", "StockBundle:Repres:livrerMarchandise.html.twig", 1);
        $this->blocks = array(
            'menu' => array($this, 'block_menu'),
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::representant.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a5587d408b5b505b2455cb18fac0cf2ac3862a85bf929a4c3df0efb4351cf12d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a5587d408b5b505b2455cb18fac0cf2ac3862a85bf929a4c3df0efb4351cf12d->enter($__internal_a5587d408b5b505b2455cb18fac0cf2ac3862a85bf929a4c3df0efb4351cf12d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Repres:livrerMarchandise.html.twig"));

        $__internal_e570d09f40b297e02b5cee404cc555caabf4413a215a61e47e8fb2ae3572a714 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e570d09f40b297e02b5cee404cc555caabf4413a215a61e47e8fb2ae3572a714->enter($__internal_e570d09f40b297e02b5cee404cc555caabf4413a215a61e47e8fb2ae3572a714_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Repres:livrerMarchandise.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a5587d408b5b505b2455cb18fac0cf2ac3862a85bf929a4c3df0efb4351cf12d->leave($__internal_a5587d408b5b505b2455cb18fac0cf2ac3862a85bf929a4c3df0efb4351cf12d_prof);

        
        $__internal_e570d09f40b297e02b5cee404cc555caabf4413a215a61e47e8fb2ae3572a714->leave($__internal_e570d09f40b297e02b5cee404cc555caabf4413a215a61e47e8fb2ae3572a714_prof);

    }

    // line 2
    public function block_menu($context, array $blocks = array())
    {
        $__internal_89a7dd5c5634e38671ba8b20ce2f4e04d7a1980ee470d005fae8f7cc23507dae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_89a7dd5c5634e38671ba8b20ce2f4e04d7a1980ee470d005fae8f7cc23507dae->enter($__internal_89a7dd5c5634e38671ba8b20ce2f4e04d7a1980ee470d005fae8f7cc23507dae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_ea08d82e713a12687cf7115da4742ae37790cf419fa9e173924ba1553db00c1c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ea08d82e713a12687cf7115da4742ae37790cf419fa9e173924ba1553db00c1c->enter($__internal_ea08d82e713a12687cf7115da4742ae37790cf419fa9e173924ba1553db00c1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 3
        echo "\t\t";
        $this->displayParentBlock("menu", $context, $blocks);
        echo "
    ";
        
        $__internal_ea08d82e713a12687cf7115da4742ae37790cf419fa9e173924ba1553db00c1c->leave($__internal_ea08d82e713a12687cf7115da4742ae37790cf419fa9e173924ba1553db00c1c_prof);

        
        $__internal_89a7dd5c5634e38671ba8b20ce2f4e04d7a1980ee470d005fae8f7cc23507dae->leave($__internal_89a7dd5c5634e38671ba8b20ce2f4e04d7a1980ee470d005fae8f7cc23507dae_prof);

    }

    // line 5
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_3cc43487181bfacb44227851b60385d7dd207a46904b1a653153dd873972e896 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3cc43487181bfacb44227851b60385d7dd207a46904b1a653153dd873972e896->enter($__internal_3cc43487181bfacb44227851b60385d7dd207a46904b1a653153dd873972e896_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_4ff7a36b3ba695692a22b361584c64fbd991dc808df425e37c6ab29b7cc07fe7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4ff7a36b3ba695692a22b361584c64fbd991dc808df425e37c6ab29b7cc07fe7->enter($__internal_4ff7a36b3ba695692a22b361584c64fbd991dc808df425e37c6ab29b7cc07fe7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        // line 6
        echo "        ";
        $this->displayBlock('debutPage', $context, $blocks);
        // line 15
        echo "\t\t";
        $this->displayBlock('modules', $context, $blocks);
        // line 20
        echo "    ";
        
        $__internal_4ff7a36b3ba695692a22b361584c64fbd991dc808df425e37c6ab29b7cc07fe7->leave($__internal_4ff7a36b3ba695692a22b361584c64fbd991dc808df425e37c6ab29b7cc07fe7_prof);

        
        $__internal_3cc43487181bfacb44227851b60385d7dd207a46904b1a653153dd873972e896->leave($__internal_3cc43487181bfacb44227851b60385d7dd207a46904b1a653153dd873972e896_prof);

    }

    // line 6
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_c444685e83a3708ae7e3523d3a1474a0037f976dec2799718fee6835eafac2cc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c444685e83a3708ae7e3523d3a1474a0037f976dec2799718fee6835eafac2cc->enter($__internal_c444685e83a3708ae7e3523d3a1474a0037f976dec2799718fee6835eafac2cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_3e572c82b99a5d45cc02e5dbc4e740cdb4aa71b198204d8d543d3ccfc2ce38ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3e572c82b99a5d45cc02e5dbc4e740cdb4aa71b198204d8d543d3ccfc2ce38ff->enter($__internal_3e572c82b99a5d45cc02e5dbc4e740cdb4aa71b198204d8d543d3ccfc2ce38ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 7
        echo "            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-20px;\">
                    <h2 style=\"color: white;margin-top:4%\"> <center>Ordre de  livraison</center></h2>
                </div>
                <br><br><br><br><br><br>
            </div>

        ";
        
        $__internal_3e572c82b99a5d45cc02e5dbc4e740cdb4aa71b198204d8d543d3ccfc2ce38ff->leave($__internal_3e572c82b99a5d45cc02e5dbc4e740cdb4aa71b198204d8d543d3ccfc2ce38ff_prof);

        
        $__internal_c444685e83a3708ae7e3523d3a1474a0037f976dec2799718fee6835eafac2cc->leave($__internal_c444685e83a3708ae7e3523d3a1474a0037f976dec2799718fee6835eafac2cc_prof);

    }

    // line 15
    public function block_modules($context, array $blocks = array())
    {
        $__internal_a42216b9e28c652f7f8f329d5c4b58c2f5f1a4e0b751cf2da757c9adfde2d25d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a42216b9e28c652f7f8f329d5c4b58c2f5f1a4e0b751cf2da757c9adfde2d25d->enter($__internal_a42216b9e28c652f7f8f329d5c4b58c2f5f1a4e0b751cf2da757c9adfde2d25d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_71c6e724699d5d2aa0bc1d9e03186b887fdef91c707dda8210b2f400deff6e33 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_71c6e724699d5d2aa0bc1d9e03186b887fdef91c707dda8210b2f400deff6e33->enter($__internal_71c6e724699d5d2aa0bc1d9e03186b887fdef91c707dda8210b2f400deff6e33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        // line 16
        echo "                <div class=\"col-xl-6\" style=\"margin-left: 25%;\" >
                   ";
        // line 17
        $this->loadTemplate("::formulaire.html.twig", "StockBundle:Repres:livrerMarchandise.html.twig", 17)->display($context);
        // line 18
        echo "                </div>
        ";
        
        $__internal_71c6e724699d5d2aa0bc1d9e03186b887fdef91c707dda8210b2f400deff6e33->leave($__internal_71c6e724699d5d2aa0bc1d9e03186b887fdef91c707dda8210b2f400deff6e33_prof);

        
        $__internal_a42216b9e28c652f7f8f329d5c4b58c2f5f1a4e0b751cf2da757c9adfde2d25d->leave($__internal_a42216b9e28c652f7f8f329d5c4b58c2f5f1a4e0b751cf2da757c9adfde2d25d_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Repres:livrerMarchandise.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 18,  128 => 17,  125 => 16,  116 => 15,  99 => 7,  90 => 6,  80 => 20,  77 => 15,  74 => 6,  65 => 5,  52 => 3,  43 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::representant.html.twig' %}
\t{% block menu %}
\t\t{{ parent() }}
    {% endblock %}
    {% block contenuDuMilieu %}
        {% block debutPage %}
            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-20px;\">
                    <h2 style=\"color: white;margin-top:4%\"> <center>Ordre de  livraison</center></h2>
                </div>
                <br><br><br><br><br><br>
            </div>

        {% endblock %}
\t\t{% block modules %}
                <div class=\"col-xl-6\" style=\"margin-left: 25%;\" >
                   {% include '::formulaire.html.twig' %}
                </div>
        {% endblock %}
    {% endblock %}", "StockBundle:Repres:livrerMarchandise.html.twig", "C:\\wamp64\\www\\stock\\src\\StockBundle/Resources/views/Repres/livrerMarchandise.html.twig");
    }
}
