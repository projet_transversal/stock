<?php

/* StockBundle:Trans:listerContrat.html.twig */
class __TwigTemplate_5cd2ac6ed39474d0c15cb24dc0715446f2a5ef29cfc6f02d0890226593df2d94 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::transporteur.html.twig", "StockBundle:Trans:listerContrat.html.twig", 1);
        $this->blocks = array(
            'menu' => array($this, 'block_menu'),
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::transporteur.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5ae4e0ee6284d59bd2f7ef134e4036062d90cda6c993468011e2d95215baf4ae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5ae4e0ee6284d59bd2f7ef134e4036062d90cda6c993468011e2d95215baf4ae->enter($__internal_5ae4e0ee6284d59bd2f7ef134e4036062d90cda6c993468011e2d95215baf4ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Trans:listerContrat.html.twig"));

        $__internal_30a8dd25772dc8a83cf6e4c01a365b82e79c164dd9d59abb4bee77d2029e67b5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_30a8dd25772dc8a83cf6e4c01a365b82e79c164dd9d59abb4bee77d2029e67b5->enter($__internal_30a8dd25772dc8a83cf6e4c01a365b82e79c164dd9d59abb4bee77d2029e67b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Trans:listerContrat.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5ae4e0ee6284d59bd2f7ef134e4036062d90cda6c993468011e2d95215baf4ae->leave($__internal_5ae4e0ee6284d59bd2f7ef134e4036062d90cda6c993468011e2d95215baf4ae_prof);

        
        $__internal_30a8dd25772dc8a83cf6e4c01a365b82e79c164dd9d59abb4bee77d2029e67b5->leave($__internal_30a8dd25772dc8a83cf6e4c01a365b82e79c164dd9d59abb4bee77d2029e67b5_prof);

    }

    // line 3
    public function block_menu($context, array $blocks = array())
    {
        $__internal_9880dacb62aa9a569fb57df2dbbf1aac7026cc3bf2489c8d46be24263c42917a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9880dacb62aa9a569fb57df2dbbf1aac7026cc3bf2489c8d46be24263c42917a->enter($__internal_9880dacb62aa9a569fb57df2dbbf1aac7026cc3bf2489c8d46be24263c42917a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_773390bf7d6eb7a78b306ba2bebe3c50f6b9ee85dc54ad09f6f6f773804f58ad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_773390bf7d6eb7a78b306ba2bebe3c50f6b9ee85dc54ad09f6f6f773804f58ad->enter($__internal_773390bf7d6eb7a78b306ba2bebe3c50f6b9ee85dc54ad09f6f6f773804f58ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 4
        echo "    ";
        $this->displayParentBlock("menu", $context, $blocks);
        echo "
";
        
        $__internal_773390bf7d6eb7a78b306ba2bebe3c50f6b9ee85dc54ad09f6f6f773804f58ad->leave($__internal_773390bf7d6eb7a78b306ba2bebe3c50f6b9ee85dc54ad09f6f6f773804f58ad_prof);

        
        $__internal_9880dacb62aa9a569fb57df2dbbf1aac7026cc3bf2489c8d46be24263c42917a->leave($__internal_9880dacb62aa9a569fb57df2dbbf1aac7026cc3bf2489c8d46be24263c42917a_prof);

    }

    // line 6
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_3abf2089237a5865b1f3551acc3a2fe8fe268cad9a267adf6c317a3f05d434b7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3abf2089237a5865b1f3551acc3a2fe8fe268cad9a267adf6c317a3f05d434b7->enter($__internal_3abf2089237a5865b1f3551acc3a2fe8fe268cad9a267adf6c317a3f05d434b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_99a2556586da8dd4ee30c98f764ba2a516264a73652d37f3071df5f7eb12af82 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_99a2556586da8dd4ee30c98f764ba2a516264a73652d37f3071df5f7eb12af82->enter($__internal_99a2556586da8dd4ee30c98f764ba2a516264a73652d37f3071df5f7eb12af82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        echo "<!--le block du milieu de la page-->
    ";
        // line 7
        $this->displayBlock('debutPage', $context, $blocks);
        // line 14
        echo "    ";
        $this->displayBlock('modules', $context, $blocks);
        
        $__internal_99a2556586da8dd4ee30c98f764ba2a516264a73652d37f3071df5f7eb12af82->leave($__internal_99a2556586da8dd4ee30c98f764ba2a516264a73652d37f3071df5f7eb12af82_prof);

        
        $__internal_3abf2089237a5865b1f3551acc3a2fe8fe268cad9a267adf6c317a3f05d434b7->leave($__internal_3abf2089237a5865b1f3551acc3a2fe8fe268cad9a267adf6c317a3f05d434b7_prof);

    }

    // line 7
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_0c2b767b1b3a1076338c92c905bda33134c9bee82ed430ca61d14ae62d283876 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0c2b767b1b3a1076338c92c905bda33134c9bee82ed430ca61d14ae62d283876->enter($__internal_0c2b767b1b3a1076338c92c905bda33134c9bee82ed430ca61d14ae62d283876_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_d203b1fc068399a651985bf75f0fb744f493bbc5bbf5f258d3f1df4c8401c65e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d203b1fc068399a651985bf75f0fb744f493bbc5bbf5f258d3f1df4c8401c65e->enter($__internal_d203b1fc068399a651985bf75f0fb744f493bbc5bbf5f258d3f1df4c8401c65e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 8
        echo "        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-45px;\">
                <h3 style=\"color: white;margin-top:4%\"> <center>Liste des Contrats</center></h3>
            </div>
        </div>
    ";
        
        $__internal_d203b1fc068399a651985bf75f0fb744f493bbc5bbf5f258d3f1df4c8401c65e->leave($__internal_d203b1fc068399a651985bf75f0fb744f493bbc5bbf5f258d3f1df4c8401c65e_prof);

        
        $__internal_0c2b767b1b3a1076338c92c905bda33134c9bee82ed430ca61d14ae62d283876->leave($__internal_0c2b767b1b3a1076338c92c905bda33134c9bee82ed430ca61d14ae62d283876_prof);

    }

    // line 14
    public function block_modules($context, array $blocks = array())
    {
        $__internal_38c94b8fa12f50ad7d0d494c775b48c4ba29607da8751c33a926b7a9020c7ca3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_38c94b8fa12f50ad7d0d494c775b48c4ba29607da8751c33a926b7a9020c7ca3->enter($__internal_38c94b8fa12f50ad7d0d494c775b48c4ba29607da8751c33a926b7a9020c7ca3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_837512fd6d9e644a716f784d3f664eb3898e153745013b974d67af24a94ea2d9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_837512fd6d9e644a716f784d3f664eb3898e153745013b974d67af24a94ea2d9->enter($__internal_837512fd6d9e644a716f784d3f664eb3898e153745013b974d67af24a94ea2d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        echo "<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"\">
                <table   class=\"display table table-striped table-bordered\" cellspacing=\"0\" width=\"100%\" id=\"table\" style=\"margin-left:-35%;margin-top:-11%\">
                    <thead>
                    <tr class=\"jumbotron\">
                        <th id=\"id\">Numero</th>
                        <th id=\"libelle\" >Libelle</th>
                        <th id=\"quantite\" >Quantite</th>
                        <th id=\"transporteurs\" >Transporteur</th>
                        <th id=\"representant\">Representant</th>
                        <th id=\"dateDepart\" >Date Depart</th>
                        <th id=\"dateArrivee\">Date Arrivee</th>
                        <th id=\"noteDebit\">Note de Debit</th>
                        <th id=\"visualiser\" >Visualiser</th>
                        <th id=\"valider\">Valider</th>
                    </tr>
                    </thead>
                    <tbody>
                    ";
        // line 33
        $context["id"] = 1;
        // line 34
        echo "                    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["liste"] ?? $this->getContext($context, "liste")));
        foreach ($context['_seq'] as $context["_key"] => $context["l"]) {
            // line 35
            echo "                        <tr>
                            <td>";
            // line 36
            echo twig_escape_filter($this->env, ($context["id"] ?? $this->getContext($context, "id")), "html", null, true);
            echo "</td>
                            <td>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "libelle", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "quantite", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["l"], "transporteur", array()), "login", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["l"], "representant", array()), "login", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 41
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["l"], "getDateDepart", array(), "method"), "d-m-Y"), "html", null, true);
            echo "</td>
                            <td>";
            // line 42
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["l"], "getDateArrivee", array(), "method"), "d-m-Y"), "html", null, true);
            echo "</td>
                            <td>";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "noteDebit", array()), "html", null, true);
            echo "</td>
                            <td><a href=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("visualiser_contrat", array("id" => $this->getAttribute($context["l"], "id", array()))), "html", null, true);
            echo "\" class=\"font-icon font-icon-doc\" style=\"color: red\"></a></td>
                            <td><a href=\"";
            // line 45
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("valider_contrat", array("id" => $this->getAttribute($context["l"], "id", array()))), "html", null, true);
            echo "\" class=\"font-icon font-icon-page\" style=\"color: green\"></a></td>
                        </tr>
                        ";
            // line 47
            $context["id"] = (($context["id"] ?? $this->getContext($context, "id")) + 1);
            // line 48
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['l'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "                    </tbody>
                </table>
            </div><!--.container-fluid-->
        </div><!--.page-content-->
    ";
        
        $__internal_837512fd6d9e644a716f784d3f664eb3898e153745013b974d67af24a94ea2d9->leave($__internal_837512fd6d9e644a716f784d3f664eb3898e153745013b974d67af24a94ea2d9_prof);

        
        $__internal_38c94b8fa12f50ad7d0d494c775b48c4ba29607da8751c33a926b7a9020c7ca3->leave($__internal_38c94b8fa12f50ad7d0d494c775b48c4ba29607da8751c33a926b7a9020c7ca3_prof);

    }

    // line 55
    public function block_script($context, array $blocks = array())
    {
        $__internal_d5ad693b6d15b7cbc88058c59b627aba392b23ee6458ad75365ab725366dc085 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d5ad693b6d15b7cbc88058c59b627aba392b23ee6458ad75365ab725366dc085->enter($__internal_d5ad693b6d15b7cbc88058c59b627aba392b23ee6458ad75365ab725366dc085_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        $__internal_a74b3098b4a16df43dfde43d6f845d992b7d86dad07c0832fa9484f0ba61bfc5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a74b3098b4a16df43dfde43d6f845d992b7d86dad07c0832fa9484f0ba61bfc5->enter($__internal_a74b3098b4a16df43dfde43d6f845d992b7d86dad07c0832fa9484f0ba61bfc5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        // line 56
        echo "    ";
        $this->displayParentBlock("script", $context, $blocks);
        echo "
    <script src=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/jquery.tablednd.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table-reorder-rows-init.js"), "html", null, true);
        echo "\"></script>
    ";
        
        $__internal_a74b3098b4a16df43dfde43d6f845d992b7d86dad07c0832fa9484f0ba61bfc5->leave($__internal_a74b3098b4a16df43dfde43d6f845d992b7d86dad07c0832fa9484f0ba61bfc5_prof);

        
        $__internal_d5ad693b6d15b7cbc88058c59b627aba392b23ee6458ad75365ab725366dc085->leave($__internal_d5ad693b6d15b7cbc88058c59b627aba392b23ee6458ad75365ab725366dc085_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Trans:listerContrat.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  242 => 60,  238 => 59,  234 => 58,  230 => 57,  225 => 56,  216 => 55,  202 => 49,  196 => 48,  194 => 47,  189 => 45,  185 => 44,  181 => 43,  177 => 42,  173 => 41,  169 => 40,  165 => 39,  161 => 38,  157 => 37,  153 => 36,  150 => 35,  145 => 34,  143 => 33,  114 => 14,  99 => 8,  90 => 7,  79 => 14,  77 => 7,  66 => 6,  53 => 4,  44 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::transporteur.html.twig' %}

{% block menu %}
    {{ parent() }}
{% endblock %}
{% block contenuDuMilieu %}<!--le block du milieu de la page-->
    {% block debutPage %}
        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-45px;\">
                <h3 style=\"color: white;margin-top:4%\"> <center>Liste des Contrats</center></h3>
            </div>
        </div>
    {% endblock %}
    {% block modules %}<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"\">
                <table   class=\"display table table-striped table-bordered\" cellspacing=\"0\" width=\"100%\" id=\"table\" style=\"margin-left:-35%;margin-top:-11%\">
                    <thead>
                    <tr class=\"jumbotron\">
                        <th id=\"id\">Numero</th>
                        <th id=\"libelle\" >Libelle</th>
                        <th id=\"quantite\" >Quantite</th>
                        <th id=\"transporteurs\" >Transporteur</th>
                        <th id=\"representant\">Representant</th>
                        <th id=\"dateDepart\" >Date Depart</th>
                        <th id=\"dateArrivee\">Date Arrivee</th>
                        <th id=\"noteDebit\">Note de Debit</th>
                        <th id=\"visualiser\" >Visualiser</th>
                        <th id=\"valider\">Valider</th>
                    </tr>
                    </thead>
                    <tbody>
                    {% set id = 1 %}
                    {% for l in liste %}
                        <tr>
                            <td>{{ id }}</td>
                            <td>{{ l.libelle }}</td>
                            <td>{{ l.quantite }}</td>
                            <td>{{ l.transporteur.login }}</td>
                            <td>{{ l.representant.login }}</td>
                            <td>{{ l.getDateDepart()|date('d-m-Y') }}</td>
                            <td>{{ l.getDateArrivee()|date('d-m-Y') }}</td>
                            <td>{{ l.noteDebit }}</td>
                            <td><a href=\"{{ path(\"visualiser_contrat\", {'id':l.id}) }}\" class=\"font-icon font-icon-doc\" style=\"color: red\"></a></td>
                            <td><a href=\"{{ path(\"valider_contrat\", {'id':l.id}) }}\" class=\"font-icon font-icon-page\" style=\"color: green\"></a></td>
                        </tr>
                        {% set id = id +1 %}
                    {% endfor %}
                    </tbody>
                </table>
            </div><!--.container-fluid-->
        </div><!--.page-content-->
    {% endblock %}
{% endblock %}
{% block script %}
    {{ parent() }}
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/jquery.tablednd.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table-reorder-rows-init.js') }}\"></script>
    {#<script type=\"text/javascript\">{{ liste(liste) }}</script>#}
{% endblock %}", "StockBundle:Trans:listerContrat.html.twig", "C:\\wamp64\\www\\stock\\src\\StockBundle/Resources/views/Trans/listerContrat.html.twig");
    }
}
