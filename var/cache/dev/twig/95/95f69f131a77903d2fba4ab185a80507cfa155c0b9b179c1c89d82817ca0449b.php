<?php

/* StockBundle:Transporteur:addTransporteur.html.twig */
class __TwigTemplate_f09f176b2cf30be7414661d5715ffeac8a1c197c76cecd03c172db36b2ed6eb9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:Transporteur:addTransporteur.html.twig", 1);
        $this->blocks = array(
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_63b3d77e7da3c1d29117641664566f700acb37bbe72ced59eaa3b9d6bf4c28cb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_63b3d77e7da3c1d29117641664566f700acb37bbe72ced59eaa3b9d6bf4c28cb->enter($__internal_63b3d77e7da3c1d29117641664566f700acb37bbe72ced59eaa3b9d6bf4c28cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Transporteur:addTransporteur.html.twig"));

        $__internal_b07011076fd206579fe2427706151181170f74229c2ea507b7e6c71858893b5d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b07011076fd206579fe2427706151181170f74229c2ea507b7e6c71858893b5d->enter($__internal_b07011076fd206579fe2427706151181170f74229c2ea507b7e6c71858893b5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Transporteur:addTransporteur.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_63b3d77e7da3c1d29117641664566f700acb37bbe72ced59eaa3b9d6bf4c28cb->leave($__internal_63b3d77e7da3c1d29117641664566f700acb37bbe72ced59eaa3b9d6bf4c28cb_prof);

        
        $__internal_b07011076fd206579fe2427706151181170f74229c2ea507b7e6c71858893b5d->leave($__internal_b07011076fd206579fe2427706151181170f74229c2ea507b7e6c71858893b5d_prof);

    }

    // line 2
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_c541b1a7fd21c8211de55d4d0ec0f9bf530e7e820bf4496f519a64858acfde91 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c541b1a7fd21c8211de55d4d0ec0f9bf530e7e820bf4496f519a64858acfde91->enter($__internal_c541b1a7fd21c8211de55d4d0ec0f9bf530e7e820bf4496f519a64858acfde91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_fc478645fe41c378f8fb4d5b33a0228883f21eb841f4d52467dbdf6644a6e711 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fc478645fe41c378f8fb4d5b33a0228883f21eb841f4d52467dbdf6644a6e711->enter($__internal_fc478645fe41c378f8fb4d5b33a0228883f21eb841f4d52467dbdf6644a6e711_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        // line 3
        echo "        ";
        $this->displayBlock('debutPage', $context, $blocks);
        // line 12
        echo "        ";
        $this->displayBlock('modules', $context, $blocks);
        // line 17
        echo "    ";
        
        $__internal_fc478645fe41c378f8fb4d5b33a0228883f21eb841f4d52467dbdf6644a6e711->leave($__internal_fc478645fe41c378f8fb4d5b33a0228883f21eb841f4d52467dbdf6644a6e711_prof);

        
        $__internal_c541b1a7fd21c8211de55d4d0ec0f9bf530e7e820bf4496f519a64858acfde91->leave($__internal_c541b1a7fd21c8211de55d4d0ec0f9bf530e7e820bf4496f519a64858acfde91_prof);

    }

    // line 3
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_4106194f1283aa514958e19cf0c631bf93b2cbaf06438727c758c61086ef2e2a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4106194f1283aa514958e19cf0c631bf93b2cbaf06438727c758c61086ef2e2a->enter($__internal_4106194f1283aa514958e19cf0c631bf93b2cbaf06438727c758c61086ef2e2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_e62244311074fe09f46ed83eccf91a3a51ca8872ce8783d8dc8f7616333c7431 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e62244311074fe09f46ed83eccf91a3a51ca8872ce8783d8dc8f7616333c7431->enter($__internal_e62244311074fe09f46ed83eccf91a3a51ca8872ce8783d8dc8f7616333c7431_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 4
        echo "            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                    <h3 style=\"color: white;margin-top:4%\"> <center>Ajouter Transporteur</center></h3>
                </div>
                <br><br><br><br><br><br>
            </div>

        ";
        
        $__internal_e62244311074fe09f46ed83eccf91a3a51ca8872ce8783d8dc8f7616333c7431->leave($__internal_e62244311074fe09f46ed83eccf91a3a51ca8872ce8783d8dc8f7616333c7431_prof);

        
        $__internal_4106194f1283aa514958e19cf0c631bf93b2cbaf06438727c758c61086ef2e2a->leave($__internal_4106194f1283aa514958e19cf0c631bf93b2cbaf06438727c758c61086ef2e2a_prof);

    }

    // line 12
    public function block_modules($context, array $blocks = array())
    {
        $__internal_4cd14870ae09090ce171afc93feed70c934fddfc375f119c7a3a663cb4e029ff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4cd14870ae09090ce171afc93feed70c934fddfc375f119c7a3a663cb4e029ff->enter($__internal_4cd14870ae09090ce171afc93feed70c934fddfc375f119c7a3a663cb4e029ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_2899885e85fc0821943d1d57a41802a6fe55f8967901ac940956b568a3a8a534 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2899885e85fc0821943d1d57a41802a6fe55f8967901ac940956b568a3a8a534->enter($__internal_2899885e85fc0821943d1d57a41802a6fe55f8967901ac940956b568a3a8a534_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        // line 13
        echo "            <div class=\"col-xl-6\" style=\"margin-left: 27%\">
                ";
        // line 14
        $this->loadTemplate("::formulaire.html.twig", "StockBundle:Transporteur:addTransporteur.html.twig", 14)->display($context);
        // line 15
        echo "            </div>
        ";
        
        $__internal_2899885e85fc0821943d1d57a41802a6fe55f8967901ac940956b568a3a8a534->leave($__internal_2899885e85fc0821943d1d57a41802a6fe55f8967901ac940956b568a3a8a534_prof);

        
        $__internal_4cd14870ae09090ce171afc93feed70c934fddfc375f119c7a3a663cb4e029ff->leave($__internal_4cd14870ae09090ce171afc93feed70c934fddfc375f119c7a3a663cb4e029ff_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Transporteur:addTransporteur.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 15,  105 => 14,  102 => 13,  93 => 12,  76 => 4,  67 => 3,  57 => 17,  54 => 12,  51 => 3,  42 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}
    {% block contenuDuMilieu %}
        {% block debutPage %}
            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                    <h3 style=\"color: white;margin-top:4%\"> <center>Ajouter Transporteur</center></h3>
                </div>
                <br><br><br><br><br><br>
            </div>

        {% endblock %}
        {% block modules %}
            <div class=\"col-xl-6\" style=\"margin-left: 27%\">
                {% include '::formulaire.html.twig' %}
            </div>
        {% endblock %}
    {% endblock %}", "StockBundle:Transporteur:addTransporteur.html.twig", "C:\\wamp64\\www\\stock\\src\\StockBundle/Resources/views/Transporteur/addTransporteur.html.twig");
    }
}
