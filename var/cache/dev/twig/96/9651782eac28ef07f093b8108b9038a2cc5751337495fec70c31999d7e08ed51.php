<?php

/* StockBundle:Trans:ajouterProformat.html.twig */
class __TwigTemplate_ec34b96c9320b4bf8a14cc971999e5ae22aa72575d552fae966be9357a1accf0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::transporteur.html.twig", "StockBundle:Trans:ajouterProformat.html.twig", 1);
        $this->blocks = array(
            'menu' => array($this, 'block_menu'),
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::transporteur.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_50d06dbd9425683eb43ec2a8ef45989990eb98275672e110cfbf4fcd2fe76d25 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_50d06dbd9425683eb43ec2a8ef45989990eb98275672e110cfbf4fcd2fe76d25->enter($__internal_50d06dbd9425683eb43ec2a8ef45989990eb98275672e110cfbf4fcd2fe76d25_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Trans:ajouterProformat.html.twig"));

        $__internal_7d799ee341b23989344b7fb724ed6619735a7ed2bf7df1cdbbeaeeb18da968f5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d799ee341b23989344b7fb724ed6619735a7ed2bf7df1cdbbeaeeb18da968f5->enter($__internal_7d799ee341b23989344b7fb724ed6619735a7ed2bf7df1cdbbeaeeb18da968f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Trans:ajouterProformat.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_50d06dbd9425683eb43ec2a8ef45989990eb98275672e110cfbf4fcd2fe76d25->leave($__internal_50d06dbd9425683eb43ec2a8ef45989990eb98275672e110cfbf4fcd2fe76d25_prof);

        
        $__internal_7d799ee341b23989344b7fb724ed6619735a7ed2bf7df1cdbbeaeeb18da968f5->leave($__internal_7d799ee341b23989344b7fb724ed6619735a7ed2bf7df1cdbbeaeeb18da968f5_prof);

    }

    // line 2
    public function block_menu($context, array $blocks = array())
    {
        $__internal_c3451eccf205de4d0b53ea91b3218e807d2c8e204618c72d511787f958ffe1ab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c3451eccf205de4d0b53ea91b3218e807d2c8e204618c72d511787f958ffe1ab->enter($__internal_c3451eccf205de4d0b53ea91b3218e807d2c8e204618c72d511787f958ffe1ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_5f51e84694fd3dc3cc3d3bfe5726b05b34720c5e56926de9086b8d84bd88f1a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5f51e84694fd3dc3cc3d3bfe5726b05b34720c5e56926de9086b8d84bd88f1a8->enter($__internal_5f51e84694fd3dc3cc3d3bfe5726b05b34720c5e56926de9086b8d84bd88f1a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 3
        echo "\t\t";
        $this->displayParentBlock("menu", $context, $blocks);
        echo "
    ";
        
        $__internal_5f51e84694fd3dc3cc3d3bfe5726b05b34720c5e56926de9086b8d84bd88f1a8->leave($__internal_5f51e84694fd3dc3cc3d3bfe5726b05b34720c5e56926de9086b8d84bd88f1a8_prof);

        
        $__internal_c3451eccf205de4d0b53ea91b3218e807d2c8e204618c72d511787f958ffe1ab->leave($__internal_c3451eccf205de4d0b53ea91b3218e807d2c8e204618c72d511787f958ffe1ab_prof);

    }

    // line 5
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_6b75465a87bbffba3c6fd5079140ba44baaf0093ce12cfe159aae9cbcb958b6f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6b75465a87bbffba3c6fd5079140ba44baaf0093ce12cfe159aae9cbcb958b6f->enter($__internal_6b75465a87bbffba3c6fd5079140ba44baaf0093ce12cfe159aae9cbcb958b6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_8a50a83250244dd4ac8d8437764b7670104e95a3253391a67f224bfd425c6f96 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a50a83250244dd4ac8d8437764b7670104e95a3253391a67f224bfd425c6f96->enter($__internal_8a50a83250244dd4ac8d8437764b7670104e95a3253391a67f224bfd425c6f96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        // line 6
        echo "        ";
        $this->displayBlock('debutPage', $context, $blocks);
        // line 15
        echo "\t\t";
        $this->displayBlock('modules', $context, $blocks);
        // line 20
        echo "    ";
        
        $__internal_8a50a83250244dd4ac8d8437764b7670104e95a3253391a67f224bfd425c6f96->leave($__internal_8a50a83250244dd4ac8d8437764b7670104e95a3253391a67f224bfd425c6f96_prof);

        
        $__internal_6b75465a87bbffba3c6fd5079140ba44baaf0093ce12cfe159aae9cbcb958b6f->leave($__internal_6b75465a87bbffba3c6fd5079140ba44baaf0093ce12cfe159aae9cbcb958b6f_prof);

    }

    // line 6
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_b9effe35cede84d38704daf729b9ee161b61ef0a68bef88b4e42a74eaf0a5db2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b9effe35cede84d38704daf729b9ee161b61ef0a68bef88b4e42a74eaf0a5db2->enter($__internal_b9effe35cede84d38704daf729b9ee161b61ef0a68bef88b4e42a74eaf0a5db2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_8776f94dacdae094edebb61db193d17b34bd3eed127eba2961a19cc288aab725 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8776f94dacdae094edebb61db193d17b34bd3eed127eba2961a19cc288aab725->enter($__internal_8776f94dacdae094edebb61db193d17b34bd3eed127eba2961a19cc288aab725_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 7
        echo "            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-45px;\">
                    <h3 style=\"color: white;margin-top:4%\"> <center>Ajouter Proformat</center></h3>
                </div>
                <br><br><br><br><br><br>
            </div>

        ";
        
        $__internal_8776f94dacdae094edebb61db193d17b34bd3eed127eba2961a19cc288aab725->leave($__internal_8776f94dacdae094edebb61db193d17b34bd3eed127eba2961a19cc288aab725_prof);

        
        $__internal_b9effe35cede84d38704daf729b9ee161b61ef0a68bef88b4e42a74eaf0a5db2->leave($__internal_b9effe35cede84d38704daf729b9ee161b61ef0a68bef88b4e42a74eaf0a5db2_prof);

    }

    // line 15
    public function block_modules($context, array $blocks = array())
    {
        $__internal_6209bb50de780d70a25eed76b9235b551570b2670a80f919835112222eba742c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6209bb50de780d70a25eed76b9235b551570b2670a80f919835112222eba742c->enter($__internal_6209bb50de780d70a25eed76b9235b551570b2670a80f919835112222eba742c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_5f25aa3c908beeb15c5cbd0585d1ae1840d017e227a792dc1c0ca1ad106439de = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5f25aa3c908beeb15c5cbd0585d1ae1840d017e227a792dc1c0ca1ad106439de->enter($__internal_5f25aa3c908beeb15c5cbd0585d1ae1840d017e227a792dc1c0ca1ad106439de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        // line 16
        echo "                <div class=\"col-xl-6\" style=\"margin-left: 25%;\" >
                   ";
        // line 17
        $this->loadTemplate("::formulaire.html.twig", "StockBundle:Trans:ajouterProformat.html.twig", 17)->display($context);
        // line 18
        echo "                </div>
        ";
        
        $__internal_5f25aa3c908beeb15c5cbd0585d1ae1840d017e227a792dc1c0ca1ad106439de->leave($__internal_5f25aa3c908beeb15c5cbd0585d1ae1840d017e227a792dc1c0ca1ad106439de_prof);

        
        $__internal_6209bb50de780d70a25eed76b9235b551570b2670a80f919835112222eba742c->leave($__internal_6209bb50de780d70a25eed76b9235b551570b2670a80f919835112222eba742c_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Trans:ajouterProformat.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 18,  128 => 17,  125 => 16,  116 => 15,  99 => 7,  90 => 6,  80 => 20,  77 => 15,  74 => 6,  65 => 5,  52 => 3,  43 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::transporteur.html.twig' %}
\t{% block menu %}
\t\t{{ parent() }}
    {% endblock %}
    {% block contenuDuMilieu %}
        {% block debutPage %}
            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-45px;\">
                    <h3 style=\"color: white;margin-top:4%\"> <center>Ajouter Proformat</center></h3>
                </div>
                <br><br><br><br><br><br>
            </div>

        {% endblock %}
\t\t{% block modules %}
                <div class=\"col-xl-6\" style=\"margin-left: 25%;\" >
                   {% include '::formulaire.html.twig' %}
                </div>
        {% endblock %}
    {% endblock %}", "StockBundle:Trans:ajouterProformat.html.twig", "C:\\wamp64\\www\\stock\\src\\StockBundle/Resources/views/Trans/ajouterProformat.html.twig");
    }
}
