<?php

/* StockBundle:InstructionLivraison:addInstructionLivraison.html.twig */
class __TwigTemplate_c2f0994829a0ce4a20f2a99fa5b28ca954fbd361677a13b7ff3b811bf14d5e77 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:InstructionLivraison:addInstructionLivraison.html.twig", 1);
        $this->blocks = array(
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ff82799ac041f3c5f578cc95261e678f3dda44c38fdd8cd5d0d27e6ca7b424eb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ff82799ac041f3c5f578cc95261e678f3dda44c38fdd8cd5d0d27e6ca7b424eb->enter($__internal_ff82799ac041f3c5f578cc95261e678f3dda44c38fdd8cd5d0d27e6ca7b424eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:InstructionLivraison:addInstructionLivraison.html.twig"));

        $__internal_eb7c6847b0dc5c8298b807da4ecf14adad693a958f82e036a1ec742221740db8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eb7c6847b0dc5c8298b807da4ecf14adad693a958f82e036a1ec742221740db8->enter($__internal_eb7c6847b0dc5c8298b807da4ecf14adad693a958f82e036a1ec742221740db8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:InstructionLivraison:addInstructionLivraison.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ff82799ac041f3c5f578cc95261e678f3dda44c38fdd8cd5d0d27e6ca7b424eb->leave($__internal_ff82799ac041f3c5f578cc95261e678f3dda44c38fdd8cd5d0d27e6ca7b424eb_prof);

        
        $__internal_eb7c6847b0dc5c8298b807da4ecf14adad693a958f82e036a1ec742221740db8->leave($__internal_eb7c6847b0dc5c8298b807da4ecf14adad693a958f82e036a1ec742221740db8_prof);

    }

    // line 2
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_6b9025670285dd5124b4058b1070c5645b7c790b04cbc4a8cae4e61fbbeb1492 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6b9025670285dd5124b4058b1070c5645b7c790b04cbc4a8cae4e61fbbeb1492->enter($__internal_6b9025670285dd5124b4058b1070c5645b7c790b04cbc4a8cae4e61fbbeb1492_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_f8a1e1855cd9f1d11160479942817b181d945046b80681116349df1c900284dd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f8a1e1855cd9f1d11160479942817b181d945046b80681116349df1c900284dd->enter($__internal_f8a1e1855cd9f1d11160479942817b181d945046b80681116349df1c900284dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        // line 3
        echo "    ";
        $this->displayBlock('debutPage', $context, $blocks);
        // line 12
        echo "    ";
        $this->displayBlock('modules', $context, $blocks);
        
        $__internal_f8a1e1855cd9f1d11160479942817b181d945046b80681116349df1c900284dd->leave($__internal_f8a1e1855cd9f1d11160479942817b181d945046b80681116349df1c900284dd_prof);

        
        $__internal_6b9025670285dd5124b4058b1070c5645b7c790b04cbc4a8cae4e61fbbeb1492->leave($__internal_6b9025670285dd5124b4058b1070c5645b7c790b04cbc4a8cae4e61fbbeb1492_prof);

    }

    // line 3
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_77085134350fd38fb982260360356db80c35718258c4e9778f3c06616a0dcb14 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_77085134350fd38fb982260360356db80c35718258c4e9778f3c06616a0dcb14->enter($__internal_77085134350fd38fb982260360356db80c35718258c4e9778f3c06616a0dcb14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_fb0e0ac59b4f1e967dacaf2e3748019c9e6e33a31d99e847ba7c195a782c033f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fb0e0ac59b4f1e967dacaf2e3748019c9e6e33a31d99e847ba7c195a782c033f->enter($__internal_fb0e0ac59b4f1e967dacaf2e3748019c9e6e33a31d99e847ba7c195a782c033f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 4
        echo "        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                <h3 style=\"color: white;margin-top:4%\"> <center>Ajouter Instruction de livraison</center></h3>
            </div>
            <br><br><br><br><br><br>
        </div>

    ";
        
        $__internal_fb0e0ac59b4f1e967dacaf2e3748019c9e6e33a31d99e847ba7c195a782c033f->leave($__internal_fb0e0ac59b4f1e967dacaf2e3748019c9e6e33a31d99e847ba7c195a782c033f_prof);

        
        $__internal_77085134350fd38fb982260360356db80c35718258c4e9778f3c06616a0dcb14->leave($__internal_77085134350fd38fb982260360356db80c35718258c4e9778f3c06616a0dcb14_prof);

    }

    // line 12
    public function block_modules($context, array $blocks = array())
    {
        $__internal_00f8f40862391db5d144424fac2b75683ffb2013145e0bcef56ec6fc1072f1f6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_00f8f40862391db5d144424fac2b75683ffb2013145e0bcef56ec6fc1072f1f6->enter($__internal_00f8f40862391db5d144424fac2b75683ffb2013145e0bcef56ec6fc1072f1f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_3c6dbde5ae4fcf964ea422288c9b018b5972e2a747a1a08c05c0a7cb0b6f7301 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3c6dbde5ae4fcf964ea422288c9b018b5972e2a747a1a08c05c0a7cb0b6f7301->enter($__internal_3c6dbde5ae4fcf964ea422288c9b018b5972e2a747a1a08c05c0a7cb0b6f7301_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        // line 13
        echo "        <div class=\"col-xl-4\" style=\"margin-left: 25%;\" >
            ";
        // line 14
        $this->loadTemplate("::formulaire.html.twig", "StockBundle:InstructionLivraison:addInstructionLivraison.html.twig", 14)->display($context);
        // line 15
        echo "        </div>
    ";
        
        $__internal_3c6dbde5ae4fcf964ea422288c9b018b5972e2a747a1a08c05c0a7cb0b6f7301->leave($__internal_3c6dbde5ae4fcf964ea422288c9b018b5972e2a747a1a08c05c0a7cb0b6f7301_prof);

        
        $__internal_00f8f40862391db5d144424fac2b75683ffb2013145e0bcef56ec6fc1072f1f6->leave($__internal_00f8f40862391db5d144424fac2b75683ffb2013145e0bcef56ec6fc1072f1f6_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:InstructionLivraison:addInstructionLivraison.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 15,  103 => 14,  100 => 13,  91 => 12,  74 => 4,  65 => 3,  54 => 12,  51 => 3,  42 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}
{% block contenuDuMilieu %}
    {% block debutPage %}
        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                <h3 style=\"color: white;margin-top:4%\"> <center>Ajouter Instruction de livraison</center></h3>
            </div>
            <br><br><br><br><br><br>
        </div>

    {% endblock %}
    {% block modules %}
        <div class=\"col-xl-4\" style=\"margin-left: 25%;\" >
            {% include '::formulaire.html.twig' %}
        </div>
    {% endblock %}
{% endblock %}", "StockBundle:InstructionLivraison:addInstructionLivraison.html.twig", "C:\\wamp64\\www\\stock\\src\\StockBundle/Resources/views/InstructionLivraison/addInstructionLivraison.html.twig");
    }
}
