<?php

/* ::base.html.twig */
class __TwigTemplate_56c0efbdbf3c70abb905f082d8ac76c68bf364dcaa9a69338eee08c13f65212b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'menu' => array($this, 'block_menu'),
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0099a362ca64efb866efb9457ce06baa0632078b91ef2492ff1e0c1010e6d91b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0099a362ca64efb866efb9457ce06baa0632078b91ef2492ff1e0c1010e6d91b->enter($__internal_0099a362ca64efb866efb9457ce06baa0632078b91ef2492ff1e0c1010e6d91b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        $__internal_f3c34ce650a0783ec055e5b491f034260611b25e2b6a035090be891fc67f9c66 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f3c34ce650a0783ec055e5b491f034260611b25e2b6a035090be891fc67f9c66->enter($__internal_f3c34ce650a0783ec055e5b491f034260611b25e2b6a035090be891fc67f9c66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head lang=\"en\">
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\">
        <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
        <title>Trading</title>

        <link href=\"img/favicon.144x144.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"144x144\">
        <link href=\"img/favicon.114x114.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"114x114\">
        <link href=\"img/favicon.72x72.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"72x72\">
        <link href=\"img/favicon.57x57.png\" rel=\"apple-touch-icon\" type=\"image/png\">
        <link href=\"img/favicon.png\" rel=\"icon\" type=\"image/png\">
        <link href=\"img/favicon.ico\" rel=\"shortcut icon\">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
        <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
        <![endif]-->
        <link rel=\"stylesheet\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/lib/lobipanel/lobipanel.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/separate/vendor/lobipanel.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/lib/jqueryui/jquery-ui.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/separate/pages/widgets.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/lib/font-awesome/font-awesome.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/lib/bootstrap/bootstrap.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/main.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/lib/acceuil.css"), "html", null, true);
        echo "\">



    </head>

    <body class=\"with-side-menu control-panel control-panel-compact\"  style=\"background: white\">
\t     ";
        // line 35
        $this->displayBlock('menu', $context, $blocks);
        // line 110
        echo "        <div class=\"page-content\" style=\"background: white\">
            <!--entete du site-->
                <div class=\"container-fluid\" style=\"height: 70px\">
                    <h2 style=\"font-weight: bold;font-size:35px;color:#3299CC; width:50%;margin-top:-10%\">Trading Platform</h2>
                    <img src=\"";
        // line 114
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/bateau.png"), "html", null, true);
        echo "\" style=\"margin-top:-9%;margin-left:30%;\">
                </div><!--.container-fluid-->
                <div class=\"site-header-search-container\"><br><br>
                    <form class=\"site-header-search\" style=\"width: 30% ;float: right;margin-top:-5%;\">
                        <input type=\"text\" placeholder=\"Search\"/>
                        <button type=\"submit\">
                            <span class=\"font-icon-search\"></span>
                        </button>
                        <div class=\"overlay\"></div>
                    </form><br><br>
                </div>
            <!--entete du site-->

            ";
        // line 127
        $this->displayBlock('contenuDuMilieu', $context, $blocks);
        // line 205
        echo "<!--Contenu du milieu-->
        </div><!--.page-content-->

        ";
        // line 208
        $this->displayBlock('script', $context, $blocks);
        // line 215
        echo "
            <script type=\"text/javascript\" src=\"js/lib/jqueryui/jquery-ui.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/lobipanel/lobipanel.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/match-height/jquery.matchHeight.min.js\"></script>
            <script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>

            <script src=\"js/lib/jquery/jquery.min.js\"></script>
            <script src=\"js/lib/tether/tether.min.js\"></script>
            <script src=\"js/lib/bootstrap/bootstrap.min.js\"></script>
            <script src=\"js/plugins.js\"></script>

            <script type=\"text/javascript\" src=\"js/lib/match-height/jquery.matchHeight.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/moment/moment-with-locales.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/eonasdan-bootstrap-datetimepicker/bootstrap-datetimepicker.min.js\"></script>
            <script src=\"js/lib/fullcalendar/fullcalendar.min.js\"></script>
            <script src=\"js/lib/fullcalendar/fullcalendar-init.js\"></script>

            <script>
                \$(document).ready(function() {
                    \$('.panel').lobiPanel({
                        sortable: true
                    });
                    \$('.panel').on('dragged.lobiPanel', function(ev, lobiPanel){
                        \$('.dahsboard-column').matchHeight();
                    });

                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawChart);
                    function drawChart() {
                        var dataTable = new google.visualization.DataTable();
                        dataTable.addColumn('string', 'Day');
                        dataTable.addColumn('number', 'Values');
                        // A column for custom tooltip content
                        dataTable.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});
                        dataTable.addRows([
                            ['MON',  130, ' '],
                            ['TUE',  130, '130'],
                            ['WED',  180, '180'],
                            ['THU',  175, '175'],
                            ['FRI',  200, '200'],
                            ['SAT',  170, '170'],
                            ['SUN',  250, '250'],
                            ['MON',  220, '220'],
                            ['TUE',  220, ' ']
                        ]);

                        var options = {
                            height: 314,
                            legend: 'none',
                            areaOpacity: 0.18,
                            axisTitlesPosition: 'out',
                            hAxis: {
                                title: '',
                                textStyle: {
                                    color: '#fff',
                                    fontName: 'Proxima Nova',
                                    fontSize: 11,
                                    bold: true,
                                    italic: false
                                },
                                textPosition: 'out'
                            },
                            vAxis: {
                                minValue: 0,
                                textPosition: 'out',
                                textStyle: {
                                    color: '#fff',
                                    fontName: 'Proxima Nova',
                                    fontSize: 11,
                                    bold: true,
                                    italic: false
                                },
                                baselineColor: '#16b4fc',
                                ticks: [0,25,50,75,100,125,150,175,200,225,250,275,300,325,350],
                                gridlines: {
                                    color: '#1ba0fc',
                                    count: 15
                                },
                            },
                            lineWidth: 2,
                            colors: ['#fff'],
                            curveType: 'function',
                            pointSize: 5,
                            pointShapeType: 'circle',
                            pointFillColor: '#f00',
                            backgroundColor: {
                                fill: '#008ffb',
                                strokeWidth: 0,
                            },
                            chartArea:{
                                left:0,
                                top:0,
                                width:'100%',
                                height:'100%'
                            },
                            fontSize: 11,
                            fontName: 'Proxima Nova',
                            tooltip: {
                                trigger: 'selection',
                                isHtml: true
                            }
                        };

                        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
                        chart.draw(dataTable, options);
                    }
                    \$(window).resize(function(){
                        drawChart();
                        setTimeout(function(){
                        }, 1000);
                    });
                });
            </script>
    </body>

</html>";
        
        $__internal_0099a362ca64efb866efb9457ce06baa0632078b91ef2492ff1e0c1010e6d91b->leave($__internal_0099a362ca64efb866efb9457ce06baa0632078b91ef2492ff1e0c1010e6d91b_prof);

        
        $__internal_f3c34ce650a0783ec055e5b491f034260611b25e2b6a035090be891fc67f9c66->leave($__internal_f3c34ce650a0783ec055e5b491f034260611b25e2b6a035090be891fc67f9c66_prof);

    }

    // line 35
    public function block_menu($context, array $blocks = array())
    {
        $__internal_0afd6a15d1d796bbcc4cd4fc7639dc010066ec4c83dabab699e1eb0940ec6405 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0afd6a15d1d796bbcc4cd4fc7639dc010066ec4c83dabab699e1eb0940ec6405->enter($__internal_0afd6a15d1d796bbcc4cd4fc7639dc010066ec4c83dabab699e1eb0940ec6405_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_98e98f3470b2ff54dddde69008e8bcfa9a87f8f6299605a6cae1f21c8cce4cd2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_98e98f3470b2ff54dddde69008e8bcfa9a87f8f6299605a6cae1f21c8cce4cd2->enter($__internal_98e98f3470b2ff54dddde69008e8bcfa9a87f8f6299605a6cae1f21c8cce4cd2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 36
        echo "        <nav class=\"side-menu side-menu-big-icon\" style=\"background: #3299CC;overflow: hidden\">
            <nav class=\"sidebar-header\">
                <img src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/man.png"), "html", null, true);
        echo "\" style=\"margin-left:35%;margin-top:-15%;\"><br><br>
                <h6 style=\"text-align:center;color:white;\">User Name</h6>
                <h6 style=\"text-align:center;color:white;\">Profil</h6>
            </nav>
            <hr style=\"margin-left:20%;margin-right: 20%;background: red;height:1px\">
            <ul class=\"side-menu-list\">
                <li class=\"opened\" >
                    <span id=\"acc\">
                    \t<a href=\"";
        // line 46
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("accueil");
        echo "\" id=\"acc\" >
\t                        <i class=\"font-icon font-icon-home\"  style=\"color: white;\" ></i>
                            <span class=\"lbl\" style=\"\" id=\"acceuil\"><h6>Accueil</h6></span>
                        </a>
                    </span>
                </li>
                <li class=\"green with-sub\">
\t\t            <span id=\"prod\">
\t\t                <i class=\"font-icon font-icon-view-grid\" style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\" ><h6>Produit</h6></span>
\t\t            </span>
                    <ul>
                        <li><a href=\"";
        // line 58
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_produit");
        echo "\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"";
        // line 59
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_produit");
        echo "\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"";
        // line 60
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_produit");
        echo "\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                <li class=\"green with-sub\">
                    <span id=\"rep\">
                        <i class=\"font-icon font-icon-user\"  style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\"><h6>Representants</h6></span>
                    </span>
                    <ul>
                        <li><a href=\"";
        // line 69
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_representant");
        echo "\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"";
        // line 70
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_representant");
        echo "\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"";
        // line 71
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_representant");
        echo "\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                <li class=\" with-sub\">
                    <span id=\"contrat\">
                        <i class=\"font-icon font-icon-archive\"  style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\"><h6>Contrats</h6></span>
                    </span>
                    <ul>
                        <li><a href=\"";
        // line 80
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_contratMarchandise");
        echo "\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"";
        // line 81
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_contrat");
        echo "\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"";
        // line 82
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_contrat");
        echo "\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                <li class=\"with-sub\">
                    <span id=\"transporteur\">
                        <i class=\"font-icon font-icon-weather-waves\" style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\"><h6>Transporteurs</h6></span>
                    </span>
                    <ul>
                        <li><a href=\"";
        // line 91
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_transporteur");
        echo "\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"";
        // line 92
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_transporteur");
        echo "\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"";
        // line 93
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_transporteur");
        echo "\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                <li class=\"with-sub\">
                    <span id=\"instr\">
                        <i class=\"font-icon font-icon-weather-cloud\" style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\"><h6>Instructions de livraison</h6></span>
                    </span>
                    <ul>
                        <li><a href=\"";
        // line 102
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_instruction");
        echo "\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"";
        // line 103
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_instruction");
        echo "\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"";
        // line 104
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_instruction");
        echo "\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
            </ul>
        </nav><!--.side-menu-->
\t\t ";
        
        $__internal_98e98f3470b2ff54dddde69008e8bcfa9a87f8f6299605a6cae1f21c8cce4cd2->leave($__internal_98e98f3470b2ff54dddde69008e8bcfa9a87f8f6299605a6cae1f21c8cce4cd2_prof);

        
        $__internal_0afd6a15d1d796bbcc4cd4fc7639dc010066ec4c83dabab699e1eb0940ec6405->leave($__internal_0afd6a15d1d796bbcc4cd4fc7639dc010066ec4c83dabab699e1eb0940ec6405_prof);

    }

    // line 127
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_55cf2eb1dc2efbff7dc7caebefe2481773103709a59b985109973eb228e853e2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_55cf2eb1dc2efbff7dc7caebefe2481773103709a59b985109973eb228e853e2->enter($__internal_55cf2eb1dc2efbff7dc7caebefe2481773103709a59b985109973eb228e853e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_579e1c2cd1fcac5ab3b2ea56c680733501f68d1cae5ce9e604b24f7194fc11c8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_579e1c2cd1fcac5ab3b2ea56c680733501f68d1cae5ce9e604b24f7194fc11c8->enter($__internal_579e1c2cd1fcac5ab3b2ea56c680733501f68d1cae5ce9e604b24f7194fc11c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        echo "<!--le block du milieu de la page-->
            <div class=\"row\">
                ";
        // line 129
        $this->displayBlock('debutPage', $context, $blocks);
        // line 135
        echo "                ";
        $this->displayBlock('modules', $context, $blocks);
        // line 203
        echo "<!--le block pour les differents modules de l'application-->
            </div><!--row-->
            ";
        
        $__internal_579e1c2cd1fcac5ab3b2ea56c680733501f68d1cae5ce9e604b24f7194fc11c8->leave($__internal_579e1c2cd1fcac5ab3b2ea56c680733501f68d1cae5ce9e604b24f7194fc11c8_prof);

        
        $__internal_55cf2eb1dc2efbff7dc7caebefe2481773103709a59b985109973eb228e853e2->leave($__internal_55cf2eb1dc2efbff7dc7caebefe2481773103709a59b985109973eb228e853e2_prof);

    }

    // line 129
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_f5d4baa27b097ec9486f55c6bef8db810056a54d1de7d0e5b51b5ab523665c90 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f5d4baa27b097ec9486f55c6bef8db810056a54d1de7d0e5b51b5ab523665c90->enter($__internal_f5d4baa27b097ec9486f55c6bef8db810056a54d1de7d0e5b51b5ab523665c90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_a38ecb08f9c1a2115e9b4c692de356fb80c484b1cce71f17e4c695bdb6703ca1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a38ecb08f9c1a2115e9b4c692de356fb80c484b1cce71f17e4c695bdb6703ca1->enter($__internal_a38ecb08f9c1a2115e9b4c692de356fb80c484b1cce71f17e4c695bdb6703ca1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 130
        echo "                    <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                        <h3 style=\"color: white;margin-top:3%\"> <center>Accueil</center></h3>
                    </div>
                    <br><br><br><br><br><br>
                ";
        
        $__internal_a38ecb08f9c1a2115e9b4c692de356fb80c484b1cce71f17e4c695bdb6703ca1->leave($__internal_a38ecb08f9c1a2115e9b4c692de356fb80c484b1cce71f17e4c695bdb6703ca1_prof);

        
        $__internal_f5d4baa27b097ec9486f55c6bef8db810056a54d1de7d0e5b51b5ab523665c90->leave($__internal_f5d4baa27b097ec9486f55c6bef8db810056a54d1de7d0e5b51b5ab523665c90_prof);

    }

    // line 135
    public function block_modules($context, array $blocks = array())
    {
        $__internal_c16c529c1daf24b1239fbf939c828174b69c0f30878fc3d2c4d10f4d39240c0b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c16c529c1daf24b1239fbf939c828174b69c0f30878fc3d2c4d10f4d39240c0b->enter($__internal_c16c529c1daf24b1239fbf939c828174b69c0f30878fc3d2c4d10f4d39240c0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_1a3d2af84728bb51a501e3f673bf9da9aa1fe892468d0e13efb2697804793b35 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1a3d2af84728bb51a501e3f673bf9da9aa1fe892468d0e13efb2697804793b35->enter($__internal_1a3d2af84728bb51a501e3f673bf9da9aa1fe892468d0e13efb2697804793b35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        echo "<!--le block pour les differents modules de l'application-->
                <div class=\"container-fluid\">
                    <div class=\"row\" style=\"margin-top: -50px;margin-left: 4%\">
                        <div class=\"  col-xl-12\">
                            <div class=\"col-xl-12\">
                                <div class=\"row\" >
                                    <div class=\" col-sm-3\">
                                        <article class=\"statistic-box red\" style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto\"><a href=\"";
        // line 145
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_produit");
        echo "\" style=\"color:white;\">Gestion Marchandises</a></h5></div>
                                            </div>
                                        </article>
                                    </div>
                                    <div class=\"col-sm-3\">
                                        <article class=\"statistic-box purple\"  style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto\"><a href=\"";
        // line 153
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_transporteur");
        echo "\"  style=\"color:white;\">Gestion Transporteurs</a></h5></div>

                                            </div>
                                        </article>
                                    </div><!--.col-->
                                    <div class=\"col-sm-3\">
                                        <article class=\"statistic-box yellow\"  style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto\"><a href=\"";
        // line 162
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_representant");
        echo "\"  style=\"color:white;\">Gestion Representants</a></h5></div>
                                            </div>
                                        </article>
                                    </div><!--.col-->
                                    <div class=\"col-sm-3\">
                                        <div class=\"statistic-box green\"  style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto;align:center;\"><a href=\"";
        // line 170
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_contratMarchandise");
        echo "\" style=\"color:white;\">Gestion Contrats</a></h5></div>
                                            </div>
                                        </div>
                                    </div><!--.col-->
                                </div><!--.row-->
                            </div><!--.col-->
                        </div><!--col-->
                    </div><!--row--><br><br>


                    <div class=\"row\">
                        <div class=\"col-lg-8\">
                                ";
        // line 182
        $this->loadTemplate("map.html.twig", "::base.html.twig", 182)->display($context);
        // line 183
        echo "                        </div>

                        <div class=\"col-lg-3 dahsboard-column\" style=\"margin-top: 50px;margin-left: 7%\">
                                <div class=\" box-typical\" >
                                    <div class=\"calendar-page\">
                                        <div class=\"calendar-page-side\">
                                            <section class=\"calendar-page-side-section\">
                                                <div class=\"calendar-page-side-section-in\">
                                                    <div id=\"side-datetimepicker\"></div>
                                                </div>
                                            </section>
                                        </div><!--.calendar-page-side-->
                                    </div><!--.calendar-page-->
                                </div>
                            </div><!--.container-fluid-->

                    </div>


                    </div><!--.container-fluid-->
                ";
        
        $__internal_1a3d2af84728bb51a501e3f673bf9da9aa1fe892468d0e13efb2697804793b35->leave($__internal_1a3d2af84728bb51a501e3f673bf9da9aa1fe892468d0e13efb2697804793b35_prof);

        
        $__internal_c16c529c1daf24b1239fbf939c828174b69c0f30878fc3d2c4d10f4d39240c0b->leave($__internal_c16c529c1daf24b1239fbf939c828174b69c0f30878fc3d2c4d10f4d39240c0b_prof);

    }

    // line 208
    public function block_script($context, array $blocks = array())
    {
        $__internal_282968f7eccc00573bae17ef939855a540a1d7f028ee20196c2716b8d06831e8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_282968f7eccc00573bae17ef939855a540a1d7f028ee20196c2716b8d06831e8->enter($__internal_282968f7eccc00573bae17ef939855a540a1d7f028ee20196c2716b8d06831e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        $__internal_a8e566627be1c789b03f02b15d60d42010fab999543cc3dc491de283d34af253 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a8e566627be1c789b03f02b15d60d42010fab999543cc3dc491de283d34af253->enter($__internal_a8e566627be1c789b03f02b15d60d42010fab999543cc3dc491de283d34af253_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        // line 209
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/jquery/jquery.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 210
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/tether/tether.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 211
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 212
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/plugins.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 213
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/app.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_a8e566627be1c789b03f02b15d60d42010fab999543cc3dc491de283d34af253->leave($__internal_a8e566627be1c789b03f02b15d60d42010fab999543cc3dc491de283d34af253_prof);

        
        $__internal_282968f7eccc00573bae17ef939855a540a1d7f028ee20196c2716b8d06831e8->leave($__internal_282968f7eccc00573bae17ef939855a540a1d7f028ee20196c2716b8d06831e8_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  570 => 213,  566 => 212,  562 => 211,  558 => 210,  553 => 209,  544 => 208,  514 => 183,  512 => 182,  497 => 170,  486 => 162,  474 => 153,  463 => 145,  443 => 135,  429 => 130,  420 => 129,  408 => 203,  405 => 135,  403 => 129,  391 => 127,  375 => 104,  371 => 103,  367 => 102,  355 => 93,  351 => 92,  347 => 91,  335 => 82,  331 => 81,  327 => 80,  315 => 71,  311 => 70,  307 => 69,  295 => 60,  291 => 59,  287 => 58,  272 => 46,  261 => 38,  257 => 36,  248 => 35,  123 => 215,  121 => 208,  116 => 205,  114 => 127,  98 => 114,  92 => 110,  90 => 35,  80 => 28,  76 => 27,  72 => 26,  68 => 25,  64 => 24,  60 => 23,  56 => 22,  52 => 21,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head lang=\"en\">
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\">
        <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
        <title>Trading</title>

        <link href=\"img/favicon.144x144.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"144x144\">
        <link href=\"img/favicon.114x114.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"114x114\">
        <link href=\"img/favicon.72x72.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"72x72\">
        <link href=\"img/favicon.57x57.png\" rel=\"apple-touch-icon\" type=\"image/png\">
        <link href=\"img/favicon.png\" rel=\"icon\" type=\"image/png\">
        <link href=\"img/favicon.ico\" rel=\"shortcut icon\">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
        <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
        <![endif]-->
        <link rel=\"stylesheet\" href=\"{{asset('css/lib/lobipanel/lobipanel.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/separate/vendor/lobipanel.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/lib/jqueryui/jquery-ui.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/separate/pages/widgets.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/lib/font-awesome/font-awesome.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/lib/bootstrap/bootstrap.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/main.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/lib/acceuil.css')}}\">



    </head>

    <body class=\"with-side-menu control-panel control-panel-compact\"  style=\"background: white\">
\t     {% block menu %}
        <nav class=\"side-menu side-menu-big-icon\" style=\"background: #3299CC;overflow: hidden\">
            <nav class=\"sidebar-header\">
                <img src=\"{{asset('img/man.png')}}\" style=\"margin-left:35%;margin-top:-15%;\"><br><br>
                <h6 style=\"text-align:center;color:white;\">User Name</h6>
                <h6 style=\"text-align:center;color:white;\">Profil</h6>
            </nav>
            <hr style=\"margin-left:20%;margin-right: 20%;background: red;height:1px\">
            <ul class=\"side-menu-list\">
                <li class=\"opened\" >
                    <span id=\"acc\">
                    \t<a href=\"{{ path('accueil') }}\" id=\"acc\" >
\t                        <i class=\"font-icon font-icon-home\"  style=\"color: white;\" ></i>
                            <span class=\"lbl\" style=\"\" id=\"acceuil\"><h6>Accueil</h6></span>
                        </a>
                    </span>
                </li>
                <li class=\"green with-sub\">
\t\t            <span id=\"prod\">
\t\t                <i class=\"font-icon font-icon-view-grid\" style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\" ><h6>Produit</h6></span>
\t\t            </span>
                    <ul>
                        <li><a href=\"{{ path('ajouter_produit') }}\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"{{ path('lister_produit') }}\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"{{ path('lister_produit') }}\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                <li class=\"green with-sub\">
                    <span id=\"rep\">
                        <i class=\"font-icon font-icon-user\"  style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\"><h6>Representants</h6></span>
                    </span>
                    <ul>
                        <li><a href=\"{{ path('ajouter_representant') }}\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"{{ path('lister_representant') }}\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"{{ path('lister_representant') }}\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                <li class=\" with-sub\">
                    <span id=\"contrat\">
                        <i class=\"font-icon font-icon-archive\"  style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\"><h6>Contrats</h6></span>
                    </span>
                    <ul>
                        <li><a href=\"{{ path('ajouter_contratMarchandise') }}\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"{{ path('lister_contrat') }}\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"{{ path('lister_contrat') }}\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                <li class=\"with-sub\">
                    <span id=\"transporteur\">
                        <i class=\"font-icon font-icon-weather-waves\" style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\"><h6>Transporteurs</h6></span>
                    </span>
                    <ul>
                        <li><a href=\"{{ path('ajouter_transporteur') }}\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"{{ path('lister_transporteur') }}\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"{{ path('lister_transporteur') }}\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                <li class=\"with-sub\">
                    <span id=\"instr\">
                        <i class=\"font-icon font-icon-weather-cloud\" style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\"><h6>Instructions de livraison</h6></span>
                    </span>
                    <ul>
                        <li><a href=\"{{ path('ajouter_instruction') }}\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"{{ path('lister_instruction') }}\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"{{ path('lister_instruction') }}\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
            </ul>
        </nav><!--.side-menu-->
\t\t {% endblock %}
        <div class=\"page-content\" style=\"background: white\">
            <!--entete du site-->
                <div class=\"container-fluid\" style=\"height: 70px\">
                    <h2 style=\"font-weight: bold;font-size:35px;color:#3299CC; width:50%;margin-top:-10%\">Trading Platform</h2>
                    <img src=\"{{asset('img/bateau.png')}}\" style=\"margin-top:-9%;margin-left:30%;\">
                </div><!--.container-fluid-->
                <div class=\"site-header-search-container\"><br><br>
                    <form class=\"site-header-search\" style=\"width: 30% ;float: right;margin-top:-5%;\">
                        <input type=\"text\" placeholder=\"Search\"/>
                        <button type=\"submit\">
                            <span class=\"font-icon-search\"></span>
                        </button>
                        <div class=\"overlay\"></div>
                    </form><br><br>
                </div>
            <!--entete du site-->

            {% block contenuDuMilieu %}<!--le block du milieu de la page-->
            <div class=\"row\">
                {% block debutPage %}
                    <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                        <h3 style=\"color: white;margin-top:3%\"> <center>Accueil</center></h3>
                    </div>
                    <br><br><br><br><br><br>
                {% endblock %}
                {% block modules %}<!--le block pour les differents modules de l'application-->
                <div class=\"container-fluid\">
                    <div class=\"row\" style=\"margin-top: -50px;margin-left: 4%\">
                        <div class=\"  col-xl-12\">
                            <div class=\"col-xl-12\">
                                <div class=\"row\" >
                                    <div class=\" col-sm-3\">
                                        <article class=\"statistic-box red\" style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto\"><a href=\"{{ path('ajouter_produit') }}\" style=\"color:white;\">Gestion Marchandises</a></h5></div>
                                            </div>
                                        </article>
                                    </div>
                                    <div class=\"col-sm-3\">
                                        <article class=\"statistic-box purple\"  style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto\"><a href=\"{{ path('ajouter_transporteur') }}\"  style=\"color:white;\">Gestion Transporteurs</a></h5></div>

                                            </div>
                                        </article>
                                    </div><!--.col-->
                                    <div class=\"col-sm-3\">
                                        <article class=\"statistic-box yellow\"  style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto\"><a href=\"{{ path('ajouter_representant') }}\"  style=\"color:white;\">Gestion Representants</a></h5></div>
                                            </div>
                                        </article>
                                    </div><!--.col-->
                                    <div class=\"col-sm-3\">
                                        <div class=\"statistic-box green\"  style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto;align:center;\"><a href=\"{{ path('ajouter_contratMarchandise') }}\" style=\"color:white;\">Gestion Contrats</a></h5></div>
                                            </div>
                                        </div>
                                    </div><!--.col-->
                                </div><!--.row-->
                            </div><!--.col-->
                        </div><!--col-->
                    </div><!--row--><br><br>


                    <div class=\"row\">
                        <div class=\"col-lg-8\">
                                {% include 'map.html.twig' %}
                        </div>

                        <div class=\"col-lg-3 dahsboard-column\" style=\"margin-top: 50px;margin-left: 7%\">
                                <div class=\" box-typical\" >
                                    <div class=\"calendar-page\">
                                        <div class=\"calendar-page-side\">
                                            <section class=\"calendar-page-side-section\">
                                                <div class=\"calendar-page-side-section-in\">
                                                    <div id=\"side-datetimepicker\"></div>
                                                </div>
                                            </section>
                                        </div><!--.calendar-page-side-->
                                    </div><!--.calendar-page-->
                                </div>
                            </div><!--.container-fluid-->

                    </div>


                    </div><!--.container-fluid-->
                {% endblock %}<!--le block pour les differents modules de l'application-->
            </div><!--row-->
            {% endblock %}<!--Contenu du milieu-->
        </div><!--.page-content-->

        {% block script %}
            <script src=\"{{ asset('js/lib/jquery/jquery.min.js')}}\"></script>
            <script src=\"{{ asset('js/lib/tether/tether.min.js')}}\"></script>
            <script src=\"{{ asset('js/lib/bootstrap/bootstrap.min.js')}}\"></script>
            <script src=\"{{ asset('js/plugins.js')}}\"></script>
            <script src=\"{{ asset('js/app.js')}}\"></script>
        {% endblock %}

            <script type=\"text/javascript\" src=\"js/lib/jqueryui/jquery-ui.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/lobipanel/lobipanel.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/match-height/jquery.matchHeight.min.js\"></script>
            <script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>

            <script src=\"js/lib/jquery/jquery.min.js\"></script>
            <script src=\"js/lib/tether/tether.min.js\"></script>
            <script src=\"js/lib/bootstrap/bootstrap.min.js\"></script>
            <script src=\"js/plugins.js\"></script>

            <script type=\"text/javascript\" src=\"js/lib/match-height/jquery.matchHeight.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/moment/moment-with-locales.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/eonasdan-bootstrap-datetimepicker/bootstrap-datetimepicker.min.js\"></script>
            <script src=\"js/lib/fullcalendar/fullcalendar.min.js\"></script>
            <script src=\"js/lib/fullcalendar/fullcalendar-init.js\"></script>

            <script>
                \$(document).ready(function() {
                    \$('.panel').lobiPanel({
                        sortable: true
                    });
                    \$('.panel').on('dragged.lobiPanel', function(ev, lobiPanel){
                        \$('.dahsboard-column').matchHeight();
                    });

                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawChart);
                    function drawChart() {
                        var dataTable = new google.visualization.DataTable();
                        dataTable.addColumn('string', 'Day');
                        dataTable.addColumn('number', 'Values');
                        // A column for custom tooltip content
                        dataTable.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});
                        dataTable.addRows([
                            ['MON',  130, ' '],
                            ['TUE',  130, '130'],
                            ['WED',  180, '180'],
                            ['THU',  175, '175'],
                            ['FRI',  200, '200'],
                            ['SAT',  170, '170'],
                            ['SUN',  250, '250'],
                            ['MON',  220, '220'],
                            ['TUE',  220, ' ']
                        ]);

                        var options = {
                            height: 314,
                            legend: 'none',
                            areaOpacity: 0.18,
                            axisTitlesPosition: 'out',
                            hAxis: {
                                title: '',
                                textStyle: {
                                    color: '#fff',
                                    fontName: 'Proxima Nova',
                                    fontSize: 11,
                                    bold: true,
                                    italic: false
                                },
                                textPosition: 'out'
                            },
                            vAxis: {
                                minValue: 0,
                                textPosition: 'out',
                                textStyle: {
                                    color: '#fff',
                                    fontName: 'Proxima Nova',
                                    fontSize: 11,
                                    bold: true,
                                    italic: false
                                },
                                baselineColor: '#16b4fc',
                                ticks: [0,25,50,75,100,125,150,175,200,225,250,275,300,325,350],
                                gridlines: {
                                    color: '#1ba0fc',
                                    count: 15
                                },
                            },
                            lineWidth: 2,
                            colors: ['#fff'],
                            curveType: 'function',
                            pointSize: 5,
                            pointShapeType: 'circle',
                            pointFillColor: '#f00',
                            backgroundColor: {
                                fill: '#008ffb',
                                strokeWidth: 0,
                            },
                            chartArea:{
                                left:0,
                                top:0,
                                width:'100%',
                                height:'100%'
                            },
                            fontSize: 11,
                            fontName: 'Proxima Nova',
                            tooltip: {
                                trigger: 'selection',
                                isHtml: true
                            }
                        };

                        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
                        chart.draw(dataTable, options);
                    }
                    \$(window).resize(function(){
                        drawChart();
                        setTimeout(function(){
                        }, 1000);
                    });
                });
            </script>
    </body>

</html>", "::base.html.twig", "C:\\wamp\\www\\symfony\\stock\\app/Resources\\views/base.html.twig");
    }
}
