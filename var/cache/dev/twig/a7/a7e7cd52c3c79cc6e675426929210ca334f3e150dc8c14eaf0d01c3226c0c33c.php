<?php

/* ::base.html.twig */
class __TwigTemplate_b6f5f80ddbf5b5f031479f724ca5cc5697de1818b37c232c0a23e4e2ee4d7058 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'menu' => array($this, 'block_menu'),
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_611ee15ca3975f41eeed04b8e2cb26f934db2146da1d443b1006f4aa89579f3c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_611ee15ca3975f41eeed04b8e2cb26f934db2146da1d443b1006f4aa89579f3c->enter($__internal_611ee15ca3975f41eeed04b8e2cb26f934db2146da1d443b1006f4aa89579f3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        $__internal_37894e1f61056a6234cb39203ee89ea0e8583abf3afce6fb4340f1a762dbf569 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_37894e1f61056a6234cb39203ee89ea0e8583abf3afce6fb4340f1a762dbf569->enter($__internal_37894e1f61056a6234cb39203ee89ea0e8583abf3afce6fb4340f1a762dbf569_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head lang=\"en\">
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\">
        <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
        <title>Trading</title>

        <link href=\"img/favicon.144x144.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"144x144\">
        <link href=\"img/favicon.114x114.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"114x114\">
        <link href=\"img/favicon.72x72.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"72x72\">
        <link href=\"img/favicon.57x57.png\" rel=\"apple-touch-icon\" type=\"image/png\">
        <link href=\"img/favicon.png\" rel=\"icon\" type=\"image/png\">
        <link href=\"img/favicon.ico\" rel=\"shortcut icon\">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
        <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
        <![endif]-->
        <link rel=\"stylesheet\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/lib/lobipanel/lobipanel.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/separate/vendor/lobipanel.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/lib/jqueryui/jquery-ui.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/separate/pages/widgets.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/lib/font-awesome/font-awesome.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/lib/bootstrap/bootstrap.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/main.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/lib/acceuil.css"), "html", null, true);
        echo "\">



    </head>

    <body class=\"with-side-menu control-panel control-panel-compact\"  style=\"background: white\">
\t     ";
        // line 35
        $this->displayBlock('menu', $context, $blocks);
        // line 110
        echo "        <div class=\"page-content\" style=\"background: white\">
            <!--entete du site-->
                <div class=\"container-fluid\" style=\"height: 70px\">
                    <h2 style=\"font-weight: bold;font-size:35px;color:#3299CC; width:50%;margin-top:-10%\">Trading Platform</h2>
                    <img src=\"";
        // line 114
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/bateau.png"), "html", null, true);
        echo "\" style=\"margin-top:-9%;margin-left:30%;\">

                </div><!--.container-fluid-->
            <a href=\"";
        // line 117
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\" ><div class=\"glyphicon glyphicon-log-out\" style=\"float:right;margin-top: -7%;margin-left: 10%\"></div></a>
                <div class=\"site-header-search-container\"><br>
                    <form class=\"site-header-search\" style=\"width: 30% ;float: right;margin-top:-5%;\">
                        <input type=\"text\" placeholder=\"Search\"/>
                        <button type=\"submit\">
                            <span class=\"font-icon-search\"></span>
                        </button>
                        <div class=\"overlay\"></div>
                    </form><br><br>
                </div>
            <!--entete du site-->

            ";
        // line 129
        $this->displayBlock('contenuDuMilieu', $context, $blocks);
        // line 207
        echo "<!--Contenu du milieu-->
        </div><!--.page-content-->

        ";
        // line 210
        $this->displayBlock('script', $context, $blocks);
        // line 217
        echo "
            <script type=\"text/javascript\" src=\"js/lib/jqueryui/jquery-ui.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/lobipanel/lobipanel.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/match-height/jquery.matchHeight.min.js\"></script>
            <script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>

            <script src=\"js/lib/jquery/jquery.min.js\"></script>
            <script src=\"js/lib/tether/tether.min.js\"></script>
            <script src=\"js/lib/bootstrap/bootstrap.min.js\"></script>
            <script src=\"js/plugins.js\"></script>

            <script type=\"text/javascript\" src=\"js/lib/match-height/jquery.matchHeight.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/moment/moment-with-locales.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/eonasdan-bootstrap-datetimepicker/bootstrap-datetimepicker.min.js\"></script>
            <script src=\"js/lib/fullcalendar/fullcalendar.min.js\"></script>
            <script src=\"js/lib/fullcalendar/fullcalendar-init.js\"></script>

            <script>
                \$(document).ready(function() {
                    \$('.panel').lobiPanel({
                        sortable: true
                    });
                    \$('.panel').on('dragged.lobiPanel', function(ev, lobiPanel){
                        \$('.dahsboard-column').matchHeight();
                    });

                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawChart);
                    function drawChart() {
                        var dataTable = new google.visualization.DataTable();
                        dataTable.addColumn('string', 'Day');
                        dataTable.addColumn('number', 'Values');
                        // A column for custom tooltip content
                        dataTable.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});
                        dataTable.addRows([
                            ['MON',  130, ' '],
                            ['TUE',  130, '130'],
                            ['WED',  180, '180'],
                            ['THU',  175, '175'],
                            ['FRI',  200, '200'],
                            ['SAT',  170, '170'],
                            ['SUN',  250, '250'],
                            ['MON',  220, '220'],
                            ['TUE',  220, ' ']
                        ]);

                        var options = {
                            height: 314,
                            legend: 'none',
                            areaOpacity: 0.18,
                            axisTitlesPosition: 'out',
                            hAxis: {
                                title: '',
                                textStyle: {
                                    color: '#fff',
                                    fontName: 'Proxima Nova',
                                    fontSize: 11,
                                    bold: true,
                                    italic: false
                                },
                                textPosition: 'out'
                            },
                            vAxis: {
                                minValue: 0,
                                textPosition: 'out',
                                textStyle: {
                                    color: '#fff',
                                    fontName: 'Proxima Nova',
                                    fontSize: 11,
                                    bold: true,
                                    italic: false
                                },
                                baselineColor: '#16b4fc',
                                ticks: [0,25,50,75,100,125,150,175,200,225,250,275,300,325,350],
                                gridlines: {
                                    color: '#1ba0fc',
                                    count: 15
                                },
                            },
                            lineWidth: 2,
                            colors: ['#fff'],
                            curveType: 'function',
                            pointSize: 5,
                            pointShapeType: 'circle',
                            pointFillColor: '#f00',
                            backgroundColor: {
                                fill: '#008ffb',
                                strokeWidth: 0,
                            },
                            chartArea:{
                                left:0,
                                top:0,
                                width:'100%',
                                height:'100%'
                            },
                            fontSize: 11,
                            fontName: 'Proxima Nova',
                            tooltip: {
                                trigger: 'selection',
                                isHtml: true
                            }
                        };

                        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
                        chart.draw(dataTable, options);
                    }
                    \$(window).resize(function(){
                        drawChart();
                        setTimeout(function(){
                        }, 1000);
                    });
                });
            </script>
    </body>

</html>";
        
        $__internal_611ee15ca3975f41eeed04b8e2cb26f934db2146da1d443b1006f4aa89579f3c->leave($__internal_611ee15ca3975f41eeed04b8e2cb26f934db2146da1d443b1006f4aa89579f3c_prof);

        
        $__internal_37894e1f61056a6234cb39203ee89ea0e8583abf3afce6fb4340f1a762dbf569->leave($__internal_37894e1f61056a6234cb39203ee89ea0e8583abf3afce6fb4340f1a762dbf569_prof);

    }

    // line 35
    public function block_menu($context, array $blocks = array())
    {
        $__internal_17785dbf0fb3eed277f8185ffe75eedf6d2943c32b4aef85be0e6e1f4d62edc8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_17785dbf0fb3eed277f8185ffe75eedf6d2943c32b4aef85be0e6e1f4d62edc8->enter($__internal_17785dbf0fb3eed277f8185ffe75eedf6d2943c32b4aef85be0e6e1f4d62edc8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_f3f27c6f1463a3f2ac0ad25abf102f1e00a695e480fc44b3c63d3379302c53be = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f3f27c6f1463a3f2ac0ad25abf102f1e00a695e480fc44b3c63d3379302c53be->enter($__internal_f3f27c6f1463a3f2ac0ad25abf102f1e00a695e480fc44b3c63d3379302c53be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 36
        echo "        <nav class=\"side-menu side-menu-big-icon\" style=\"background: #3299CC;overflow: hidden\">
            <nav class=\"sidebar-header\">
                <img src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/man.png"), "html", null, true);
        echo "\" style=\"margin-left:35%;margin-top:-15%;\"><br><br>
                <h6 style=\"text-align:center;color:white;\">NIANG</h6>
                <h6 style=\"text-align:center;color:white;\">Trader</h6>
            </nav>
            <hr style=\"margin-left:20%;margin-right: 20%;background: red;height:1px\">
            <ul class=\"side-menu-list\">
                <li class=\"opened\" >
                    <span id=\"acc\">
                    \t<a href=\"";
        // line 46
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("accueil");
        echo "\" id=\"acc\" >
\t                        <i class=\"font-icon font-icon-home\"  style=\"color: white;\" ></i>
                            <span class=\"lbl\" style=\"\" id=\"acceuil\"><h6>Accueil</h6></span>
                        </a>
                    </span>
                </li>
                <li class=\"green with-sub\">
\t\t            <span id=\"prod\">
\t\t                <i class=\"font-icon font-icon-view-grid\" style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\" ><h6>Produit</h6></span>
\t\t            </span>
                    <ul>
                        <li><a href=\"";
        // line 58
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_produit");
        echo "\"  id=\"prod\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"";
        // line 59
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_produit");
        echo "\"  id=\"prod\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"";
        // line 60
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_produit");
        echo "\"  id=\"prod\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                <li class=\"green with-sub\">
                    <span id=\"rep\">
                        <i class=\"font-icon font-icon-user\"  style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\"><h6>Representants</h6></span>
                    </span>
                    <ul>
                        <li><a href=\"";
        // line 69
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_representant");
        echo "\" id=\"rep\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"";
        // line 70
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_representant");
        echo "\" id=\"rep\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"";
        // line 71
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_representant");
        echo "\" id=\"rep\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                <li class=\" with-sub\">
                    <span id=\"contrat\">
                        <i class=\"font-icon font-icon-archive\"  style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\"><h6>Contrats</h6></span>
                    </span>
                    <ul>
                        <li><a href=\"";
        // line 80
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_contratMarchandise");
        echo "\" id=\"contrat\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"";
        // line 81
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_contrat");
        echo "\" id=\"contrat\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"";
        // line 82
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_contrat");
        echo "\" id=\"contrat\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                <li class=\"with-sub\">
                    <span id=\"transporteur\">
                        <i class=\"font-icon font-icon-weather-waves\" style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\"><h6>Transporteurs</h6></span>
                    </span>
                    <ul>
                        <li><a href=\"";
        // line 91
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_transporteur");
        echo "\" id=\"transporteur\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"";
        // line 92
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_transporteur");
        echo "\" id=\"transporteur\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"";
        // line 93
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_transporteur");
        echo "\" id=\"transporteur\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                ";
        // line 97
        echo "                    ";
        // line 98
        echo "                        ";
        // line 99
        echo "                        ";
        // line 100
        echo "                    ";
        // line 101
        echo "                    ";
        // line 102
        echo "                        ";
        // line 103
        echo "                        ";
        // line 104
        echo "                        ";
        // line 105
        echo "                    ";
        // line 106
        echo "                ";
        // line 107
        echo "            </ul>
        </nav><!--.side-menu-->
\t\t ";
        
        $__internal_f3f27c6f1463a3f2ac0ad25abf102f1e00a695e480fc44b3c63d3379302c53be->leave($__internal_f3f27c6f1463a3f2ac0ad25abf102f1e00a695e480fc44b3c63d3379302c53be_prof);

        
        $__internal_17785dbf0fb3eed277f8185ffe75eedf6d2943c32b4aef85be0e6e1f4d62edc8->leave($__internal_17785dbf0fb3eed277f8185ffe75eedf6d2943c32b4aef85be0e6e1f4d62edc8_prof);

    }

    // line 129
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_51e77fd83240191e1b98ee56f4f94c97e6eccb82264ca52f3255a1b7d79fd42f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_51e77fd83240191e1b98ee56f4f94c97e6eccb82264ca52f3255a1b7d79fd42f->enter($__internal_51e77fd83240191e1b98ee56f4f94c97e6eccb82264ca52f3255a1b7d79fd42f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_0d2dbc144932115e14dfaaf48b41382fca4ea139599d2c591abba35b5bf20c3b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0d2dbc144932115e14dfaaf48b41382fca4ea139599d2c591abba35b5bf20c3b->enter($__internal_0d2dbc144932115e14dfaaf48b41382fca4ea139599d2c591abba35b5bf20c3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        echo "<!--le block du milieu de la page-->
            <div class=\"row\">
                ";
        // line 131
        $this->displayBlock('debutPage', $context, $blocks);
        // line 137
        echo "                ";
        $this->displayBlock('modules', $context, $blocks);
        // line 205
        echo "<!--le block pour les differents modules de l'application-->
            </div><!--row-->
            ";
        
        $__internal_0d2dbc144932115e14dfaaf48b41382fca4ea139599d2c591abba35b5bf20c3b->leave($__internal_0d2dbc144932115e14dfaaf48b41382fca4ea139599d2c591abba35b5bf20c3b_prof);

        
        $__internal_51e77fd83240191e1b98ee56f4f94c97e6eccb82264ca52f3255a1b7d79fd42f->leave($__internal_51e77fd83240191e1b98ee56f4f94c97e6eccb82264ca52f3255a1b7d79fd42f_prof);

    }

    // line 131
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_8fd23b12c871f206f2251823f0387e511f53ada2f0e6e45730d3053e2ad876a7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8fd23b12c871f206f2251823f0387e511f53ada2f0e6e45730d3053e2ad876a7->enter($__internal_8fd23b12c871f206f2251823f0387e511f53ada2f0e6e45730d3053e2ad876a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_3aecb33332417fe5066fba8155743f75652d280b7999d99ef6838ce802a55d06 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3aecb33332417fe5066fba8155743f75652d280b7999d99ef6838ce802a55d06->enter($__internal_3aecb33332417fe5066fba8155743f75652d280b7999d99ef6838ce802a55d06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 132
        echo "                    <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                        <h3 style=\"color: white;margin-top:3%\"> <center>Accueil</center></h3>
                    </div>
                    <br><br><br><br><br><br>
                ";
        
        $__internal_3aecb33332417fe5066fba8155743f75652d280b7999d99ef6838ce802a55d06->leave($__internal_3aecb33332417fe5066fba8155743f75652d280b7999d99ef6838ce802a55d06_prof);

        
        $__internal_8fd23b12c871f206f2251823f0387e511f53ada2f0e6e45730d3053e2ad876a7->leave($__internal_8fd23b12c871f206f2251823f0387e511f53ada2f0e6e45730d3053e2ad876a7_prof);

    }

    // line 137
    public function block_modules($context, array $blocks = array())
    {
        $__internal_df60a7ca81b00a87dc10c2e496e1611f3d7d011fa97f0acb751470211be5280f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_df60a7ca81b00a87dc10c2e496e1611f3d7d011fa97f0acb751470211be5280f->enter($__internal_df60a7ca81b00a87dc10c2e496e1611f3d7d011fa97f0acb751470211be5280f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_6d17a8d23e58cb71ed34dbc908c995f9c6ec51fa4566b5eff9222c0b9637f873 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d17a8d23e58cb71ed34dbc908c995f9c6ec51fa4566b5eff9222c0b9637f873->enter($__internal_6d17a8d23e58cb71ed34dbc908c995f9c6ec51fa4566b5eff9222c0b9637f873_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        echo "<!--le block pour les differents modules de l'application-->
                <div class=\"container-fluid\">
                    <div class=\"row\" style=\"margin-top: -50px;margin-left: 4%\">
                        <div class=\"  col-xl-12\">
                            <div class=\"col-xl-12\">
                                <div class=\"row\" >
                                    <div class=\" col-sm-3\">
                                        <article class=\"statistic-box red\" style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto\"><a href=\"";
        // line 147
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_produit");
        echo "\" style=\"color:white;\">Gestion Marchandises</a></h5></div>
                                            </div>
                                        </article>
                                    </div>
                                    <div class=\"col-sm-3\">
                                        <article class=\"statistic-box purple\"  style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto\"><a href=\"";
        // line 155
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_transporteur");
        echo "\"  style=\"color:white;\">Gestion Transporteurs</a></h5></div>

                                            </div>
                                        </article>
                                    </div><!--.col-->
                                    <div class=\"col-sm-3\">
                                        <article class=\"statistic-box yellow\"  style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto\"><a href=\"";
        // line 164
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_representant");
        echo "\"  style=\"color:white;\">Gestion Representants</a></h5></div>
                                            </div>
                                        </article>
                                    </div><!--.col-->
                                    <div class=\"col-sm-3\">
                                        <div class=\"statistic-box green\"  style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto;align:center;\"><a href=\"";
        // line 172
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_contratMarchandise");
        echo "\" style=\"color:white;\">Gestion Contrats</a></h5></div>
                                            </div>
                                        </div>
                                    </div><!--.col-->
                                </div><!--.row-->
                            </div><!--.col-->
                        </div><!--col-->
                    </div><!--row--><br><br>


                    <div class=\"row\">
                        <div class=\"col-lg-8\">
                                ";
        // line 184
        $this->loadTemplate("map.html.twig", "::base.html.twig", 184)->display($context);
        // line 185
        echo "                        </div>

                        <div class=\"col-lg-3 dahsboard-column\" style=\"margin-top: 50px;margin-left: 7%\">
                                <div class=\" box-typical\" >
                                    <div class=\"calendar-page\">
                                        <div class=\"calendar-page-side\">
                                            <section class=\"calendar-page-side-section\">
                                                <div class=\"calendar-page-side-section-in\">
                                                    <div id=\"side-datetimepicker\"></div>
                                                </div>
                                            </section>
                                        </div><!--.calendar-page-side-->
                                    </div><!--.calendar-page-->
                                </div>
                            </div><!--.container-fluid-->

                    </div>


                    </div><!--.container-fluid-->
                ";
        
        $__internal_6d17a8d23e58cb71ed34dbc908c995f9c6ec51fa4566b5eff9222c0b9637f873->leave($__internal_6d17a8d23e58cb71ed34dbc908c995f9c6ec51fa4566b5eff9222c0b9637f873_prof);

        
        $__internal_df60a7ca81b00a87dc10c2e496e1611f3d7d011fa97f0acb751470211be5280f->leave($__internal_df60a7ca81b00a87dc10c2e496e1611f3d7d011fa97f0acb751470211be5280f_prof);

    }

    // line 210
    public function block_script($context, array $blocks = array())
    {
        $__internal_403ccf72acc71835664e813e43de435f4aaabceba2ff2ee62758f67d74e75d8a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_403ccf72acc71835664e813e43de435f4aaabceba2ff2ee62758f67d74e75d8a->enter($__internal_403ccf72acc71835664e813e43de435f4aaabceba2ff2ee62758f67d74e75d8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        $__internal_d8c73c9283c4b6199473db550d6045f26ba2b8cc9aff4a7ebfa5a8979710b516 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d8c73c9283c4b6199473db550d6045f26ba2b8cc9aff4a7ebfa5a8979710b516->enter($__internal_d8c73c9283c4b6199473db550d6045f26ba2b8cc9aff4a7ebfa5a8979710b516_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        // line 211
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/jquery/jquery.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 212
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/tether/tether.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 213
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 214
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/plugins.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 215
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/app.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_d8c73c9283c4b6199473db550d6045f26ba2b8cc9aff4a7ebfa5a8979710b516->leave($__internal_d8c73c9283c4b6199473db550d6045f26ba2b8cc9aff4a7ebfa5a8979710b516_prof);

        
        $__internal_403ccf72acc71835664e813e43de435f4aaabceba2ff2ee62758f67d74e75d8a->leave($__internal_403ccf72acc71835664e813e43de435f4aaabceba2ff2ee62758f67d74e75d8a_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  577 => 215,  573 => 214,  569 => 213,  565 => 212,  560 => 211,  551 => 210,  521 => 185,  519 => 184,  504 => 172,  493 => 164,  481 => 155,  470 => 147,  450 => 137,  436 => 132,  427 => 131,  415 => 205,  412 => 137,  410 => 131,  398 => 129,  386 => 107,  384 => 106,  382 => 105,  380 => 104,  378 => 103,  376 => 102,  374 => 101,  372 => 100,  370 => 99,  368 => 98,  366 => 97,  360 => 93,  356 => 92,  352 => 91,  340 => 82,  336 => 81,  332 => 80,  320 => 71,  316 => 70,  312 => 69,  300 => 60,  296 => 59,  292 => 58,  277 => 46,  266 => 38,  262 => 36,  253 => 35,  128 => 217,  126 => 210,  121 => 207,  119 => 129,  104 => 117,  98 => 114,  92 => 110,  90 => 35,  80 => 28,  76 => 27,  72 => 26,  68 => 25,  64 => 24,  60 => 23,  56 => 22,  52 => 21,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head lang=\"en\">
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\">
        <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
        <title>Trading</title>

        <link href=\"img/favicon.144x144.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"144x144\">
        <link href=\"img/favicon.114x114.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"114x114\">
        <link href=\"img/favicon.72x72.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"72x72\">
        <link href=\"img/favicon.57x57.png\" rel=\"apple-touch-icon\" type=\"image/png\">
        <link href=\"img/favicon.png\" rel=\"icon\" type=\"image/png\">
        <link href=\"img/favicon.ico\" rel=\"shortcut icon\">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
        <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
        <![endif]-->
        <link rel=\"stylesheet\" href=\"{{asset('css/lib/lobipanel/lobipanel.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/separate/vendor/lobipanel.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/lib/jqueryui/jquery-ui.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/separate/pages/widgets.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/lib/font-awesome/font-awesome.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/lib/bootstrap/bootstrap.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/main.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/lib/acceuil.css')}}\">



    </head>

    <body class=\"with-side-menu control-panel control-panel-compact\"  style=\"background: white\">
\t     {% block menu %}
        <nav class=\"side-menu side-menu-big-icon\" style=\"background: #3299CC;overflow: hidden\">
            <nav class=\"sidebar-header\">
                <img src=\"{{asset('img/man.png')}}\" style=\"margin-left:35%;margin-top:-15%;\"><br><br>
                <h6 style=\"text-align:center;color:white;\">NIANG</h6>
                <h6 style=\"text-align:center;color:white;\">Trader</h6>
            </nav>
            <hr style=\"margin-left:20%;margin-right: 20%;background: red;height:1px\">
            <ul class=\"side-menu-list\">
                <li class=\"opened\" >
                    <span id=\"acc\">
                    \t<a href=\"{{ path('accueil') }}\" id=\"acc\" >
\t                        <i class=\"font-icon font-icon-home\"  style=\"color: white;\" ></i>
                            <span class=\"lbl\" style=\"\" id=\"acceuil\"><h6>Accueil</h6></span>
                        </a>
                    </span>
                </li>
                <li class=\"green with-sub\">
\t\t            <span id=\"prod\">
\t\t                <i class=\"font-icon font-icon-view-grid\" style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\" ><h6>Produit</h6></span>
\t\t            </span>
                    <ul>
                        <li><a href=\"{{ path('ajouter_produit') }}\"  id=\"prod\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"{{ path('lister_produit') }}\"  id=\"prod\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"{{ path('lister_produit') }}\"  id=\"prod\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                <li class=\"green with-sub\">
                    <span id=\"rep\">
                        <i class=\"font-icon font-icon-user\"  style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\"><h6>Representants</h6></span>
                    </span>
                    <ul>
                        <li><a href=\"{{ path('ajouter_representant') }}\" id=\"rep\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"{{ path('lister_representant') }}\" id=\"rep\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"{{ path('lister_representant') }}\" id=\"rep\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                <li class=\" with-sub\">
                    <span id=\"contrat\">
                        <i class=\"font-icon font-icon-archive\"  style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\"><h6>Contrats</h6></span>
                    </span>
                    <ul>
                        <li><a href=\"{{ path('ajouter_contratMarchandise') }}\" id=\"contrat\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"{{ path('lister_contrat') }}\" id=\"contrat\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"{{ path('lister_contrat') }}\" id=\"contrat\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                <li class=\"with-sub\">
                    <span id=\"transporteur\">
                        <i class=\"font-icon font-icon-weather-waves\" style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\"><h6>Transporteurs</h6></span>
                    </span>
                    <ul>
                        <li><a href=\"{{ path('ajouter_transporteur') }}\" id=\"transporteur\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"{{ path('lister_transporteur') }}\" id=\"transporteur\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"{{ path('lister_transporteur') }}\" id=\"transporteur\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                {#<li class=\"with-sub\">#}
                    {#<span id=\"instr\">#}
                        {#<i class=\"font-icon font-icon-weather-cloud\" style=\"color: white\"></i>#}
                        {#<span class=\"lbl\" style=\"color: white\"><h6>Instructions de livraison</h6></span>#}
                    {#</span>#}
                    {#<ul>#}
                        {#<li><a href=\"{{ path('ajouter_instruction') }}\" id=\"instr\" ><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>#}
                        {#<li><a href=\"{{ path('lister_instruction') }}\" id=\"instr\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>#}
                        {#<li><a href=\"{{ path('lister_instruction') }}\" id=\"instr\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>#}
                    {#</ul>#}
                {#</li>#}
            </ul>
        </nav><!--.side-menu-->
\t\t {% endblock %}
        <div class=\"page-content\" style=\"background: white\">
            <!--entete du site-->
                <div class=\"container-fluid\" style=\"height: 70px\">
                    <h2 style=\"font-weight: bold;font-size:35px;color:#3299CC; width:50%;margin-top:-10%\">Trading Platform</h2>
                    <img src=\"{{asset('img/bateau.png')}}\" style=\"margin-top:-9%;margin-left:30%;\">

                </div><!--.container-fluid-->
            <a href=\"{{ path('login') }}\" ><div class=\"glyphicon glyphicon-log-out\" style=\"float:right;margin-top: -7%;margin-left: 10%\"></div></a>
                <div class=\"site-header-search-container\"><br>
                    <form class=\"site-header-search\" style=\"width: 30% ;float: right;margin-top:-5%;\">
                        <input type=\"text\" placeholder=\"Search\"/>
                        <button type=\"submit\">
                            <span class=\"font-icon-search\"></span>
                        </button>
                        <div class=\"overlay\"></div>
                    </form><br><br>
                </div>
            <!--entete du site-->

            {% block contenuDuMilieu %}<!--le block du milieu de la page-->
            <div class=\"row\">
                {% block debutPage %}
                    <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                        <h3 style=\"color: white;margin-top:3%\"> <center>Accueil</center></h3>
                    </div>
                    <br><br><br><br><br><br>
                {% endblock %}
                {% block modules %}<!--le block pour les differents modules de l'application-->
                <div class=\"container-fluid\">
                    <div class=\"row\" style=\"margin-top: -50px;margin-left: 4%\">
                        <div class=\"  col-xl-12\">
                            <div class=\"col-xl-12\">
                                <div class=\"row\" >
                                    <div class=\" col-sm-3\">
                                        <article class=\"statistic-box red\" style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto\"><a href=\"{{ path('ajouter_produit') }}\" style=\"color:white;\">Gestion Marchandises</a></h5></div>
                                            </div>
                                        </article>
                                    </div>
                                    <div class=\"col-sm-3\">
                                        <article class=\"statistic-box purple\"  style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto\"><a href=\"{{ path('ajouter_transporteur') }}\"  style=\"color:white;\">Gestion Transporteurs</a></h5></div>

                                            </div>
                                        </article>
                                    </div><!--.col-->
                                    <div class=\"col-sm-3\">
                                        <article class=\"statistic-box yellow\"  style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto\"><a href=\"{{ path('ajouter_representant') }}\"  style=\"color:white;\">Gestion Representants</a></h5></div>
                                            </div>
                                        </article>
                                    </div><!--.col-->
                                    <div class=\"col-sm-3\">
                                        <div class=\"statistic-box green\"  style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto;align:center;\"><a href=\"{{ path('ajouter_contratMarchandise') }}\" style=\"color:white;\">Gestion Contrats</a></h5></div>
                                            </div>
                                        </div>
                                    </div><!--.col-->
                                </div><!--.row-->
                            </div><!--.col-->
                        </div><!--col-->
                    </div><!--row--><br><br>


                    <div class=\"row\">
                        <div class=\"col-lg-8\">
                                {% include 'map.html.twig' %}
                        </div>

                        <div class=\"col-lg-3 dahsboard-column\" style=\"margin-top: 50px;margin-left: 7%\">
                                <div class=\" box-typical\" >
                                    <div class=\"calendar-page\">
                                        <div class=\"calendar-page-side\">
                                            <section class=\"calendar-page-side-section\">
                                                <div class=\"calendar-page-side-section-in\">
                                                    <div id=\"side-datetimepicker\"></div>
                                                </div>
                                            </section>
                                        </div><!--.calendar-page-side-->
                                    </div><!--.calendar-page-->
                                </div>
                            </div><!--.container-fluid-->

                    </div>


                    </div><!--.container-fluid-->
                {% endblock %}<!--le block pour les differents modules de l'application-->
            </div><!--row-->
            {% endblock %}<!--Contenu du milieu-->
        </div><!--.page-content-->

        {% block script %}
            <script src=\"{{ asset('js/lib/jquery/jquery.min.js')}}\"></script>
            <script src=\"{{ asset('js/lib/tether/tether.min.js')}}\"></script>
            <script src=\"{{ asset('js/lib/bootstrap/bootstrap.min.js')}}\"></script>
            <script src=\"{{ asset('js/plugins.js')}}\"></script>
            <script src=\"{{ asset('js/app.js')}}\"></script>
        {% endblock %}

            <script type=\"text/javascript\" src=\"js/lib/jqueryui/jquery-ui.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/lobipanel/lobipanel.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/match-height/jquery.matchHeight.min.js\"></script>
            <script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>

            <script src=\"js/lib/jquery/jquery.min.js\"></script>
            <script src=\"js/lib/tether/tether.min.js\"></script>
            <script src=\"js/lib/bootstrap/bootstrap.min.js\"></script>
            <script src=\"js/plugins.js\"></script>

            <script type=\"text/javascript\" src=\"js/lib/match-height/jquery.matchHeight.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/moment/moment-with-locales.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/eonasdan-bootstrap-datetimepicker/bootstrap-datetimepicker.min.js\"></script>
            <script src=\"js/lib/fullcalendar/fullcalendar.min.js\"></script>
            <script src=\"js/lib/fullcalendar/fullcalendar-init.js\"></script>

            <script>
                \$(document).ready(function() {
                    \$('.panel').lobiPanel({
                        sortable: true
                    });
                    \$('.panel').on('dragged.lobiPanel', function(ev, lobiPanel){
                        \$('.dahsboard-column').matchHeight();
                    });

                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawChart);
                    function drawChart() {
                        var dataTable = new google.visualization.DataTable();
                        dataTable.addColumn('string', 'Day');
                        dataTable.addColumn('number', 'Values');
                        // A column for custom tooltip content
                        dataTable.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});
                        dataTable.addRows([
                            ['MON',  130, ' '],
                            ['TUE',  130, '130'],
                            ['WED',  180, '180'],
                            ['THU',  175, '175'],
                            ['FRI',  200, '200'],
                            ['SAT',  170, '170'],
                            ['SUN',  250, '250'],
                            ['MON',  220, '220'],
                            ['TUE',  220, ' ']
                        ]);

                        var options = {
                            height: 314,
                            legend: 'none',
                            areaOpacity: 0.18,
                            axisTitlesPosition: 'out',
                            hAxis: {
                                title: '',
                                textStyle: {
                                    color: '#fff',
                                    fontName: 'Proxima Nova',
                                    fontSize: 11,
                                    bold: true,
                                    italic: false
                                },
                                textPosition: 'out'
                            },
                            vAxis: {
                                minValue: 0,
                                textPosition: 'out',
                                textStyle: {
                                    color: '#fff',
                                    fontName: 'Proxima Nova',
                                    fontSize: 11,
                                    bold: true,
                                    italic: false
                                },
                                baselineColor: '#16b4fc',
                                ticks: [0,25,50,75,100,125,150,175,200,225,250,275,300,325,350],
                                gridlines: {
                                    color: '#1ba0fc',
                                    count: 15
                                },
                            },
                            lineWidth: 2,
                            colors: ['#fff'],
                            curveType: 'function',
                            pointSize: 5,
                            pointShapeType: 'circle',
                            pointFillColor: '#f00',
                            backgroundColor: {
                                fill: '#008ffb',
                                strokeWidth: 0,
                            },
                            chartArea:{
                                left:0,
                                top:0,
                                width:'100%',
                                height:'100%'
                            },
                            fontSize: 11,
                            fontName: 'Proxima Nova',
                            tooltip: {
                                trigger: 'selection',
                                isHtml: true
                            }
                        };

                        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
                        chart.draw(dataTable, options);
                    }
                    \$(window).resize(function(){
                        drawChart();
                        setTimeout(function(){
                        }, 1000);
                    });
                });
            </script>
    </body>

</html>", "::base.html.twig", "C:\\wamp64\\www\\stock\\app/Resources\\views/base.html.twig");
    }
}
