<?php

/* map.html.twig */
class __TwigTemplate_964987e2d5459857899aad711ba7d8a1ccca6710564c386a8e192a9d0b85271d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3140894c154f445f47bf78b10f92fe0eb9bbdd7610c369732e14d1b5682b502d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3140894c154f445f47bf78b10f92fe0eb9bbdd7610c369732e14d1b5682b502d->enter($__internal_3140894c154f445f47bf78b10f92fe0eb9bbdd7610c369732e14d1b5682b502d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "map.html.twig"));

        $__internal_1c3d08121d5d44551479d9d938cf791ed6beb03c181254236866dda41e7864d0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c3d08121d5d44551479d9d938cf791ed6beb03c181254236866dda41e7864d0->enter($__internal_1c3d08121d5d44551479d9d938cf791ed6beb03c181254236866dda41e7864d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "map.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <script src=\"https://www.amcharts.com/lib/3/ammap.js\"></script>
    <script src=\"https://www.amcharts.com/lib/3/maps/js/worldLow.js\"></script>
    <script src=\"https://www.amcharts.com/lib/3/plugins/export/export.min.js\"></script>
    <link rel=\"stylesheet\" href=\"https://www.amcharts.com/lib/3/plugins/export/export.css\" type=\"text/css\" media=\"all\" />
    <script src=\"https://www.amcharts.com/lib/3/themes/light.js\"></script>
</head>
<body>
    <h3>Suivie des transports des marchandises</h3>
    <div id=\"chartdiv\" style=\" height: 250px\"></div>


    ";
        // line 16
        echo "    ";
        // line 17
        echo "    ";
        // line 18
        echo "    ";
        // line 19
        echo "

<script>    /**
    * SVG path for target icon
    */
    var targetSVG = \"M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z\";

    /**
    * SVG path for plane icon
    */
    var planeSVG = \"m2,106h28l24,30h72l-44,-133h35l80,132h98c21,0 21,34 0,34l-98,0 -80,134h-35l43,-133h-71l-24,30h-28l15,-47\";

    /**
    * Create the map
    */
    var map = AmCharts.makeChart( \"chartdiv\", {
    \"type\": \"map\",
    \"theme\": \"light\",


    \"dataProvider\": {
    \"map\": \"worldLow\",
    \"zoomLevel\": 3.5,
    \"zoomLongitude\": -55,
    \"zoomLatitude\": 42,

    \"lines\": [ {
    \"id\": \"line1\",
    \"arc\": -0.85,
    \"alpha\": 0.3,
    \"latitudes\": [ 48.8567, 43.8163, 34.3, 23 ],
    \"longitudes\": [ 2.3510, -79.4287, -118.15, -82 ]
    }, {
    \"id\": \"line2\",
    \"alpha\": 0,
    \"color\": \"#000000\",
    \"latitudes\": [ 48.8567, 43.8163, 34.3, 23 ],
    \"longitudes\": [ 2.3510, -79.4287, -118.15, -82 ]
    } ],
    \"images\": [ {
    \"svgPath\": targetSVG,
    \"title\": \"Paris\",
    \"latitude\": 48.8567,
    \"longitude\": 2.3510
    }, {
    \"svgPath\": targetSVG,
    \"title\": \"Toronto\",
    \"latitude\": 43.8163,
    \"longitude\": -79.4287
    }, {
    \"svgPath\": targetSVG,
    \"title\": \"Los Angeles\",
    \"latitude\": 34.3,
    \"longitude\": -118.15
    }, {
    \"svgPath\": targetSVG,
    \"title\": \"Havana\",
    \"latitude\": 23,
    \"longitude\": -82
    }, {
    \"svgPath\": planeSVG,
    \"positionOnLine\": 0,
    \"color\": \"#000000\",
    \"alpha\": 0.1,
    \"animateAlongLine\": true,
    \"lineId\": \"line2\",
    \"flipDirection\": true,
    \"loop\": true,
    \"scale\": 0.03,
    \"positionScale\": 1.3
    }, {
    \"svgPath\": planeSVG,
    \"positionOnLine\": 0,
    \"color\": \"#585869\",
    \"animateAlongLine\": true,
    \"lineId\": \"line1\",
    \"flipDirection\": true,
    \"loop\": true,
    \"scale\": 0.03,
    \"positionScale\": 1.8
    } ]
    },

    \"areasSettings\": {
    \"unlistedAreasColor\": \"#8dd9ef\"
    },

    \"imagesSettings\": {
    \"color\": \"#585869\",
    \"rollOverColor\": \"#585869\",
    \"selectedColor\": \"#585869\",
    \"pauseDuration\": 0.2,
    \"animationDuration\": 2.5,
    \"adjustAnimationSpeed\": true
    },

    \"linesSettings\": {
    \"color\": \"#585869\",
    \"alpha\": 0.4
    },

    \"export\": {
    \"enabled\": true
    }

    } );
</script>
</body>";
        
        $__internal_3140894c154f445f47bf78b10f92fe0eb9bbdd7610c369732e14d1b5682b502d->leave($__internal_3140894c154f445f47bf78b10f92fe0eb9bbdd7610c369732e14d1b5682b502d_prof);

        
        $__internal_1c3d08121d5d44551479d9d938cf791ed6beb03c181254236866dda41e7864d0->leave($__internal_1c3d08121d5d44551479d9d938cf791ed6beb03c181254236866dda41e7864d0_prof);

    }

    public function getTemplateName()
    {
        return "map.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  47 => 19,  45 => 18,  43 => 17,  41 => 16,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
<head>
    <script src=\"https://www.amcharts.com/lib/3/ammap.js\"></script>
    <script src=\"https://www.amcharts.com/lib/3/maps/js/worldLow.js\"></script>
    <script src=\"https://www.amcharts.com/lib/3/plugins/export/export.min.js\"></script>
    <link rel=\"stylesheet\" href=\"https://www.amcharts.com/lib/3/plugins/export/export.css\" type=\"text/css\" media=\"all\" />
    <script src=\"https://www.amcharts.com/lib/3/themes/light.js\"></script>
</head>
<body>
    <h3>Suivie des transports des marchandises</h3>
    <div id=\"chartdiv\" style=\" height: 250px\"></div>


    {##chartdiv {#}
    {#width: 100%;#}
    {#height: 500px;#}
    {#}#}


<script>    /**
    * SVG path for target icon
    */
    var targetSVG = \"M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z\";

    /**
    * SVG path for plane icon
    */
    var planeSVG = \"m2,106h28l24,30h72l-44,-133h35l80,132h98c21,0 21,34 0,34l-98,0 -80,134h-35l43,-133h-71l-24,30h-28l15,-47\";

    /**
    * Create the map
    */
    var map = AmCharts.makeChart( \"chartdiv\", {
    \"type\": \"map\",
    \"theme\": \"light\",


    \"dataProvider\": {
    \"map\": \"worldLow\",
    \"zoomLevel\": 3.5,
    \"zoomLongitude\": -55,
    \"zoomLatitude\": 42,

    \"lines\": [ {
    \"id\": \"line1\",
    \"arc\": -0.85,
    \"alpha\": 0.3,
    \"latitudes\": [ 48.8567, 43.8163, 34.3, 23 ],
    \"longitudes\": [ 2.3510, -79.4287, -118.15, -82 ]
    }, {
    \"id\": \"line2\",
    \"alpha\": 0,
    \"color\": \"#000000\",
    \"latitudes\": [ 48.8567, 43.8163, 34.3, 23 ],
    \"longitudes\": [ 2.3510, -79.4287, -118.15, -82 ]
    } ],
    \"images\": [ {
    \"svgPath\": targetSVG,
    \"title\": \"Paris\",
    \"latitude\": 48.8567,
    \"longitude\": 2.3510
    }, {
    \"svgPath\": targetSVG,
    \"title\": \"Toronto\",
    \"latitude\": 43.8163,
    \"longitude\": -79.4287
    }, {
    \"svgPath\": targetSVG,
    \"title\": \"Los Angeles\",
    \"latitude\": 34.3,
    \"longitude\": -118.15
    }, {
    \"svgPath\": targetSVG,
    \"title\": \"Havana\",
    \"latitude\": 23,
    \"longitude\": -82
    }, {
    \"svgPath\": planeSVG,
    \"positionOnLine\": 0,
    \"color\": \"#000000\",
    \"alpha\": 0.1,
    \"animateAlongLine\": true,
    \"lineId\": \"line2\",
    \"flipDirection\": true,
    \"loop\": true,
    \"scale\": 0.03,
    \"positionScale\": 1.3
    }, {
    \"svgPath\": planeSVG,
    \"positionOnLine\": 0,
    \"color\": \"#585869\",
    \"animateAlongLine\": true,
    \"lineId\": \"line1\",
    \"flipDirection\": true,
    \"loop\": true,
    \"scale\": 0.03,
    \"positionScale\": 1.8
    } ]
    },

    \"areasSettings\": {
    \"unlistedAreasColor\": \"#8dd9ef\"
    },

    \"imagesSettings\": {
    \"color\": \"#585869\",
    \"rollOverColor\": \"#585869\",
    \"selectedColor\": \"#585869\",
    \"pauseDuration\": 0.2,
    \"animationDuration\": 2.5,
    \"adjustAnimationSpeed\": true
    },

    \"linesSettings\": {
    \"color\": \"#585869\",
    \"alpha\": 0.4
    },

    \"export\": {
    \"enabled\": true
    }

    } );
</script>
</body>", "map.html.twig", "C:\\wamp\\www\\symfony\\stock\\app\\Resources\\views\\map.html.twig");
    }
}
