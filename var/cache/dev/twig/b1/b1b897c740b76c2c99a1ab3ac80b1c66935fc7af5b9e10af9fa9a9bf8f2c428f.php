<?php

/* @WebProfiler/Profiler/open.html.twig */
class __TwigTemplate_59a7f2cf66cc71c79aff3e943c656ab80587bd346f930efcff47e499f82dd8e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "@WebProfiler/Profiler/open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_56ae1dbac44e50a03c7a9af7534fabeb10deb75075b015d08337c306f21d934f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_56ae1dbac44e50a03c7a9af7534fabeb10deb75075b015d08337c306f21d934f->enter($__internal_56ae1dbac44e50a03c7a9af7534fabeb10deb75075b015d08337c306f21d934f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $__internal_ba7fb021d95aa0b150a306b450e2e460a9ec322d4585cd75e92fb0fd9a88dae8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ba7fb021d95aa0b150a306b450e2e460a9ec322d4585cd75e92fb0fd9a88dae8->enter($__internal_ba7fb021d95aa0b150a306b450e2e460a9ec322d4585cd75e92fb0fd9a88dae8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_56ae1dbac44e50a03c7a9af7534fabeb10deb75075b015d08337c306f21d934f->leave($__internal_56ae1dbac44e50a03c7a9af7534fabeb10deb75075b015d08337c306f21d934f_prof);

        
        $__internal_ba7fb021d95aa0b150a306b450e2e460a9ec322d4585cd75e92fb0fd9a88dae8->leave($__internal_ba7fb021d95aa0b150a306b450e2e460a9ec322d4585cd75e92fb0fd9a88dae8_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_e68c2e75d17ee94162f866712c7cdd609af137e90dfb63491ae594c3da203002 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e68c2e75d17ee94162f866712c7cdd609af137e90dfb63491ae594c3da203002->enter($__internal_e68c2e75d17ee94162f866712c7cdd609af137e90dfb63491ae594c3da203002_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_6c8e1320f0fb3f0481a046d5372c96de975f2deccb498d01a1ba408c65971a22 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6c8e1320f0fb3f0481a046d5372c96de975f2deccb498d01a1ba408c65971a22->enter($__internal_6c8e1320f0fb3f0481a046d5372c96de975f2deccb498d01a1ba408c65971a22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_6c8e1320f0fb3f0481a046d5372c96de975f2deccb498d01a1ba408c65971a22->leave($__internal_6c8e1320f0fb3f0481a046d5372c96de975f2deccb498d01a1ba408c65971a22_prof);

        
        $__internal_e68c2e75d17ee94162f866712c7cdd609af137e90dfb63491ae594c3da203002->leave($__internal_e68c2e75d17ee94162f866712c7cdd609af137e90dfb63491ae594c3da203002_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_074d2ab41b638cda37e56c62a3b1c3420b329dc9d71c6c29e7990a139c1e73df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_074d2ab41b638cda37e56c62a3b1c3420b329dc9d71c6c29e7990a139c1e73df->enter($__internal_074d2ab41b638cda37e56c62a3b1c3420b329dc9d71c6c29e7990a139c1e73df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_ca6513f1476a7f3406a9355482bb456310adb5995a9ce185a46a1da4fcb76856 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca6513f1476a7f3406a9355482bb456310adb5995a9ce185a46a1da4fcb76856->enter($__internal_ca6513f1476a7f3406a9355482bb456310adb5995a9ce185a46a1da4fcb76856_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, ($context["file"] ?? $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, ($context["line"] ?? $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(($context["filename"] ?? $this->getContext($context, "filename")), ($context["line"] ?? $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_ca6513f1476a7f3406a9355482bb456310adb5995a9ce185a46a1da4fcb76856->leave($__internal_ca6513f1476a7f3406a9355482bb456310adb5995a9ce185a46a1da4fcb76856_prof);

        
        $__internal_074d2ab41b638cda37e56c62a3b1c3420b329dc9d71c6c29e7990a139c1e73df->leave($__internal_074d2ab41b638cda37e56c62a3b1c3420b329dc9d71c6c29e7990a139c1e73df_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "@WebProfiler/Profiler/open.html.twig", "C:\\wamp64\\www\\stock\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\open.html.twig");
    }
}
