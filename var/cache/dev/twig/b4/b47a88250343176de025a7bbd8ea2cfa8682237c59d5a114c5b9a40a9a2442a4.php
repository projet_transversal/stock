<?php

/* StockBundle:Trader:addTrader.html.twig */
class __TwigTemplate_8cd024bf06c80a134e242cabb8babd9a98d40c7158accfad6de3a2841167e61a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:Trader:addTrader.html.twig", 1);
        $this->blocks = array(
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9a6a15333308f44324675b45ed27a5a602c61cddcf4389d8ecc7cfdd66ebdf9c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9a6a15333308f44324675b45ed27a5a602c61cddcf4389d8ecc7cfdd66ebdf9c->enter($__internal_9a6a15333308f44324675b45ed27a5a602c61cddcf4389d8ecc7cfdd66ebdf9c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Trader:addTrader.html.twig"));

        $__internal_6556986fe333fc4c60b5b4f403ebbecf9c960ab46a7556c4c1ba24946d60d583 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6556986fe333fc4c60b5b4f403ebbecf9c960ab46a7556c4c1ba24946d60d583->enter($__internal_6556986fe333fc4c60b5b4f403ebbecf9c960ab46a7556c4c1ba24946d60d583_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Trader:addTrader.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9a6a15333308f44324675b45ed27a5a602c61cddcf4389d8ecc7cfdd66ebdf9c->leave($__internal_9a6a15333308f44324675b45ed27a5a602c61cddcf4389d8ecc7cfdd66ebdf9c_prof);

        
        $__internal_6556986fe333fc4c60b5b4f403ebbecf9c960ab46a7556c4c1ba24946d60d583->leave($__internal_6556986fe333fc4c60b5b4f403ebbecf9c960ab46a7556c4c1ba24946d60d583_prof);

    }

    // line 3
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_12d3f315aa2671a9679aa1f5e3ce474310b30e81d5defdabd63b6d5dee1990e0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_12d3f315aa2671a9679aa1f5e3ce474310b30e81d5defdabd63b6d5dee1990e0->enter($__internal_12d3f315aa2671a9679aa1f5e3ce474310b30e81d5defdabd63b6d5dee1990e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_ea9d1e56bb03cda6722a82fb77a063ac2af73c63d204a58d69235db61f6844f4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ea9d1e56bb03cda6722a82fb77a063ac2af73c63d204a58d69235db61f6844f4->enter($__internal_ea9d1e56bb03cda6722a82fb77a063ac2af73c63d204a58d69235db61f6844f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        // line 4
        echo "        ";
        $this->displayBlock('debutPage', $context, $blocks);
        // line 10
        echo "        ";
        $this->displayBlock('modules', $context, $blocks);
        // line 21
        echo "    ";
        
        $__internal_ea9d1e56bb03cda6722a82fb77a063ac2af73c63d204a58d69235db61f6844f4->leave($__internal_ea9d1e56bb03cda6722a82fb77a063ac2af73c63d204a58d69235db61f6844f4_prof);

        
        $__internal_12d3f315aa2671a9679aa1f5e3ce474310b30e81d5defdabd63b6d5dee1990e0->leave($__internal_12d3f315aa2671a9679aa1f5e3ce474310b30e81d5defdabd63b6d5dee1990e0_prof);

    }

    // line 4
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_cabd6b309a9b26ada656bfcf32e62345b067f576140485830e58f551611551fd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cabd6b309a9b26ada656bfcf32e62345b067f576140485830e58f551611551fd->enter($__internal_cabd6b309a9b26ada656bfcf32e62345b067f576140485830e58f551611551fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_fd481bfc350f5349211c1427c7434ae08a5051a38d0f8525f2a126638f006a78 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fd481bfc350f5349211c1427c7434ae08a5051a38d0f8525f2a126638f006a78->enter($__internal_fd481bfc350f5349211c1427c7434ae08a5051a38d0f8525f2a126638f006a78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 5
        echo "            <div class=\"col-lg-12\" style=\"width:104%;height: 150px;background: silver;margin-top:-26px;\">
                <h1 style=\"color: white;margin-top:5%\"> <center>Ajouter Trader</center></h1></center>
            </div>
            <br><br><br><br><br><br>
        ";
        
        $__internal_fd481bfc350f5349211c1427c7434ae08a5051a38d0f8525f2a126638f006a78->leave($__internal_fd481bfc350f5349211c1427c7434ae08a5051a38d0f8525f2a126638f006a78_prof);

        
        $__internal_cabd6b309a9b26ada656bfcf32e62345b067f576140485830e58f551611551fd->leave($__internal_cabd6b309a9b26ada656bfcf32e62345b067f576140485830e58f551611551fd_prof);

    }

    // line 10
    public function block_modules($context, array $blocks = array())
    {
        $__internal_ba74843b854e28eb574e8c31b11347fba382b164bab7b56574919b39962bbc8f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ba74843b854e28eb574e8c31b11347fba382b164bab7b56574919b39962bbc8f->enter($__internal_ba74843b854e28eb574e8c31b11347fba382b164bab7b56574919b39962bbc8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_d1e3c0702b4679a4abfaddf7196fa37309a52034e0ceaffb6838ac95962c2d0a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d1e3c0702b4679a4abfaddf7196fa37309a52034e0ceaffb6838ac95962c2d0a->enter($__internal_d1e3c0702b4679a4abfaddf7196fa37309a52034e0ceaffb6838ac95962c2d0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        // line 11
        echo "            <div class=\"col-xl-10\">
                ";
        // line 12
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
                ";
        // line 13
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
                <div class=\"form-group\" align=\"center\">
                    <input type=\"submit\" name=\"addTrader\" class=\"btn btn-success\" value=\"Ajouter\"/>
                    <input type=\"reset\" name=\"resetTrader\" class=\"btn btn-danger\" value=\"Annuler\"/>
                </div>
                ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
            </div>
        ";
        
        $__internal_d1e3c0702b4679a4abfaddf7196fa37309a52034e0ceaffb6838ac95962c2d0a->leave($__internal_d1e3c0702b4679a4abfaddf7196fa37309a52034e0ceaffb6838ac95962c2d0a_prof);

        
        $__internal_ba74843b854e28eb574e8c31b11347fba382b164bab7b56574919b39962bbc8f->leave($__internal_ba74843b854e28eb574e8c31b11347fba382b164bab7b56574919b39962bbc8f_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Trader:addTrader.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 18,  106 => 13,  102 => 12,  99 => 11,  90 => 10,  76 => 5,  67 => 4,  57 => 21,  54 => 10,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}

    {% block contenuDuMilieu %}
        {% block debutPage %}
            <div class=\"col-lg-12\" style=\"width:104%;height: 150px;background: silver;margin-top:-26px;\">
                <h1 style=\"color: white;margin-top:5%\"> <center>Ajouter Trader</center></h1></center>
            </div>
            <br><br><br><br><br><br>
        {% endblock %}
        {% block modules %}
            <div class=\"col-xl-10\">
                {{ form_start(form) }}
                {{ form_widget(form) }}
                <div class=\"form-group\" align=\"center\">
                    <input type=\"submit\" name=\"addTrader\" class=\"btn btn-success\" value=\"Ajouter\"/>
                    <input type=\"reset\" name=\"resetTrader\" class=\"btn btn-danger\" value=\"Annuler\"/>
                </div>
                {{ form_end(form) }}
            </div>
        {% endblock %}
    {% endblock %}", "StockBundle:Trader:addTrader.html.twig", "C:\\wamp64\\www\\stock\\src\\StockBundle/Resources/views/Trader/addTrader.html.twig");
    }
}
