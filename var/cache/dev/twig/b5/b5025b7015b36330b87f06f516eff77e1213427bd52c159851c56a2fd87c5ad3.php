<?php

/* form/layout.html.twig */
class __TwigTemplate_fd9285de7d8f9685be52a2c5e74c11171c5315244dd95d6d9839c4066d0c7bd3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("bootstrap_3_layout.html.twig", "form/layout.html.twig", 1);
        $this->blocks = array(
            'form_errors' => array($this, 'block_form_errors'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "bootstrap_3_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f4d06f2f69af9d36733d051345b5b6ca1458f8494509895cb9a320e13091b307 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f4d06f2f69af9d36733d051345b5b6ca1458f8494509895cb9a320e13091b307->enter($__internal_f4d06f2f69af9d36733d051345b5b6ca1458f8494509895cb9a320e13091b307_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/layout.html.twig"));

        $__internal_9245c7e388bec562d3264490dd607ed458e68022f348f5a55d5e9093d978e510 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9245c7e388bec562d3264490dd607ed458e68022f348f5a55d5e9093d978e510->enter($__internal_9245c7e388bec562d3264490dd607ed458e68022f348f5a55d5e9093d978e510_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f4d06f2f69af9d36733d051345b5b6ca1458f8494509895cb9a320e13091b307->leave($__internal_f4d06f2f69af9d36733d051345b5b6ca1458f8494509895cb9a320e13091b307_prof);

        
        $__internal_9245c7e388bec562d3264490dd607ed458e68022f348f5a55d5e9093d978e510->leave($__internal_9245c7e388bec562d3264490dd607ed458e68022f348f5a55d5e9093d978e510_prof);

    }

    // line 5
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_8989b8ba3a4daa380785a4bcea3546889e32f99dfa2cfe508ba8f55738bd7035 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8989b8ba3a4daa380785a4bcea3546889e32f99dfa2cfe508ba8f55738bd7035->enter($__internal_8989b8ba3a4daa380785a4bcea3546889e32f99dfa2cfe508ba8f55738bd7035_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        $__internal_2ba318920f7f97c6e99827fe38eaeb7181ce4b7167cd2a8cae43695f478b41da = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2ba318920f7f97c6e99827fe38eaeb7181ce4b7167cd2a8cae43695f478b41da->enter($__internal_2ba318920f7f97c6e99827fe38eaeb7181ce4b7167cd2a8cae43695f478b41da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 6
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 7
            if ($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "parent", array())) {
                echo "<span class=\"help-block\">";
            } else {
                echo "<div class=\"alert alert-danger\">";
            }
            // line 8
            echo "        <ul class=\"list-unstyled\">";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? $this->getContext($context, "errors")));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 11
                echo "            <li><span class=\"fa fa-exclamation-triangle\"></span> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "</li>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 13
            echo "</ul>
        ";
            // line 14
            if ($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "parent", array())) {
                echo "</span>";
            } else {
                echo "</div>";
            }
        }
        
        $__internal_2ba318920f7f97c6e99827fe38eaeb7181ce4b7167cd2a8cae43695f478b41da->leave($__internal_2ba318920f7f97c6e99827fe38eaeb7181ce4b7167cd2a8cae43695f478b41da_prof);

        
        $__internal_8989b8ba3a4daa380785a4bcea3546889e32f99dfa2cfe508ba8f55738bd7035->leave($__internal_8989b8ba3a4daa380785a4bcea3546889e32f99dfa2cfe508ba8f55738bd7035_prof);

    }

    public function getTemplateName()
    {
        return "form/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 14,  71 => 13,  63 => 11,  59 => 9,  57 => 8,  51 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'bootstrap_3_layout.html.twig' %}

{# Errors #}

{% block form_errors -%}
    {% if errors|length > 0 -%}
        {% if form.parent %}<span class=\"help-block\">{% else %}<div class=\"alert alert-danger\">{% endif %}
        <ul class=\"list-unstyled\">
        {%- for error in errors -%}
            {# use font-awesome icon library #}
            <li><span class=\"fa fa-exclamation-triangle\"></span> {{ error.message }}</li>
        {%- endfor -%}
        </ul>
        {% if form.parent %}</span>{% else %}</div>{% endif %}
    {%- endif %}
{%- endblock form_errors %}
", "form/layout.html.twig", "C:\\wamp64\\www\\stock\\app\\Resources\\views\\form\\layout.html.twig");
    }
}
