<?php

/* base.html.twig */
class __TwigTemplate_1bfce23da49e58eaa81b8cb6349090a8dd37e21fcb4329e17bc2fb76de72320b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'menu' => array($this, 'block_menu'),
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1a640ea1bb50ce7f512f6281573a67d40643d81b3d7e3ae28ebc753a5b783180 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1a640ea1bb50ce7f512f6281573a67d40643d81b3d7e3ae28ebc753a5b783180->enter($__internal_1a640ea1bb50ce7f512f6281573a67d40643d81b3d7e3ae28ebc753a5b783180_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_f600e7b214dfe34880f8932b6afc92e40fc88c9a303fb59e6d4a26643a9d95a1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f600e7b214dfe34880f8932b6afc92e40fc88c9a303fb59e6d4a26643a9d95a1->enter($__internal_f600e7b214dfe34880f8932b6afc92e40fc88c9a303fb59e6d4a26643a9d95a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head lang=\"en\">
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\">
        <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
        <title>Trading</title>

        <link href=\"img/favicon.144x144.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"144x144\">
        <link href=\"img/favicon.114x114.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"114x114\">
        <link href=\"img/favicon.72x72.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"72x72\">
        <link href=\"img/favicon.57x57.png\" rel=\"apple-touch-icon\" type=\"image/png\">
        <link href=\"img/favicon.png\" rel=\"icon\" type=\"image/png\">
        <link href=\"img/favicon.ico\" rel=\"shortcut icon\">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
        <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
        <![endif]-->
        <link rel=\"stylesheet\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/lib/lobipanel/lobipanel.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/separate/vendor/lobipanel.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/lib/jqueryui/jquery-ui.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/separate/pages/widgets.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/lib/font-awesome/font-awesome.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/lib/bootstrap/bootstrap.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/main.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/lib/acceuil.css"), "html", null, true);
        echo "\">



    </head>

    <body class=\"with-side-menu control-panel control-panel-compact\"  style=\"background: white\">
\t     ";
        // line 35
        $this->displayBlock('menu', $context, $blocks);
        // line 110
        echo "        <div class=\"page-content\" style=\"background: white\">
            <!--entete du site-->
                <div class=\"container-fluid\" style=\"height: 70px\">
                    <h2 style=\"font-weight: bold;font-size:35px;color:#3299CC; width:50%;margin-top:-10%\">Trading Platform</h2>
                    <img src=\"";
        // line 114
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/bateau.png"), "html", null, true);
        echo "\" style=\"margin-top:-9%;margin-left:30%;\">

                </div><!--.container-fluid-->
            <a href=\"";
        // line 117
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\" ><div class=\"glyphicon glyphicon-log-out\" style=\"float:right;margin-top: -7%;margin-left: 10%\"></div></a>
                <div class=\"site-header-search-container\"><br>
                    <form class=\"site-header-search\" style=\"width: 30% ;float: right;margin-top:-5%;\">
                        <input type=\"text\" placeholder=\"Search\"/>
                        <button type=\"submit\">
                            <span class=\"font-icon-search\"></span>
                        </button>
                        <div class=\"overlay\"></div>
                    </form><br><br>
                </div>
            <!--entete du site-->

            ";
        // line 129
        $this->displayBlock('contenuDuMilieu', $context, $blocks);
        // line 207
        echo "<!--Contenu du milieu-->
        </div><!--.page-content-->

        ";
        // line 210
        $this->displayBlock('script', $context, $blocks);
        // line 217
        echo "
            <script type=\"text/javascript\" src=\"js/lib/jqueryui/jquery-ui.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/lobipanel/lobipanel.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/match-height/jquery.matchHeight.min.js\"></script>
            <script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>

            <script src=\"js/lib/jquery/jquery.min.js\"></script>
            <script src=\"js/lib/tether/tether.min.js\"></script>
            <script src=\"js/lib/bootstrap/bootstrap.min.js\"></script>
            <script src=\"js/plugins.js\"></script>

            <script type=\"text/javascript\" src=\"js/lib/match-height/jquery.matchHeight.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/moment/moment-with-locales.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/eonasdan-bootstrap-datetimepicker/bootstrap-datetimepicker.min.js\"></script>
            <script src=\"js/lib/fullcalendar/fullcalendar.min.js\"></script>
            <script src=\"js/lib/fullcalendar/fullcalendar-init.js\"></script>

            <script>
                \$(document).ready(function() {
                    \$('.panel').lobiPanel({
                        sortable: true
                    });
                    \$('.panel').on('dragged.lobiPanel', function(ev, lobiPanel){
                        \$('.dahsboard-column').matchHeight();
                    });

                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawChart);
                    function drawChart() {
                        var dataTable = new google.visualization.DataTable();
                        dataTable.addColumn('string', 'Day');
                        dataTable.addColumn('number', 'Values');
                        // A column for custom tooltip content
                        dataTable.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});
                        dataTable.addRows([
                            ['MON',  130, ' '],
                            ['TUE',  130, '130'],
                            ['WED',  180, '180'],
                            ['THU',  175, '175'],
                            ['FRI',  200, '200'],
                            ['SAT',  170, '170'],
                            ['SUN',  250, '250'],
                            ['MON',  220, '220'],
                            ['TUE',  220, ' ']
                        ]);

                        var options = {
                            height: 314,
                            legend: 'none',
                            areaOpacity: 0.18,
                            axisTitlesPosition: 'out',
                            hAxis: {
                                title: '',
                                textStyle: {
                                    color: '#fff',
                                    fontName: 'Proxima Nova',
                                    fontSize: 11,
                                    bold: true,
                                    italic: false
                                },
                                textPosition: 'out'
                            },
                            vAxis: {
                                minValue: 0,
                                textPosition: 'out',
                                textStyle: {
                                    color: '#fff',
                                    fontName: 'Proxima Nova',
                                    fontSize: 11,
                                    bold: true,
                                    italic: false
                                },
                                baselineColor: '#16b4fc',
                                ticks: [0,25,50,75,100,125,150,175,200,225,250,275,300,325,350],
                                gridlines: {
                                    color: '#1ba0fc',
                                    count: 15
                                },
                            },
                            lineWidth: 2,
                            colors: ['#fff'],
                            curveType: 'function',
                            pointSize: 5,
                            pointShapeType: 'circle',
                            pointFillColor: '#f00',
                            backgroundColor: {
                                fill: '#008ffb',
                                strokeWidth: 0,
                            },
                            chartArea:{
                                left:0,
                                top:0,
                                width:'100%',
                                height:'100%'
                            },
                            fontSize: 11,
                            fontName: 'Proxima Nova',
                            tooltip: {
                                trigger: 'selection',
                                isHtml: true
                            }
                        };

                        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
                        chart.draw(dataTable, options);
                    }
                    \$(window).resize(function(){
                        drawChart();
                        setTimeout(function(){
                        }, 1000);
                    });
                });
            </script>
    </body>

</html>";
        
        $__internal_1a640ea1bb50ce7f512f6281573a67d40643d81b3d7e3ae28ebc753a5b783180->leave($__internal_1a640ea1bb50ce7f512f6281573a67d40643d81b3d7e3ae28ebc753a5b783180_prof);

        
        $__internal_f600e7b214dfe34880f8932b6afc92e40fc88c9a303fb59e6d4a26643a9d95a1->leave($__internal_f600e7b214dfe34880f8932b6afc92e40fc88c9a303fb59e6d4a26643a9d95a1_prof);

    }

    // line 35
    public function block_menu($context, array $blocks = array())
    {
        $__internal_d66e5552717590856be631f15f334a8a302e24a6d6242e9c2fd0e4b349c5c462 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d66e5552717590856be631f15f334a8a302e24a6d6242e9c2fd0e4b349c5c462->enter($__internal_d66e5552717590856be631f15f334a8a302e24a6d6242e9c2fd0e4b349c5c462_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_a4c3872b0287ac8770ef0a68ed6f520a2fff875a505f3a3ff2265f4b0392ccec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a4c3872b0287ac8770ef0a68ed6f520a2fff875a505f3a3ff2265f4b0392ccec->enter($__internal_a4c3872b0287ac8770ef0a68ed6f520a2fff875a505f3a3ff2265f4b0392ccec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 36
        echo "        <nav class=\"side-menu side-menu-big-icon\" style=\"background: #3299CC;overflow: hidden\">
            <nav class=\"sidebar-header\">
                <img src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/man.png"), "html", null, true);
        echo "\" style=\"margin-left:35%;margin-top:-15%;\"><br><br>
                <h6 style=\"text-align:center;color:white;\">NIANG</h6>
                <h6 style=\"text-align:center;color:white;\">Trader</h6>
            </nav>
            <hr style=\"margin-left:20%;margin-right: 20%;background: red;height:1px\">
            <ul class=\"side-menu-list\">
                <li class=\"opened\" >
                    <span id=\"acc\">
                    \t<a href=\"";
        // line 46
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("accueil");
        echo "\" id=\"acc\" >
\t                        <i class=\"font-icon font-icon-home\"  style=\"color: white;\" ></i>
                            <span class=\"lbl\" style=\"\" id=\"acceuil\"><h6>Accueil</h6></span>
                        </a>
                    </span>
                </li>
                <li class=\"green with-sub\">
\t\t            <span id=\"prod\">
\t\t                <i class=\"font-icon font-icon-view-grid\" style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\" ><h6>Produit</h6></span>
\t\t            </span>
                    <ul>
                        <li><a href=\"";
        // line 58
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_produit");
        echo "\"  id=\"prod\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"";
        // line 59
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_produit");
        echo "\"  id=\"prod\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"";
        // line 60
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_produit");
        echo "\"  id=\"prod\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                <li class=\"green with-sub\">
                    <span id=\"rep\">
                        <i class=\"font-icon font-icon-user\"  style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\"><h6>Representants</h6></span>
                    </span>
                    <ul>
                        <li><a href=\"";
        // line 69
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_representant");
        echo "\" id=\"rep\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"";
        // line 70
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_representant");
        echo "\" id=\"rep\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"";
        // line 71
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_representant");
        echo "\" id=\"rep\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                <li class=\" with-sub\">
                    <span id=\"contrat\">
                        <i class=\"font-icon font-icon-archive\"  style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\"><h6>Contrats</h6></span>
                    </span>
                    <ul>
                        <li><a href=\"";
        // line 80
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_contratMarchandise");
        echo "\" id=\"contrat\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"";
        // line 81
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_contrat");
        echo "\" id=\"contrat\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"";
        // line 82
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_contrat");
        echo "\" id=\"contrat\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                <li class=\"with-sub\">
                    <span id=\"transporteur\">
                        <i class=\"font-icon font-icon-weather-waves\" style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\"><h6>Transporteurs</h6></span>
                    </span>
                    <ul>
                        <li><a href=\"";
        // line 91
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_transporteur");
        echo "\" id=\"transporteur\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"";
        // line 92
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_transporteur");
        echo "\" id=\"transporteur\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"";
        // line 93
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_transporteur");
        echo "\" id=\"transporteur\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                ";
        // line 97
        echo "                    ";
        // line 98
        echo "                        ";
        // line 99
        echo "                        ";
        // line 100
        echo "                    ";
        // line 101
        echo "                    ";
        // line 102
        echo "                        ";
        // line 103
        echo "                        ";
        // line 104
        echo "                        ";
        // line 105
        echo "                    ";
        // line 106
        echo "                ";
        // line 107
        echo "            </ul>
        </nav><!--.side-menu-->
\t\t ";
        
        $__internal_a4c3872b0287ac8770ef0a68ed6f520a2fff875a505f3a3ff2265f4b0392ccec->leave($__internal_a4c3872b0287ac8770ef0a68ed6f520a2fff875a505f3a3ff2265f4b0392ccec_prof);

        
        $__internal_d66e5552717590856be631f15f334a8a302e24a6d6242e9c2fd0e4b349c5c462->leave($__internal_d66e5552717590856be631f15f334a8a302e24a6d6242e9c2fd0e4b349c5c462_prof);

    }

    // line 129
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_351aa46e3b33961655672b338e8ff6fb4d45448f2654233aca1b0d82e510852d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_351aa46e3b33961655672b338e8ff6fb4d45448f2654233aca1b0d82e510852d->enter($__internal_351aa46e3b33961655672b338e8ff6fb4d45448f2654233aca1b0d82e510852d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_7aeace7bbf3e65b50e0f0c4bbe7efc0f67fb160b2df6e44a55cb1cfbd3c4e5a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7aeace7bbf3e65b50e0f0c4bbe7efc0f67fb160b2df6e44a55cb1cfbd3c4e5a7->enter($__internal_7aeace7bbf3e65b50e0f0c4bbe7efc0f67fb160b2df6e44a55cb1cfbd3c4e5a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        echo "<!--le block du milieu de la page-->
            <div class=\"row\">
                ";
        // line 131
        $this->displayBlock('debutPage', $context, $blocks);
        // line 137
        echo "                ";
        $this->displayBlock('modules', $context, $blocks);
        // line 205
        echo "<!--le block pour les differents modules de l'application-->
            </div><!--row-->
            ";
        
        $__internal_7aeace7bbf3e65b50e0f0c4bbe7efc0f67fb160b2df6e44a55cb1cfbd3c4e5a7->leave($__internal_7aeace7bbf3e65b50e0f0c4bbe7efc0f67fb160b2df6e44a55cb1cfbd3c4e5a7_prof);

        
        $__internal_351aa46e3b33961655672b338e8ff6fb4d45448f2654233aca1b0d82e510852d->leave($__internal_351aa46e3b33961655672b338e8ff6fb4d45448f2654233aca1b0d82e510852d_prof);

    }

    // line 131
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_58774d93a0027c574e513c550e77a693ce5572a33d4384bc881dfebf94b3eaae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_58774d93a0027c574e513c550e77a693ce5572a33d4384bc881dfebf94b3eaae->enter($__internal_58774d93a0027c574e513c550e77a693ce5572a33d4384bc881dfebf94b3eaae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_b0a18fa112b16404a24dfeb262062458fca7fd22f1db2b0ac5ac567b636b4723 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b0a18fa112b16404a24dfeb262062458fca7fd22f1db2b0ac5ac567b636b4723->enter($__internal_b0a18fa112b16404a24dfeb262062458fca7fd22f1db2b0ac5ac567b636b4723_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 132
        echo "                    <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                        <h3 style=\"color: white;margin-top:3%\"> <center>Accueil</center></h3>
                    </div>
                    <br><br><br><br><br><br>
                ";
        
        $__internal_b0a18fa112b16404a24dfeb262062458fca7fd22f1db2b0ac5ac567b636b4723->leave($__internal_b0a18fa112b16404a24dfeb262062458fca7fd22f1db2b0ac5ac567b636b4723_prof);

        
        $__internal_58774d93a0027c574e513c550e77a693ce5572a33d4384bc881dfebf94b3eaae->leave($__internal_58774d93a0027c574e513c550e77a693ce5572a33d4384bc881dfebf94b3eaae_prof);

    }

    // line 137
    public function block_modules($context, array $blocks = array())
    {
        $__internal_66f06a977a3f6ef01045efa42f3d8b918498640bd011ff64db4fd7d7664a8fa4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_66f06a977a3f6ef01045efa42f3d8b918498640bd011ff64db4fd7d7664a8fa4->enter($__internal_66f06a977a3f6ef01045efa42f3d8b918498640bd011ff64db4fd7d7664a8fa4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_30631446910d1777640e2f1e77a7b5f94ad701c5904615ed7989cb69f10baf57 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_30631446910d1777640e2f1e77a7b5f94ad701c5904615ed7989cb69f10baf57->enter($__internal_30631446910d1777640e2f1e77a7b5f94ad701c5904615ed7989cb69f10baf57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        echo "<!--le block pour les differents modules de l'application-->
                <div class=\"container-fluid\">
                    <div class=\"row\" style=\"margin-top: -50px;margin-left: 4%\">
                        <div class=\"  col-xl-12\">
                            <div class=\"col-xl-12\">
                                <div class=\"row\" >
                                    <div class=\" col-sm-3\">
                                        <article class=\"statistic-box red\" style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto\"><a href=\"";
        // line 147
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_produit");
        echo "\" style=\"color:white;\">Gestion Marchandises</a></h5></div>
                                            </div>
                                        </article>
                                    </div>
                                    <div class=\"col-sm-3\">
                                        <article class=\"statistic-box purple\"  style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto\"><a href=\"";
        // line 155
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_transporteur");
        echo "\"  style=\"color:white;\">Gestion Transporteurs</a></h5></div>

                                            </div>
                                        </article>
                                    </div><!--.col-->
                                    <div class=\"col-sm-3\">
                                        <article class=\"statistic-box yellow\"  style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto\"><a href=\"";
        // line 164
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_representant");
        echo "\"  style=\"color:white;\">Gestion Representants</a></h5></div>
                                            </div>
                                        </article>
                                    </div><!--.col-->
                                    <div class=\"col-sm-3\">
                                        <div class=\"statistic-box green\"  style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto;align:center;\"><a href=\"";
        // line 172
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_contratMarchandise");
        echo "\" style=\"color:white;\">Gestion Contrats</a></h5></div>
                                            </div>
                                        </div>
                                    </div><!--.col-->
                                </div><!--.row-->
                            </div><!--.col-->
                        </div><!--col-->
                    </div><!--row--><br><br>


                    <div class=\"row\">
                        <div class=\"col-lg-8\">
                                ";
        // line 184
        $this->loadTemplate("map.html.twig", "base.html.twig", 184)->display($context);
        // line 185
        echo "                        </div>

                        <div class=\"col-lg-3 dahsboard-column\" style=\"margin-top: 50px;margin-left: 7%\">
                                <div class=\" box-typical\" >
                                    <div class=\"calendar-page\">
                                        <div class=\"calendar-page-side\">
                                            <section class=\"calendar-page-side-section\">
                                                <div class=\"calendar-page-side-section-in\">
                                                    <div id=\"side-datetimepicker\"></div>
                                                </div>
                                            </section>
                                        </div><!--.calendar-page-side-->
                                    </div><!--.calendar-page-->
                                </div>
                            </div><!--.container-fluid-->

                    </div>


                    </div><!--.container-fluid-->
                ";
        
        $__internal_30631446910d1777640e2f1e77a7b5f94ad701c5904615ed7989cb69f10baf57->leave($__internal_30631446910d1777640e2f1e77a7b5f94ad701c5904615ed7989cb69f10baf57_prof);

        
        $__internal_66f06a977a3f6ef01045efa42f3d8b918498640bd011ff64db4fd7d7664a8fa4->leave($__internal_66f06a977a3f6ef01045efa42f3d8b918498640bd011ff64db4fd7d7664a8fa4_prof);

    }

    // line 210
    public function block_script($context, array $blocks = array())
    {
        $__internal_ba1184667339ffa2b0dd96cb8a1753fc868e873610e270dfb89560734660f094 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ba1184667339ffa2b0dd96cb8a1753fc868e873610e270dfb89560734660f094->enter($__internal_ba1184667339ffa2b0dd96cb8a1753fc868e873610e270dfb89560734660f094_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        $__internal_cba0918358e68d445fa11dc628f90ab34bf1d9052932026abc0a37d962a2dbdb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cba0918358e68d445fa11dc628f90ab34bf1d9052932026abc0a37d962a2dbdb->enter($__internal_cba0918358e68d445fa11dc628f90ab34bf1d9052932026abc0a37d962a2dbdb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        // line 211
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/jquery/jquery.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 212
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/tether/tether.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 213
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 214
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/plugins.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 215
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/app.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_cba0918358e68d445fa11dc628f90ab34bf1d9052932026abc0a37d962a2dbdb->leave($__internal_cba0918358e68d445fa11dc628f90ab34bf1d9052932026abc0a37d962a2dbdb_prof);

        
        $__internal_ba1184667339ffa2b0dd96cb8a1753fc868e873610e270dfb89560734660f094->leave($__internal_ba1184667339ffa2b0dd96cb8a1753fc868e873610e270dfb89560734660f094_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  577 => 215,  573 => 214,  569 => 213,  565 => 212,  560 => 211,  551 => 210,  521 => 185,  519 => 184,  504 => 172,  493 => 164,  481 => 155,  470 => 147,  450 => 137,  436 => 132,  427 => 131,  415 => 205,  412 => 137,  410 => 131,  398 => 129,  386 => 107,  384 => 106,  382 => 105,  380 => 104,  378 => 103,  376 => 102,  374 => 101,  372 => 100,  370 => 99,  368 => 98,  366 => 97,  360 => 93,  356 => 92,  352 => 91,  340 => 82,  336 => 81,  332 => 80,  320 => 71,  316 => 70,  312 => 69,  300 => 60,  296 => 59,  292 => 58,  277 => 46,  266 => 38,  262 => 36,  253 => 35,  128 => 217,  126 => 210,  121 => 207,  119 => 129,  104 => 117,  98 => 114,  92 => 110,  90 => 35,  80 => 28,  76 => 27,  72 => 26,  68 => 25,  64 => 24,  60 => 23,  56 => 22,  52 => 21,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head lang=\"en\">
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\">
        <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
        <title>Trading</title>

        <link href=\"img/favicon.144x144.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"144x144\">
        <link href=\"img/favicon.114x114.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"114x114\">
        <link href=\"img/favicon.72x72.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"72x72\">
        <link href=\"img/favicon.57x57.png\" rel=\"apple-touch-icon\" type=\"image/png\">
        <link href=\"img/favicon.png\" rel=\"icon\" type=\"image/png\">
        <link href=\"img/favicon.ico\" rel=\"shortcut icon\">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
        <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
        <![endif]-->
        <link rel=\"stylesheet\" href=\"{{asset('css/lib/lobipanel/lobipanel.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/separate/vendor/lobipanel.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/lib/jqueryui/jquery-ui.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/separate/pages/widgets.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/lib/font-awesome/font-awesome.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/lib/bootstrap/bootstrap.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/main.css')}}\">
        <link rel=\"stylesheet\" href=\"{{asset('css/lib/acceuil.css')}}\">



    </head>

    <body class=\"with-side-menu control-panel control-panel-compact\"  style=\"background: white\">
\t     {% block menu %}
        <nav class=\"side-menu side-menu-big-icon\" style=\"background: #3299CC;overflow: hidden\">
            <nav class=\"sidebar-header\">
                <img src=\"{{asset('img/man.png')}}\" style=\"margin-left:35%;margin-top:-15%;\"><br><br>
                <h6 style=\"text-align:center;color:white;\">NIANG</h6>
                <h6 style=\"text-align:center;color:white;\">Trader</h6>
            </nav>
            <hr style=\"margin-left:20%;margin-right: 20%;background: red;height:1px\">
            <ul class=\"side-menu-list\">
                <li class=\"opened\" >
                    <span id=\"acc\">
                    \t<a href=\"{{ path('accueil') }}\" id=\"acc\" >
\t                        <i class=\"font-icon font-icon-home\"  style=\"color: white;\" ></i>
                            <span class=\"lbl\" style=\"\" id=\"acceuil\"><h6>Accueil</h6></span>
                        </a>
                    </span>
                </li>
                <li class=\"green with-sub\">
\t\t            <span id=\"prod\">
\t\t                <i class=\"font-icon font-icon-view-grid\" style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\" ><h6>Produit</h6></span>
\t\t            </span>
                    <ul>
                        <li><a href=\"{{ path('ajouter_produit') }}\"  id=\"prod\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"{{ path('lister_produit') }}\"  id=\"prod\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"{{ path('lister_produit') }}\"  id=\"prod\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                <li class=\"green with-sub\">
                    <span id=\"rep\">
                        <i class=\"font-icon font-icon-user\"  style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\"><h6>Representants</h6></span>
                    </span>
                    <ul>
                        <li><a href=\"{{ path('ajouter_representant') }}\" id=\"rep\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"{{ path('lister_representant') }}\" id=\"rep\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"{{ path('lister_representant') }}\" id=\"rep\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                <li class=\" with-sub\">
                    <span id=\"contrat\">
                        <i class=\"font-icon font-icon-archive\"  style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\"><h6>Contrats</h6></span>
                    </span>
                    <ul>
                        <li><a href=\"{{ path('ajouter_contratMarchandise') }}\" id=\"contrat\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"{{ path('lister_contrat') }}\" id=\"contrat\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"{{ path('lister_contrat') }}\" id=\"contrat\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                <li class=\"with-sub\">
                    <span id=\"transporteur\">
                        <i class=\"font-icon font-icon-weather-waves\" style=\"color: white\"></i>
                        <span class=\"lbl\" style=\"color: white\"><h6>Transporteurs</h6></span>
                    </span>
                    <ul>
                        <li><a href=\"{{ path('ajouter_transporteur') }}\" id=\"transporteur\"><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>
                        <li><a href=\"{{ path('lister_transporteur') }}\" id=\"transporteur\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>
                        <li><a href=\"{{ path('lister_transporteur') }}\" id=\"transporteur\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>
                    </ul>
                </li>
                {#<li class=\"with-sub\">#}
                    {#<span id=\"instr\">#}
                        {#<i class=\"font-icon font-icon-weather-cloud\" style=\"color: white\"></i>#}
                        {#<span class=\"lbl\" style=\"color: white\"><h6>Instructions de livraison</h6></span>#}
                    {#</span>#}
                    {#<ul>#}
                        {#<li><a href=\"{{ path('ajouter_instruction') }}\" id=\"instr\" ><span class=\"lbl\" style=\"color: white\">Ajouter</span></a></li>#}
                        {#<li><a href=\"{{ path('lister_instruction') }}\" id=\"instr\"><span class=\"lbl\" style=\"color: white\">Modifier</span></a></li>#}
                        {#<li><a href=\"{{ path('lister_instruction') }}\" id=\"instr\"><span class=\"lbl\" style=\"color: white\">Lister</span></a></li>#}
                    {#</ul>#}
                {#</li>#}
            </ul>
        </nav><!--.side-menu-->
\t\t {% endblock %}
        <div class=\"page-content\" style=\"background: white\">
            <!--entete du site-->
                <div class=\"container-fluid\" style=\"height: 70px\">
                    <h2 style=\"font-weight: bold;font-size:35px;color:#3299CC; width:50%;margin-top:-10%\">Trading Platform</h2>
                    <img src=\"{{asset('img/bateau.png')}}\" style=\"margin-top:-9%;margin-left:30%;\">

                </div><!--.container-fluid-->
            <a href=\"{{ path('login') }}\" ><div class=\"glyphicon glyphicon-log-out\" style=\"float:right;margin-top: -7%;margin-left: 10%\"></div></a>
                <div class=\"site-header-search-container\"><br>
                    <form class=\"site-header-search\" style=\"width: 30% ;float: right;margin-top:-5%;\">
                        <input type=\"text\" placeholder=\"Search\"/>
                        <button type=\"submit\">
                            <span class=\"font-icon-search\"></span>
                        </button>
                        <div class=\"overlay\"></div>
                    </form><br><br>
                </div>
            <!--entete du site-->

            {% block contenuDuMilieu %}<!--le block du milieu de la page-->
            <div class=\"row\">
                {% block debutPage %}
                    <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                        <h3 style=\"color: white;margin-top:3%\"> <center>Accueil</center></h3>
                    </div>
                    <br><br><br><br><br><br>
                {% endblock %}
                {% block modules %}<!--le block pour les differents modules de l'application-->
                <div class=\"container-fluid\">
                    <div class=\"row\" style=\"margin-top: -50px;margin-left: 4%\">
                        <div class=\"  col-xl-12\">
                            <div class=\"col-xl-12\">
                                <div class=\"row\" >
                                    <div class=\" col-sm-3\">
                                        <article class=\"statistic-box red\" style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto\"><a href=\"{{ path('ajouter_produit') }}\" style=\"color:white;\">Gestion Marchandises</a></h5></div>
                                            </div>
                                        </article>
                                    </div>
                                    <div class=\"col-sm-3\">
                                        <article class=\"statistic-box purple\"  style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto\"><a href=\"{{ path('ajouter_transporteur') }}\"  style=\"color:white;\">Gestion Transporteurs</a></h5></div>

                                            </div>
                                        </article>
                                    </div><!--.col-->
                                    <div class=\"col-sm-3\">
                                        <article class=\"statistic-box yellow\"  style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto\"><a href=\"{{ path('ajouter_representant') }}\"  style=\"color:white;\">Gestion Representants</a></h5></div>
                                            </div>
                                        </article>
                                    </div><!--.col-->
                                    <div class=\"col-sm-3\">
                                        <div class=\"statistic-box green\"  style=\"height: 100px\">
                                            <div>
                                                <br> <br>
                                                <div class=\"caption\"><h5 style=\"margin: auto;align:center;\"><a href=\"{{ path('ajouter_contratMarchandise') }}\" style=\"color:white;\">Gestion Contrats</a></h5></div>
                                            </div>
                                        </div>
                                    </div><!--.col-->
                                </div><!--.row-->
                            </div><!--.col-->
                        </div><!--col-->
                    </div><!--row--><br><br>


                    <div class=\"row\">
                        <div class=\"col-lg-8\">
                                {% include 'map.html.twig' %}
                        </div>

                        <div class=\"col-lg-3 dahsboard-column\" style=\"margin-top: 50px;margin-left: 7%\">
                                <div class=\" box-typical\" >
                                    <div class=\"calendar-page\">
                                        <div class=\"calendar-page-side\">
                                            <section class=\"calendar-page-side-section\">
                                                <div class=\"calendar-page-side-section-in\">
                                                    <div id=\"side-datetimepicker\"></div>
                                                </div>
                                            </section>
                                        </div><!--.calendar-page-side-->
                                    </div><!--.calendar-page-->
                                </div>
                            </div><!--.container-fluid-->

                    </div>


                    </div><!--.container-fluid-->
                {% endblock %}<!--le block pour les differents modules de l'application-->
            </div><!--row-->
            {% endblock %}<!--Contenu du milieu-->
        </div><!--.page-content-->

        {% block script %}
            <script src=\"{{ asset('js/lib/jquery/jquery.min.js')}}\"></script>
            <script src=\"{{ asset('js/lib/tether/tether.min.js')}}\"></script>
            <script src=\"{{ asset('js/lib/bootstrap/bootstrap.min.js')}}\"></script>
            <script src=\"{{ asset('js/plugins.js')}}\"></script>
            <script src=\"{{ asset('js/app.js')}}\"></script>
        {% endblock %}

            <script type=\"text/javascript\" src=\"js/lib/jqueryui/jquery-ui.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/lobipanel/lobipanel.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/match-height/jquery.matchHeight.min.js\"></script>
            <script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>

            <script src=\"js/lib/jquery/jquery.min.js\"></script>
            <script src=\"js/lib/tether/tether.min.js\"></script>
            <script src=\"js/lib/bootstrap/bootstrap.min.js\"></script>
            <script src=\"js/plugins.js\"></script>

            <script type=\"text/javascript\" src=\"js/lib/match-height/jquery.matchHeight.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/moment/moment-with-locales.min.js\"></script>
            <script type=\"text/javascript\" src=\"js/lib/eonasdan-bootstrap-datetimepicker/bootstrap-datetimepicker.min.js\"></script>
            <script src=\"js/lib/fullcalendar/fullcalendar.min.js\"></script>
            <script src=\"js/lib/fullcalendar/fullcalendar-init.js\"></script>

            <script>
                \$(document).ready(function() {
                    \$('.panel').lobiPanel({
                        sortable: true
                    });
                    \$('.panel').on('dragged.lobiPanel', function(ev, lobiPanel){
                        \$('.dahsboard-column').matchHeight();
                    });

                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawChart);
                    function drawChart() {
                        var dataTable = new google.visualization.DataTable();
                        dataTable.addColumn('string', 'Day');
                        dataTable.addColumn('number', 'Values');
                        // A column for custom tooltip content
                        dataTable.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});
                        dataTable.addRows([
                            ['MON',  130, ' '],
                            ['TUE',  130, '130'],
                            ['WED',  180, '180'],
                            ['THU',  175, '175'],
                            ['FRI',  200, '200'],
                            ['SAT',  170, '170'],
                            ['SUN',  250, '250'],
                            ['MON',  220, '220'],
                            ['TUE',  220, ' ']
                        ]);

                        var options = {
                            height: 314,
                            legend: 'none',
                            areaOpacity: 0.18,
                            axisTitlesPosition: 'out',
                            hAxis: {
                                title: '',
                                textStyle: {
                                    color: '#fff',
                                    fontName: 'Proxima Nova',
                                    fontSize: 11,
                                    bold: true,
                                    italic: false
                                },
                                textPosition: 'out'
                            },
                            vAxis: {
                                minValue: 0,
                                textPosition: 'out',
                                textStyle: {
                                    color: '#fff',
                                    fontName: 'Proxima Nova',
                                    fontSize: 11,
                                    bold: true,
                                    italic: false
                                },
                                baselineColor: '#16b4fc',
                                ticks: [0,25,50,75,100,125,150,175,200,225,250,275,300,325,350],
                                gridlines: {
                                    color: '#1ba0fc',
                                    count: 15
                                },
                            },
                            lineWidth: 2,
                            colors: ['#fff'],
                            curveType: 'function',
                            pointSize: 5,
                            pointShapeType: 'circle',
                            pointFillColor: '#f00',
                            backgroundColor: {
                                fill: '#008ffb',
                                strokeWidth: 0,
                            },
                            chartArea:{
                                left:0,
                                top:0,
                                width:'100%',
                                height:'100%'
                            },
                            fontSize: 11,
                            fontName: 'Proxima Nova',
                            tooltip: {
                                trigger: 'selection',
                                isHtml: true
                            }
                        };

                        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
                        chart.draw(dataTable, options);
                    }
                    \$(window).resize(function(){
                        drawChart();
                        setTimeout(function(){
                        }, 1000);
                    });
                });
            </script>
    </body>

</html>", "base.html.twig", "C:\\wamp64\\www\\stock\\app\\Resources\\views\\base.html.twig");
    }
}
