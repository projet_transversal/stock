<?php

/* StockBundle:Marchandise:addMarchandise.html.twig */
class __TwigTemplate_e51422f012b3ac74e0807c16cd27dd4d8bb5a666c095cb7e98d9b32c1fcc1475 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:Marchandise:addMarchandise.html.twig", 1);
        $this->blocks = array(
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_95bf405364324fd3e4c5e3ae11ddcd95eb8cf47cc22512670a7083ffa3b586c1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95bf405364324fd3e4c5e3ae11ddcd95eb8cf47cc22512670a7083ffa3b586c1->enter($__internal_95bf405364324fd3e4c5e3ae11ddcd95eb8cf47cc22512670a7083ffa3b586c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Marchandise:addMarchandise.html.twig"));

        $__internal_8141b838ad37e07a703c31397148497296455f89baa9798c967625dcb4068c28 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8141b838ad37e07a703c31397148497296455f89baa9798c967625dcb4068c28->enter($__internal_8141b838ad37e07a703c31397148497296455f89baa9798c967625dcb4068c28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Marchandise:addMarchandise.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_95bf405364324fd3e4c5e3ae11ddcd95eb8cf47cc22512670a7083ffa3b586c1->leave($__internal_95bf405364324fd3e4c5e3ae11ddcd95eb8cf47cc22512670a7083ffa3b586c1_prof);

        
        $__internal_8141b838ad37e07a703c31397148497296455f89baa9798c967625dcb4068c28->leave($__internal_8141b838ad37e07a703c31397148497296455f89baa9798c967625dcb4068c28_prof);

    }

    // line 3
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_3d4e1808a363407659f2f6bbdbda3328d2996b0c5957a178fbe0d8a7eac72643 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3d4e1808a363407659f2f6bbdbda3328d2996b0c5957a178fbe0d8a7eac72643->enter($__internal_3d4e1808a363407659f2f6bbdbda3328d2996b0c5957a178fbe0d8a7eac72643_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_8ea2704d0198ebf8edd5a113a9af9d26998aebb6454aa6a866cd8c32f5caec41 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8ea2704d0198ebf8edd5a113a9af9d26998aebb6454aa6a866cd8c32f5caec41->enter($__internal_8ea2704d0198ebf8edd5a113a9af9d26998aebb6454aa6a866cd8c32f5caec41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        // line 4
        echo "        ";
        $this->displayBlock('debutPage', $context, $blocks);
        // line 13
        echo "        ";
        $this->displayBlock('modules', $context, $blocks);
        // line 18
        echo "    ";
        
        $__internal_8ea2704d0198ebf8edd5a113a9af9d26998aebb6454aa6a866cd8c32f5caec41->leave($__internal_8ea2704d0198ebf8edd5a113a9af9d26998aebb6454aa6a866cd8c32f5caec41_prof);

        
        $__internal_3d4e1808a363407659f2f6bbdbda3328d2996b0c5957a178fbe0d8a7eac72643->leave($__internal_3d4e1808a363407659f2f6bbdbda3328d2996b0c5957a178fbe0d8a7eac72643_prof);

    }

    // line 4
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_936b1491332d734934604bc0a9de1507f8e8cc5d2fca074c4ac781114b3c781d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_936b1491332d734934604bc0a9de1507f8e8cc5d2fca074c4ac781114b3c781d->enter($__internal_936b1491332d734934604bc0a9de1507f8e8cc5d2fca074c4ac781114b3c781d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_544ca752d0f367c4af0bf8226dab0cf7c21994068d10ab73e87fd3e436c5f220 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_544ca752d0f367c4af0bf8226dab0cf7c21994068d10ab73e87fd3e436c5f220->enter($__internal_544ca752d0f367c4af0bf8226dab0cf7c21994068d10ab73e87fd3e436c5f220_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 5
        echo "            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                    <h3 style=\"color: white;margin-top:4%\"> <center>Ajouter Marchandise</center></h3>
                </div>
                <br><br><br><br><br><br>
            </div>

        ";
        
        $__internal_544ca752d0f367c4af0bf8226dab0cf7c21994068d10ab73e87fd3e436c5f220->leave($__internal_544ca752d0f367c4af0bf8226dab0cf7c21994068d10ab73e87fd3e436c5f220_prof);

        
        $__internal_936b1491332d734934604bc0a9de1507f8e8cc5d2fca074c4ac781114b3c781d->leave($__internal_936b1491332d734934604bc0a9de1507f8e8cc5d2fca074c4ac781114b3c781d_prof);

    }

    // line 13
    public function block_modules($context, array $blocks = array())
    {
        $__internal_868695e8d2576d1211d86315f52b0178f29a87faef6dbe8f9aa5f7bcb913537b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_868695e8d2576d1211d86315f52b0178f29a87faef6dbe8f9aa5f7bcb913537b->enter($__internal_868695e8d2576d1211d86315f52b0178f29a87faef6dbe8f9aa5f7bcb913537b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_9f1db0f423732d1e41aaf3d9c2e6acf0384b4849d20b3c5a149655bbdf905ae0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f1db0f423732d1e41aaf3d9c2e6acf0384b4849d20b3c5a149655bbdf905ae0->enter($__internal_9f1db0f423732d1e41aaf3d9c2e6acf0384b4849d20b3c5a149655bbdf905ae0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        // line 14
        echo "            <div class=\"col-xl-6\" style=\"margin-left: 25%;\" >
                ";
        // line 15
        $this->loadTemplate("::formulaire.html.twig", "StockBundle:Marchandise:addMarchandise.html.twig", 15)->display($context);
        // line 16
        echo "            </div>
        ";
        
        $__internal_9f1db0f423732d1e41aaf3d9c2e6acf0384b4849d20b3c5a149655bbdf905ae0->leave($__internal_9f1db0f423732d1e41aaf3d9c2e6acf0384b4849d20b3c5a149655bbdf905ae0_prof);

        
        $__internal_868695e8d2576d1211d86315f52b0178f29a87faef6dbe8f9aa5f7bcb913537b->leave($__internal_868695e8d2576d1211d86315f52b0178f29a87faef6dbe8f9aa5f7bcb913537b_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Marchandise:addMarchandise.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 16,  105 => 15,  102 => 14,  93 => 13,  76 => 5,  67 => 4,  57 => 18,  54 => 13,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}

    {% block contenuDuMilieu %}
        {% block debutPage %}
            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                    <h3 style=\"color: white;margin-top:4%\"> <center>Ajouter Marchandise</center></h3>
                </div>
                <br><br><br><br><br><br>
            </div>

        {% endblock %}
        {% block modules %}
            <div class=\"col-xl-6\" style=\"margin-left: 25%;\" >
                {% include '::formulaire.html.twig' %}
            </div>
        {% endblock %}
    {% endblock %}", "StockBundle:Marchandise:addMarchandise.html.twig", "C:\\wamp\\www\\symfony\\stock\\src\\StockBundle/Resources/views/Marchandise/addMarchandise.html.twig");
    }
}
