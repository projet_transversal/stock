<?php

/* ::representant.html.twig */
class __TwigTemplate_0bf78dc392d46f360c67bf4ba9817158ee35f6f1e123a210ad1ae70496d920ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "::representant.html.twig", 1);
        $this->blocks = array(
            'menu' => array($this, 'block_menu'),
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aae1c1f15f5edf04da131fe42cb0c87ad9fc8ff7ef5039405e0b6bf5ae3f22e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aae1c1f15f5edf04da131fe42cb0c87ad9fc8ff7ef5039405e0b6bf5ae3f22e3->enter($__internal_aae1c1f15f5edf04da131fe42cb0c87ad9fc8ff7ef5039405e0b6bf5ae3f22e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::representant.html.twig"));

        $__internal_9c97ceb33fc733531b5b802ab1f890709ccbb0838e1437d314e0b3fe63d8c0ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c97ceb33fc733531b5b802ab1f890709ccbb0838e1437d314e0b3fe63d8c0ac->enter($__internal_9c97ceb33fc733531b5b802ab1f890709ccbb0838e1437d314e0b3fe63d8c0ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::representant.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_aae1c1f15f5edf04da131fe42cb0c87ad9fc8ff7ef5039405e0b6bf5ae3f22e3->leave($__internal_aae1c1f15f5edf04da131fe42cb0c87ad9fc8ff7ef5039405e0b6bf5ae3f22e3_prof);

        
        $__internal_9c97ceb33fc733531b5b802ab1f890709ccbb0838e1437d314e0b3fe63d8c0ac->leave($__internal_9c97ceb33fc733531b5b802ab1f890709ccbb0838e1437d314e0b3fe63d8c0ac_prof);

    }

    // line 3
    public function block_menu($context, array $blocks = array())
    {
        $__internal_26c66ad5fcbfaa7c8f6f1932bede7075e87eb1a2041b2bd90b58be8a04d92ac8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_26c66ad5fcbfaa7c8f6f1932bede7075e87eb1a2041b2bd90b58be8a04d92ac8->enter($__internal_26c66ad5fcbfaa7c8f6f1932bede7075e87eb1a2041b2bd90b58be8a04d92ac8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_170ee53ecbfb48b656228a52db91eb92c7fa00086729de692746736f0c5df525 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_170ee53ecbfb48b656228a52db91eb92c7fa00086729de692746736f0c5df525->enter($__internal_170ee53ecbfb48b656228a52db91eb92c7fa00086729de692746736f0c5df525_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 4
        echo "        <nav class=\"side-menu side-menu-big-icon\" style=\"background: #3299CC;overflow: hidden\">
            <nav class=\"sidebar-header\">
                <img src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/man.png"), "html", null, true);
        echo "\" style=\"margin-left:35%;margin-top:-15%;\"><br><br>
                <h5 style=\"text-align:center\">User Name</h5>
                <h5 style=\"text-align:center\">Profil</h5>
            </nav>
            <hr style=\"margin-left:20%;margin-right: 20%;background: red;height:1px\">
            <ul class=\"side-menu-list\">
                <li class=\"opened\">
                    <span id=\"instr\">
                    \t<a href=\"";
        // line 14
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("representant_livrer_marchandise");
        echo "\" >
\t                        <i class=\"glyphicon glyphicon-inbox\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Livrer Marchandise</span>
                        </a>
                    </span>
                </li>
                <li class=\"opened\">
                    <span id=\"prod\">
                    \t<a href=\"";
        // line 22
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("representant_lister_marchandise");
        echo "\" >
\t                        <i class=\"glyphicon glyphicon-th-list\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Lister Marchandise</span>
                        </a>
                    </span>
                </li>
                <li class=\"opened\">
                    <span id=\"rep\">
                    \t<a href=\"";
        // line 30
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("representant_ajouter_comm");
        echo "\" >
\t                        <i class=\"glyphicon glyphicon-plus\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Ajouter commerçant</span>
                        </a>
                    </span>
                </li>
            </ul>
        </nav>
    ";
        
        $__internal_170ee53ecbfb48b656228a52db91eb92c7fa00086729de692746736f0c5df525->leave($__internal_170ee53ecbfb48b656228a52db91eb92c7fa00086729de692746736f0c5df525_prof);

        
        $__internal_26c66ad5fcbfaa7c8f6f1932bede7075e87eb1a2041b2bd90b58be8a04d92ac8->leave($__internal_26c66ad5fcbfaa7c8f6f1932bede7075e87eb1a2041b2bd90b58be8a04d92ac8_prof);

    }

    // line 39
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_c60ef0e3123e63f8984b7dc5a1f6bbc397e4da03a28072e3519c2cba174d0bdb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c60ef0e3123e63f8984b7dc5a1f6bbc397e4da03a28072e3519c2cba174d0bdb->enter($__internal_c60ef0e3123e63f8984b7dc5a1f6bbc397e4da03a28072e3519c2cba174d0bdb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_0b63a817eaff822c59018a7aa7077ae659b39bb400998bc15462099616cc4523 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b63a817eaff822c59018a7aa7077ae659b39bb400998bc15462099616cc4523->enter($__internal_0b63a817eaff822c59018a7aa7077ae659b39bb400998bc15462099616cc4523_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        // line 40
        echo "        ";
        $this->displayBlock('debutPage', $context, $blocks);
        // line 49
        echo "        ";
        $this->displayBlock('modules', $context, $blocks);
        // line 54
        echo "    ";
        
        $__internal_0b63a817eaff822c59018a7aa7077ae659b39bb400998bc15462099616cc4523->leave($__internal_0b63a817eaff822c59018a7aa7077ae659b39bb400998bc15462099616cc4523_prof);

        
        $__internal_c60ef0e3123e63f8984b7dc5a1f6bbc397e4da03a28072e3519c2cba174d0bdb->leave($__internal_c60ef0e3123e63f8984b7dc5a1f6bbc397e4da03a28072e3519c2cba174d0bdb_prof);

    }

    // line 40
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_ae2c7702c48eaefa2ee5ea99e5636a870ebda61ad4820b55eae52c1d4bdae915 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ae2c7702c48eaefa2ee5ea99e5636a870ebda61ad4820b55eae52c1d4bdae915->enter($__internal_ae2c7702c48eaefa2ee5ea99e5636a870ebda61ad4820b55eae52c1d4bdae915_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_4020f738b5034ca45ff88b8606cac6e6c862d56c166c339b93d60c610fe8469d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4020f738b5034ca45ff88b8606cac6e6c862d56c166c339b93d60c610fe8469d->enter($__internal_4020f738b5034ca45ff88b8606cac6e6c862d56c166c339b93d60c610fe8469d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 41
        echo "            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-20px;\">
                    <h2 style=\"color: white;margin-top:4%\"> <center>Representant</center></h2>
                </div>
                <br><br><br><br><br><br>
            </div>

        ";
        
        $__internal_4020f738b5034ca45ff88b8606cac6e6c862d56c166c339b93d60c610fe8469d->leave($__internal_4020f738b5034ca45ff88b8606cac6e6c862d56c166c339b93d60c610fe8469d_prof);

        
        $__internal_ae2c7702c48eaefa2ee5ea99e5636a870ebda61ad4820b55eae52c1d4bdae915->leave($__internal_ae2c7702c48eaefa2ee5ea99e5636a870ebda61ad4820b55eae52c1d4bdae915_prof);

    }

    // line 49
    public function block_modules($context, array $blocks = array())
    {
        $__internal_b205534c7d7648089d6417ec21f023a7fbd0b1f9b2493c9b0634534d7938adbf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b205534c7d7648089d6417ec21f023a7fbd0b1f9b2493c9b0634534d7938adbf->enter($__internal_b205534c7d7648089d6417ec21f023a7fbd0b1f9b2493c9b0634534d7938adbf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_bb6648b182d703ceba52b88f3b5a6ac9dd9687506a34872810135b72641e1dff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bb6648b182d703ceba52b88f3b5a6ac9dd9687506a34872810135b72641e1dff->enter($__internal_bb6648b182d703ceba52b88f3b5a6ac9dd9687506a34872810135b72641e1dff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        // line 50
        echo "            <div class=\"col-xl-6\" style=\"margin-left: 27%\">
                ";
        // line 51
        $this->loadTemplate("formulaire.html.twig", "::representant.html.twig", 51)->display($context);
        // line 52
        echo "            </div>
        ";
        
        $__internal_bb6648b182d703ceba52b88f3b5a6ac9dd9687506a34872810135b72641e1dff->leave($__internal_bb6648b182d703ceba52b88f3b5a6ac9dd9687506a34872810135b72641e1dff_prof);

        
        $__internal_b205534c7d7648089d6417ec21f023a7fbd0b1f9b2493c9b0634534d7938adbf->leave($__internal_b205534c7d7648089d6417ec21f023a7fbd0b1f9b2493c9b0634534d7938adbf_prof);

    }

    public function getTemplateName()
    {
        return "::representant.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  173 => 52,  171 => 51,  168 => 50,  159 => 49,  142 => 41,  133 => 40,  123 => 54,  120 => 49,  117 => 40,  108 => 39,  89 => 30,  78 => 22,  67 => 14,  56 => 6,  52 => 4,  43 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

\t{% block menu %}
        <nav class=\"side-menu side-menu-big-icon\" style=\"background: #3299CC;overflow: hidden\">
            <nav class=\"sidebar-header\">
                <img src=\"{{asset('img/man.png')}}\" style=\"margin-left:35%;margin-top:-15%;\"><br><br>
                <h5 style=\"text-align:center\">User Name</h5>
                <h5 style=\"text-align:center\">Profil</h5>
            </nav>
            <hr style=\"margin-left:20%;margin-right: 20%;background: red;height:1px\">
            <ul class=\"side-menu-list\">
                <li class=\"opened\">
                    <span id=\"instr\">
                    \t<a href=\"{{ path('representant_livrer_marchandise') }}\" >
\t                        <i class=\"glyphicon glyphicon-inbox\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Livrer Marchandise</span>
                        </a>
                    </span>
                </li>
                <li class=\"opened\">
                    <span id=\"prod\">
                    \t<a href=\"{{ path('representant_lister_marchandise') }}\" >
\t                        <i class=\"glyphicon glyphicon-th-list\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Lister Marchandise</span>
                        </a>
                    </span>
                </li>
                <li class=\"opened\">
                    <span id=\"rep\">
                    \t<a href=\"{{ path('representant_ajouter_comm') }}\" >
\t                        <i class=\"glyphicon glyphicon-plus\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Ajouter commerçant</span>
                        </a>
                    </span>
                </li>
            </ul>
        </nav>
    {% endblock %}
    {% block contenuDuMilieu %}
        {% block debutPage %}
            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-20px;\">
                    <h2 style=\"color: white;margin-top:4%\"> <center>Representant</center></h2>
                </div>
                <br><br><br><br><br><br>
            </div>

        {% endblock %}
        {% block modules %}
            <div class=\"col-xl-6\" style=\"margin-left: 27%\">
                {% include 'formulaire.html.twig' %}
            </div>
        {% endblock %}
    {% endblock %}", "::representant.html.twig", "C:\\wamp64\\www\\stock\\app/Resources\\views/representant.html.twig");
    }
}
