<?php

/* StockBundle:Representant:addRepresentant.html.twig */
class __TwigTemplate_0b35f3331b23bd8df9bb601589affaf0b3cfc538bbfc9dae7856d99327cb4480 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:Representant:addRepresentant.html.twig", 1);
        $this->blocks = array(
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d30cc3e5c9a8ffc13702b87ca88add11efbbfcd31ec3bbfe8a314d7251bba954 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d30cc3e5c9a8ffc13702b87ca88add11efbbfcd31ec3bbfe8a314d7251bba954->enter($__internal_d30cc3e5c9a8ffc13702b87ca88add11efbbfcd31ec3bbfe8a314d7251bba954_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Representant:addRepresentant.html.twig"));

        $__internal_986f53ba11bb809bd54cc9877adf24c98acec5bb433e632a6079bd4094486f0f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_986f53ba11bb809bd54cc9877adf24c98acec5bb433e632a6079bd4094486f0f->enter($__internal_986f53ba11bb809bd54cc9877adf24c98acec5bb433e632a6079bd4094486f0f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Representant:addRepresentant.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d30cc3e5c9a8ffc13702b87ca88add11efbbfcd31ec3bbfe8a314d7251bba954->leave($__internal_d30cc3e5c9a8ffc13702b87ca88add11efbbfcd31ec3bbfe8a314d7251bba954_prof);

        
        $__internal_986f53ba11bb809bd54cc9877adf24c98acec5bb433e632a6079bd4094486f0f->leave($__internal_986f53ba11bb809bd54cc9877adf24c98acec5bb433e632a6079bd4094486f0f_prof);

    }

    // line 3
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_5c916288fc050a0cc1d3451c9b9b4601c771b94c6ffb8228db4e509db2b5c8e5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5c916288fc050a0cc1d3451c9b9b4601c771b94c6ffb8228db4e509db2b5c8e5->enter($__internal_5c916288fc050a0cc1d3451c9b9b4601c771b94c6ffb8228db4e509db2b5c8e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_7a0dbce03caf42d9497ee71f222bea72b7be4fa762a9df3aced15c0dbcf24b2d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7a0dbce03caf42d9497ee71f222bea72b7be4fa762a9df3aced15c0dbcf24b2d->enter($__internal_7a0dbce03caf42d9497ee71f222bea72b7be4fa762a9df3aced15c0dbcf24b2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        // line 4
        echo "        ";
        $this->displayBlock('debutPage', $context, $blocks);
        // line 13
        echo "        ";
        $this->displayBlock('modules', $context, $blocks);
        // line 18
        echo "    ";
        
        $__internal_7a0dbce03caf42d9497ee71f222bea72b7be4fa762a9df3aced15c0dbcf24b2d->leave($__internal_7a0dbce03caf42d9497ee71f222bea72b7be4fa762a9df3aced15c0dbcf24b2d_prof);

        
        $__internal_5c916288fc050a0cc1d3451c9b9b4601c771b94c6ffb8228db4e509db2b5c8e5->leave($__internal_5c916288fc050a0cc1d3451c9b9b4601c771b94c6ffb8228db4e509db2b5c8e5_prof);

    }

    // line 4
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_0ac1becbbb04046707ef5bf375a55dc2e9bb291ce040929e8e95790071b2f56a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0ac1becbbb04046707ef5bf375a55dc2e9bb291ce040929e8e95790071b2f56a->enter($__internal_0ac1becbbb04046707ef5bf375a55dc2e9bb291ce040929e8e95790071b2f56a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_61af4f43442eebd9c34d1fd78d457cc5b45136a5ce084d604a5a01fcc9a85625 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_61af4f43442eebd9c34d1fd78d457cc5b45136a5ce084d604a5a01fcc9a85625->enter($__internal_61af4f43442eebd9c34d1fd78d457cc5b45136a5ce084d604a5a01fcc9a85625_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 5
        echo "            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                    <h3 style=\"color: white;margin-top:4%\"> <center>Ajouter Representant</center></h3>
                </div>
                <br><br><br><br><br><br>
            </div>

        ";
        
        $__internal_61af4f43442eebd9c34d1fd78d457cc5b45136a5ce084d604a5a01fcc9a85625->leave($__internal_61af4f43442eebd9c34d1fd78d457cc5b45136a5ce084d604a5a01fcc9a85625_prof);

        
        $__internal_0ac1becbbb04046707ef5bf375a55dc2e9bb291ce040929e8e95790071b2f56a->leave($__internal_0ac1becbbb04046707ef5bf375a55dc2e9bb291ce040929e8e95790071b2f56a_prof);

    }

    // line 13
    public function block_modules($context, array $blocks = array())
    {
        $__internal_554f7715d8e96ecdf11e6b0299a1fb7468404d00461c675fcba6c410c74a7b59 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_554f7715d8e96ecdf11e6b0299a1fb7468404d00461c675fcba6c410c74a7b59->enter($__internal_554f7715d8e96ecdf11e6b0299a1fb7468404d00461c675fcba6c410c74a7b59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_0df6f69df91ae8572919fa0949c31d747901b452ad5dd069f305e0d73b1e9ebb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0df6f69df91ae8572919fa0949c31d747901b452ad5dd069f305e0d73b1e9ebb->enter($__internal_0df6f69df91ae8572919fa0949c31d747901b452ad5dd069f305e0d73b1e9ebb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        // line 14
        echo "            <div class=\"col-xl-6\" style=\"margin-left: 27%\">
                ";
        // line 15
        $this->loadTemplate("::formulaire.html.twig", "StockBundle:Representant:addRepresentant.html.twig", 15)->display($context);
        // line 16
        echo "            </div>
        ";
        
        $__internal_0df6f69df91ae8572919fa0949c31d747901b452ad5dd069f305e0d73b1e9ebb->leave($__internal_0df6f69df91ae8572919fa0949c31d747901b452ad5dd069f305e0d73b1e9ebb_prof);

        
        $__internal_554f7715d8e96ecdf11e6b0299a1fb7468404d00461c675fcba6c410c74a7b59->leave($__internal_554f7715d8e96ecdf11e6b0299a1fb7468404d00461c675fcba6c410c74a7b59_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Representant:addRepresentant.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 16,  105 => 15,  102 => 14,  93 => 13,  76 => 5,  67 => 4,  57 => 18,  54 => 13,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}

    {% block contenuDuMilieu %}
        {% block debutPage %}
            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                    <h3 style=\"color: white;margin-top:4%\"> <center>Ajouter Representant</center></h3>
                </div>
                <br><br><br><br><br><br>
            </div>

        {% endblock %}
        {% block modules %}
            <div class=\"col-xl-6\" style=\"margin-left: 27%\">
                {% include '::formulaire.html.twig' %}
            </div>
        {% endblock %}
    {% endblock %}", "StockBundle:Representant:addRepresentant.html.twig", "C:\\wamp64\\www\\stock\\src\\StockBundle/Resources/views/Representant/addRepresentant.html.twig");
    }
}
