<?php

/* StockBundle:Default:utilisateur.html.twig */
class __TwigTemplate_0a9d1e93bff5c79cf3ceed937cc2cfce5c9379d510d5a9cae2d4d2db2caada3b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_53c2382bab6697a94d913f11c4b7b5079efc3030fd3bcb9f224e3ef677394c77 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_53c2382bab6697a94d913f11c4b7b5079efc3030fd3bcb9f224e3ef677394c77->enter($__internal_53c2382bab6697a94d913f11c4b7b5079efc3030fd3bcb9f224e3ef677394c77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Default:utilisateur.html.twig"));

        $__internal_c4973579b1c62b9890607fb0b194533543cf4f07c5764ea487b2cdcf45f1069f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c4973579b1c62b9890607fb0b194533543cf4f07c5764ea487b2cdcf45f1069f->enter($__internal_c4973579b1c62b9890607fb0b194533543cf4f07c5764ea487b2cdcf45f1069f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Default:utilisateur.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head lang=\"en\">
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\">
    <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
    <title>StartUI - Premium Bootstrap 4 Admin Dashboard Template</title>

    <link href=\"img/favicon.144x144.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"144x144\">
    <link href=\"img/favicon.114x114.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"114x114\">
    <link href=\"img/favicon.72x72.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"72x72\">
    <link href=\"img/favicon.57x57.png\" rel=\"apple-touch-icon\" type=\"image/png\">
    <link href=\"img/favicon.png\" rel=\"icon\" type=\"image/png\">
    <link href=\"img/favicon.ico\" rel=\"shortcut icon\">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
    <link rel=\"stylesheet\" href=\"css/separate/pages/login.min.css\">
    <link rel=\"stylesheet\" href=\"css/lib/font-awesome/font-awesome.min.css\">
    <link rel=\"stylesheet\" href=\"css/lib/bootstrap/bootstrap.min.css\">
    <link rel=\"stylesheet\" href=\"css/main.css\">
</head>
<body>

<div class=\"page-center\">
    <div class=\"page-center-in\">
        <div class=\"container-fluid\">
            <form class=\"sign-box\" action=\"";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\" method=\"POST\">
                <div class=\"sign-avatar\">
                    <img src=\"img/avatar-sign.png\" alt=\"\">
                </div>
                <header class=\"sign-title\">Sign In</header>
                <div class=\"form-group\">
                    <input id=\"login\" name=\"login\" type=\"text\" class=\"form-control\" placeholder=\"E-Mail or Phone\"/>
                </div>
                <div class=\"form-group\">
                    <input name=\"password\" type=\"password\" class=\"form-control\" placeholder=\"Password\"/>
                </div>
                <div class=\"form-group\">
                    <div class=\"checkbox float-left\">
                        <input type=\"checkbox\" id=\"signed-in\"/>
                        <label for=\"signed-in\">Keep me signed in</label>
                    </div>
                    <div class=\"float-right reset\">
                        <a href=\"reset-password.html\">Reset Password</a>
                    </div>
                </div>
                <button type=\"submit\" class=\"btn btn-rounded\">Sign in</button>
                <p class=\"sign-note\">New to our website? <a href=\"sign-up.html\">Sign up</a></p>
                <!--<button type=\"button\" class=\"close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>-->
            </form>
        </div>
    </div>
</div><!--.page-center-->


<script src=\"js/lib/jquery/jquery.min.js\"></script>
<script src=\"js/lib/tether/tether.min.js\"></script>
<script src=\"js/lib/bootstrap/bootstrap.min.js\"></script>
<script src=\"js/plugins.js\"></script>
<script type=\"text/javascript\" src=\"js/lib/match-height/jquery.matchHeight.min.js\"></script>
<script>
    \$(function() {
        \$('.page-center').matchHeight({
            target: \$('html')
        });

        \$(window).resize(function(){
            setTimeout(function(){
                \$('.page-center').matchHeight({ remove: true });
                \$('.page-center').matchHeight({
                    target: \$('html')
                });
            },100);
        });
    });
</script>
<script src=\"js/app.js\"></script>
</body>
</html>";
        
        $__internal_53c2382bab6697a94d913f11c4b7b5079efc3030fd3bcb9f224e3ef677394c77->leave($__internal_53c2382bab6697a94d913f11c4b7b5079efc3030fd3bcb9f224e3ef677394c77_prof);

        
        $__internal_c4973579b1c62b9890607fb0b194533543cf4f07c5764ea487b2cdcf45f1069f->leave($__internal_c4973579b1c62b9890607fb0b194533543cf4f07c5764ea487b2cdcf45f1069f_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Default:utilisateur.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 31,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
<head lang=\"en\">
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\">
    <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
    <title>StartUI - Premium Bootstrap 4 Admin Dashboard Template</title>

    <link href=\"img/favicon.144x144.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"144x144\">
    <link href=\"img/favicon.114x114.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"114x114\">
    <link href=\"img/favicon.72x72.png\" rel=\"apple-touch-icon\" type=\"image/png\" sizes=\"72x72\">
    <link href=\"img/favicon.57x57.png\" rel=\"apple-touch-icon\" type=\"image/png\">
    <link href=\"img/favicon.png\" rel=\"icon\" type=\"image/png\">
    <link href=\"img/favicon.ico\" rel=\"shortcut icon\">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
    <link rel=\"stylesheet\" href=\"css/separate/pages/login.min.css\">
    <link rel=\"stylesheet\" href=\"css/lib/font-awesome/font-awesome.min.css\">
    <link rel=\"stylesheet\" href=\"css/lib/bootstrap/bootstrap.min.css\">
    <link rel=\"stylesheet\" href=\"css/main.css\">
</head>
<body>

<div class=\"page-center\">
    <div class=\"page-center-in\">
        <div class=\"container-fluid\">
            <form class=\"sign-box\" action=\"{{ path('login') }}\" method=\"POST\">
                <div class=\"sign-avatar\">
                    <img src=\"img/avatar-sign.png\" alt=\"\">
                </div>
                <header class=\"sign-title\">Sign In</header>
                <div class=\"form-group\">
                    <input id=\"login\" name=\"login\" type=\"text\" class=\"form-control\" placeholder=\"E-Mail or Phone\"/>
                </div>
                <div class=\"form-group\">
                    <input name=\"password\" type=\"password\" class=\"form-control\" placeholder=\"Password\"/>
                </div>
                <div class=\"form-group\">
                    <div class=\"checkbox float-left\">
                        <input type=\"checkbox\" id=\"signed-in\"/>
                        <label for=\"signed-in\">Keep me signed in</label>
                    </div>
                    <div class=\"float-right reset\">
                        <a href=\"reset-password.html\">Reset Password</a>
                    </div>
                </div>
                <button type=\"submit\" class=\"btn btn-rounded\">Sign in</button>
                <p class=\"sign-note\">New to our website? <a href=\"sign-up.html\">Sign up</a></p>
                <!--<button type=\"button\" class=\"close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>-->
            </form>
        </div>
    </div>
</div><!--.page-center-->


<script src=\"js/lib/jquery/jquery.min.js\"></script>
<script src=\"js/lib/tether/tether.min.js\"></script>
<script src=\"js/lib/bootstrap/bootstrap.min.js\"></script>
<script src=\"js/plugins.js\"></script>
<script type=\"text/javascript\" src=\"js/lib/match-height/jquery.matchHeight.min.js\"></script>
<script>
    \$(function() {
        \$('.page-center').matchHeight({
            target: \$('html')
        });

        \$(window).resize(function(){
            setTimeout(function(){
                \$('.page-center').matchHeight({ remove: true });
                \$('.page-center').matchHeight({
                    target: \$('html')
                });
            },100);
        });
    });
</script>
<script src=\"js/app.js\"></script>
</body>
</html>", "StockBundle:Default:utilisateur.html.twig", "C:\\wamp64\\www\\stock\\src\\StockBundle/Resources/views/Default/utilisateur.html.twig");
    }
}
