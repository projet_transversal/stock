<?php

/* StockBundle:ContratMarchandise:visualiserContrat.html.twig */
class __TwigTemplate_642acfb64c63d472a9a9500f8fa0c7844b156b549951ebd5cf77add4173a68f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:ContratMarchandise:visualiserContrat.html.twig", 1);
        $this->blocks = array(
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c03c0291ca72585ed8ca7468dafab831bca873c2769c03ea2b2e310d468484cb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c03c0291ca72585ed8ca7468dafab831bca873c2769c03ea2b2e310d468484cb->enter($__internal_c03c0291ca72585ed8ca7468dafab831bca873c2769c03ea2b2e310d468484cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:ContratMarchandise:visualiserContrat.html.twig"));

        $__internal_67bf71c2bdb5605b0e855bdd3834c981b87d5debc09cc3678a6483b7be518fb6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_67bf71c2bdb5605b0e855bdd3834c981b87d5debc09cc3678a6483b7be518fb6->enter($__internal_67bf71c2bdb5605b0e855bdd3834c981b87d5debc09cc3678a6483b7be518fb6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:ContratMarchandise:visualiserContrat.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c03c0291ca72585ed8ca7468dafab831bca873c2769c03ea2b2e310d468484cb->leave($__internal_c03c0291ca72585ed8ca7468dafab831bca873c2769c03ea2b2e310d468484cb_prof);

        
        $__internal_67bf71c2bdb5605b0e855bdd3834c981b87d5debc09cc3678a6483b7be518fb6->leave($__internal_67bf71c2bdb5605b0e855bdd3834c981b87d5debc09cc3678a6483b7be518fb6_prof);

    }

    // line 2
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_d3b81552fde48d5e0925760d45121b191a9cee0934cfe871e6e12c84a7306918 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d3b81552fde48d5e0925760d45121b191a9cee0934cfe871e6e12c84a7306918->enter($__internal_d3b81552fde48d5e0925760d45121b191a9cee0934cfe871e6e12c84a7306918_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_f473958224f60f424586090c156a0a04c9187c353fb7e9597eb53d8a10263611 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f473958224f60f424586090c156a0a04c9187c353fb7e9597eb53d8a10263611->enter($__internal_f473958224f60f424586090c156a0a04c9187c353fb7e9597eb53d8a10263611_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        echo "<!--le block du milieu de la page-->
    ";
        // line 3
        $this->displayBlock('debutPage', $context, $blocks);
        // line 11
        echo "    ";
        $this->displayBlock('modules', $context, $blocks);
        
        $__internal_f473958224f60f424586090c156a0a04c9187c353fb7e9597eb53d8a10263611->leave($__internal_f473958224f60f424586090c156a0a04c9187c353fb7e9597eb53d8a10263611_prof);

        
        $__internal_d3b81552fde48d5e0925760d45121b191a9cee0934cfe871e6e12c84a7306918->leave($__internal_d3b81552fde48d5e0925760d45121b191a9cee0934cfe871e6e12c84a7306918_prof);

    }

    // line 3
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_540413fd7c438815ef70c25eb62b0e2f3cff3332b3145ce79c35e5cda4271cc7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_540413fd7c438815ef70c25eb62b0e2f3cff3332b3145ce79c35e5cda4271cc7->enter($__internal_540413fd7c438815ef70c25eb62b0e2f3cff3332b3145ce79c35e5cda4271cc7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_be0182386c06ee259683880239fa0d7d29a16e4af197d806adbc2796ed486b60 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_be0182386c06ee259683880239fa0d7d29a16e4af197d806adbc2796ed486b60->enter($__internal_be0182386c06ee259683880239fa0d7d29a16e4af197d806adbc2796ed486b60_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 4
        echo "        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-45px;\">
                <h3 style=\"color: white;margin-top:4%\"> <center>Details du contrat ";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute(($context["contrat"] ?? $this->getContext($context, "contrat")), "libelle", array()), "html", null, true);
        echo "</center></h3>
            </div>
            <br><br>
        </div>
    ";
        
        $__internal_be0182386c06ee259683880239fa0d7d29a16e4af197d806adbc2796ed486b60->leave($__internal_be0182386c06ee259683880239fa0d7d29a16e4af197d806adbc2796ed486b60_prof);

        
        $__internal_540413fd7c438815ef70c25eb62b0e2f3cff3332b3145ce79c35e5cda4271cc7->leave($__internal_540413fd7c438815ef70c25eb62b0e2f3cff3332b3145ce79c35e5cda4271cc7_prof);

    }

    // line 11
    public function block_modules($context, array $blocks = array())
    {
        $__internal_217382d2c687b1df0400419c9fa79fa454df40553e51bd354f3b5a3cad1a49f5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_217382d2c687b1df0400419c9fa79fa454df40553e51bd354f3b5a3cad1a49f5->enter($__internal_217382d2c687b1df0400419c9fa79fa454df40553e51bd354f3b5a3cad1a49f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_77781be79f30f2e7fd36099a510f53ea3fe80f9e7ab6f78edeb9a5e3f1a64ff1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_77781be79f30f2e7fd36099a510f53ea3fe80f9e7ab6f78edeb9a5e3f1a64ff1->enter($__internal_77781be79f30f2e7fd36099a510f53ea3fe80f9e7ab6f78edeb9a5e3f1a64ff1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        echo "<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"margin-left: -12%\">
                <h2>Contrat numero ";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["contrat"] ?? $this->getContext($context, "contrat")), "id", array()), "html", null, true);
        echo "</h2>
                <h3>Libelle du contrat ";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute(($context["contrat"] ?? $this->getContext($context, "contrat")), "libelle", array()), "html", null, true);
        echo "</h3>
                <p>Entre la societe de trading TradingInternational, représentée par monsieur Niang,
                    gérant et monsieur ";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["contrat"] ?? $this->getContext($context, "contrat")), "transporteur", array()), "nom", array()), "html", null, true);
        echo ", titulaire du certificat de
                    capacité professionnelle au transport international, a été convenu ce qui
                    suit :
                </p>
                <p>Monsieur ";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["contrat"] ?? $this->getContext($context, "contrat")), "transporteur", array()), "nom", array()), "html", null, true);
        echo ", exercera, en qualité de gestionnaire de transport, de
                    manière effective et permanente pour le compte de la societe TradingInternational, les activités
                    suivantes :<br>
                    1) gérer l'entretien des marchandises ;<br>
                    2) vérifier les contrats et les documents de transport ;<br>
                    3) effectuer la comptabilité de base ;<br>
                    4) vérifier les procédures en matière de sécurité.
                </p>
                <p>Ce contrat concerne le transport de ";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute(($context["contrat"] ?? $this->getContext($context, "contrat")), "quantite", array()), "html", null, true);
        echo " de marchandise.<br>
                    La date de depart est ";
        // line 30
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["contrat"] ?? $this->getContext($context, "contrat")), "getDateDepart", array(), "method"), "d-m-Y"), "html", null, true);
        echo ".<br>
                    La date d'arrivee a destination est ";
        // line 31
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["contrat"] ?? $this->getContext($context, "contrat")), "getDateArrivee", array(), "method"), "d-m-Y"), "html", null, true);
        echo ".<br>
                    La note de debit fixee estimee par le transporteur est ";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute(($context["contrat"] ?? $this->getContext($context, "contrat")), "noteDebit", array()), "html", null, true);
        echo "
                </p>
            </div><!--.container-fluid-->
        </div><!--.page-content-->
    ";
        
        $__internal_77781be79f30f2e7fd36099a510f53ea3fe80f9e7ab6f78edeb9a5e3f1a64ff1->leave($__internal_77781be79f30f2e7fd36099a510f53ea3fe80f9e7ab6f78edeb9a5e3f1a64ff1_prof);

        
        $__internal_217382d2c687b1df0400419c9fa79fa454df40553e51bd354f3b5a3cad1a49f5->leave($__internal_217382d2c687b1df0400419c9fa79fa454df40553e51bd354f3b5a3cad1a49f5_prof);

    }

    // line 38
    public function block_script($context, array $blocks = array())
    {
        $__internal_8b9a4f86fc5ffd7d98e519e4a0c53a291caad340f0b4320503705e35ea1ab1f7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8b9a4f86fc5ffd7d98e519e4a0c53a291caad340f0b4320503705e35ea1ab1f7->enter($__internal_8b9a4f86fc5ffd7d98e519e4a0c53a291caad340f0b4320503705e35ea1ab1f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        $__internal_c4dcf7beb23b904c1d613915cfa5a94c424cfda66542e266b5dbfbe0e8455486 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c4dcf7beb23b904c1d613915cfa5a94c424cfda66542e266b5dbfbe0e8455486->enter($__internal_c4dcf7beb23b904c1d613915cfa5a94c424cfda66542e266b5dbfbe0e8455486_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        // line 39
        echo "    ";
        $this->displayParentBlock("script", $context, $blocks);
        echo "
    <script src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/jquery.tablednd.js"), "html", null, true);
        echo "\"></script>

";
        
        $__internal_c4dcf7beb23b904c1d613915cfa5a94c424cfda66542e266b5dbfbe0e8455486->leave($__internal_c4dcf7beb23b904c1d613915cfa5a94c424cfda66542e266b5dbfbe0e8455486_prof);

        
        $__internal_8b9a4f86fc5ffd7d98e519e4a0c53a291caad340f0b4320503705e35ea1ab1f7->leave($__internal_8b9a4f86fc5ffd7d98e519e4a0c53a291caad340f0b4320503705e35ea1ab1f7_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:ContratMarchandise:visualiserContrat.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  184 => 42,  180 => 41,  176 => 40,  171 => 39,  162 => 38,  147 => 32,  143 => 31,  139 => 30,  135 => 29,  124 => 21,  117 => 17,  112 => 15,  108 => 14,  95 => 11,  80 => 6,  76 => 4,  67 => 3,  56 => 11,  54 => 3,  43 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}
{% block contenuDuMilieu %}<!--le block du milieu de la page-->
    {% block debutPage %}
        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-45px;\">
                <h3 style=\"color: white;margin-top:4%\"> <center>Details du contrat {{ contrat.libelle }}</center></h3>
            </div>
            <br><br>
        </div>
    {% endblock %}
    {% block modules %}<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\">
            <div class=\"container-fluid\"  style=\"margin-left: -12%\">
                <h2>Contrat numero {{ contrat.id }}</h2>
                <h3>Libelle du contrat {{ contrat.libelle }}</h3>
                <p>Entre la societe de trading TradingInternational, représentée par monsieur Niang,
                    gérant et monsieur {{ contrat.transporteur.nom }}, titulaire du certificat de
                    capacité professionnelle au transport international, a été convenu ce qui
                    suit :
                </p>
                <p>Monsieur {{ contrat.transporteur.nom }}, exercera, en qualité de gestionnaire de transport, de
                    manière effective et permanente pour le compte de la societe TradingInternational, les activités
                    suivantes :<br>
                    1) gérer l'entretien des marchandises ;<br>
                    2) vérifier les contrats et les documents de transport ;<br>
                    3) effectuer la comptabilité de base ;<br>
                    4) vérifier les procédures en matière de sécurité.
                </p>
                <p>Ce contrat concerne le transport de {{ contrat.quantite }} de marchandise.<br>
                    La date de depart est {{ contrat.getDateDepart()|date('d-m-Y') }}.<br>
                    La date d'arrivee a destination est {{ contrat.getDateArrivee()|date('d-m-Y') }}.<br>
                    La note de debit fixee estimee par le transporteur est {{ contrat.noteDebit }}
                </p>
            </div><!--.container-fluid-->
        </div><!--.page-content-->
    {% endblock %}
{% endblock %}
{% block script %}
    {{ parent() }}
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/jquery.tablednd.js') }}\"></script>

{% endblock %}", "StockBundle:ContratMarchandise:visualiserContrat.html.twig", "C:\\wamp64\\www\\stock\\src\\StockBundle/Resources/views/ContratMarchandise/visualiserContrat.html.twig");
    }
}
