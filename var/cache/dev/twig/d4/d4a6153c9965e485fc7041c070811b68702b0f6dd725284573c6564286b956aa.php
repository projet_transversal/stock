<?php

/* ::transporteur.html.twig */
class __TwigTemplate_232c8be6f75c73c0148c65d99733f1f25bd7123a34061824fb79a4acc97b8110 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "::transporteur.html.twig", 1);
        $this->blocks = array(
            'menu' => array($this, 'block_menu'),
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_66c4c99f6f34db3f1f324a9bee581e09cf8ef4d39520e66da4b52542681c2cf7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_66c4c99f6f34db3f1f324a9bee581e09cf8ef4d39520e66da4b52542681c2cf7->enter($__internal_66c4c99f6f34db3f1f324a9bee581e09cf8ef4d39520e66da4b52542681c2cf7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::transporteur.html.twig"));

        $__internal_9a0eff4b620dcc7336edb1c7fdc96e8d3afd79150fb5f6f276ef5ae021327564 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9a0eff4b620dcc7336edb1c7fdc96e8d3afd79150fb5f6f276ef5ae021327564->enter($__internal_9a0eff4b620dcc7336edb1c7fdc96e8d3afd79150fb5f6f276ef5ae021327564_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::transporteur.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_66c4c99f6f34db3f1f324a9bee581e09cf8ef4d39520e66da4b52542681c2cf7->leave($__internal_66c4c99f6f34db3f1f324a9bee581e09cf8ef4d39520e66da4b52542681c2cf7_prof);

        
        $__internal_9a0eff4b620dcc7336edb1c7fdc96e8d3afd79150fb5f6f276ef5ae021327564->leave($__internal_9a0eff4b620dcc7336edb1c7fdc96e8d3afd79150fb5f6f276ef5ae021327564_prof);

    }

    // line 3
    public function block_menu($context, array $blocks = array())
    {
        $__internal_161626c9cd201f897c5951accb1e2cbe0da56695a9f1a9c981478f84ebb2e8dc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_161626c9cd201f897c5951accb1e2cbe0da56695a9f1a9c981478f84ebb2e8dc->enter($__internal_161626c9cd201f897c5951accb1e2cbe0da56695a9f1a9c981478f84ebb2e8dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_d5357d15c26145a4123dc0ab55448f899d756aedbe4034fad1d55b3c99292c23 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d5357d15c26145a4123dc0ab55448f899d756aedbe4034fad1d55b3c99292c23->enter($__internal_d5357d15c26145a4123dc0ab55448f899d756aedbe4034fad1d55b3c99292c23_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 4
        echo "        <nav class=\"side-menu side-menu-big-icon\" style=\"background: #3299CC;overflow: hidden\">
            <nav class=\"sidebar-header\">
                <img src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/man.png"), "html", null, true);
        echo "\" style=\"margin-left:35%;margin-top:-15%;\"><br><br>
                <h5 style=\"text-align:center\">User Name</h5>
                <h5 style=\"text-align:center\">Profil</h5>
            </nav>
            <hr style=\"margin-left:20%;margin-right: 20%;background: red;height:1px\">
            <ul class=\"side-menu-list\">
                <li class=\"opened\">
                    <span id=\"instr\">
                    \t<a href=\"";
        // line 14
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("transporteur_ajouter_proformat");
        echo "\" >
\t                        <i class=\"glyphicon glyphicon-inbox\"  style=\"color: white\"></i>
                            <span class=\"lbl\" style=\"color: white\"><h6>Ajouter Proformat</h6></span>
                        </a>
                    </span>
                </li>
                <li class=\"opened\">
                    <span id=\"prod\">
                    \t<a href=\"";
        // line 22
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("transporteur_listerContrat");
        echo "\" >
\t                        <i class=\"glyphicon glyphicon-th-list\"  style=\"color: white\"></i>
                            <span class=\"lbl\" style=\"color: white\"><h6>Lister Contrat</h6></span>
                        </a>
                    </span>
                </li>
            </ul>
        </nav>
    ";
        
        $__internal_d5357d15c26145a4123dc0ab55448f899d756aedbe4034fad1d55b3c99292c23->leave($__internal_d5357d15c26145a4123dc0ab55448f899d756aedbe4034fad1d55b3c99292c23_prof);

        
        $__internal_161626c9cd201f897c5951accb1e2cbe0da56695a9f1a9c981478f84ebb2e8dc->leave($__internal_161626c9cd201f897c5951accb1e2cbe0da56695a9f1a9c981478f84ebb2e8dc_prof);

    }

    // line 31
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_eb16731a22a2f1fa598150558891ecc231f068ba54b1bbbfb5363e336e2f69af = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eb16731a22a2f1fa598150558891ecc231f068ba54b1bbbfb5363e336e2f69af->enter($__internal_eb16731a22a2f1fa598150558891ecc231f068ba54b1bbbfb5363e336e2f69af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_455d464721b009d6414152e6901bbc68065789b397526cfecb0062a2d3add85b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_455d464721b009d6414152e6901bbc68065789b397526cfecb0062a2d3add85b->enter($__internal_455d464721b009d6414152e6901bbc68065789b397526cfecb0062a2d3add85b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        // line 32
        echo "        ";
        $this->displayBlock('debutPage', $context, $blocks);
        // line 41
        echo "        ";
        $this->displayBlock('modules', $context, $blocks);
        // line 46
        echo "    ";
        
        $__internal_455d464721b009d6414152e6901bbc68065789b397526cfecb0062a2d3add85b->leave($__internal_455d464721b009d6414152e6901bbc68065789b397526cfecb0062a2d3add85b_prof);

        
        $__internal_eb16731a22a2f1fa598150558891ecc231f068ba54b1bbbfb5363e336e2f69af->leave($__internal_eb16731a22a2f1fa598150558891ecc231f068ba54b1bbbfb5363e336e2f69af_prof);

    }

    // line 32
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_d60ed286af5539789b8fe13487ccd210141f595a5b3964971b219da53244b55f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d60ed286af5539789b8fe13487ccd210141f595a5b3964971b219da53244b55f->enter($__internal_d60ed286af5539789b8fe13487ccd210141f595a5b3964971b219da53244b55f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_aa721fcccd576e8fd673207e09af98f29a8b94e6372d8fe55e30af1faeaf302e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aa721fcccd576e8fd673207e09af98f29a8b94e6372d8fe55e30af1faeaf302e->enter($__internal_aa721fcccd576e8fd673207e09af98f29a8b94e6372d8fe55e30af1faeaf302e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 33
        echo "            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-45px;\">
                    <h4 style=\"color: white;margin-top:4%\"> <center>Liste des contrats</center></h4>
                </div>
                <br><br><br><br>
            </div>

        ";
        
        $__internal_aa721fcccd576e8fd673207e09af98f29a8b94e6372d8fe55e30af1faeaf302e->leave($__internal_aa721fcccd576e8fd673207e09af98f29a8b94e6372d8fe55e30af1faeaf302e_prof);

        
        $__internal_d60ed286af5539789b8fe13487ccd210141f595a5b3964971b219da53244b55f->leave($__internal_d60ed286af5539789b8fe13487ccd210141f595a5b3964971b219da53244b55f_prof);

    }

    // line 41
    public function block_modules($context, array $blocks = array())
    {
        $__internal_95c95fc18dd769a22bf54dbe30ea9fb67c6af4c59e8f451c8e2d91568d3f3068 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95c95fc18dd769a22bf54dbe30ea9fb67c6af4c59e8f451c8e2d91568d3f3068->enter($__internal_95c95fc18dd769a22bf54dbe30ea9fb67c6af4c59e8f451c8e2d91568d3f3068_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_707960fd9184bf74d96584bcb7fe0efe573f428dea75a43f05f195a840a87673 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_707960fd9184bf74d96584bcb7fe0efe573f428dea75a43f05f195a840a87673->enter($__internal_707960fd9184bf74d96584bcb7fe0efe573f428dea75a43f05f195a840a87673_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        // line 42
        echo "            <div class=\"col-xl-6\" style=\"margin-left: 27%\">
                ";
        // line 43
        $this->loadTemplate("formulaire.html.twig", "::transporteur.html.twig", 43)->display($context);
        // line 44
        echo "            </div>
        ";
        
        $__internal_707960fd9184bf74d96584bcb7fe0efe573f428dea75a43f05f195a840a87673->leave($__internal_707960fd9184bf74d96584bcb7fe0efe573f428dea75a43f05f195a840a87673_prof);

        
        $__internal_95c95fc18dd769a22bf54dbe30ea9fb67c6af4c59e8f451c8e2d91568d3f3068->leave($__internal_95c95fc18dd769a22bf54dbe30ea9fb67c6af4c59e8f451c8e2d91568d3f3068_prof);

    }

    public function getTemplateName()
    {
        return "::transporteur.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  162 => 44,  160 => 43,  157 => 42,  148 => 41,  131 => 33,  122 => 32,  112 => 46,  109 => 41,  106 => 32,  97 => 31,  78 => 22,  67 => 14,  56 => 6,  52 => 4,  43 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

\t{% block menu %}
        <nav class=\"side-menu side-menu-big-icon\" style=\"background: #3299CC;overflow: hidden\">
            <nav class=\"sidebar-header\">
                <img src=\"{{asset('img/man.png')}}\" style=\"margin-left:35%;margin-top:-15%;\"><br><br>
                <h5 style=\"text-align:center\">User Name</h5>
                <h5 style=\"text-align:center\">Profil</h5>
            </nav>
            <hr style=\"margin-left:20%;margin-right: 20%;background: red;height:1px\">
            <ul class=\"side-menu-list\">
                <li class=\"opened\">
                    <span id=\"instr\">
                    \t<a href=\"{{ path('transporteur_ajouter_proformat') }}\" >
\t                        <i class=\"glyphicon glyphicon-inbox\"  style=\"color: white\"></i>
                            <span class=\"lbl\" style=\"color: white\"><h6>Ajouter Proformat</h6></span>
                        </a>
                    </span>
                </li>
                <li class=\"opened\">
                    <span id=\"prod\">
                    \t<a href=\"{{ path('transporteur_listerContrat') }}\" >
\t                        <i class=\"glyphicon glyphicon-th-list\"  style=\"color: white\"></i>
                            <span class=\"lbl\" style=\"color: white\"><h6>Lister Contrat</h6></span>
                        </a>
                    </span>
                </li>
            </ul>
        </nav>
    {% endblock %}
    {% block contenuDuMilieu %}
        {% block debutPage %}
            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-45px;\">
                    <h4 style=\"color: white;margin-top:4%\"> <center>Liste des contrats</center></h4>
                </div>
                <br><br><br><br>
            </div>

        {% endblock %}
        {% block modules %}
            <div class=\"col-xl-6\" style=\"margin-left: 27%\">
                {% include 'formulaire.html.twig' %}
            </div>
        {% endblock %}
    {% endblock %}", "::transporteur.html.twig", "C:\\wamp64\\www\\stock\\app/Resources\\views/transporteur.html.twig");
    }
}
