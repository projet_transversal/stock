<?php

/* StockBundle:ContratMarchandise:addContratMarchandise.html.twig */
class __TwigTemplate_21d921402f6eb960112c4e332796ae72cbbb05998dce5e65b5c9b4585b822c87 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:ContratMarchandise:addContratMarchandise.html.twig", 1);
        $this->blocks = array(
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d97ba51067ac17d2312f6eb19cb6ec883d4c82eae26a1dbee36dee6ca7fdf204 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d97ba51067ac17d2312f6eb19cb6ec883d4c82eae26a1dbee36dee6ca7fdf204->enter($__internal_d97ba51067ac17d2312f6eb19cb6ec883d4c82eae26a1dbee36dee6ca7fdf204_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:ContratMarchandise:addContratMarchandise.html.twig"));

        $__internal_fc55676da65ca2e8ff2ae8761d6833679fd9b526fb7697516f8bc3365e28ebf1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fc55676da65ca2e8ff2ae8761d6833679fd9b526fb7697516f8bc3365e28ebf1->enter($__internal_fc55676da65ca2e8ff2ae8761d6833679fd9b526fb7697516f8bc3365e28ebf1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:ContratMarchandise:addContratMarchandise.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d97ba51067ac17d2312f6eb19cb6ec883d4c82eae26a1dbee36dee6ca7fdf204->leave($__internal_d97ba51067ac17d2312f6eb19cb6ec883d4c82eae26a1dbee36dee6ca7fdf204_prof);

        
        $__internal_fc55676da65ca2e8ff2ae8761d6833679fd9b526fb7697516f8bc3365e28ebf1->leave($__internal_fc55676da65ca2e8ff2ae8761d6833679fd9b526fb7697516f8bc3365e28ebf1_prof);

    }

    // line 3
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_bc67eca19a752b6e54cb8bf22dd078aac9d5a2f2eddbf96fc4b38d40e66676d2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bc67eca19a752b6e54cb8bf22dd078aac9d5a2f2eddbf96fc4b38d40e66676d2->enter($__internal_bc67eca19a752b6e54cb8bf22dd078aac9d5a2f2eddbf96fc4b38d40e66676d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_1f36ede62c8bafa52c2096e44883304db59cf3486b66c27a5145b35018965970 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f36ede62c8bafa52c2096e44883304db59cf3486b66c27a5145b35018965970->enter($__internal_1f36ede62c8bafa52c2096e44883304db59cf3486b66c27a5145b35018965970_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        // line 4
        echo "        ";
        $this->displayBlock('debutPage', $context, $blocks);
        // line 13
        echo "        ";
        $this->displayBlock('modules', $context, $blocks);
        // line 29
        echo "    ";
        
        $__internal_1f36ede62c8bafa52c2096e44883304db59cf3486b66c27a5145b35018965970->leave($__internal_1f36ede62c8bafa52c2096e44883304db59cf3486b66c27a5145b35018965970_prof);

        
        $__internal_bc67eca19a752b6e54cb8bf22dd078aac9d5a2f2eddbf96fc4b38d40e66676d2->leave($__internal_bc67eca19a752b6e54cb8bf22dd078aac9d5a2f2eddbf96fc4b38d40e66676d2_prof);

    }

    // line 4
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_71540af2104526247551adce83d19a6c270e2a4ac828cf4513067203771d933e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_71540af2104526247551adce83d19a6c270e2a4ac828cf4513067203771d933e->enter($__internal_71540af2104526247551adce83d19a6c270e2a4ac828cf4513067203771d933e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_6d41acd9d48768b7be5dd5fa0857e2e066ac7b136f58fc5a08bb859bf4176b74 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d41acd9d48768b7be5dd5fa0857e2e066ac7b136f58fc5a08bb859bf4176b74->enter($__internal_6d41acd9d48768b7be5dd5fa0857e2e066ac7b136f58fc5a08bb859bf4176b74_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 5
        echo "            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-20px;\">
                    <h2 style=\"color: white;margin-top:4%\"> <center>Ajouter Contrat</center></h2>
                </div>
                <br><br><br><br><br><br>
            </div>

        ";
        
        $__internal_6d41acd9d48768b7be5dd5fa0857e2e066ac7b136f58fc5a08bb859bf4176b74->leave($__internal_6d41acd9d48768b7be5dd5fa0857e2e066ac7b136f58fc5a08bb859bf4176b74_prof);

        
        $__internal_71540af2104526247551adce83d19a6c270e2a4ac828cf4513067203771d933e->leave($__internal_71540af2104526247551adce83d19a6c270e2a4ac828cf4513067203771d933e_prof);

    }

    // line 13
    public function block_modules($context, array $blocks = array())
    {
        $__internal_fa9e243a82a78903a6f203d2f22c01db4b6aba3c3c59f87805389b2a919b2886 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fa9e243a82a78903a6f203d2f22c01db4b6aba3c3c59f87805389b2a919b2886->enter($__internal_fa9e243a82a78903a6f203d2f22c01db4b6aba3c3c59f87805389b2a919b2886_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_eb859e1c17234aa97539805a424746326c83cebab4e8e7a32c6f28fcc80534d1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eb859e1c17234aa97539805a424746326c83cebab4e8e7a32c6f28fcc80534d1->enter($__internal_eb859e1c17234aa97539805a424746326c83cebab4e8e7a32c6f28fcc80534d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        // line 14
        echo "            <div class=\"col-xl-6\" style=\"margin-left: 27%\">
                <div class=\"form-group\"></div>
                ";
        // line 16
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
                <div class=\"form-group\" style=\"\">
                    ";
        // line 18
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "libelle", array()), 'label', array("label" => "Libelle"));
        echo "
                    ";
        // line 19
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "libelle", array()), 'widget');
        echo "
                </div>
                ";
        // line 21
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
                <div class=\"form-group\" align=\"center\">
                    <input type=\"reset\" name=\"resetContratMarchandise\" class=\"btn btn-danger\" value=\"Annuler\"/>
                    <input type=\"submit\" name=\"addContratMarchandise\" class=\"btn btn-success\" value=\"Ajouter\"/>
                </div>
                ";
        // line 26
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
            </div>
        ";
        
        $__internal_eb859e1c17234aa97539805a424746326c83cebab4e8e7a32c6f28fcc80534d1->leave($__internal_eb859e1c17234aa97539805a424746326c83cebab4e8e7a32c6f28fcc80534d1_prof);

        
        $__internal_fa9e243a82a78903a6f203d2f22c01db4b6aba3c3c59f87805389b2a919b2886->leave($__internal_fa9e243a82a78903a6f203d2f22c01db4b6aba3c3c59f87805389b2a919b2886_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:ContratMarchandise:addContratMarchandise.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 26,  120 => 21,  115 => 19,  111 => 18,  106 => 16,  102 => 14,  93 => 13,  76 => 5,  67 => 4,  57 => 29,  54 => 13,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}

    {% block contenuDuMilieu %}
        {% block debutPage %}
            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-20px;\">
                    <h2 style=\"color: white;margin-top:4%\"> <center>Ajouter Contrat</center></h2>
                </div>
                <br><br><br><br><br><br>
            </div>

        {% endblock %}
        {% block modules %}
            <div class=\"col-xl-6\" style=\"margin-left: 27%\">
                <div class=\"form-group\"></div>
                {{ form_start(form) }}
                <div class=\"form-group\" style=\"\">
                    {{ form_label(form.libelle, \"Libelle\") }}
                    {{ form_widget(form.libelle) }}
                </div>
                {{ form_widget(form) }}
                <div class=\"form-group\" align=\"center\">
                    <input type=\"reset\" name=\"resetContratMarchandise\" class=\"btn btn-danger\" value=\"Annuler\"/>
                    <input type=\"submit\" name=\"addContratMarchandise\" class=\"btn btn-success\" value=\"Ajouter\"/>
                </div>
                {{ form_end(form) }}
            </div>
        {% endblock %}
    {% endblock %}", "StockBundle:ContratMarchandise:addContratMarchandise.html.twig", "C:\\wamp\\www\\symfony\\stock\\src\\StockBundle/Resources/views/ContratMarchandise/addContratMarchandise.html.twig");
    }
}
