<?php

/* StockBundle:Transporteur:listerTransporteur.html.twig */
class __TwigTemplate_45f37cc0b2f2d7ba2b6bd69a3b358161fa7f1e6e74af282cfb62e0e81341245a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:Transporteur:listerTransporteur.html.twig", 2);
        $this->blocks = array(
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c1c82f3581c95e8899c9e241db943606704b20dd2b554013fa89f31c6e7c19e7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c1c82f3581c95e8899c9e241db943606704b20dd2b554013fa89f31c6e7c19e7->enter($__internal_c1c82f3581c95e8899c9e241db943606704b20dd2b554013fa89f31c6e7c19e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Transporteur:listerTransporteur.html.twig"));

        $__internal_fee3ffb61ded73d656308a047232e5b34c61791def5bf30973d213a7a7353d14 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fee3ffb61ded73d656308a047232e5b34c61791def5bf30973d213a7a7353d14->enter($__internal_fee3ffb61ded73d656308a047232e5b34c61791def5bf30973d213a7a7353d14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Transporteur:listerTransporteur.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c1c82f3581c95e8899c9e241db943606704b20dd2b554013fa89f31c6e7c19e7->leave($__internal_c1c82f3581c95e8899c9e241db943606704b20dd2b554013fa89f31c6e7c19e7_prof);

        
        $__internal_fee3ffb61ded73d656308a047232e5b34c61791def5bf30973d213a7a7353d14->leave($__internal_fee3ffb61ded73d656308a047232e5b34c61791def5bf30973d213a7a7353d14_prof);

    }

    // line 3
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_32747f5c9465225f3909bae9c932babe6cb5a9566ca735def9a2c907729e271e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_32747f5c9465225f3909bae9c932babe6cb5a9566ca735def9a2c907729e271e->enter($__internal_32747f5c9465225f3909bae9c932babe6cb5a9566ca735def9a2c907729e271e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_cf3863c1fd0047ad47de5536233c28a34130aee52913c3e418a2e0fda3c2e0d9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cf3863c1fd0047ad47de5536233c28a34130aee52913c3e418a2e0fda3c2e0d9->enter($__internal_cf3863c1fd0047ad47de5536233c28a34130aee52913c3e418a2e0fda3c2e0d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        echo "<!--le block du milieu de la page-->
    ";
        // line 4
        $this->displayBlock('debutPage', $context, $blocks);
        // line 12
        echo "    ";
        $this->displayBlock('modules', $context, $blocks);
        
        $__internal_cf3863c1fd0047ad47de5536233c28a34130aee52913c3e418a2e0fda3c2e0d9->leave($__internal_cf3863c1fd0047ad47de5536233c28a34130aee52913c3e418a2e0fda3c2e0d9_prof);

        
        $__internal_32747f5c9465225f3909bae9c932babe6cb5a9566ca735def9a2c907729e271e->leave($__internal_32747f5c9465225f3909bae9c932babe6cb5a9566ca735def9a2c907729e271e_prof);

    }

    // line 4
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_93221a7f2979adaae944af440cbd1833ff787bc798ad64fd3375fa4645d2f459 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_93221a7f2979adaae944af440cbd1833ff787bc798ad64fd3375fa4645d2f459->enter($__internal_93221a7f2979adaae944af440cbd1833ff787bc798ad64fd3375fa4645d2f459_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_432b9b5983a5e9ee97ecc7079f2188f500305dfde4c03757ca62877cfe1a5dba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_432b9b5983a5e9ee97ecc7079f2188f500305dfde4c03757ca62877cfe1a5dba->enter($__internal_432b9b5983a5e9ee97ecc7079f2188f500305dfde4c03757ca62877cfe1a5dba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 5
        echo "        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                <h3 style=\"color: white;margin-top:4%\"> <center>Liste des Transporteurs</center></h3>
            </div>

        </div>
    ";
        
        $__internal_432b9b5983a5e9ee97ecc7079f2188f500305dfde4c03757ca62877cfe1a5dba->leave($__internal_432b9b5983a5e9ee97ecc7079f2188f500305dfde4c03757ca62877cfe1a5dba_prof);

        
        $__internal_93221a7f2979adaae944af440cbd1833ff787bc798ad64fd3375fa4645d2f459->leave($__internal_93221a7f2979adaae944af440cbd1833ff787bc798ad64fd3375fa4645d2f459_prof);

    }

    // line 12
    public function block_modules($context, array $blocks = array())
    {
        $__internal_e82a787c326b14b8b554f6b5d28b491dd8da8298fd8f9d18c2c44527c2b940bc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e82a787c326b14b8b554f6b5d28b491dd8da8298fd8f9d18c2c44527c2b940bc->enter($__internal_e82a787c326b14b8b554f6b5d28b491dd8da8298fd8f9d18c2c44527c2b940bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_db3420c05ce40ec4e66689cdcc98bb1172d9e1a92a0dc430e1735e241e73df23 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_db3420c05ce40ec4e66689cdcc98bb1172d9e1a92a0dc430e1735e241e73df23->enter($__internal_db3420c05ce40ec4e66689cdcc98bb1172d9e1a92a0dc430e1735e241e73df23_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        echo "<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\" style=\"margin-top: -50px\">
            <div class=\"container-fluid\"  style=\"margin-left: -12%\">

                    <div class=\"\">
                        <table id=\"table\"  class=\"display table table-striped table-bordered\" cellspacing=\"0\" width=\"100%\" style=\"margin-left:-11%;margin-top:0%\" >

                            <thead>
                            <tr class=\"jumbotron\">
                                <th data-field=\"id\">ID</th>
                                <th data-field=\"name\" >Nom</th>
                                <th data-field=\"telephone\" >Telephone</th>
                                <th data-field=\"adresse\" >Adresse</th>
                                <th data-field=\"mail\" >Courriel</th>
                                <th data-field=\"Modifier\" >Modifier</th>
                                <th data-field=\"Supprimer\" >Supprimer</th>
                            </tr>
                            </thead>
                            <tbody>
                            ";
        // line 31
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["liste"] ?? $this->getContext($context, "liste")));
        foreach ($context['_seq'] as $context["_key"] => $context["l"]) {
            // line 32
            echo "                                <tr>
                                    <td id=\"id\">";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "id", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "login", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "telephone", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "adresse", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "email", array()), "html", null, true);
            echo "</td>
                                    <td><i class=\"font-icon font-icon-pen\" style=\"color: green\"></i></td>
                                    <td><i class=\"font-icon font-icon-del\" style=\"color: red\"></i></td>
                                </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['l'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "                            </tbody>
                        </table>
                    </div>
                </section><!--.box-typical-->

            </div><!--.container-fluid-->
        </div><!--.page-content-->
    ";
        
        $__internal_db3420c05ce40ec4e66689cdcc98bb1172d9e1a92a0dc430e1735e241e73df23->leave($__internal_db3420c05ce40ec4e66689cdcc98bb1172d9e1a92a0dc430e1735e241e73df23_prof);

        
        $__internal_e82a787c326b14b8b554f6b5d28b491dd8da8298fd8f9d18c2c44527c2b940bc->leave($__internal_e82a787c326b14b8b554f6b5d28b491dd8da8298fd8f9d18c2c44527c2b940bc_prof);

    }

    // line 51
    public function block_script($context, array $blocks = array())
    {
        $__internal_5f05761312123427615904d2684a91841dba510584dd8efa036c297e437bd591 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5f05761312123427615904d2684a91841dba510584dd8efa036c297e437bd591->enter($__internal_5f05761312123427615904d2684a91841dba510584dd8efa036c297e437bd591_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        $__internal_b464ec2caf10e6e6ed7293e2bd2191cfd4eb983719a5caf7b4710ad751827344 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b464ec2caf10e6e6ed7293e2bd2191cfd4eb983719a5caf7b4710ad751827344->enter($__internal_b464ec2caf10e6e6ed7293e2bd2191cfd4eb983719a5caf7b4710ad751827344_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        // line 52
        echo "    ";
        $this->displayParentBlock("script", $context, $blocks);
        echo "
    <script src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/jquery.tablednd.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/lib/bootstrap-table/listerTransporteur.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_b464ec2caf10e6e6ed7293e2bd2191cfd4eb983719a5caf7b4710ad751827344->leave($__internal_b464ec2caf10e6e6ed7293e2bd2191cfd4eb983719a5caf7b4710ad751827344_prof);

        
        $__internal_5f05761312123427615904d2684a91841dba510584dd8efa036c297e437bd591->leave($__internal_5f05761312123427615904d2684a91841dba510584dd8efa036c297e437bd591_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Transporteur:listerTransporteur.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  198 => 56,  194 => 55,  190 => 54,  186 => 53,  181 => 52,  172 => 51,  155 => 42,  144 => 37,  140 => 36,  136 => 35,  132 => 34,  128 => 33,  125 => 32,  121 => 31,  92 => 12,  76 => 5,  67 => 4,  56 => 12,  54 => 4,  43 => 3,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("
{% extends '::base.html.twig' %}
{% block contenuDuMilieu %}<!--le block du milieu de la page-->
    {% block debutPage %}
        <div class=\"row\">
            <div class=\"col-lg-12\" style=\"width:106%;height: 100px;background: silver;margin-top:-48px;\">
                <h3 style=\"color: white;margin-top:4%\"> <center>Liste des Transporteurs</center></h3>
            </div>

        </div>
    {% endblock %}
    {% block modules %}<!--le block pour les differents modules de l'application-->
        <div class=\"page-content\" style=\"margin-top: -50px\">
            <div class=\"container-fluid\"  style=\"margin-left: -12%\">

                    <div class=\"\">
                        <table id=\"table\"  class=\"display table table-striped table-bordered\" cellspacing=\"0\" width=\"100%\" style=\"margin-left:-11%;margin-top:0%\" >

                            <thead>
                            <tr class=\"jumbotron\">
                                <th data-field=\"id\">ID</th>
                                <th data-field=\"name\" >Nom</th>
                                <th data-field=\"telephone\" >Telephone</th>
                                <th data-field=\"adresse\" >Adresse</th>
                                <th data-field=\"mail\" >Courriel</th>
                                <th data-field=\"Modifier\" >Modifier</th>
                                <th data-field=\"Supprimer\" >Supprimer</th>
                            </tr>
                            </thead>
                            <tbody>
                            {% for l in liste %}
                                <tr>
                                    <td id=\"id\">{{ l.id }}</td>
                                    <td>{{ l.login }}</td>
                                    <td>{{ l.telephone }}</td>
                                    <td>{{ l.adresse }}</td>
                                    <td>{{ l.email }}</td>
                                    <td><i class=\"font-icon font-icon-pen\" style=\"color: green\"></i></td>
                                    <td><i class=\"font-icon font-icon-del\" style=\"color: red\"></i></td>
                                </tr>
                            {% endfor %}
                            </tbody>
                        </table>
                    </div>
                </section><!--.box-typical-->

            </div><!--.container-fluid-->
        </div><!--.page-content-->
    {% endblock %}
{% endblock %}
{% block script %}
    {{ parent() }}
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/bootstrap-table-reorder-rows.min.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/jquery.tablednd.js') }}\"></script>
    <script src=\"{{ asset('js/lib/bootstrap-table/listerTransporteur.js') }}\"></script>
{% endblock %}", "StockBundle:Transporteur:listerTransporteur.html.twig", "C:\\wamp64\\www\\stock\\src\\StockBundle/Resources/views/Transporteur/listerTransporteur.html.twig");
    }
}
