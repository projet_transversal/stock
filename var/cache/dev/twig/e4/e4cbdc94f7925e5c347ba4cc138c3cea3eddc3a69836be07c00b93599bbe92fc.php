<?php

/* @WebProfiler/Icon/twig.svg */
class __TwigTemplate_c84b841572ba474061fa1585d69dd34fdc78177756ba7721042a98ceefbd8403 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0e9cdf34b2f21e8a5ac27be517305a6835ffdbd95e0460e062daa5d01466fb9f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0e9cdf34b2f21e8a5ac27be517305a6835ffdbd95e0460e062daa5d01466fb9f->enter($__internal_0e9cdf34b2f21e8a5ac27be517305a6835ffdbd95e0460e062daa5d01466fb9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/twig.svg"));

        $__internal_bea5193ac9b0416fe91f7e35f58c960385c82da8d048c65b7aeb688660814ac3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bea5193ac9b0416fe91f7e35f58c960385c82da8d048c65b7aeb688660814ac3->enter($__internal_bea5193ac9b0416fe91f7e35f58c960385c82da8d048c65b7aeb688660814ac3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/twig.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M20.1,1H3.9C2.3,1,1,2.3,1,3.9v16.3C1,21.7,2.3,23,3.9,23h16.3c1.6,0,2.9-1.3,2.9-2.9V3.9
    C23,2.3,21.7,1,20.1,1z M21,20.1c0,0.5-0.4,0.9-0.9,0.9H3.9C3.4,21,3,20.6,3,20.1V3.9C3,3.4,3.4,3,3.9,3h16.3C20.6,3,21,3.4,21,3.9
    V20.1z M5,5h14v3H5V5z M5,10h3v9H5V10z M10,10h9v9h-9V10z\"/>
</svg>
";
        
        $__internal_0e9cdf34b2f21e8a5ac27be517305a6835ffdbd95e0460e062daa5d01466fb9f->leave($__internal_0e9cdf34b2f21e8a5ac27be517305a6835ffdbd95e0460e062daa5d01466fb9f_prof);

        
        $__internal_bea5193ac9b0416fe91f7e35f58c960385c82da8d048c65b7aeb688660814ac3->leave($__internal_bea5193ac9b0416fe91f7e35f58c960385c82da8d048c65b7aeb688660814ac3_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/twig.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M20.1,1H3.9C2.3,1,1,2.3,1,3.9v16.3C1,21.7,2.3,23,3.9,23h16.3c1.6,0,2.9-1.3,2.9-2.9V3.9
    C23,2.3,21.7,1,20.1,1z M21,20.1c0,0.5-0.4,0.9-0.9,0.9H3.9C3.4,21,3,20.6,3,20.1V3.9C3,3.4,3.4,3,3.9,3h16.3C20.6,3,21,3.4,21,3.9
    V20.1z M5,5h14v3H5V5z M5,10h3v9H5V10z M10,10h9v9h-9V10z\"/>
</svg>
", "@WebProfiler/Icon/twig.svg", "C:\\wamp\\www\\symfony\\stock\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Icon\\twig.svg");
    }
}
