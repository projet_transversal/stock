<?php

/* StockBundle:Trans:ajouterProformat.html.twig */
class __TwigTemplate_1829431fdd54cdce7b396306b0ec465edd04d3cd4325eb9d4a8f175d5bde9200 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::transporteur.html.twig", "StockBundle:Trans:ajouterProformat.html.twig", 1);
        $this->blocks = array(
            'menu' => array($this, 'block_menu'),
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::transporteur.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_91e0a37bbf6d13869143b9bc94d04b102c6c2cb17e1e233a08946c90a5a6b916 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_91e0a37bbf6d13869143b9bc94d04b102c6c2cb17e1e233a08946c90a5a6b916->enter($__internal_91e0a37bbf6d13869143b9bc94d04b102c6c2cb17e1e233a08946c90a5a6b916_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Trans:ajouterProformat.html.twig"));

        $__internal_e779249d2f07aa484771aba94c8efaa2a9423059284410d9af99c9815d103e27 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e779249d2f07aa484771aba94c8efaa2a9423059284410d9af99c9815d103e27->enter($__internal_e779249d2f07aa484771aba94c8efaa2a9423059284410d9af99c9815d103e27_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Trans:ajouterProformat.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_91e0a37bbf6d13869143b9bc94d04b102c6c2cb17e1e233a08946c90a5a6b916->leave($__internal_91e0a37bbf6d13869143b9bc94d04b102c6c2cb17e1e233a08946c90a5a6b916_prof);

        
        $__internal_e779249d2f07aa484771aba94c8efaa2a9423059284410d9af99c9815d103e27->leave($__internal_e779249d2f07aa484771aba94c8efaa2a9423059284410d9af99c9815d103e27_prof);

    }

    // line 2
    public function block_menu($context, array $blocks = array())
    {
        $__internal_0ad2005490993a99258b902cd8d5273cc3f56ed33efb50c07f3323a7d655f911 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0ad2005490993a99258b902cd8d5273cc3f56ed33efb50c07f3323a7d655f911->enter($__internal_0ad2005490993a99258b902cd8d5273cc3f56ed33efb50c07f3323a7d655f911_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_5924bc133d1dd49799622745b71883a3cd6455ce7ebfdcd5d2d7ed0b82865c48 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5924bc133d1dd49799622745b71883a3cd6455ce7ebfdcd5d2d7ed0b82865c48->enter($__internal_5924bc133d1dd49799622745b71883a3cd6455ce7ebfdcd5d2d7ed0b82865c48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 3
        echo "\t\t";
        $this->displayParentBlock("menu", $context, $blocks);
        echo "
    ";
        
        $__internal_5924bc133d1dd49799622745b71883a3cd6455ce7ebfdcd5d2d7ed0b82865c48->leave($__internal_5924bc133d1dd49799622745b71883a3cd6455ce7ebfdcd5d2d7ed0b82865c48_prof);

        
        $__internal_0ad2005490993a99258b902cd8d5273cc3f56ed33efb50c07f3323a7d655f911->leave($__internal_0ad2005490993a99258b902cd8d5273cc3f56ed33efb50c07f3323a7d655f911_prof);

    }

    // line 5
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_050fad7137ce05704c8d07b49f7dc421e3ac3581625bb63ff7bbc9bb50706e34 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_050fad7137ce05704c8d07b49f7dc421e3ac3581625bb63ff7bbc9bb50706e34->enter($__internal_050fad7137ce05704c8d07b49f7dc421e3ac3581625bb63ff7bbc9bb50706e34_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_5a2787039848d2188ea7fce1c9244ad6b8d91851fea0355506d4011b9d394418 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5a2787039848d2188ea7fce1c9244ad6b8d91851fea0355506d4011b9d394418->enter($__internal_5a2787039848d2188ea7fce1c9244ad6b8d91851fea0355506d4011b9d394418_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        // line 6
        echo "        ";
        $this->displayBlock('debutPage', $context, $blocks);
        // line 15
        echo "\t\t";
        $this->displayBlock('modules', $context, $blocks);
        // line 20
        echo "    ";
        
        $__internal_5a2787039848d2188ea7fce1c9244ad6b8d91851fea0355506d4011b9d394418->leave($__internal_5a2787039848d2188ea7fce1c9244ad6b8d91851fea0355506d4011b9d394418_prof);

        
        $__internal_050fad7137ce05704c8d07b49f7dc421e3ac3581625bb63ff7bbc9bb50706e34->leave($__internal_050fad7137ce05704c8d07b49f7dc421e3ac3581625bb63ff7bbc9bb50706e34_prof);

    }

    // line 6
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_eef442c6a42ac08df8052e65c7d437b0142b9c03a61d707da3fdd14d5809aa3f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eef442c6a42ac08df8052e65c7d437b0142b9c03a61d707da3fdd14d5809aa3f->enter($__internal_eef442c6a42ac08df8052e65c7d437b0142b9c03a61d707da3fdd14d5809aa3f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_3b592979843444fe90e8fd852f0fda619e853bab5114cedf12b6853b8bc05d45 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3b592979843444fe90e8fd852f0fda619e853bab5114cedf12b6853b8bc05d45->enter($__internal_3b592979843444fe90e8fd852f0fda619e853bab5114cedf12b6853b8bc05d45_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 7
        echo "            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-20px;\">
                    <h2 style=\"color: white;margin-top:4%\"> <center>Livrer marchandises</center></h2>
                </div>
                <br><br><br><br><br><br>
            </div>

        ";
        
        $__internal_3b592979843444fe90e8fd852f0fda619e853bab5114cedf12b6853b8bc05d45->leave($__internal_3b592979843444fe90e8fd852f0fda619e853bab5114cedf12b6853b8bc05d45_prof);

        
        $__internal_eef442c6a42ac08df8052e65c7d437b0142b9c03a61d707da3fdd14d5809aa3f->leave($__internal_eef442c6a42ac08df8052e65c7d437b0142b9c03a61d707da3fdd14d5809aa3f_prof);

    }

    // line 15
    public function block_modules($context, array $blocks = array())
    {
        $__internal_587cb7252b45195c68773fcc0cc205453033a92ab70e6acab887df34a35cd770 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_587cb7252b45195c68773fcc0cc205453033a92ab70e6acab887df34a35cd770->enter($__internal_587cb7252b45195c68773fcc0cc205453033a92ab70e6acab887df34a35cd770_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_0af4e50cfa628bfcc5802acab5673bcc871484474a74f31682fb1b3ba6170c37 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0af4e50cfa628bfcc5802acab5673bcc871484474a74f31682fb1b3ba6170c37->enter($__internal_0af4e50cfa628bfcc5802acab5673bcc871484474a74f31682fb1b3ba6170c37_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        // line 16
        echo "                <div class=\"col-xl-6\" style=\"margin-left: 25%;\" >
                   ";
        // line 17
        $this->loadTemplate("::formulaire.html.twig", "StockBundle:Trans:ajouterProformat.html.twig", 17)->display($context);
        // line 18
        echo "                </div>
        ";
        
        $__internal_0af4e50cfa628bfcc5802acab5673bcc871484474a74f31682fb1b3ba6170c37->leave($__internal_0af4e50cfa628bfcc5802acab5673bcc871484474a74f31682fb1b3ba6170c37_prof);

        
        $__internal_587cb7252b45195c68773fcc0cc205453033a92ab70e6acab887df34a35cd770->leave($__internal_587cb7252b45195c68773fcc0cc205453033a92ab70e6acab887df34a35cd770_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Trans:ajouterProformat.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 18,  128 => 17,  125 => 16,  116 => 15,  99 => 7,  90 => 6,  80 => 20,  77 => 15,  74 => 6,  65 => 5,  52 => 3,  43 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::transporteur.html.twig' %}
\t{% block menu %}
\t\t{{ parent() }}
    {% endblock %}
    {% block contenuDuMilieu %}
        {% block debutPage %}
            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-20px;\">
                    <h2 style=\"color: white;margin-top:4%\"> <center>Livrer marchandises</center></h2>
                </div>
                <br><br><br><br><br><br>
            </div>

        {% endblock %}
\t\t{% block modules %}
                <div class=\"col-xl-6\" style=\"margin-left: 25%;\" >
                   {% include '::formulaire.html.twig' %}
                </div>
        {% endblock %}
    {% endblock %}", "StockBundle:Trans:ajouterProformat.html.twig", "C:\\wamp\\www\\symfony\\stock\\src\\StockBundle/Resources/views/Trans/ajouterProformat.html.twig");
    }
}
