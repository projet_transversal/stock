<?php

/* @WebProfiler/Collector/ajax.html.twig */
class __TwigTemplate_40b17d943594c1b7bd07e932bd004a17d05bdc48aee33f231fe6eb126770508f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_053082aa9846ba5681520b1670f6935d189f810d2204157711c904f1ff385cd4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_053082aa9846ba5681520b1670f6935d189f810d2204157711c904f1ff385cd4->enter($__internal_053082aa9846ba5681520b1670f6935d189f810d2204157711c904f1ff385cd4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $__internal_eb5df75848eedc3fdcfcfa7402b95b0d76d394ec015965cff33f6eb43c5b267a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eb5df75848eedc3fdcfcfa7402b95b0d76d394ec015965cff33f6eb43c5b267a->enter($__internal_eb5df75848eedc3fdcfcfa7402b95b0d76d394ec015965cff33f6eb43c5b267a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_053082aa9846ba5681520b1670f6935d189f810d2204157711c904f1ff385cd4->leave($__internal_053082aa9846ba5681520b1670f6935d189f810d2204157711c904f1ff385cd4_prof);

        
        $__internal_eb5df75848eedc3fdcfcfa7402b95b0d76d394ec015965cff33f6eb43c5b267a->leave($__internal_eb5df75848eedc3fdcfcfa7402b95b0d76d394ec015965cff33f6eb43c5b267a_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_b4ce6e7dd0df7c1d78eb6d9004df9544af1355a8fd5f67688fec4c22207f4c2a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b4ce6e7dd0df7c1d78eb6d9004df9544af1355a8fd5f67688fec4c22207f4c2a->enter($__internal_b4ce6e7dd0df7c1d78eb6d9004df9544af1355a8fd5f67688fec4c22207f4c2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_83f7e84b7b45e6b69c30c5ddc3230421bd09bc383748d3a01045490820612b66 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_83f7e84b7b45e6b69c30c5ddc3230421bd09bc383748d3a01045490820612b66->enter($__internal_83f7e84b7b45e6b69c30c5ddc3230421bd09bc383748d3a01045490820612b66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_83f7e84b7b45e6b69c30c5ddc3230421bd09bc383748d3a01045490820612b66->leave($__internal_83f7e84b7b45e6b69c30c5ddc3230421bd09bc383748d3a01045490820612b66_prof);

        
        $__internal_b4ce6e7dd0df7c1d78eb6d9004df9544af1355a8fd5f67688fec4c22207f4c2a->leave($__internal_b4ce6e7dd0df7c1d78eb6d9004df9544af1355a8fd5f67688fec4c22207f4c2a_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "@WebProfiler/Collector/ajax.html.twig", "C:\\wamp\\www\\symfony\\stock\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\ajax.html.twig");
    }
}
