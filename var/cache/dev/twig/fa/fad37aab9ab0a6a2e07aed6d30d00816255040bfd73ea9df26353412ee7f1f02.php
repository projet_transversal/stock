<?php

/* StockBundle:Trans:ajouterProformat.html.twig */
class __TwigTemplate_080daa0c216929f47a3b2c9c9be8c83aa7a62eb3f950c2cda5f00272054539ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:Trans:ajouterProformat.html.twig", 1);
        $this->blocks = array(
            'menu' => array($this, 'block_menu'),
            'contenuDuMilieu' => array($this, 'block_contenuDuMilieu'),
            'debutPage' => array($this, 'block_debutPage'),
            'modules' => array($this, 'block_modules'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9feaf7f5b060babf645883759f36e4317d1182e9cd02816d2243c7aa21c08757 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9feaf7f5b060babf645883759f36e4317d1182e9cd02816d2243c7aa21c08757->enter($__internal_9feaf7f5b060babf645883759f36e4317d1182e9cd02816d2243c7aa21c08757_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Trans:ajouterProformat.html.twig"));

        $__internal_be31f24a70413c42c2b0d2e41ca93270f9d4933fbfff17bb4081373b16d8bb8f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_be31f24a70413c42c2b0d2e41ca93270f9d4933fbfff17bb4081373b16d8bb8f->enter($__internal_be31f24a70413c42c2b0d2e41ca93270f9d4933fbfff17bb4081373b16d8bb8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Trans:ajouterProformat.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9feaf7f5b060babf645883759f36e4317d1182e9cd02816d2243c7aa21c08757->leave($__internal_9feaf7f5b060babf645883759f36e4317d1182e9cd02816d2243c7aa21c08757_prof);

        
        $__internal_be31f24a70413c42c2b0d2e41ca93270f9d4933fbfff17bb4081373b16d8bb8f->leave($__internal_be31f24a70413c42c2b0d2e41ca93270f9d4933fbfff17bb4081373b16d8bb8f_prof);

    }

    // line 4
    public function block_menu($context, array $blocks = array())
    {
        $__internal_d5e65284ecc80a8dd2b637115ce8b8d63757c1ddbd372026122289c9a9b63f73 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d5e65284ecc80a8dd2b637115ce8b8d63757c1ddbd372026122289c9a9b63f73->enter($__internal_d5e65284ecc80a8dd2b637115ce8b8d63757c1ddbd372026122289c9a9b63f73_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_f7c73dfa42f98e56e0e83229a4419101be5e79c358021bfcd14d89787fa76fae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f7c73dfa42f98e56e0e83229a4419101be5e79c358021bfcd14d89787fa76fae->enter($__internal_f7c73dfa42f98e56e0e83229a4419101be5e79c358021bfcd14d89787fa76fae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 5
        echo "\t\t<nav class=\"side-menu side-menu-big-icon\" style=\"background: #3299CC;overflow: hidden\">
            <nav class=\"sidebar-header\">
                <img src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/man.png"), "html", null, true);
        echo "\" style=\"margin-left:35%;margin-top:-15%;\"><br><br>
                <h5 style=\"text-align:center\">User Name</h5>
                <h5 style=\"text-align:center\">Profil</h5>
            </nav>
            <hr style=\"margin-left:20%;margin-right: 20%;background: red;height:1px\">
\t\t<ul class=\"side-menu-list\">
                <li class=\"opened\">
                    <span>
                    \t<a href=\"";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("livrer_marchandise");
        echo "\" >
\t                        <i class=\"glyphicon glyphicon-inbox\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Livrer marchandise</span>
                        </a>
                    </span>
                </li>
\t\t\t\t<li class=\"opened\">
                    <span>
                    \t<a href=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("lister_marchandises");
        echo "\" >
\t                        <i class=\"glyphicon glyphicon-th-list\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Lister marchandise</span>
                        </a>
                    </span>
                </li>
\t\t\t\t<li class=\"opened\">
                    <span>
                    \t<a href=\"";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter_comm");
        echo "\" >
\t                        <i class=\"glyphicon glyphicon-plus\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Ajouter commerçant</span>
                        </a>
                    </span>
                </li>
\t\t</ul>
\t  </nav>
\t ";
        
        $__internal_f7c73dfa42f98e56e0e83229a4419101be5e79c358021bfcd14d89787fa76fae->leave($__internal_f7c73dfa42f98e56e0e83229a4419101be5e79c358021bfcd14d89787fa76fae_prof);

        
        $__internal_d5e65284ecc80a8dd2b637115ce8b8d63757c1ddbd372026122289c9a9b63f73->leave($__internal_d5e65284ecc80a8dd2b637115ce8b8d63757c1ddbd372026122289c9a9b63f73_prof);

    }

    // line 40
    public function block_contenuDuMilieu($context, array $blocks = array())
    {
        $__internal_c52f629c387216627c9453cd6a3c8eced6f6e5fa6a8a9380828e3c16315670a6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c52f629c387216627c9453cd6a3c8eced6f6e5fa6a8a9380828e3c16315670a6->enter($__internal_c52f629c387216627c9453cd6a3c8eced6f6e5fa6a8a9380828e3c16315670a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        $__internal_7aa8c0dc6375484745b57e00091a5d4707d9951b933d2f5ec3d4b3fc9928b35e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7aa8c0dc6375484745b57e00091a5d4707d9951b933d2f5ec3d4b3fc9928b35e->enter($__internal_7aa8c0dc6375484745b57e00091a5d4707d9951b933d2f5ec3d4b3fc9928b35e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenuDuMilieu"));

        // line 41
        echo "        ";
        $this->displayBlock('debutPage', $context, $blocks);
        // line 50
        echo "\t\t";
        $this->displayBlock('modules', $context, $blocks);
        // line 65
        echo "    ";
        
        $__internal_7aa8c0dc6375484745b57e00091a5d4707d9951b933d2f5ec3d4b3fc9928b35e->leave($__internal_7aa8c0dc6375484745b57e00091a5d4707d9951b933d2f5ec3d4b3fc9928b35e_prof);

        
        $__internal_c52f629c387216627c9453cd6a3c8eced6f6e5fa6a8a9380828e3c16315670a6->leave($__internal_c52f629c387216627c9453cd6a3c8eced6f6e5fa6a8a9380828e3c16315670a6_prof);

    }

    // line 41
    public function block_debutPage($context, array $blocks = array())
    {
        $__internal_6b20d497220feee112f2589e33c2c82927fc06db0e207eef21a3a0a2ec80cb65 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6b20d497220feee112f2589e33c2c82927fc06db0e207eef21a3a0a2ec80cb65->enter($__internal_6b20d497220feee112f2589e33c2c82927fc06db0e207eef21a3a0a2ec80cb65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        $__internal_5d065cdd3163d1d15443e5f9878ba53fd52a64026cc1c20943d955c754d83e1d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5d065cdd3163d1d15443e5f9878ba53fd52a64026cc1c20943d955c754d83e1d->enter($__internal_5d065cdd3163d1d15443e5f9878ba53fd52a64026cc1c20943d955c754d83e1d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "debutPage"));

        // line 42
        echo "            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-20px;\">
                    <h2 style=\"color: white;margin-top:4%\"> <center>Livrer marchandises</center></h2>
                </div>
                <br><br><br><br><br><br>
            </div>

        ";
        
        $__internal_5d065cdd3163d1d15443e5f9878ba53fd52a64026cc1c20943d955c754d83e1d->leave($__internal_5d065cdd3163d1d15443e5f9878ba53fd52a64026cc1c20943d955c754d83e1d_prof);

        
        $__internal_6b20d497220feee112f2589e33c2c82927fc06db0e207eef21a3a0a2ec80cb65->leave($__internal_6b20d497220feee112f2589e33c2c82927fc06db0e207eef21a3a0a2ec80cb65_prof);

    }

    // line 50
    public function block_modules($context, array $blocks = array())
    {
        $__internal_3cb44503f1c8e896230c50b86b1707ed446db9158dbba31b598216400550263b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3cb44503f1c8e896230c50b86b1707ed446db9158dbba31b598216400550263b->enter($__internal_3cb44503f1c8e896230c50b86b1707ed446db9158dbba31b598216400550263b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        $__internal_d453e8192c83d453f84e32a3362323318deab672a1579bc2546a86aa9ee660e0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d453e8192c83d453f84e32a3362323318deab672a1579bc2546a86aa9ee660e0->enter($__internal_d453e8192c83d453f84e32a3362323318deab672a1579bc2546a86aa9ee660e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modules"));

        // line 51
        echo "
                <div class=\"col-xl-6\" style=\"margin-left: 25%;\" >
                    ";
        // line 53
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
                    ";
        // line 54
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
                    <div class=\"form-group\" align=\"center\" >
                        <input type=\"reset\" name=\"resetMarchandise\" class=\"btn btn-danger\" value=\"Annuler\"/>
                        <input type=\"submit\" name=\"addMarchandise\" class=\"btn btn-success\" value=\"Livrer\"/>

                    </div>
                    ";
        // line 60
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
                </div>


        ";
        
        $__internal_d453e8192c83d453f84e32a3362323318deab672a1579bc2546a86aa9ee660e0->leave($__internal_d453e8192c83d453f84e32a3362323318deab672a1579bc2546a86aa9ee660e0_prof);

        
        $__internal_3cb44503f1c8e896230c50b86b1707ed446db9158dbba31b598216400550263b->leave($__internal_3cb44503f1c8e896230c50b86b1707ed446db9158dbba31b598216400550263b_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Trans:ajouterProformat.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 60,  176 => 54,  172 => 53,  168 => 51,  159 => 50,  142 => 42,  133 => 41,  123 => 65,  120 => 50,  117 => 41,  108 => 40,  89 => 31,  78 => 23,  67 => 15,  56 => 7,  52 => 5,  43 => 4,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}


\t{% block menu %}
\t\t<nav class=\"side-menu side-menu-big-icon\" style=\"background: #3299CC;overflow: hidden\">
            <nav class=\"sidebar-header\">
                <img src=\"{{asset('img/man.png')}}\" style=\"margin-left:35%;margin-top:-15%;\"><br><br>
                <h5 style=\"text-align:center\">User Name</h5>
                <h5 style=\"text-align:center\">Profil</h5>
            </nav>
            <hr style=\"margin-left:20%;margin-right: 20%;background: red;height:1px\">
\t\t<ul class=\"side-menu-list\">
                <li class=\"opened\">
                    <span>
                    \t<a href=\"{{ path('livrer_marchandise') }}\" >
\t                        <i class=\"glyphicon glyphicon-inbox\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Livrer marchandise</span>
                        </a>
                    </span>
                </li>
\t\t\t\t<li class=\"opened\">
                    <span>
                    \t<a href=\"{{ path('lister_marchandises') }}\" >
\t                        <i class=\"glyphicon glyphicon-th-list\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Lister marchandise</span>
                        </a>
                    </span>
                </li>
\t\t\t\t<li class=\"opened\">
                    <span>
                    \t<a href=\"{{ path('ajouter_comm') }}\" >
\t                        <i class=\"glyphicon glyphicon-plus\"  style=\"color: white\"></i>
\t                        <span class=\"lbl\" style=\"color: white\">Ajouter commerçant</span>
                        </a>
                    </span>
                </li>
\t\t</ul>
\t  </nav>
\t {% endblock %}
\t {% block contenuDuMilieu %}
        {% block debutPage %}
            <div class=\"row\">
                <div class=\"col-lg-12\" style=\"width:106%;height: 120px;background: silver;margin-top:-20px;\">
                    <h2 style=\"color: white;margin-top:4%\"> <center>Livrer marchandises</center></h2>
                </div>
                <br><br><br><br><br><br>
            </div>

        {% endblock %}
\t\t{% block modules %}

                <div class=\"col-xl-6\" style=\"margin-left: 25%;\" >
                    {{ form_start(form) }}
                    {{ form_widget(form) }}
                    <div class=\"form-group\" align=\"center\" >
                        <input type=\"reset\" name=\"resetMarchandise\" class=\"btn btn-danger\" value=\"Annuler\"/>
                        <input type=\"submit\" name=\"addMarchandise\" class=\"btn btn-success\" value=\"Livrer\"/>

                    </div>
                    {{ form_end(form) }}
                </div>


        {% endblock %}
    {% endblock %}", "StockBundle:Trans:ajouterProformat.html.twig", "C:\\wamp\\www\\symfony\\stock\\src\\StockBundle/Resources/views/Trans/ajouterProformat.html.twig");
    }
}
