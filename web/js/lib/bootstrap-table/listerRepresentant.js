$(document).ready(function() {
    var $table = $('#table');
    var data = [];

    for (var i = 1; i <= 15; i++) {
        data.push({
            "id": i,
            "name": "Representant " + i,
            "prenom": "prenom" + Math.ceil(Math.random() * 10),
            "Modifier": '<i class="font-icon font-icon-pen" style="color: green"></i>',
            "Supprimer": '<i class="font-icon font-icon-del" style="color: red"></i>',
            "pays": "pays"+Math.ceil(Math.random()*10)
        });
    }

    $table.bootstrapTable({
        iconsPrefix: 'font-icon',
        paginationPreText: '<i class="font-icon font-icon-arrow-left"></i>',
        paginationNextText: '<i class="font-icon font-icon-arrow-right"></i>',
        data: data
    });
});
