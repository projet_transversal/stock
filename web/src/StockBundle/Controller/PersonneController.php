<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/07/17
 * Time: 10:53
 */

namespace StockBundle\Controller;


use StockBundle\Form\SocieteFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PersonneController extends Controller
{

    /**
     * @Route("/addPersonne")
     */
    public function addPersonneAction()
    {
        $form = $this->createForm(SocieteFormType::class);
        return $this->render('StockBundle:Societe:addSociete.html.twig',
            array(
                'form'=>$form->createView()
            )
            );

    }
}