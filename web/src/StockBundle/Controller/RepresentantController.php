<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/07/17
 * Time: 11:18
 */

namespace StockBundle\Controller;


use StockBundle\Form\RepresentantFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
class RepresentantController extends Controller
{

    /**
     * @Route("/addRepresentant")
     */
    public function addRepresentantAction()
    {
        $form = $this->createForm(RepresentantFormType::class);
        return $this->render('StockBundle:Representant:addRepresentant.html.twig',
            array(
                'form'=>$form->createView()
            )
        );

    }
}