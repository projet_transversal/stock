<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/07/17
 * Time: 11:06
 */

namespace StockBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use StockBundle\Form\TraderFormType;
class TraderController extends Controller
{

    /**
     * @Route("/addTrader")
     */
    public function addTraderAction()
    {
        $form = $this->createForm(TraderFormType::class);
        return $this->render('StockBundle:Trader:addTrader.html.twig',
            array(
                'form'=>$form->createView()
            )
        );

    }
}