<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03/07/17
 * Time: 11:11
 */

namespace StockBundle\Controller;


use StockBundle\Form\TransporteurFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
class TransporteurContriller extends Controller
{
    /**
     * @Route("/addTransporteur")
     */
    public function addTransporteurAction()
    {
        $form = $this->createForm(TransporteurFormType::class);
        return $this->render('StockBundle:Transporteur:addTransporteur.html.twig',
            array(
                'form'=>$form->createView()
            )
        );

    }

}