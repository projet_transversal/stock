<?php

namespace StockBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transporteur
 *
 * @ORM\Table(name="transporteur")
 * @ORM\Entity(repositoryClass="StockBundle\Repository\TransporteurRepository")
 */
class Transporteur extends Societe
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomEntreprise", type="string", length=255)
     */
    private $nomEntreprise;

    /**
     * @var int
     *
     * @ORM\Column(name="numCertificat", type="integer")
     */
    private $numCertificat;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomEntreprise
     *
     * @param string $nomEntreprise
     *
     * @return Transporteur
     */
    public function setNomEntreprise($nomEntreprise)
    {
        $this->nomEntreprise = $nomEntreprise;

        return $this;
    }

    /**
     * Get nomEntreprise
     *
     * @return string
     */
    public function getNomEntreprise()
    {
        return $this->nomEntreprise;
    }

    /**
     * Set numCertificat
     *
     * @param integer $numCertificat
     *
     * @return Transporteur
     */
    public function setNumCertificat($numCertificat)
    {
        $this->numCertificat = $numCertificat;

        return $this;
    }

    /**
     * Get numCertificat
     *
     * @return int
     */
    public function getNumCertificat()
    {
        return $this->numCertificat;
    }
}

