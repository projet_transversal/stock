<?php

/* form/fields.html.twig */
class __TwigTemplate_c3cd4c12c5856ce7669d847f7191022d05a64796dfc76eabfdc0bc6f08cf09b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'date_time_picker_widget' => array($this, 'block_date_time_picker_widget'),
            'tags_input_widget' => array($this, 'block_tags_input_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9666fbf68c567228f470422fca4fab16893b1a0ffca2d9af0aa62c17388a604a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9666fbf68c567228f470422fca4fab16893b1a0ffca2d9af0aa62c17388a604a->enter($__internal_9666fbf68c567228f470422fca4fab16893b1a0ffca2d9af0aa62c17388a604a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/fields.html.twig"));

        $__internal_6bac9d36c16133a59b24f98598a8ef79ee0696bf2293f66068dbd90b5e4d8c62 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6bac9d36c16133a59b24f98598a8ef79ee0696bf2293f66068dbd90b5e4d8c62->enter($__internal_6bac9d36c16133a59b24f98598a8ef79ee0696bf2293f66068dbd90b5e4d8c62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/fields.html.twig"));

        // line 9
        echo "
";
        // line 10
        $this->displayBlock('date_time_picker_widget', $context, $blocks);
        // line 18
        echo "
";
        // line 19
        $this->displayBlock('tags_input_widget', $context, $blocks);
        
        $__internal_9666fbf68c567228f470422fca4fab16893b1a0ffca2d9af0aa62c17388a604a->leave($__internal_9666fbf68c567228f470422fca4fab16893b1a0ffca2d9af0aa62c17388a604a_prof);

        
        $__internal_6bac9d36c16133a59b24f98598a8ef79ee0696bf2293f66068dbd90b5e4d8c62->leave($__internal_6bac9d36c16133a59b24f98598a8ef79ee0696bf2293f66068dbd90b5e4d8c62_prof);

    }

    // line 10
    public function block_date_time_picker_widget($context, array $blocks = array())
    {
        $__internal_2455c7a4e78f5fd39098079b927769bf71fe7dee5fe748f935b5882b7009f87a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2455c7a4e78f5fd39098079b927769bf71fe7dee5fe748f935b5882b7009f87a->enter($__internal_2455c7a4e78f5fd39098079b927769bf71fe7dee5fe748f935b5882b7009f87a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_time_picker_widget"));

        $__internal_397c04403784fdd237bdd641f0b4ceb51f726b0b476c50f9394ca0b1fd20afea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_397c04403784fdd237bdd641f0b4ceb51f726b0b476c50f9394ca0b1fd20afea->enter($__internal_397c04403784fdd237bdd641f0b4ceb51f726b0b476c50f9394ca0b1fd20afea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_time_picker_widget"));

        // line 11
        echo "    <div class=\"input-group date\" data-toggle=\"datetimepicker\">
        ";
        // line 12
        $this->displayBlock("datetime_widget", $context, $blocks);
        echo "
        <span class=\"input-group-addon\">
            <span class=\"fa fa-calendar\" aria-hidden=\"true\"></span>
        </span>
    </div>
";
        
        $__internal_397c04403784fdd237bdd641f0b4ceb51f726b0b476c50f9394ca0b1fd20afea->leave($__internal_397c04403784fdd237bdd641f0b4ceb51f726b0b476c50f9394ca0b1fd20afea_prof);

        
        $__internal_2455c7a4e78f5fd39098079b927769bf71fe7dee5fe748f935b5882b7009f87a->leave($__internal_2455c7a4e78f5fd39098079b927769bf71fe7dee5fe748f935b5882b7009f87a_prof);

    }

    // line 19
    public function block_tags_input_widget($context, array $blocks = array())
    {
        $__internal_29ac0a29b7e070d6dd8c12457c165f76a30ba33e80ffa5c3eb96ee83e0baa72d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_29ac0a29b7e070d6dd8c12457c165f76a30ba33e80ffa5c3eb96ee83e0baa72d->enter($__internal_29ac0a29b7e070d6dd8c12457c165f76a30ba33e80ffa5c3eb96ee83e0baa72d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tags_input_widget"));

        $__internal_5b4d6e6a2593d6f144d43bb456fa9148a2f6f1915926a60cb4f5960ea583818f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5b4d6e6a2593d6f144d43bb456fa9148a2f6f1915926a60cb4f5960ea583818f->enter($__internal_5b4d6e6a2593d6f144d43bb456fa9148a2f6f1915926a60cb4f5960ea583818f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tags_input_widget"));

        // line 20
        echo "    <div class=\"input-group\">
        ";
        // line 21
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget', array("attr" => array("data-toggle" => "tagsinput", "data-tags" => twig_jsonencode_filter(($context["tags"] ?? $this->getContext($context, "tags"))))));
        echo "
        <span class=\"input-group-addon\">
            <span class=\"fa fa-tags\" aria-hidden=\"true\"></span>
        </span>
    </div>
";
        
        $__internal_5b4d6e6a2593d6f144d43bb456fa9148a2f6f1915926a60cb4f5960ea583818f->leave($__internal_5b4d6e6a2593d6f144d43bb456fa9148a2f6f1915926a60cb4f5960ea583818f_prof);

        
        $__internal_29ac0a29b7e070d6dd8c12457c165f76a30ba33e80ffa5c3eb96ee83e0baa72d->leave($__internal_29ac0a29b7e070d6dd8c12457c165f76a30ba33e80ffa5c3eb96ee83e0baa72d_prof);

    }

    public function getTemplateName()
    {
        return "form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 21,  82 => 20,  73 => 19,  57 => 12,  54 => 11,  45 => 10,  35 => 19,  32 => 18,  30 => 10,  27 => 9,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
   Each field type is rendered by a template fragment, which is determined
   by the name of your form type class (DateTimePickerType -> date_time_picker)
   and the suffix \"_widget\". This can be controlled by overriding getBlockPrefix()
   in DateTimePickerType.

   See http://symfony.com/doc/current/cookbook/form/create_custom_field_type.html#creating-a-template-for-the-field
#}

{% block date_time_picker_widget %}
    <div class=\"input-group date\" data-toggle=\"datetimepicker\">
        {{ block('datetime_widget') }}
        <span class=\"input-group-addon\">
            <span class=\"fa fa-calendar\" aria-hidden=\"true\"></span>
        </span>
    </div>
{% endblock %}

{% block tags_input_widget %}
    <div class=\"input-group\">
        {{ form_widget(form, {'attr': {'data-toggle': 'tagsinput', 'data-tags': tags|json_encode}}) }}
        <span class=\"input-group-addon\">
            <span class=\"fa fa-tags\" aria-hidden=\"true\"></span>
        </span>
    </div>
{% endblock %}
", "form/fields.html.twig", "/var/www/html/GestionStock/app/Resources/views/form/fields.html.twig");
    }
}
