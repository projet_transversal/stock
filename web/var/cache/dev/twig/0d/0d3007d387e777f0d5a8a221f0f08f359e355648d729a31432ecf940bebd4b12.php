<?php

/* StockBundle:Trader:addTrader.html.twig */
class __TwigTemplate_f404a06c28eb5947731fd30773e554f2d9eb6576d7da17018543a6fda019451e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:Trader:addTrader.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_01857a03d82c71a3133d18e38cb0ac01d43a643258013271c139bf0cc83de11a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_01857a03d82c71a3133d18e38cb0ac01d43a643258013271c139bf0cc83de11a->enter($__internal_01857a03d82c71a3133d18e38cb0ac01d43a643258013271c139bf0cc83de11a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Trader:addTrader.html.twig"));

        $__internal_42d3b992012b0480423241f3a09fe0ec7cb0c088a2e8b53d2801d46038c844e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_42d3b992012b0480423241f3a09fe0ec7cb0c088a2e8b53d2801d46038c844e2->enter($__internal_42d3b992012b0480423241f3a09fe0ec7cb0c088a2e8b53d2801d46038c844e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Trader:addTrader.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_01857a03d82c71a3133d18e38cb0ac01d43a643258013271c139bf0cc83de11a->leave($__internal_01857a03d82c71a3133d18e38cb0ac01d43a643258013271c139bf0cc83de11a_prof);

        
        $__internal_42d3b992012b0480423241f3a09fe0ec7cb0c088a2e8b53d2801d46038c844e2->leave($__internal_42d3b992012b0480423241f3a09fe0ec7cb0c088a2e8b53d2801d46038c844e2_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_950a2140c544029e9cf3015af4984c8c6761a8090d5bd8311e890612d28e0840 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_950a2140c544029e9cf3015af4984c8c6761a8090d5bd8311e890612d28e0840->enter($__internal_950a2140c544029e9cf3015af4984c8c6761a8090d5bd8311e890612d28e0840_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_12f6c6c9b788333d81ec21db6ba1171f6031a5095ff5b030262389f045234160 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_12f6c6c9b788333d81ec21db6ba1171f6031a5095ff5b030262389f045234160->enter($__internal_12f6c6c9b788333d81ec21db6ba1171f6031a5095ff5b030262389f045234160_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    ";
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form');
        echo "
";
        
        $__internal_12f6c6c9b788333d81ec21db6ba1171f6031a5095ff5b030262389f045234160->leave($__internal_12f6c6c9b788333d81ec21db6ba1171f6031a5095ff5b030262389f045234160_prof);

        
        $__internal_950a2140c544029e9cf3015af4984c8c6761a8090d5bd8311e890612d28e0840->leave($__internal_950a2140c544029e9cf3015af4984c8c6761a8090d5bd8311e890612d28e0840_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Trader:addTrader.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 3,  40 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}
{% block body %}
    {{ form(form) }}
{% endblock %}", "StockBundle:Trader:addTrader.html.twig", "/var/www/html/GestionStock/src/StockBundle/Resources/views/Trader/addTrader.html.twig");
    }
}
