<?php

/* bootstrap_3_layout.html.twig */
class __TwigTemplate_dd3d243aff1306d0a6ca9c44070e0702075ca814a4d65b13eaf028f42e3a173f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("form_div_layout.html.twig", "bootstrap_3_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."form_div_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_widget_simple' => array($this, 'block_form_widget_simple'),
                'textarea_widget' => array($this, 'block_textarea_widget'),
                'button_widget' => array($this, 'block_button_widget'),
                'money_widget' => array($this, 'block_money_widget'),
                'percent_widget' => array($this, 'block_percent_widget'),
                'datetime_widget' => array($this, 'block_datetime_widget'),
                'date_widget' => array($this, 'block_date_widget'),
                'time_widget' => array($this, 'block_time_widget'),
                'dateinterval_widget' => array($this, 'block_dateinterval_widget'),
                'choice_widget_collapsed' => array($this, 'block_choice_widget_collapsed'),
                'choice_widget_expanded' => array($this, 'block_choice_widget_expanded'),
                'checkbox_widget' => array($this, 'block_checkbox_widget'),
                'radio_widget' => array($this, 'block_radio_widget'),
                'form_label' => array($this, 'block_form_label'),
                'choice_label' => array($this, 'block_choice_label'),
                'checkbox_label' => array($this, 'block_checkbox_label'),
                'radio_label' => array($this, 'block_radio_label'),
                'checkbox_radio_label' => array($this, 'block_checkbox_radio_label'),
                'form_row' => array($this, 'block_form_row'),
                'button_row' => array($this, 'block_button_row'),
                'choice_row' => array($this, 'block_choice_row'),
                'date_row' => array($this, 'block_date_row'),
                'time_row' => array($this, 'block_time_row'),
                'datetime_row' => array($this, 'block_datetime_row'),
                'checkbox_row' => array($this, 'block_checkbox_row'),
                'radio_row' => array($this, 'block_radio_row'),
                'form_errors' => array($this, 'block_form_errors'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b8d89c011c66f9cab5bff96b4c76eb011803689000bd2cef0102e7582ec890aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b8d89c011c66f9cab5bff96b4c76eb011803689000bd2cef0102e7582ec890aa->enter($__internal_b8d89c011c66f9cab5bff96b4c76eb011803689000bd2cef0102e7582ec890aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_3_layout.html.twig"));

        $__internal_90bde87f04b9c2c26e0c1d0b5bf56d2ff3bf11fb02fd188ab3372586531b7099 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_90bde87f04b9c2c26e0c1d0b5bf56d2ff3bf11fb02fd188ab3372586531b7099->enter($__internal_90bde87f04b9c2c26e0c1d0b5bf56d2ff3bf11fb02fd188ab3372586531b7099_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_3_layout.html.twig"));

        // line 2
        echo "
";
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('form_widget_simple', $context, $blocks);
        // line 11
        echo "
";
        // line 12
        $this->displayBlock('textarea_widget', $context, $blocks);
        // line 16
        echo "
";
        // line 17
        $this->displayBlock('button_widget', $context, $blocks);
        // line 21
        echo "
";
        // line 22
        $this->displayBlock('money_widget', $context, $blocks);
        // line 34
        echo "
";
        // line 35
        $this->displayBlock('percent_widget', $context, $blocks);
        // line 41
        echo "
";
        // line 42
        $this->displayBlock('datetime_widget', $context, $blocks);
        // line 55
        echo "
";
        // line 56
        $this->displayBlock('date_widget', $context, $blocks);
        // line 74
        echo "
";
        // line 75
        $this->displayBlock('time_widget', $context, $blocks);
        // line 90
        $this->displayBlock('dateinterval_widget', $context, $blocks);
        // line 109
        $this->displayBlock('choice_widget_collapsed', $context, $blocks);
        // line 113
        echo "
";
        // line 114
        $this->displayBlock('choice_widget_expanded', $context, $blocks);
        // line 133
        echo "
";
        // line 134
        $this->displayBlock('checkbox_widget', $context, $blocks);
        // line 144
        echo "
";
        // line 145
        $this->displayBlock('radio_widget', $context, $blocks);
        // line 155
        echo "
";
        // line 157
        echo "
";
        // line 158
        $this->displayBlock('form_label', $context, $blocks);
        // line 162
        echo "
";
        // line 163
        $this->displayBlock('choice_label', $context, $blocks);
        // line 168
        echo "
";
        // line 169
        $this->displayBlock('checkbox_label', $context, $blocks);
        // line 172
        echo "
";
        // line 173
        $this->displayBlock('radio_label', $context, $blocks);
        // line 176
        echo "
";
        // line 177
        $this->displayBlock('checkbox_radio_label', $context, $blocks);
        // line 201
        echo "
";
        // line 203
        echo "
";
        // line 204
        $this->displayBlock('form_row', $context, $blocks);
        // line 211
        echo "
";
        // line 212
        $this->displayBlock('button_row', $context, $blocks);
        // line 217
        echo "
";
        // line 218
        $this->displayBlock('choice_row', $context, $blocks);
        // line 222
        echo "
";
        // line 223
        $this->displayBlock('date_row', $context, $blocks);
        // line 227
        echo "
";
        // line 228
        $this->displayBlock('time_row', $context, $blocks);
        // line 232
        echo "
";
        // line 233
        $this->displayBlock('datetime_row', $context, $blocks);
        // line 237
        echo "
";
        // line 238
        $this->displayBlock('checkbox_row', $context, $blocks);
        // line 244
        echo "
";
        // line 245
        $this->displayBlock('radio_row', $context, $blocks);
        // line 251
        echo "
";
        // line 253
        echo "
";
        // line 254
        $this->displayBlock('form_errors', $context, $blocks);
        
        $__internal_b8d89c011c66f9cab5bff96b4c76eb011803689000bd2cef0102e7582ec890aa->leave($__internal_b8d89c011c66f9cab5bff96b4c76eb011803689000bd2cef0102e7582ec890aa_prof);

        
        $__internal_90bde87f04b9c2c26e0c1d0b5bf56d2ff3bf11fb02fd188ab3372586531b7099->leave($__internal_90bde87f04b9c2c26e0c1d0b5bf56d2ff3bf11fb02fd188ab3372586531b7099_prof);

    }

    // line 5
    public function block_form_widget_simple($context, array $blocks = array())
    {
        $__internal_b28cdb8f3e1c7ca9346cd778016aea0a1bedbb2e12d46cb9e50126c885890225 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b28cdb8f3e1c7ca9346cd778016aea0a1bedbb2e12d46cb9e50126c885890225->enter($__internal_b28cdb8f3e1c7ca9346cd778016aea0a1bedbb2e12d46cb9e50126c885890225_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        $__internal_7ca014822bff890a7d6a757b6adefa570b7fedff6a69cdb2f4e1b82306c390f7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7ca014822bff890a7d6a757b6adefa570b7fedff6a69cdb2f4e1b82306c390f7->enter($__internal_7ca014822bff890a7d6a757b6adefa570b7fedff6a69cdb2f4e1b82306c390f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        // line 6
        if (( !array_key_exists("type", $context) || !twig_in_filter(($context["type"] ?? $this->getContext($context, "type")), array(0 => "file", 1 => "hidden")))) {
            // line 7
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control"))));
        }
        // line 9
        $this->displayParentBlock("form_widget_simple", $context, $blocks);
        
        $__internal_7ca014822bff890a7d6a757b6adefa570b7fedff6a69cdb2f4e1b82306c390f7->leave($__internal_7ca014822bff890a7d6a757b6adefa570b7fedff6a69cdb2f4e1b82306c390f7_prof);

        
        $__internal_b28cdb8f3e1c7ca9346cd778016aea0a1bedbb2e12d46cb9e50126c885890225->leave($__internal_b28cdb8f3e1c7ca9346cd778016aea0a1bedbb2e12d46cb9e50126c885890225_prof);

    }

    // line 12
    public function block_textarea_widget($context, array $blocks = array())
    {
        $__internal_979ee316b365391f58c93120a8e456cc7c54e7aec9d5e47e9ded2e0b3df3d75d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_979ee316b365391f58c93120a8e456cc7c54e7aec9d5e47e9ded2e0b3df3d75d->enter($__internal_979ee316b365391f58c93120a8e456cc7c54e7aec9d5e47e9ded2e0b3df3d75d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "textarea_widget"));

        $__internal_7e4c8733e50ebe7e54d2c60955cc6ba2fd5cbea7967a214ad8f34885cc899158 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e4c8733e50ebe7e54d2c60955cc6ba2fd5cbea7967a214ad8f34885cc899158->enter($__internal_7e4c8733e50ebe7e54d2c60955cc6ba2fd5cbea7967a214ad8f34885cc899158_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "textarea_widget"));

        // line 13
        $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control"))));
        // line 14
        $this->displayParentBlock("textarea_widget", $context, $blocks);
        
        $__internal_7e4c8733e50ebe7e54d2c60955cc6ba2fd5cbea7967a214ad8f34885cc899158->leave($__internal_7e4c8733e50ebe7e54d2c60955cc6ba2fd5cbea7967a214ad8f34885cc899158_prof);

        
        $__internal_979ee316b365391f58c93120a8e456cc7c54e7aec9d5e47e9ded2e0b3df3d75d->leave($__internal_979ee316b365391f58c93120a8e456cc7c54e7aec9d5e47e9ded2e0b3df3d75d_prof);

    }

    // line 17
    public function block_button_widget($context, array $blocks = array())
    {
        $__internal_5a087d01122d03e313416e2dd64d509593b63cb1d7755022a3c704c0ec03a544 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5a087d01122d03e313416e2dd64d509593b63cb1d7755022a3c704c0ec03a544->enter($__internal_5a087d01122d03e313416e2dd64d509593b63cb1d7755022a3c704c0ec03a544_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        $__internal_59fb6bba109355accf7d91bbf93c1e38454930a48f3753aa1038c44e2e7ea41e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_59fb6bba109355accf7d91bbf93c1e38454930a48f3753aa1038c44e2e7ea41e->enter($__internal_59fb6bba109355accf7d91bbf93c1e38454930a48f3753aa1038c44e2e7ea41e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        // line 18
        $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "btn-default")) : ("btn-default")) . " btn"))));
        // line 19
        $this->displayParentBlock("button_widget", $context, $blocks);
        
        $__internal_59fb6bba109355accf7d91bbf93c1e38454930a48f3753aa1038c44e2e7ea41e->leave($__internal_59fb6bba109355accf7d91bbf93c1e38454930a48f3753aa1038c44e2e7ea41e_prof);

        
        $__internal_5a087d01122d03e313416e2dd64d509593b63cb1d7755022a3c704c0ec03a544->leave($__internal_5a087d01122d03e313416e2dd64d509593b63cb1d7755022a3c704c0ec03a544_prof);

    }

    // line 22
    public function block_money_widget($context, array $blocks = array())
    {
        $__internal_5d2a9df1a275da8233d552d82c18ac9449cc33051bd2e276d1badc85d7a6befa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d2a9df1a275da8233d552d82c18ac9449cc33051bd2e276d1badc85d7a6befa->enter($__internal_5d2a9df1a275da8233d552d82c18ac9449cc33051bd2e276d1badc85d7a6befa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        $__internal_a3db959e1c7636907b612b31f690a363083da7d7cdb2c627dbdf7e5f42ec4b6a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a3db959e1c7636907b612b31f690a363083da7d7cdb2c627dbdf7e5f42ec4b6a->enter($__internal_a3db959e1c7636907b612b31f690a363083da7d7cdb2c627dbdf7e5f42ec4b6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        // line 23
        echo "<div class=\"input-group\">
        ";
        // line 24
        $context["append"] = (is_string($__internal_34c1f0539addff55227caf23ae0823221682f80e01f637799cc1f3f153add512 = ($context["money_pattern"] ?? $this->getContext($context, "money_pattern"))) && is_string($__internal_32e8a50bec4241dc45ed2f8fc738a4d7a6b72abb30fc5487c4cb065431831655 = "{{") && ('' === $__internal_32e8a50bec4241dc45ed2f8fc738a4d7a6b72abb30fc5487c4cb065431831655 || 0 === strpos($__internal_34c1f0539addff55227caf23ae0823221682f80e01f637799cc1f3f153add512, $__internal_32e8a50bec4241dc45ed2f8fc738a4d7a6b72abb30fc5487c4cb065431831655)));
        // line 25
        echo "        ";
        if ( !($context["append"] ?? $this->getContext($context, "append"))) {
            // line 26
            echo "            <span class=\"input-group-addon\">";
            echo twig_escape_filter($this->env, twig_replace_filter(($context["money_pattern"] ?? $this->getContext($context, "money_pattern")), array("{{ widget }}" => "")), "html", null, true);
            echo "</span>
        ";
        }
        // line 28
        $this->displayBlock("form_widget_simple", $context, $blocks);
        // line 29
        if (($context["append"] ?? $this->getContext($context, "append"))) {
            // line 30
            echo "            <span class=\"input-group-addon\">";
            echo twig_escape_filter($this->env, twig_replace_filter(($context["money_pattern"] ?? $this->getContext($context, "money_pattern")), array("{{ widget }}" => "")), "html", null, true);
            echo "</span>
        ";
        }
        // line 32
        echo "    </div>";
        
        $__internal_a3db959e1c7636907b612b31f690a363083da7d7cdb2c627dbdf7e5f42ec4b6a->leave($__internal_a3db959e1c7636907b612b31f690a363083da7d7cdb2c627dbdf7e5f42ec4b6a_prof);

        
        $__internal_5d2a9df1a275da8233d552d82c18ac9449cc33051bd2e276d1badc85d7a6befa->leave($__internal_5d2a9df1a275da8233d552d82c18ac9449cc33051bd2e276d1badc85d7a6befa_prof);

    }

    // line 35
    public function block_percent_widget($context, array $blocks = array())
    {
        $__internal_27188ef0af604b5b7376ca8e7dceb589e738d7e7510d1f0e553c52ff8167514c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_27188ef0af604b5b7376ca8e7dceb589e738d7e7510d1f0e553c52ff8167514c->enter($__internal_27188ef0af604b5b7376ca8e7dceb589e738d7e7510d1f0e553c52ff8167514c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        $__internal_346433ff2f3fb16f66f926c0e147ad6dd89cfa8b78ab534a507ea3c9cd300fb3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_346433ff2f3fb16f66f926c0e147ad6dd89cfa8b78ab534a507ea3c9cd300fb3->enter($__internal_346433ff2f3fb16f66f926c0e147ad6dd89cfa8b78ab534a507ea3c9cd300fb3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        // line 36
        echo "<div class=\"input-group\">";
        // line 37
        $this->displayBlock("form_widget_simple", $context, $blocks);
        // line 38
        echo "<span class=\"input-group-addon\">%</span>
    </div>";
        
        $__internal_346433ff2f3fb16f66f926c0e147ad6dd89cfa8b78ab534a507ea3c9cd300fb3->leave($__internal_346433ff2f3fb16f66f926c0e147ad6dd89cfa8b78ab534a507ea3c9cd300fb3_prof);

        
        $__internal_27188ef0af604b5b7376ca8e7dceb589e738d7e7510d1f0e553c52ff8167514c->leave($__internal_27188ef0af604b5b7376ca8e7dceb589e738d7e7510d1f0e553c52ff8167514c_prof);

    }

    // line 42
    public function block_datetime_widget($context, array $blocks = array())
    {
        $__internal_ecba6bba10d3ed6bff0b2bc998c0501bc04922169180399136e7ca2f01ab342a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ecba6bba10d3ed6bff0b2bc998c0501bc04922169180399136e7ca2f01ab342a->enter($__internal_ecba6bba10d3ed6bff0b2bc998c0501bc04922169180399136e7ca2f01ab342a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        $__internal_19d6570830be43df572e49c6a29d9dec64c5d8f7c42303c1967b5c4be9c44129 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_19d6570830be43df572e49c6a29d9dec64c5d8f7c42303c1967b5c4be9c44129->enter($__internal_19d6570830be43df572e49c6a29d9dec64c5d8f7c42303c1967b5c4be9c44129_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        // line 43
        if ((($context["widget"] ?? $this->getContext($context, "widget")) == "single_text")) {
            // line 44
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 46
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-inline"))));
            // line 47
            echo "<div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">";
            // line 48
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "date", array()), 'errors');
            // line 49
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "time", array()), 'errors');
            // line 50
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "date", array()), 'widget', array("datetime" => true));
            // line 51
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "time", array()), 'widget', array("datetime" => true));
            // line 52
            echo "</div>";
        }
        
        $__internal_19d6570830be43df572e49c6a29d9dec64c5d8f7c42303c1967b5c4be9c44129->leave($__internal_19d6570830be43df572e49c6a29d9dec64c5d8f7c42303c1967b5c4be9c44129_prof);

        
        $__internal_ecba6bba10d3ed6bff0b2bc998c0501bc04922169180399136e7ca2f01ab342a->leave($__internal_ecba6bba10d3ed6bff0b2bc998c0501bc04922169180399136e7ca2f01ab342a_prof);

    }

    // line 56
    public function block_date_widget($context, array $blocks = array())
    {
        $__internal_8b50a499d2402e14197bf3ebdb0ddbf747c3ceafa92d342d72eacc61b7e066d0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8b50a499d2402e14197bf3ebdb0ddbf747c3ceafa92d342d72eacc61b7e066d0->enter($__internal_8b50a499d2402e14197bf3ebdb0ddbf747c3ceafa92d342d72eacc61b7e066d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        $__internal_4ea39249b1903367c5f10f6d427555fcedb72aee1ac78122b283cec914f033c1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4ea39249b1903367c5f10f6d427555fcedb72aee1ac78122b283cec914f033c1->enter($__internal_4ea39249b1903367c5f10f6d427555fcedb72aee1ac78122b283cec914f033c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        // line 57
        if ((($context["widget"] ?? $this->getContext($context, "widget")) == "single_text")) {
            // line 58
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 60
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-inline"))));
            // line 61
            if (( !array_key_exists("datetime", $context) ||  !($context["datetime"] ?? $this->getContext($context, "datetime")))) {
                // line 62
                echo "<div ";
                $this->displayBlock("widget_container_attributes", $context, $blocks);
                echo ">";
            }
            // line 64
            echo twig_replace_filter(($context["date_pattern"] ?? $this->getContext($context, "date_pattern")), array("{{ year }}" =>             // line 65
$this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "year", array()), 'widget'), "{{ month }}" =>             // line 66
$this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "month", array()), 'widget'), "{{ day }}" =>             // line 67
$this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "day", array()), 'widget')));
            // line 69
            if (( !array_key_exists("datetime", $context) ||  !($context["datetime"] ?? $this->getContext($context, "datetime")))) {
                // line 70
                echo "</div>";
            }
        }
        
        $__internal_4ea39249b1903367c5f10f6d427555fcedb72aee1ac78122b283cec914f033c1->leave($__internal_4ea39249b1903367c5f10f6d427555fcedb72aee1ac78122b283cec914f033c1_prof);

        
        $__internal_8b50a499d2402e14197bf3ebdb0ddbf747c3ceafa92d342d72eacc61b7e066d0->leave($__internal_8b50a499d2402e14197bf3ebdb0ddbf747c3ceafa92d342d72eacc61b7e066d0_prof);

    }

    // line 75
    public function block_time_widget($context, array $blocks = array())
    {
        $__internal_c6ce6607c3ad9ae32ea4f16a4b1659b5966d0e6e17666a7397aa98293707a1ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c6ce6607c3ad9ae32ea4f16a4b1659b5966d0e6e17666a7397aa98293707a1ba->enter($__internal_c6ce6607c3ad9ae32ea4f16a4b1659b5966d0e6e17666a7397aa98293707a1ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        $__internal_a4f06df4f49893c1dd9d85ffadcd3b0842a7b4b93abc58f07f91851ef7f48147 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a4f06df4f49893c1dd9d85ffadcd3b0842a7b4b93abc58f07f91851ef7f48147->enter($__internal_a4f06df4f49893c1dd9d85ffadcd3b0842a7b4b93abc58f07f91851ef7f48147_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        // line 76
        if ((($context["widget"] ?? $this->getContext($context, "widget")) == "single_text")) {
            // line 77
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 79
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-inline"))));
            // line 80
            if (( !array_key_exists("datetime", $context) || (false == ($context["datetime"] ?? $this->getContext($context, "datetime"))))) {
                // line 81
                echo "<div ";
                $this->displayBlock("widget_container_attributes", $context, $blocks);
                echo ">";
            }
            // line 83
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "hour", array()), 'widget');
            if (($context["with_minutes"] ?? $this->getContext($context, "with_minutes"))) {
                echo ":";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "minute", array()), 'widget');
            }
            if (($context["with_seconds"] ?? $this->getContext($context, "with_seconds"))) {
                echo ":";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "second", array()), 'widget');
            }
            // line 84
            echo "        ";
            if (( !array_key_exists("datetime", $context) || (false == ($context["datetime"] ?? $this->getContext($context, "datetime"))))) {
                // line 85
                echo "</div>";
            }
        }
        
        $__internal_a4f06df4f49893c1dd9d85ffadcd3b0842a7b4b93abc58f07f91851ef7f48147->leave($__internal_a4f06df4f49893c1dd9d85ffadcd3b0842a7b4b93abc58f07f91851ef7f48147_prof);

        
        $__internal_c6ce6607c3ad9ae32ea4f16a4b1659b5966d0e6e17666a7397aa98293707a1ba->leave($__internal_c6ce6607c3ad9ae32ea4f16a4b1659b5966d0e6e17666a7397aa98293707a1ba_prof);

    }

    // line 90
    public function block_dateinterval_widget($context, array $blocks = array())
    {
        $__internal_dc279e7f949da2bdca67bd8bb15c076534be8d90eb299f9f9cd0d96663f5696f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dc279e7f949da2bdca67bd8bb15c076534be8d90eb299f9f9cd0d96663f5696f->enter($__internal_dc279e7f949da2bdca67bd8bb15c076534be8d90eb299f9f9cd0d96663f5696f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "dateinterval_widget"));

        $__internal_fb67671d39cd12278ea3bbe587f33a43a868447f41b44c79470b3023cb7774a4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fb67671d39cd12278ea3bbe587f33a43a868447f41b44c79470b3023cb7774a4->enter($__internal_fb67671d39cd12278ea3bbe587f33a43a868447f41b44c79470b3023cb7774a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "dateinterval_widget"));

        // line 91
        if ((($context["widget"] ?? $this->getContext($context, "widget")) == "single_text")) {
            // line 92
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 94
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-inline"))));
            // line 95
            echo "<div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">";
            // line 96
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
            // line 97
            if (($context["with_years"] ?? $this->getContext($context, "with_years"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "years", array()), 'widget');
            }
            // line 98
            if (($context["with_months"] ?? $this->getContext($context, "with_months"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "months", array()), 'widget');
            }
            // line 99
            if (($context["with_weeks"] ?? $this->getContext($context, "with_weeks"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "weeks", array()), 'widget');
            }
            // line 100
            if (($context["with_days"] ?? $this->getContext($context, "with_days"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "days", array()), 'widget');
            }
            // line 101
            if (($context["with_hours"] ?? $this->getContext($context, "with_hours"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "hours", array()), 'widget');
            }
            // line 102
            if (($context["with_minutes"] ?? $this->getContext($context, "with_minutes"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "minutes", array()), 'widget');
            }
            // line 103
            if (($context["with_seconds"] ?? $this->getContext($context, "with_seconds"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "seconds", array()), 'widget');
            }
            // line 104
            if (($context["with_invert"] ?? $this->getContext($context, "with_invert"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "invert", array()), 'widget');
            }
            // line 105
            echo "</div>";
        }
        
        $__internal_fb67671d39cd12278ea3bbe587f33a43a868447f41b44c79470b3023cb7774a4->leave($__internal_fb67671d39cd12278ea3bbe587f33a43a868447f41b44c79470b3023cb7774a4_prof);

        
        $__internal_dc279e7f949da2bdca67bd8bb15c076534be8d90eb299f9f9cd0d96663f5696f->leave($__internal_dc279e7f949da2bdca67bd8bb15c076534be8d90eb299f9f9cd0d96663f5696f_prof);

    }

    // line 109
    public function block_choice_widget_collapsed($context, array $blocks = array())
    {
        $__internal_b1e101bf38db80d9cc1e280cbc9473032dc5a5743e9e3497201b2e5e09a2a7da = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b1e101bf38db80d9cc1e280cbc9473032dc5a5743e9e3497201b2e5e09a2a7da->enter($__internal_b1e101bf38db80d9cc1e280cbc9473032dc5a5743e9e3497201b2e5e09a2a7da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_collapsed"));

        $__internal_133b30847641c23564a08ecfde302771b4282b4c00783e9d711d3912827e929d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_133b30847641c23564a08ecfde302771b4282b4c00783e9d711d3912827e929d->enter($__internal_133b30847641c23564a08ecfde302771b4282b4c00783e9d711d3912827e929d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_collapsed"));

        // line 110
        $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => trim(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control"))));
        // line 111
        $this->displayParentBlock("choice_widget_collapsed", $context, $blocks);
        
        $__internal_133b30847641c23564a08ecfde302771b4282b4c00783e9d711d3912827e929d->leave($__internal_133b30847641c23564a08ecfde302771b4282b4c00783e9d711d3912827e929d_prof);

        
        $__internal_b1e101bf38db80d9cc1e280cbc9473032dc5a5743e9e3497201b2e5e09a2a7da->leave($__internal_b1e101bf38db80d9cc1e280cbc9473032dc5a5743e9e3497201b2e5e09a2a7da_prof);

    }

    // line 114
    public function block_choice_widget_expanded($context, array $blocks = array())
    {
        $__internal_0d1b9ce6f52c11dac855fc40c51e957fd49f416c93d0345d572ac2b542bab700 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0d1b9ce6f52c11dac855fc40c51e957fd49f416c93d0345d572ac2b542bab700->enter($__internal_0d1b9ce6f52c11dac855fc40c51e957fd49f416c93d0345d572ac2b542bab700_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        $__internal_d77eeb75e7eaeac90168613a14c70b7c4cadfcfd9c15d8eaeb0ff0f099320d4b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d77eeb75e7eaeac90168613a14c70b7c4cadfcfd9c15d8eaeb0ff0f099320d4b->enter($__internal_d77eeb75e7eaeac90168613a14c70b7c4cadfcfd9c15d8eaeb0ff0f099320d4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        // line 115
        if (twig_in_filter("-inline", (($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")))) {
            // line 116
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 117
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 118
($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")), "translation_domain" =>                 // line 119
($context["choice_translation_domain"] ?? $this->getContext($context, "choice_translation_domain"))));
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 123
            echo "<div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">";
            // line 124
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 125
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 126
($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")), "translation_domain" =>                 // line 127
($context["choice_translation_domain"] ?? $this->getContext($context, "choice_translation_domain"))));
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 130
            echo "</div>";
        }
        
        $__internal_d77eeb75e7eaeac90168613a14c70b7c4cadfcfd9c15d8eaeb0ff0f099320d4b->leave($__internal_d77eeb75e7eaeac90168613a14c70b7c4cadfcfd9c15d8eaeb0ff0f099320d4b_prof);

        
        $__internal_0d1b9ce6f52c11dac855fc40c51e957fd49f416c93d0345d572ac2b542bab700->leave($__internal_0d1b9ce6f52c11dac855fc40c51e957fd49f416c93d0345d572ac2b542bab700_prof);

    }

    // line 134
    public function block_checkbox_widget($context, array $blocks = array())
    {
        $__internal_5e9e8cdbe2a046c142ff72843eb34fce64116f1cda82dc15b11b5c5e1f070d4e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5e9e8cdbe2a046c142ff72843eb34fce64116f1cda82dc15b11b5c5e1f070d4e->enter($__internal_5e9e8cdbe2a046c142ff72843eb34fce64116f1cda82dc15b11b5c5e1f070d4e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        $__internal_9afc6dec26bc1340da4bce8959b452f5fc327fd08d82af0464f3fab54f4e5183 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9afc6dec26bc1340da4bce8959b452f5fc327fd08d82af0464f3fab54f4e5183->enter($__internal_9afc6dec26bc1340da4bce8959b452f5fc327fd08d82af0464f3fab54f4e5183_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        // line 135
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter(($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")), (($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")))) : ((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : (""))));
        // line 136
        if (twig_in_filter("checkbox-inline", ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")))) {
            // line 137
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
        } else {
            // line 139
            echo "<div class=\"checkbox\">";
            // line 140
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
            // line 141
            echo "</div>";
        }
        
        $__internal_9afc6dec26bc1340da4bce8959b452f5fc327fd08d82af0464f3fab54f4e5183->leave($__internal_9afc6dec26bc1340da4bce8959b452f5fc327fd08d82af0464f3fab54f4e5183_prof);

        
        $__internal_5e9e8cdbe2a046c142ff72843eb34fce64116f1cda82dc15b11b5c5e1f070d4e->leave($__internal_5e9e8cdbe2a046c142ff72843eb34fce64116f1cda82dc15b11b5c5e1f070d4e_prof);

    }

    // line 145
    public function block_radio_widget($context, array $blocks = array())
    {
        $__internal_2b6b61a92ab329736a39b41ca2a3e34cd6ad189c2a034922a98377ac8b101eb6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2b6b61a92ab329736a39b41ca2a3e34cd6ad189c2a034922a98377ac8b101eb6->enter($__internal_2b6b61a92ab329736a39b41ca2a3e34cd6ad189c2a034922a98377ac8b101eb6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        $__internal_9d178e2c149ea23a2f07ff5b017ba4cbd5c16b7c22106b49c4800669c25947b8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9d178e2c149ea23a2f07ff5b017ba4cbd5c16b7c22106b49c4800669c25947b8->enter($__internal_9d178e2c149ea23a2f07ff5b017ba4cbd5c16b7c22106b49c4800669c25947b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        // line 146
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter(($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")), (($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")))) : ((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : (""))));
        // line 147
        if (twig_in_filter("radio-inline", ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")))) {
            // line 148
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
        } else {
            // line 150
            echo "<div class=\"radio\">";
            // line 151
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
            // line 152
            echo "</div>";
        }
        
        $__internal_9d178e2c149ea23a2f07ff5b017ba4cbd5c16b7c22106b49c4800669c25947b8->leave($__internal_9d178e2c149ea23a2f07ff5b017ba4cbd5c16b7c22106b49c4800669c25947b8_prof);

        
        $__internal_2b6b61a92ab329736a39b41ca2a3e34cd6ad189c2a034922a98377ac8b101eb6->leave($__internal_2b6b61a92ab329736a39b41ca2a3e34cd6ad189c2a034922a98377ac8b101eb6_prof);

    }

    // line 158
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_57143fe832a4e8b619a60d3c5a918a13df1a34feea1064318ad1796e2203503e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_57143fe832a4e8b619a60d3c5a918a13df1a34feea1064318ad1796e2203503e->enter($__internal_57143fe832a4e8b619a60d3c5a918a13df1a34feea1064318ad1796e2203503e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_4beb64841837ef7d98d3a85a8ca1f09f7e001f7affd27f7a529ddd83668f2de2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4beb64841837ef7d98d3a85a8ca1f09f7e001f7affd27f7a529ddd83668f2de2->enter($__internal_4beb64841837ef7d98d3a85a8ca1f09f7e001f7affd27f7a529ddd83668f2de2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 159
        $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => trim(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " control-label"))));
        // line 160
        $this->displayParentBlock("form_label", $context, $blocks);
        
        $__internal_4beb64841837ef7d98d3a85a8ca1f09f7e001f7affd27f7a529ddd83668f2de2->leave($__internal_4beb64841837ef7d98d3a85a8ca1f09f7e001f7affd27f7a529ddd83668f2de2_prof);

        
        $__internal_57143fe832a4e8b619a60d3c5a918a13df1a34feea1064318ad1796e2203503e->leave($__internal_57143fe832a4e8b619a60d3c5a918a13df1a34feea1064318ad1796e2203503e_prof);

    }

    // line 163
    public function block_choice_label($context, array $blocks = array())
    {
        $__internal_7ec6d97ea507f1e427461ec9e3923af29138da2cccb9d08ae6377b101c73abde = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7ec6d97ea507f1e427461ec9e3923af29138da2cccb9d08ae6377b101c73abde->enter($__internal_7ec6d97ea507f1e427461ec9e3923af29138da2cccb9d08ae6377b101c73abde_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_label"));

        $__internal_d42af11acc9f3b59418aa8f0e83657596e9aa0466c52994cb033da9cbadbc8b5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d42af11acc9f3b59418aa8f0e83657596e9aa0466c52994cb033da9cbadbc8b5->enter($__internal_d42af11acc9f3b59418aa8f0e83657596e9aa0466c52994cb033da9cbadbc8b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_label"));

        // line 165
        $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => trim(twig_replace_filter((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")), array("checkbox-inline" => "", "radio-inline" => "")))));
        // line 166
        $this->displayBlock("form_label", $context, $blocks);
        
        $__internal_d42af11acc9f3b59418aa8f0e83657596e9aa0466c52994cb033da9cbadbc8b5->leave($__internal_d42af11acc9f3b59418aa8f0e83657596e9aa0466c52994cb033da9cbadbc8b5_prof);

        
        $__internal_7ec6d97ea507f1e427461ec9e3923af29138da2cccb9d08ae6377b101c73abde->leave($__internal_7ec6d97ea507f1e427461ec9e3923af29138da2cccb9d08ae6377b101c73abde_prof);

    }

    // line 169
    public function block_checkbox_label($context, array $blocks = array())
    {
        $__internal_07d29be60865d1913db5b4cb69e16b8fca88a951a4c145d7dfdcc7e84f89f632 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_07d29be60865d1913db5b4cb69e16b8fca88a951a4c145d7dfdcc7e84f89f632->enter($__internal_07d29be60865d1913db5b4cb69e16b8fca88a951a4c145d7dfdcc7e84f89f632_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_label"));

        $__internal_a184a880d40c8fafd60871c88ef6390c8e45623cf835700ecbcd1eabeaafadb7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a184a880d40c8fafd60871c88ef6390c8e45623cf835700ecbcd1eabeaafadb7->enter($__internal_a184a880d40c8fafd60871c88ef6390c8e45623cf835700ecbcd1eabeaafadb7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_label"));

        // line 170
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_a184a880d40c8fafd60871c88ef6390c8e45623cf835700ecbcd1eabeaafadb7->leave($__internal_a184a880d40c8fafd60871c88ef6390c8e45623cf835700ecbcd1eabeaafadb7_prof);

        
        $__internal_07d29be60865d1913db5b4cb69e16b8fca88a951a4c145d7dfdcc7e84f89f632->leave($__internal_07d29be60865d1913db5b4cb69e16b8fca88a951a4c145d7dfdcc7e84f89f632_prof);

    }

    // line 173
    public function block_radio_label($context, array $blocks = array())
    {
        $__internal_99b234fea42f3639d1ead1784e220f0acb2beb88d80419f2c1537cd20051456c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_99b234fea42f3639d1ead1784e220f0acb2beb88d80419f2c1537cd20051456c->enter($__internal_99b234fea42f3639d1ead1784e220f0acb2beb88d80419f2c1537cd20051456c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_label"));

        $__internal_249037720b54f4e549e5e66ad5ada22c6ec79afd186a24f8f07af5af17c2f746 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_249037720b54f4e549e5e66ad5ada22c6ec79afd186a24f8f07af5af17c2f746->enter($__internal_249037720b54f4e549e5e66ad5ada22c6ec79afd186a24f8f07af5af17c2f746_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_label"));

        // line 174
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_249037720b54f4e549e5e66ad5ada22c6ec79afd186a24f8f07af5af17c2f746->leave($__internal_249037720b54f4e549e5e66ad5ada22c6ec79afd186a24f8f07af5af17c2f746_prof);

        
        $__internal_99b234fea42f3639d1ead1784e220f0acb2beb88d80419f2c1537cd20051456c->leave($__internal_99b234fea42f3639d1ead1784e220f0acb2beb88d80419f2c1537cd20051456c_prof);

    }

    // line 177
    public function block_checkbox_radio_label($context, array $blocks = array())
    {
        $__internal_f4688af213957860044e315cb34395306865f2b8da9fadb79697035d87ca50b1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f4688af213957860044e315cb34395306865f2b8da9fadb79697035d87ca50b1->enter($__internal_f4688af213957860044e315cb34395306865f2b8da9fadb79697035d87ca50b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        $__internal_768328bec2ccbd05859b3e57ee65d60c2f9b312ba746fbd92cedbcb7c39e7599 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_768328bec2ccbd05859b3e57ee65d60c2f9b312ba746fbd92cedbcb7c39e7599->enter($__internal_768328bec2ccbd05859b3e57ee65d60c2f9b312ba746fbd92cedbcb7c39e7599_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        // line 178
        echo "    ";
        // line 179
        echo "    ";
        if (array_key_exists("widget", $context)) {
            // line 180
            echo "        ";
            if (($context["required"] ?? $this->getContext($context, "required"))) {
                // line 181
                echo "            ";
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => trim(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " required"))));
                // line 182
                echo "        ";
            }
            // line 183
            echo "        ";
            if (array_key_exists("parent_label_class", $context)) {
                // line 184
                echo "            ";
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => trim((((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " ") . ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class"))))));
                // line 185
                echo "        ";
            }
            // line 186
            echo "        ";
            if (( !(($context["label"] ?? $this->getContext($context, "label")) === false) && twig_test_empty(($context["label"] ?? $this->getContext($context, "label"))))) {
                // line 187
                if ( !twig_test_empty(($context["label_format"] ?? $this->getContext($context, "label_format")))) {
                    // line 188
                    $context["label"] = twig_replace_filter(($context["label_format"] ?? $this->getContext($context, "label_format")), array("%name%" =>                     // line 189
($context["name"] ?? $this->getContext($context, "name")), "%id%" =>                     // line 190
($context["id"] ?? $this->getContext($context, "id"))));
                } else {
                    // line 193
                    $context["label"] = $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->humanize(($context["name"] ?? $this->getContext($context, "name")));
                }
            }
            // line 196
            echo "        <label";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["label_attr"] ?? $this->getContext($context, "label_attr")));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">";
            // line 197
            echo ($context["widget"] ?? $this->getContext($context, "widget"));
            echo " ";
            echo twig_escape_filter($this->env, (( !(($context["label"] ?? $this->getContext($context, "label")) === false)) ? ((((($context["translation_domain"] ?? $this->getContext($context, "translation_domain")) === false)) ? (($context["label"] ?? $this->getContext($context, "label"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(($context["label"] ?? $this->getContext($context, "label")), array(), ($context["translation_domain"] ?? $this->getContext($context, "translation_domain")))))) : ("")), "html", null, true);
            // line 198
            echo "</label>
    ";
        }
        
        $__internal_768328bec2ccbd05859b3e57ee65d60c2f9b312ba746fbd92cedbcb7c39e7599->leave($__internal_768328bec2ccbd05859b3e57ee65d60c2f9b312ba746fbd92cedbcb7c39e7599_prof);

        
        $__internal_f4688af213957860044e315cb34395306865f2b8da9fadb79697035d87ca50b1->leave($__internal_f4688af213957860044e315cb34395306865f2b8da9fadb79697035d87ca50b1_prof);

    }

    // line 204
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_298a02b10f87e630050c9c13d2afd85cb0d25cf3089958d97e1e387b34033c79 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_298a02b10f87e630050c9c13d2afd85cb0d25cf3089958d97e1e387b34033c79->enter($__internal_298a02b10f87e630050c9c13d2afd85cb0d25cf3089958d97e1e387b34033c79_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_01d3389702f300c158218301e4066033be5e6a3226c6bd9e92b84e212030752c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_01d3389702f300c158218301e4066033be5e6a3226c6bd9e92b84e212030752c->enter($__internal_01d3389702f300c158218301e4066033be5e6a3226c6bd9e92b84e212030752c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 205
        echo "<div class=\"form-group";
        if ((( !($context["compound"] ?? $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter(($context["force_error"] ?? $this->getContext($context, "force_error")), false)) : (false))) &&  !($context["valid"] ?? $this->getContext($context, "valid")))) {
            echo " has-error";
        }
        echo "\">";
        // line 206
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label');
        // line 207
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 208
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 209
        echo "</div>";
        
        $__internal_01d3389702f300c158218301e4066033be5e6a3226c6bd9e92b84e212030752c->leave($__internal_01d3389702f300c158218301e4066033be5e6a3226c6bd9e92b84e212030752c_prof);

        
        $__internal_298a02b10f87e630050c9c13d2afd85cb0d25cf3089958d97e1e387b34033c79->leave($__internal_298a02b10f87e630050c9c13d2afd85cb0d25cf3089958d97e1e387b34033c79_prof);

    }

    // line 212
    public function block_button_row($context, array $blocks = array())
    {
        $__internal_a97435762c81ce41f2a58bd322630a69f0c02f5df1b9c106e540831dad508eb7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a97435762c81ce41f2a58bd322630a69f0c02f5df1b9c106e540831dad508eb7->enter($__internal_a97435762c81ce41f2a58bd322630a69f0c02f5df1b9c106e540831dad508eb7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        $__internal_251b5b3608c196e32577896b5159dffafe9afd261205f94932360cd755390a2e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_251b5b3608c196e32577896b5159dffafe9afd261205f94932360cd755390a2e->enter($__internal_251b5b3608c196e32577896b5159dffafe9afd261205f94932360cd755390a2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        // line 213
        echo "<div class=\"form-group\">";
        // line 214
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 215
        echo "</div>";
        
        $__internal_251b5b3608c196e32577896b5159dffafe9afd261205f94932360cd755390a2e->leave($__internal_251b5b3608c196e32577896b5159dffafe9afd261205f94932360cd755390a2e_prof);

        
        $__internal_a97435762c81ce41f2a58bd322630a69f0c02f5df1b9c106e540831dad508eb7->leave($__internal_a97435762c81ce41f2a58bd322630a69f0c02f5df1b9c106e540831dad508eb7_prof);

    }

    // line 218
    public function block_choice_row($context, array $blocks = array())
    {
        $__internal_e15ee64a20534013d6bcf8347b4ee4e86287bf5422f78abe45a51c87ea04ce6c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e15ee64a20534013d6bcf8347b4ee4e86287bf5422f78abe45a51c87ea04ce6c->enter($__internal_e15ee64a20534013d6bcf8347b4ee4e86287bf5422f78abe45a51c87ea04ce6c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_row"));

        $__internal_fc44fbf9d2d1bceaade06ccef2ad761df1846683010d22e7128d7d5308da9ea8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fc44fbf9d2d1bceaade06ccef2ad761df1846683010d22e7128d7d5308da9ea8->enter($__internal_fc44fbf9d2d1bceaade06ccef2ad761df1846683010d22e7128d7d5308da9ea8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_row"));

        // line 219
        $context["force_error"] = true;
        // line 220
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_fc44fbf9d2d1bceaade06ccef2ad761df1846683010d22e7128d7d5308da9ea8->leave($__internal_fc44fbf9d2d1bceaade06ccef2ad761df1846683010d22e7128d7d5308da9ea8_prof);

        
        $__internal_e15ee64a20534013d6bcf8347b4ee4e86287bf5422f78abe45a51c87ea04ce6c->leave($__internal_e15ee64a20534013d6bcf8347b4ee4e86287bf5422f78abe45a51c87ea04ce6c_prof);

    }

    // line 223
    public function block_date_row($context, array $blocks = array())
    {
        $__internal_1150c6580355db6f4a42112a67dd131896489ff4d7edf79e8d9d0af5a0ff7237 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1150c6580355db6f4a42112a67dd131896489ff4d7edf79e8d9d0af5a0ff7237->enter($__internal_1150c6580355db6f4a42112a67dd131896489ff4d7edf79e8d9d0af5a0ff7237_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_row"));

        $__internal_2c3c6270b0bddcb4668d3bfe6b2d178a4e5a22568c3b51d01afb013521e015b3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2c3c6270b0bddcb4668d3bfe6b2d178a4e5a22568c3b51d01afb013521e015b3->enter($__internal_2c3c6270b0bddcb4668d3bfe6b2d178a4e5a22568c3b51d01afb013521e015b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_row"));

        // line 224
        $context["force_error"] = true;
        // line 225
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_2c3c6270b0bddcb4668d3bfe6b2d178a4e5a22568c3b51d01afb013521e015b3->leave($__internal_2c3c6270b0bddcb4668d3bfe6b2d178a4e5a22568c3b51d01afb013521e015b3_prof);

        
        $__internal_1150c6580355db6f4a42112a67dd131896489ff4d7edf79e8d9d0af5a0ff7237->leave($__internal_1150c6580355db6f4a42112a67dd131896489ff4d7edf79e8d9d0af5a0ff7237_prof);

    }

    // line 228
    public function block_time_row($context, array $blocks = array())
    {
        $__internal_d13a02c8f06c676637fa1da1ddccbe4b356b1e472eec3fa5f5ed410d295a7d78 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d13a02c8f06c676637fa1da1ddccbe4b356b1e472eec3fa5f5ed410d295a7d78->enter($__internal_d13a02c8f06c676637fa1da1ddccbe4b356b1e472eec3fa5f5ed410d295a7d78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_row"));

        $__internal_3fb8803b70d805d8610410fb1649c4f23707410537d5e84f19ad2208578ec0bc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3fb8803b70d805d8610410fb1649c4f23707410537d5e84f19ad2208578ec0bc->enter($__internal_3fb8803b70d805d8610410fb1649c4f23707410537d5e84f19ad2208578ec0bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_row"));

        // line 229
        $context["force_error"] = true;
        // line 230
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_3fb8803b70d805d8610410fb1649c4f23707410537d5e84f19ad2208578ec0bc->leave($__internal_3fb8803b70d805d8610410fb1649c4f23707410537d5e84f19ad2208578ec0bc_prof);

        
        $__internal_d13a02c8f06c676637fa1da1ddccbe4b356b1e472eec3fa5f5ed410d295a7d78->leave($__internal_d13a02c8f06c676637fa1da1ddccbe4b356b1e472eec3fa5f5ed410d295a7d78_prof);

    }

    // line 233
    public function block_datetime_row($context, array $blocks = array())
    {
        $__internal_364f147b727b038e904349965a9794a5191a89041cbb6e2536478b86f4abe343 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_364f147b727b038e904349965a9794a5191a89041cbb6e2536478b86f4abe343->enter($__internal_364f147b727b038e904349965a9794a5191a89041cbb6e2536478b86f4abe343_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_row"));

        $__internal_a391181b6955155a2ededaf6462d67bc4d393f71a0e05ac932ef633666ed3e42 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a391181b6955155a2ededaf6462d67bc4d393f71a0e05ac932ef633666ed3e42->enter($__internal_a391181b6955155a2ededaf6462d67bc4d393f71a0e05ac932ef633666ed3e42_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_row"));

        // line 234
        $context["force_error"] = true;
        // line 235
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_a391181b6955155a2ededaf6462d67bc4d393f71a0e05ac932ef633666ed3e42->leave($__internal_a391181b6955155a2ededaf6462d67bc4d393f71a0e05ac932ef633666ed3e42_prof);

        
        $__internal_364f147b727b038e904349965a9794a5191a89041cbb6e2536478b86f4abe343->leave($__internal_364f147b727b038e904349965a9794a5191a89041cbb6e2536478b86f4abe343_prof);

    }

    // line 238
    public function block_checkbox_row($context, array $blocks = array())
    {
        $__internal_7266f266f0e3be91ace267603c5377055193bd30a4067779a6fab57a96afbcd2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7266f266f0e3be91ace267603c5377055193bd30a4067779a6fab57a96afbcd2->enter($__internal_7266f266f0e3be91ace267603c5377055193bd30a4067779a6fab57a96afbcd2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        $__internal_6c5eb00a17ce0b3b8e9b6a12be8c3e8c49cb201d57f322a5fe1baeb3f5d2e267 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6c5eb00a17ce0b3b8e9b6a12be8c3e8c49cb201d57f322a5fe1baeb3f5d2e267->enter($__internal_6c5eb00a17ce0b3b8e9b6a12be8c3e8c49cb201d57f322a5fe1baeb3f5d2e267_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        // line 239
        echo "<div class=\"form-group";
        if ( !($context["valid"] ?? $this->getContext($context, "valid"))) {
            echo " has-error";
        }
        echo "\">";
        // line 240
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 241
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 242
        echo "</div>";
        
        $__internal_6c5eb00a17ce0b3b8e9b6a12be8c3e8c49cb201d57f322a5fe1baeb3f5d2e267->leave($__internal_6c5eb00a17ce0b3b8e9b6a12be8c3e8c49cb201d57f322a5fe1baeb3f5d2e267_prof);

        
        $__internal_7266f266f0e3be91ace267603c5377055193bd30a4067779a6fab57a96afbcd2->leave($__internal_7266f266f0e3be91ace267603c5377055193bd30a4067779a6fab57a96afbcd2_prof);

    }

    // line 245
    public function block_radio_row($context, array $blocks = array())
    {
        $__internal_01f60883e22ad4e7472a592be4db0f62bde0946d4117591ae59987dffbff5349 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_01f60883e22ad4e7472a592be4db0f62bde0946d4117591ae59987dffbff5349->enter($__internal_01f60883e22ad4e7472a592be4db0f62bde0946d4117591ae59987dffbff5349_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        $__internal_9cdf03afbd6ed5fa8e5aaf56b511a3acafaf63904baa8a138986ea5eb8c78d90 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9cdf03afbd6ed5fa8e5aaf56b511a3acafaf63904baa8a138986ea5eb8c78d90->enter($__internal_9cdf03afbd6ed5fa8e5aaf56b511a3acafaf63904baa8a138986ea5eb8c78d90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        // line 246
        echo "<div class=\"form-group";
        if ( !($context["valid"] ?? $this->getContext($context, "valid"))) {
            echo " has-error";
        }
        echo "\">";
        // line 247
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 248
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 249
        echo "</div>";
        
        $__internal_9cdf03afbd6ed5fa8e5aaf56b511a3acafaf63904baa8a138986ea5eb8c78d90->leave($__internal_9cdf03afbd6ed5fa8e5aaf56b511a3acafaf63904baa8a138986ea5eb8c78d90_prof);

        
        $__internal_01f60883e22ad4e7472a592be4db0f62bde0946d4117591ae59987dffbff5349->leave($__internal_01f60883e22ad4e7472a592be4db0f62bde0946d4117591ae59987dffbff5349_prof);

    }

    // line 254
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_e06da351fce4a3a428a7f8855d4d95ef1c09b556f561647431c9d9dd827c03bf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e06da351fce4a3a428a7f8855d4d95ef1c09b556f561647431c9d9dd827c03bf->enter($__internal_e06da351fce4a3a428a7f8855d4d95ef1c09b556f561647431c9d9dd827c03bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        $__internal_aa572dfc5649d824e9fc6e9f7b546b2a552f764528505d2c1ea0370a12ebce9a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aa572dfc5649d824e9fc6e9f7b546b2a552f764528505d2c1ea0370a12ebce9a->enter($__internal_aa572dfc5649d824e9fc6e9f7b546b2a552f764528505d2c1ea0370a12ebce9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 255
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 256
            if ($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "parent", array())) {
                echo "<span class=\"help-block\">";
            } else {
                echo "<div class=\"alert alert-danger\">";
            }
            // line 257
            echo "    <ul class=\"list-unstyled\">";
            // line 258
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? $this->getContext($context, "errors")));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 259
                echo "<li><span class=\"glyphicon glyphicon-exclamation-sign\"></span> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "</li>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 261
            echo "</ul>
    ";
            // line 262
            if ($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "parent", array())) {
                echo "</span>";
            } else {
                echo "</div>";
            }
        }
        
        $__internal_aa572dfc5649d824e9fc6e9f7b546b2a552f764528505d2c1ea0370a12ebce9a->leave($__internal_aa572dfc5649d824e9fc6e9f7b546b2a552f764528505d2c1ea0370a12ebce9a_prof);

        
        $__internal_e06da351fce4a3a428a7f8855d4d95ef1c09b556f561647431c9d9dd827c03bf->leave($__internal_e06da351fce4a3a428a7f8855d4d95ef1c09b556f561647431c9d9dd827c03bf_prof);

    }

    public function getTemplateName()
    {
        return "bootstrap_3_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  1061 => 262,  1058 => 261,  1050 => 259,  1046 => 258,  1044 => 257,  1038 => 256,  1036 => 255,  1027 => 254,  1017 => 249,  1015 => 248,  1013 => 247,  1007 => 246,  998 => 245,  988 => 242,  986 => 241,  984 => 240,  978 => 239,  969 => 238,  959 => 235,  957 => 234,  948 => 233,  938 => 230,  936 => 229,  927 => 228,  917 => 225,  915 => 224,  906 => 223,  896 => 220,  894 => 219,  885 => 218,  875 => 215,  873 => 214,  871 => 213,  862 => 212,  852 => 209,  850 => 208,  848 => 207,  846 => 206,  840 => 205,  831 => 204,  819 => 198,  815 => 197,  800 => 196,  796 => 193,  793 => 190,  792 => 189,  791 => 188,  789 => 187,  786 => 186,  783 => 185,  780 => 184,  777 => 183,  774 => 182,  771 => 181,  768 => 180,  765 => 179,  763 => 178,  754 => 177,  744 => 174,  735 => 173,  725 => 170,  716 => 169,  706 => 166,  704 => 165,  695 => 163,  685 => 160,  683 => 159,  674 => 158,  663 => 152,  661 => 151,  659 => 150,  656 => 148,  654 => 147,  652 => 146,  643 => 145,  632 => 141,  630 => 140,  628 => 139,  625 => 137,  623 => 136,  621 => 135,  612 => 134,  601 => 130,  595 => 127,  594 => 126,  593 => 125,  589 => 124,  585 => 123,  578 => 119,  577 => 118,  576 => 117,  572 => 116,  570 => 115,  561 => 114,  551 => 111,  549 => 110,  540 => 109,  529 => 105,  525 => 104,  521 => 103,  517 => 102,  513 => 101,  509 => 100,  505 => 99,  501 => 98,  497 => 97,  495 => 96,  491 => 95,  489 => 94,  486 => 92,  484 => 91,  475 => 90,  463 => 85,  460 => 84,  450 => 83,  445 => 81,  443 => 80,  441 => 79,  438 => 77,  436 => 76,  427 => 75,  415 => 70,  413 => 69,  411 => 67,  410 => 66,  409 => 65,  408 => 64,  403 => 62,  401 => 61,  399 => 60,  396 => 58,  394 => 57,  385 => 56,  374 => 52,  372 => 51,  370 => 50,  368 => 49,  366 => 48,  362 => 47,  360 => 46,  357 => 44,  355 => 43,  346 => 42,  335 => 38,  333 => 37,  331 => 36,  322 => 35,  312 => 32,  306 => 30,  304 => 29,  302 => 28,  296 => 26,  293 => 25,  291 => 24,  288 => 23,  279 => 22,  269 => 19,  267 => 18,  258 => 17,  248 => 14,  246 => 13,  237 => 12,  227 => 9,  224 => 7,  222 => 6,  213 => 5,  203 => 254,  200 => 253,  197 => 251,  195 => 245,  192 => 244,  190 => 238,  187 => 237,  185 => 233,  182 => 232,  180 => 228,  177 => 227,  175 => 223,  172 => 222,  170 => 218,  167 => 217,  165 => 212,  162 => 211,  160 => 204,  157 => 203,  154 => 201,  152 => 177,  149 => 176,  147 => 173,  144 => 172,  142 => 169,  139 => 168,  137 => 163,  134 => 162,  132 => 158,  129 => 157,  126 => 155,  124 => 145,  121 => 144,  119 => 134,  116 => 133,  114 => 114,  111 => 113,  109 => 109,  107 => 90,  105 => 75,  102 => 74,  100 => 56,  97 => 55,  95 => 42,  92 => 41,  90 => 35,  87 => 34,  85 => 22,  82 => 21,  80 => 17,  77 => 16,  75 => 12,  72 => 11,  70 => 5,  67 => 4,  64 => 2,  14 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"form_div_layout.html.twig\" %}

{# Widgets #}

{% block form_widget_simple -%}
    {% if type is not defined or type not in ['file', 'hidden'] %}
        {%- set attr = attr|merge({class: (attr.class|default('') ~ ' form-control')|trim}) -%}
    {% endif %}
    {{- parent() -}}
{%- endblock form_widget_simple %}

{% block textarea_widget -%}
    {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-control')|trim}) %}
    {{- parent() -}}
{%- endblock textarea_widget %}

{% block button_widget -%}
    {% set attr = attr|merge({class: (attr.class|default('btn-default') ~ ' btn')|trim}) %}
    {{- parent() -}}
{%- endblock %}

{% block money_widget -%}
    <div class=\"input-group\">
        {% set append = money_pattern starts with '{{' %}
        {% if not append %}
            <span class=\"input-group-addon\">{{ money_pattern|replace({ '{{ widget }}':''}) }}</span>
        {% endif %}
        {{- block('form_widget_simple') -}}
        {% if append %}
            <span class=\"input-group-addon\">{{ money_pattern|replace({ '{{ widget }}':''}) }}</span>
        {% endif %}
    </div>
{%- endblock money_widget %}

{% block percent_widget -%}
    <div class=\"input-group\">
        {{- block('form_widget_simple') -}}
        <span class=\"input-group-addon\">%</span>
    </div>
{%- endblock percent_widget %}

{% block datetime_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-inline')|trim}) -%}
        <div {{ block('widget_container_attributes') }}>
            {{- form_errors(form.date) -}}
            {{- form_errors(form.time) -}}
            {{- form_widget(form.date, { datetime: true } ) -}}
            {{- form_widget(form.time, { datetime: true } ) -}}
        </div>
    {%- endif %}
{%- endblock datetime_widget %}

{% block date_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-inline')|trim}) -%}
        {% if datetime is not defined or not datetime -%}
            <div {{ block('widget_container_attributes') -}}>
        {%- endif %}
            {{- date_pattern|replace({
                '{{ year }}': form_widget(form.year),
                '{{ month }}': form_widget(form.month),
                '{{ day }}': form_widget(form.day),
            })|raw -}}
        {% if datetime is not defined or not datetime -%}
            </div>
        {%- endif -%}
    {% endif %}
{%- endblock date_widget %}

{% block time_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-inline')|trim}) -%}
        {% if datetime is not defined or false == datetime -%}
            <div {{ block('widget_container_attributes') -}}>
        {%- endif -%}
        {{- form_widget(form.hour) }}{% if with_minutes %}:{{ form_widget(form.minute) }}{% endif %}{% if with_seconds %}:{{ form_widget(form.second) }}{% endif %}
        {% if datetime is not defined or false == datetime -%}
            </div>
        {%- endif -%}
    {% endif %}
{%- endblock time_widget %}

{%- block dateinterval_widget -%}
    {%- if widget == 'single_text' -%}
        {{- block('form_widget_simple') -}}
    {%- else -%}
        {%- set attr = attr|merge({class: (attr.class|default('') ~ ' form-inline')|trim}) -%}
        <div {{ block('widget_container_attributes') }}>
            {{- form_errors(form) -}}
            {%- if with_years %}{{ form_widget(form.years) }}{% endif -%}
            {%- if with_months %}{{ form_widget(form.months) }}{% endif -%}
            {%- if with_weeks %}{{ form_widget(form.weeks) }}{% endif -%}
            {%- if with_days %}{{ form_widget(form.days) }}{% endif -%}
            {%- if with_hours %}{{ form_widget(form.hours) }}{% endif -%}
            {%- if with_minutes %}{{ form_widget(form.minutes) }}{% endif -%}
            {%- if with_seconds %}{{ form_widget(form.seconds) }}{% endif -%}
            {%- if with_invert %}{{ form_widget(form.invert) }}{% endif -%}
        </div>
    {%- endif -%}
{%- endblock dateinterval_widget -%}

{% block choice_widget_collapsed -%}
    {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-control')|trim}) %}
    {{- parent() -}}
{%- endblock %}

{% block choice_widget_expanded -%}
    {% if '-inline' in label_attr.class|default('') -%}
        {%- for child in form %}
            {{- form_widget(child, {
                parent_label_class: label_attr.class|default(''),
                translation_domain: choice_translation_domain,
            }) -}}
        {% endfor -%}
    {%- else -%}
        <div {{ block('widget_container_attributes') }}>
            {%- for child in form %}
                {{- form_widget(child, {
                    parent_label_class: label_attr.class|default(''),
                    translation_domain: choice_translation_domain,
                }) -}}
            {% endfor -%}
        </div>
    {%- endif %}
{%- endblock choice_widget_expanded %}

{% block checkbox_widget -%}
    {%- set parent_label_class = parent_label_class|default(label_attr.class|default('')) -%}
    {% if 'checkbox-inline' in parent_label_class %}
        {{- form_label(form, null, { widget: parent() }) -}}
    {% else -%}
        <div class=\"checkbox\">
            {{- form_label(form, null, { widget: parent() }) -}}
        </div>
    {%- endif %}
{%- endblock checkbox_widget %}

{% block radio_widget -%}
    {%- set parent_label_class = parent_label_class|default(label_attr.class|default('')) -%}
    {% if 'radio-inline' in parent_label_class %}
        {{- form_label(form, null, { widget: parent() }) -}}
    {% else -%}
        <div class=\"radio\">
            {{- form_label(form, null, { widget: parent() }) -}}
        </div>
    {%- endif %}
{%- endblock radio_widget %}

{# Labels #}

{% block form_label -%}
    {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' control-label')|trim}) -%}
    {{- parent() -}}
{%- endblock form_label %}

{% block choice_label -%}
    {# remove the checkbox-inline and radio-inline class, it's only useful for embed labels #}
    {%- set label_attr = label_attr|merge({class: label_attr.class|default('')|replace({'checkbox-inline': '', 'radio-inline': ''})|trim}) -%}
    {{- block('form_label') -}}
{% endblock %}

{% block checkbox_label -%}
    {{- block('checkbox_radio_label') -}}
{%- endblock checkbox_label %}

{% block radio_label -%}
    {{- block('checkbox_radio_label') -}}
{%- endblock radio_label %}

{% block checkbox_radio_label %}
    {# Do not display the label if widget is not defined in order to prevent double label rendering #}
    {% if widget is defined %}
        {% if required %}
            {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' required')|trim}) %}
        {% endif %}
        {% if parent_label_class is defined %}
            {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' ' ~ parent_label_class)|trim}) %}
        {% endif %}
        {% if label is not same as(false) and label is empty %}
            {%- if label_format is not empty -%}
                {% set label = label_format|replace({
                    '%name%': name,
                    '%id%': id,
                }) %}
            {%- else -%}
                {% set label = name|humanize %}
            {%- endif -%}
        {% endif %}
        <label{% for attrname, attrvalue in label_attr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}>
            {{- widget|raw }} {{ label is not same as(false) ? (translation_domain is same as(false) ? label : label|trans({}, translation_domain)) -}}
        </label>
    {% endif %}
{% endblock checkbox_radio_label %}

{# Rows #}

{% block form_row -%}
    <div class=\"form-group{% if (not compound or force_error|default(false)) and not valid %} has-error{% endif %}\">
        {{- form_label(form) -}}
        {{- form_widget(form) -}}
        {{- form_errors(form) -}}
    </div>
{%- endblock form_row %}

{% block button_row -%}
    <div class=\"form-group\">
        {{- form_widget(form) -}}
    </div>
{%- endblock button_row %}

{% block choice_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock choice_row %}

{% block date_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock date_row %}

{% block time_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock time_row %}

{% block datetime_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock datetime_row %}

{% block checkbox_row -%}
    <div class=\"form-group{% if not valid %} has-error{% endif %}\">
        {{- form_widget(form) -}}
        {{- form_errors(form) -}}
    </div>
{%- endblock checkbox_row %}

{% block radio_row -%}
    <div class=\"form-group{% if not valid %} has-error{% endif %}\">
        {{- form_widget(form) -}}
        {{- form_errors(form) -}}
    </div>
{%- endblock radio_row %}

{# Errors #}

{% block form_errors -%}
    {% if errors|length > 0 -%}
    {% if form.parent %}<span class=\"help-block\">{% else %}<div class=\"alert alert-danger\">{% endif %}
    <ul class=\"list-unstyled\">
        {%- for error in errors -%}
            <li><span class=\"glyphicon glyphicon-exclamation-sign\"></span> {{ error.message }}</li>
        {%- endfor -%}
    </ul>
    {% if form.parent %}</span>{% else %}</div>{% endif %}
    {%- endif %}
{%- endblock form_errors %}
", "bootstrap_3_layout.html.twig", "/var/www/html/GestionStock/vendor/symfony/symfony/src/Symfony/Bridge/Twig/Resources/views/Form/bootstrap_3_layout.html.twig");
    }
}
