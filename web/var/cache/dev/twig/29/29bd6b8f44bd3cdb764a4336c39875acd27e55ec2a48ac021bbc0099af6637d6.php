<?php

/* form/layout.html.twig */
class __TwigTemplate_a96d6a7d4eebe9a45875f6ad7ffd9c604a31a2f783cc08b3b599822b08b0637c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("bootstrap_3_layout.html.twig", "form/layout.html.twig", 1);
        $this->blocks = array(
            'form_errors' => array($this, 'block_form_errors'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "bootstrap_3_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f40eaa007bc03fd8691e67569ecc509f23b84c6f64eb36ed1af98f49b6780706 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f40eaa007bc03fd8691e67569ecc509f23b84c6f64eb36ed1af98f49b6780706->enter($__internal_f40eaa007bc03fd8691e67569ecc509f23b84c6f64eb36ed1af98f49b6780706_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/layout.html.twig"));

        $__internal_0d96dce54b21e555d23e80e7d3ba33f6e48c217e3682af1ac68ece807366d0a6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0d96dce54b21e555d23e80e7d3ba33f6e48c217e3682af1ac68ece807366d0a6->enter($__internal_0d96dce54b21e555d23e80e7d3ba33f6e48c217e3682af1ac68ece807366d0a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f40eaa007bc03fd8691e67569ecc509f23b84c6f64eb36ed1af98f49b6780706->leave($__internal_f40eaa007bc03fd8691e67569ecc509f23b84c6f64eb36ed1af98f49b6780706_prof);

        
        $__internal_0d96dce54b21e555d23e80e7d3ba33f6e48c217e3682af1ac68ece807366d0a6->leave($__internal_0d96dce54b21e555d23e80e7d3ba33f6e48c217e3682af1ac68ece807366d0a6_prof);

    }

    // line 5
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_59a18556d144051857e835471dec4bf32b8b99c72770ccd332f62968a1f80093 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_59a18556d144051857e835471dec4bf32b8b99c72770ccd332f62968a1f80093->enter($__internal_59a18556d144051857e835471dec4bf32b8b99c72770ccd332f62968a1f80093_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        $__internal_7d8452245f90a22d29160240bd8ea7b23c083927098bf5c9bd2ead77b3551369 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d8452245f90a22d29160240bd8ea7b23c083927098bf5c9bd2ead77b3551369->enter($__internal_7d8452245f90a22d29160240bd8ea7b23c083927098bf5c9bd2ead77b3551369_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 6
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 7
            if ($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "parent", array())) {
                echo "<span class=\"help-block\">";
            } else {
                echo "<div class=\"alert alert-danger\">";
            }
            // line 8
            echo "        <ul class=\"list-unstyled\">";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? $this->getContext($context, "errors")));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 11
                echo "            <li><span class=\"fa fa-exclamation-triangle\"></span> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "</li>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 13
            echo "</ul>
        ";
            // line 14
            if ($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "parent", array())) {
                echo "</span>";
            } else {
                echo "</div>";
            }
        }
        
        $__internal_7d8452245f90a22d29160240bd8ea7b23c083927098bf5c9bd2ead77b3551369->leave($__internal_7d8452245f90a22d29160240bd8ea7b23c083927098bf5c9bd2ead77b3551369_prof);

        
        $__internal_59a18556d144051857e835471dec4bf32b8b99c72770ccd332f62968a1f80093->leave($__internal_59a18556d144051857e835471dec4bf32b8b99c72770ccd332f62968a1f80093_prof);

    }

    public function getTemplateName()
    {
        return "form/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 14,  71 => 13,  63 => 11,  59 => 9,  57 => 8,  51 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'bootstrap_3_layout.html.twig' %}

{# Errors #}

{% block form_errors -%}
    {% if errors|length > 0 -%}
        {% if form.parent %}<span class=\"help-block\">{% else %}<div class=\"alert alert-danger\">{% endif %}
        <ul class=\"list-unstyled\">
        {%- for error in errors -%}
            {# use font-awesome icon library #}
            <li><span class=\"fa fa-exclamation-triangle\"></span> {{ error.message }}</li>
        {%- endfor -%}
        </ul>
        {% if form.parent %}</span>{% else %}</div>{% endif %}
    {%- endif %}
{%- endblock form_errors %}
", "form/layout.html.twig", "/var/www/html/GestionStock/app/Resources/views/form/layout.html.twig");
    }
}
