<?php

/* ::base.html.twig */
class __TwigTemplate_6c39ae6c510e4fbc5312b61a0c33c110dae040ead159f8dc2579eb9105f84247 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body_id' => array($this, 'block_body_id'),
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b446d0fa2e40009d343ffbda503af70d849a3ce8bd8cb408b0c1a6aa67ed9db1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b446d0fa2e40009d343ffbda503af70d849a3ce8bd8cb408b0c1a6aa67ed9db1->enter($__internal_b446d0fa2e40009d343ffbda503af70d849a3ce8bd8cb408b0c1a6aa67ed9db1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        $__internal_043c249d0399b139df4c1a9a5c08713e97ff39d925f35a74632605b67bc47078 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_043c249d0399b139df4c1a9a5c08713e97ff39d925f35a74632605b67bc47078->enter($__internal_043c249d0399b139df4c1a9a5c08713e97ff39d925f35a74632605b67bc47078_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 6
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "locale", array()), "html", null, true);
        echo "\">
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>
        <title>";
        // line 11
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"alternate\" type=\"application/rss+xml\" title=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("rss.title"), "html", null, true);
        echo "\" href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_rss");
        echo "\">
        ";
        // line 13
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 22
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>

    <body id=\"";
        // line 25
        $this->displayBlock('body_id', $context, $blocks);
        echo "\">

        ";
        // line 27
        $this->displayBlock('header', $context, $blocks);
        // line 66
        echo "
        <div class=\"container body-container\">
            ";
        // line 68
        $this->displayBlock('body', $context, $blocks);
        // line 87
        echo "        </div>

        ";
        // line 89
        $this->displayBlock('footer', $context, $blocks);
        // line 114
        echo "
        ";
        // line 115
        $this->displayBlock('javascripts', $context, $blocks);
        // line 124
        echo "
        ";
        // line 128
        echo "        <!-- Page rendered on ";
        echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, "now", "long", "long", null, "UTC"), "html", null, true);
        echo " -->
    </body>
</html>
";
        
        $__internal_b446d0fa2e40009d343ffbda503af70d849a3ce8bd8cb408b0c1a6aa67ed9db1->leave($__internal_b446d0fa2e40009d343ffbda503af70d849a3ce8bd8cb408b0c1a6aa67ed9db1_prof);

        
        $__internal_043c249d0399b139df4c1a9a5c08713e97ff39d925f35a74632605b67bc47078->leave($__internal_043c249d0399b139df4c1a9a5c08713e97ff39d925f35a74632605b67bc47078_prof);

    }

    // line 11
    public function block_title($context, array $blocks = array())
    {
        $__internal_1ce498a3b91d71a4b3e27cb4708d1dabab0d9ca656d82a6eebc0234daeb315dc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1ce498a3b91d71a4b3e27cb4708d1dabab0d9ca656d82a6eebc0234daeb315dc->enter($__internal_1ce498a3b91d71a4b3e27cb4708d1dabab0d9ca656d82a6eebc0234daeb315dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_213887a4e8f023dedfe162e45e08b71b7d6cb7f3e7d83f40360f7a98f53a3440 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_213887a4e8f023dedfe162e45e08b71b7d6cb7f3e7d83f40360f7a98f53a3440->enter($__internal_213887a4e8f023dedfe162e45e08b71b7d6cb7f3e7d83f40360f7a98f53a3440_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Symfony Demo application";
        
        $__internal_213887a4e8f023dedfe162e45e08b71b7d6cb7f3e7d83f40360f7a98f53a3440->leave($__internal_213887a4e8f023dedfe162e45e08b71b7d6cb7f3e7d83f40360f7a98f53a3440_prof);

        
        $__internal_1ce498a3b91d71a4b3e27cb4708d1dabab0d9ca656d82a6eebc0234daeb315dc->leave($__internal_1ce498a3b91d71a4b3e27cb4708d1dabab0d9ca656d82a6eebc0234daeb315dc_prof);

    }

    // line 13
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_ad6748ece7cc805c28c553682804c079d3895185db8a3edfb15e1c6f4c23838f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ad6748ece7cc805c28c553682804c079d3895185db8a3edfb15e1c6f4c23838f->enter($__internal_ad6748ece7cc805c28c553682804c079d3895185db8a3edfb15e1c6f4c23838f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_0100619052cc6d57e71d581de2c387d2e704e86654eb9d1478aed40caffee211 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0100619052cc6d57e71d581de2c387d2e704e86654eb9d1478aed40caffee211->enter($__internal_0100619052cc6d57e71d581de2c387d2e704e86654eb9d1478aed40caffee211_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 14
        echo "            <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-flatly-3.3.7.min.css"), "html", null, true);
        echo "\">
            <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/font-awesome-4.6.3.min.css"), "html", null, true);
        echo "\">
            <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/font-lato.css"), "html", null, true);
        echo "\">
            <link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\">
            <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/highlight-solarized-light.css"), "html", null, true);
        echo "\">
            <link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-tagsinput.css"), "html", null, true);
        echo "\">
            <link rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/main.css"), "html", null, true);
        echo "\">
        ";
        
        $__internal_0100619052cc6d57e71d581de2c387d2e704e86654eb9d1478aed40caffee211->leave($__internal_0100619052cc6d57e71d581de2c387d2e704e86654eb9d1478aed40caffee211_prof);

        
        $__internal_ad6748ece7cc805c28c553682804c079d3895185db8a3edfb15e1c6f4c23838f->leave($__internal_ad6748ece7cc805c28c553682804c079d3895185db8a3edfb15e1c6f4c23838f_prof);

    }

    // line 25
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_4ab994c47ce1f868f7e7a55cfad1237c02e2d4747150548baf8cef0ede261206 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4ab994c47ce1f868f7e7a55cfad1237c02e2d4747150548baf8cef0ede261206->enter($__internal_4ab994c47ce1f868f7e7a55cfad1237c02e2d4747150548baf8cef0ede261206_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_8b534a6b33ec1d325a60d6fc3a70e319e378b1c70d7c06d4ab96d7a838051fbf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b534a6b33ec1d325a60d6fc3a70e319e378b1c70d7c06d4ab96d7a838051fbf->enter($__internal_8b534a6b33ec1d325a60d6fc3a70e319e378b1c70d7c06d4ab96d7a838051fbf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        
        $__internal_8b534a6b33ec1d325a60d6fc3a70e319e378b1c70d7c06d4ab96d7a838051fbf->leave($__internal_8b534a6b33ec1d325a60d6fc3a70e319e378b1c70d7c06d4ab96d7a838051fbf_prof);

        
        $__internal_4ab994c47ce1f868f7e7a55cfad1237c02e2d4747150548baf8cef0ede261206->leave($__internal_4ab994c47ce1f868f7e7a55cfad1237c02e2d4747150548baf8cef0ede261206_prof);

    }

    // line 27
    public function block_header($context, array $blocks = array())
    {
        $__internal_c4c3ce7acf34dd824198ff30644b504b2b4e26b9c7eff2fb2bf853043047beb1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c4c3ce7acf34dd824198ff30644b504b2b4e26b9c7eff2fb2bf853043047beb1->enter($__internal_c4c3ce7acf34dd824198ff30644b504b2b4e26b9c7eff2fb2bf853043047beb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_667a425e015730461b2afd4545aeac1f8f5b60b86b39b728525c2f24649b5817 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_667a425e015730461b2afd4545aeac1f8f5b60b86b39b728525c2f24649b5817->enter($__internal_667a425e015730461b2afd4545aeac1f8f5b60b86b39b728525c2f24649b5817_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 28
        echo "            <header>
                <div class=\"navbar navbar-default navbar-static-top\" role=\"navigation\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"#\">
                                Gestion Stock
                            </a>

                            <button type=\"button\" class=\"navbar-toggle\"
                                    data-toggle=\"collapse\"
                                    data-target=\".navbar-collapse\">
                                <span class=\"sr-only\">";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("menu.toggle_nav"), "html", null, true);
        echo "</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                            </button>
                        </div>
                        <div class=\"navbar-collapse collapse\">
                            <ul class=\"nav navbar-nav navbar-right\">

                                <li class=\"dropdown\">
                                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\" id=\"locales\">
                                        <i class=\"fa fa-globe\" aria-hidden=\"true\"></i>
                                        <span class=\"caret\"></span>
                                        <span class=\"sr-only\">";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("menu.choose_language"), "html", null, true);
        echo "</span>
                                    </a>
                                    <ul class=\"dropdown-menu locales\" role=\"menu\" aria-labelledby=\"locales\">
                                        ";
        // line 55
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->env->getExtension('AppBundle\Twig\AppExtension')->getLocales());
        foreach ($context['_seq'] as $context["_key"] => $context["locale"]) {
            // line 56
            echo "                                            <li ";
            if (($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "locale", array()) == $this->getAttribute($context["locale"], "code", array()))) {
                echo "aria-checked=\"true\" class=\"active\"";
            } else {
                echo "aria-checked=\"false\"";
            }
            echo " role=\"menuitem\"><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route", 1 => "blog_index"), "method"), twig_array_merge($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route_params", 1 => array()), "method"), array("_locale" => $this->getAttribute($context["locale"], "code", array())))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($context["locale"], "name", array())), "html", null, true);
            echo "</a></li>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['locale'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        echo "                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        ";
        
        $__internal_667a425e015730461b2afd4545aeac1f8f5b60b86b39b728525c2f24649b5817->leave($__internal_667a425e015730461b2afd4545aeac1f8f5b60b86b39b728525c2f24649b5817_prof);

        
        $__internal_c4c3ce7acf34dd824198ff30644b504b2b4e26b9c7eff2fb2bf853043047beb1->leave($__internal_c4c3ce7acf34dd824198ff30644b504b2b4e26b9c7eff2fb2bf853043047beb1_prof);

    }

    // line 68
    public function block_body($context, array $blocks = array())
    {
        $__internal_7dd9cad73b93118e8c401c50bdad656eea96b5ae82dbe1d58961caa514de6f66 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7dd9cad73b93118e8c401c50bdad656eea96b5ae82dbe1d58961caa514de6f66->enter($__internal_7dd9cad73b93118e8c401c50bdad656eea96b5ae82dbe1d58961caa514de6f66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_ead2b48693a7ad4353363d89e4509bf7e96838c57284029ab99112dd1895d50e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ead2b48693a7ad4353363d89e4509bf7e96838c57284029ab99112dd1895d50e->enter($__internal_ead2b48693a7ad4353363d89e4509bf7e96838c57284029ab99112dd1895d50e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 69
        echo "                <div class=\"row\">
                    <div id=\"main\" class=\"col-sm-9\">
                        ";
        // line 71
        echo twig_include($this->env, $context, "default/_flash_messages.html.twig");
        echo "

                        ";
        // line 73
        $this->displayBlock('main', $context, $blocks);
        // line 74
        echo "                    </div>

                    <div id=\"sidebar\" class=\"col-sm-3\">
                        ";
        // line 77
        $this->displayBlock('sidebar', $context, $blocks);
        // line 84
        echo "                    </div>
                </div>
            ";
        
        $__internal_ead2b48693a7ad4353363d89e4509bf7e96838c57284029ab99112dd1895d50e->leave($__internal_ead2b48693a7ad4353363d89e4509bf7e96838c57284029ab99112dd1895d50e_prof);

        
        $__internal_7dd9cad73b93118e8c401c50bdad656eea96b5ae82dbe1d58961caa514de6f66->leave($__internal_7dd9cad73b93118e8c401c50bdad656eea96b5ae82dbe1d58961caa514de6f66_prof);

    }

    // line 73
    public function block_main($context, array $blocks = array())
    {
        $__internal_0b01bd22b0fa29395f51f99b7eac6faf1cfd11cac12539bfcdd4caebcf355a71 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0b01bd22b0fa29395f51f99b7eac6faf1cfd11cac12539bfcdd4caebcf355a71->enter($__internal_0b01bd22b0fa29395f51f99b7eac6faf1cfd11cac12539bfcdd4caebcf355a71_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_b25c6e9857ac58dfa694fa681e431e3bee5b0ffb9d783fb2b1754cb60f4e4caf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b25c6e9857ac58dfa694fa681e431e3bee5b0ffb9d783fb2b1754cb60f4e4caf->enter($__internal_b25c6e9857ac58dfa694fa681e431e3bee5b0ffb9d783fb2b1754cb60f4e4caf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        
        $__internal_b25c6e9857ac58dfa694fa681e431e3bee5b0ffb9d783fb2b1754cb60f4e4caf->leave($__internal_b25c6e9857ac58dfa694fa681e431e3bee5b0ffb9d783fb2b1754cb60f4e4caf_prof);

        
        $__internal_0b01bd22b0fa29395f51f99b7eac6faf1cfd11cac12539bfcdd4caebcf355a71->leave($__internal_0b01bd22b0fa29395f51f99b7eac6faf1cfd11cac12539bfcdd4caebcf355a71_prof);

    }

    // line 77
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_29ae1823809b6ba8be068afa6a230ef4a08bc750d59bb6650eddf577fadd0e92 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_29ae1823809b6ba8be068afa6a230ef4a08bc750d59bb6650eddf577fadd0e92->enter($__internal_29ae1823809b6ba8be068afa6a230ef4a08bc750d59bb6650eddf577fadd0e92_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_9642c33df037a84ab274191b304c4b552be1e4a5fac5ea547bc2426741053155 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9642c33df037a84ab274191b304c4b552be1e4a5fac5ea547bc2426741053155->enter($__internal_9642c33df037a84ab274191b304c4b552be1e4a5fac5ea547bc2426741053155_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 78
        echo "                            ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragmentStrategy("esi", Symfony\Bridge\Twig\Extension\HttpKernelExtension::controller("FrameworkBundle:Template:template", array("template" => "blog/about.html.twig", "sharedAge" => 600, "_locale" => $this->getAttribute($this->getAttribute(        // line 81
($context["app"] ?? $this->getContext($context, "app")), "request", array()), "locale", array()))));
        // line 82
        echo "
                        ";
        
        $__internal_9642c33df037a84ab274191b304c4b552be1e4a5fac5ea547bc2426741053155->leave($__internal_9642c33df037a84ab274191b304c4b552be1e4a5fac5ea547bc2426741053155_prof);

        
        $__internal_29ae1823809b6ba8be068afa6a230ef4a08bc750d59bb6650eddf577fadd0e92->leave($__internal_29ae1823809b6ba8be068afa6a230ef4a08bc750d59bb6650eddf577fadd0e92_prof);

    }

    // line 89
    public function block_footer($context, array $blocks = array())
    {
        $__internal_a0dba97563eb515bfe91ca8029c1bc2269fca344180e2547c1cf30e19e4b07f2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a0dba97563eb515bfe91ca8029c1bc2269fca344180e2547c1cf30e19e4b07f2->enter($__internal_a0dba97563eb515bfe91ca8029c1bc2269fca344180e2547c1cf30e19e4b07f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        $__internal_89f7ed7136e060242e6dbe8e568beffbde8f7363adf36b3ab7a7103784e21ed2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_89f7ed7136e060242e6dbe8e568beffbde8f7363adf36b3ab7a7103784e21ed2->enter($__internal_89f7ed7136e060242e6dbe8e568beffbde8f7363adf36b3ab7a7103784e21ed2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 90
        echo "            <footer>
                <div class=\"container\">
                    <div class=\"row\">
                        <div id=\"footer-copyright\" class=\"col-md-6\">
                            <p>&copy; ";
        // line 94
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " - Projet Transversal</p>
                            <p>";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("mit_license"), "html", null, true);
        echo "</p>
                        </div>
                        <div id=\"footer-resources\" class=\"col-md-6\">
                            <p>
                                <a href=\"https://twitter.com/symfony\" title=\"Symfony Twitter\">
                                    <i class=\"fa fa-twitter\" aria-hidden=\"true\"></i>
                                </a>
                                <a href=\"https://www.facebook.com/SensioLabs\" title=\"SensioLabs Facebook\">
                                    <i class=\"fa fa-facebook\" aria-hidden=\"true\"></i>
                                </a>
                                <a href=\"https://symfony.com/blog/\" title=\"Symfony Blog\">
                                    <i class=\"fa fa-rss\" aria-hidden=\"true\"></i>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        ";
        
        $__internal_89f7ed7136e060242e6dbe8e568beffbde8f7363adf36b3ab7a7103784e21ed2->leave($__internal_89f7ed7136e060242e6dbe8e568beffbde8f7363adf36b3ab7a7103784e21ed2_prof);

        
        $__internal_a0dba97563eb515bfe91ca8029c1bc2269fca344180e2547c1cf30e19e4b07f2->leave($__internal_a0dba97563eb515bfe91ca8029c1bc2269fca344180e2547c1cf30e19e4b07f2_prof);

    }

    // line 115
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_f6d1ecbeb117f71220cc3895f1033e73c5ca5344e85971a020ce218856de9d2e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f6d1ecbeb117f71220cc3895f1033e73c5ca5344e85971a020ce218856de9d2e->enter($__internal_f6d1ecbeb117f71220cc3895f1033e73c5ca5344e85971a020ce218856de9d2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_2ee97eb1ab6660bba53116f6b3e6955998546129689643aa03e4a7ec0a867359 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2ee97eb1ab6660bba53116f6b3e6955998546129689643aa03e4a7ec0a867359->enter($__internal_2ee97eb1ab6660bba53116f6b3e6955998546129689643aa03e4a7ec0a867359_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 116
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-2.2.4.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 117
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/moment.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 118
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-3.3.7.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/highlight.pack.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 120
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-tagsinput.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 122
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/main.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_2ee97eb1ab6660bba53116f6b3e6955998546129689643aa03e4a7ec0a867359->leave($__internal_2ee97eb1ab6660bba53116f6b3e6955998546129689643aa03e4a7ec0a867359_prof);

        
        $__internal_f6d1ecbeb117f71220cc3895f1033e73c5ca5344e85971a020ce218856de9d2e->leave($__internal_f6d1ecbeb117f71220cc3895f1033e73c5ca5344e85971a020ce218856de9d2e_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  430 => 122,  426 => 121,  422 => 120,  418 => 119,  414 => 118,  410 => 117,  405 => 116,  396 => 115,  367 => 95,  363 => 94,  357 => 90,  348 => 89,  337 => 82,  335 => 81,  333 => 78,  324 => 77,  307 => 73,  295 => 84,  293 => 77,  288 => 74,  286 => 73,  281 => 71,  277 => 69,  268 => 68,  251 => 58,  234 => 56,  230 => 55,  224 => 52,  208 => 39,  195 => 28,  186 => 27,  169 => 25,  157 => 20,  153 => 19,  149 => 18,  145 => 17,  141 => 16,  137 => 15,  132 => 14,  123 => 13,  105 => 11,  90 => 128,  87 => 124,  85 => 115,  82 => 114,  80 => 89,  76 => 87,  74 => 68,  70 => 66,  68 => 27,  63 => 25,  56 => 22,  54 => 13,  48 => 12,  44 => 11,  37 => 7,  34 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
   This is the base template used as the application layout which contains the
   common elements and decorates all the other templates.
   See http://symfony.com/doc/current/book/templating.html#template-inheritance-and-layouts
#}
<!DOCTYPE html>
<html lang=\"{{ app.request.locale }}\">
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>
        <title>{% block title %}Symfony Demo application{% endblock %}</title>
        <link rel=\"alternate\" type=\"application/rss+xml\" title=\"{{ 'rss.title'|trans }}\" href=\"{{ path('blog_rss') }}\">
        {% block stylesheets %}
            <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap-flatly-3.3.7.min.css') }}\">
            <link rel=\"stylesheet\" href=\"{{ asset('css/font-awesome-4.6.3.min.css') }}\">
            <link rel=\"stylesheet\" href=\"{{ asset('css/font-lato.css') }}\">
            <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap-datetimepicker.min.css') }}\">
            <link rel=\"stylesheet\" href=\"{{ asset('css/highlight-solarized-light.css') }}\">
            <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap-tagsinput.css') }}\">
            <link rel=\"stylesheet\" href=\"{{ asset('css/main.css') }}\">
        {% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>

    <body id=\"{% block body_id %}{% endblock %}\">

        {% block header %}
            <header>
                <div class=\"navbar navbar-default navbar-static-top\" role=\"navigation\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"#\">
                                Gestion Stock
                            </a>

                            <button type=\"button\" class=\"navbar-toggle\"
                                    data-toggle=\"collapse\"
                                    data-target=\".navbar-collapse\">
                                <span class=\"sr-only\">{{ 'menu.toggle_nav'|trans }}</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                            </button>
                        </div>
                        <div class=\"navbar-collapse collapse\">
                            <ul class=\"nav navbar-nav navbar-right\">

                                <li class=\"dropdown\">
                                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\" id=\"locales\">
                                        <i class=\"fa fa-globe\" aria-hidden=\"true\"></i>
                                        <span class=\"caret\"></span>
                                        <span class=\"sr-only\">{{ 'menu.choose_language'|trans }}</span>
                                    </a>
                                    <ul class=\"dropdown-menu locales\" role=\"menu\" aria-labelledby=\"locales\">
                                        {% for locale in locales() %}
                                            <li {% if app.request.locale == locale.code %}aria-checked=\"true\" class=\"active\"{%else%}aria-checked=\"false\"{% endif %} role=\"menuitem\"><a href=\"{{ path(app.request.get('_route', 'blog_index'), app.request.get('_route_params', [])|merge({ _locale: locale.code })) }}\">{{ locale.name|capitalize }}</a></li>
                                        {% endfor %}
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        {% endblock %}

        <div class=\"container body-container\">
            {% block body %}
                <div class=\"row\">
                    <div id=\"main\" class=\"col-sm-9\">
                        {{ include('default/_flash_messages.html.twig') }}

                        {% block main %}{% endblock %}
                    </div>

                    <div id=\"sidebar\" class=\"col-sm-3\">
                        {% block sidebar %}
                            {{ render_esi(controller('FrameworkBundle:Template:template', {
                                'template': 'blog/about.html.twig',
                                'sharedAge': 600,
                                '_locale': app.request.locale
                            })) }}
                        {% endblock %}
                    </div>
                </div>
            {% endblock %}
        </div>

        {% block footer %}
            <footer>
                <div class=\"container\">
                    <div class=\"row\">
                        <div id=\"footer-copyright\" class=\"col-md-6\">
                            <p>&copy; {{ 'now'|date('Y') }} - Projet Transversal</p>
                            <p>{{ 'mit_license'|trans }}</p>
                        </div>
                        <div id=\"footer-resources\" class=\"col-md-6\">
                            <p>
                                <a href=\"https://twitter.com/symfony\" title=\"Symfony Twitter\">
                                    <i class=\"fa fa-twitter\" aria-hidden=\"true\"></i>
                                </a>
                                <a href=\"https://www.facebook.com/SensioLabs\" title=\"SensioLabs Facebook\">
                                    <i class=\"fa fa-facebook\" aria-hidden=\"true\"></i>
                                </a>
                                <a href=\"https://symfony.com/blog/\" title=\"Symfony Blog\">
                                    <i class=\"fa fa-rss\" aria-hidden=\"true\"></i>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        {% endblock %}

        {% block javascripts %}
            <script src=\"{{ asset('js/jquery-2.2.4.min.js') }}\"></script>
            <script src=\"{{ asset('js/moment.min.js') }}\"></script>
            <script src=\"{{ asset('js/bootstrap-3.3.7.min.js') }}\"></script>
            <script src=\"{{ asset('js/highlight.pack.js') }}\"></script>
            <script src=\"{{ asset('js/bootstrap-datetimepicker.min.js') }}\"></script>
            <script src=\"{{ asset('js/bootstrap-tagsinput.min.js') }}\"></script>
            <script src=\"{{ asset('js/main.js') }}\"></script>
        {% endblock %}

        {# it's not mandatory to set the timezone in localizeddate(). This is done to
           avoid errors when the 'intl' PHP extension is not available and the application
           is forced to use the limited \"intl polyfill\", which only supports UTC and GMT #}
        <!-- Page rendered on {{ 'now'|localizeddate('long', 'long', null, 'UTC') }} -->
    </body>
</html>
", "::base.html.twig", "/var/www/html/GestionStock/app/Resources/views/base.html.twig");
    }
}
