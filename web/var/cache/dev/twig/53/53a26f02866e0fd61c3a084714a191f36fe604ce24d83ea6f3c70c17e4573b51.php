<?php

/* StockBundle:Representant:addRepresentant.html.twig */
class __TwigTemplate_4451156e311511f7ac226e173ce27216ded33fb018591657d0576db610e472fb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:Representant:addRepresentant.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_271d6caa07fb24466835372992671a5b6056d0f96fa4ff97bf2c0a9f927e1e47 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_271d6caa07fb24466835372992671a5b6056d0f96fa4ff97bf2c0a9f927e1e47->enter($__internal_271d6caa07fb24466835372992671a5b6056d0f96fa4ff97bf2c0a9f927e1e47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Representant:addRepresentant.html.twig"));

        $__internal_50e9eb7a2ed0185c67fbc6c8c6c8f294eee006027f2860fefe954b0969fa5b68 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_50e9eb7a2ed0185c67fbc6c8c6c8f294eee006027f2860fefe954b0969fa5b68->enter($__internal_50e9eb7a2ed0185c67fbc6c8c6c8f294eee006027f2860fefe954b0969fa5b68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Representant:addRepresentant.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_271d6caa07fb24466835372992671a5b6056d0f96fa4ff97bf2c0a9f927e1e47->leave($__internal_271d6caa07fb24466835372992671a5b6056d0f96fa4ff97bf2c0a9f927e1e47_prof);

        
        $__internal_50e9eb7a2ed0185c67fbc6c8c6c8f294eee006027f2860fefe954b0969fa5b68->leave($__internal_50e9eb7a2ed0185c67fbc6c8c6c8f294eee006027f2860fefe954b0969fa5b68_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_440dc794642b4e504f1c831ec5ef8110f57c94c898731457142c992e53d5a961 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_440dc794642b4e504f1c831ec5ef8110f57c94c898731457142c992e53d5a961->enter($__internal_440dc794642b4e504f1c831ec5ef8110f57c94c898731457142c992e53d5a961_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_c306ffbd01c9e1d6429e1b2abe5646e2a7f46f0b4eaadaadd6efa5c49dc2ee0d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c306ffbd01c9e1d6429e1b2abe5646e2a7f46f0b4eaadaadd6efa5c49dc2ee0d->enter($__internal_c306ffbd01c9e1d6429e1b2abe5646e2a7f46f0b4eaadaadd6efa5c49dc2ee0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    ";
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form');
        echo "
";
        
        $__internal_c306ffbd01c9e1d6429e1b2abe5646e2a7f46f0b4eaadaadd6efa5c49dc2ee0d->leave($__internal_c306ffbd01c9e1d6429e1b2abe5646e2a7f46f0b4eaadaadd6efa5c49dc2ee0d_prof);

        
        $__internal_440dc794642b4e504f1c831ec5ef8110f57c94c898731457142c992e53d5a961->leave($__internal_440dc794642b4e504f1c831ec5ef8110f57c94c898731457142c992e53d5a961_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Representant:addRepresentant.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 3,  40 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}
{% block body %}
    {{ form(form) }}
{% endblock %}", "StockBundle:Representant:addRepresentant.html.twig", "/var/www/html/GestionStock/src/StockBundle/Resources/views/Representant/addRepresentant.html.twig");
    }
}
