<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_a395682a2aea563b5faf1c1407a1625740da8afe2f7201f60ebb2c22a93f899f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_af9ffb05ee704bac5d869542e78a0ccfd85303f51423eeedc648e8507f83615e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_af9ffb05ee704bac5d869542e78a0ccfd85303f51423eeedc648e8507f83615e->enter($__internal_af9ffb05ee704bac5d869542e78a0ccfd85303f51423eeedc648e8507f83615e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_5aacb60a13d577d20d8be615bf894d975ee32467204c62e2dd134eefdf0b9361 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5aacb60a13d577d20d8be615bf894d975ee32467204c62e2dd134eefdf0b9361->enter($__internal_5aacb60a13d577d20d8be615bf894d975ee32467204c62e2dd134eefdf0b9361_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_af9ffb05ee704bac5d869542e78a0ccfd85303f51423eeedc648e8507f83615e->leave($__internal_af9ffb05ee704bac5d869542e78a0ccfd85303f51423eeedc648e8507f83615e_prof);

        
        $__internal_5aacb60a13d577d20d8be615bf894d975ee32467204c62e2dd134eefdf0b9361->leave($__internal_5aacb60a13d577d20d8be615bf894d975ee32467204c62e2dd134eefdf0b9361_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_22f2d4effe9ce82c9e20394153a6c5974158087c01c8d4a85e312e8a84f6a95c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_22f2d4effe9ce82c9e20394153a6c5974158087c01c8d4a85e312e8a84f6a95c->enter($__internal_22f2d4effe9ce82c9e20394153a6c5974158087c01c8d4a85e312e8a84f6a95c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_095fcbab3e06d8f815ff7315ba6fd5d38be878fa61c0d7a6236de6edfe63c845 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_095fcbab3e06d8f815ff7315ba6fd5d38be878fa61c0d7a6236de6edfe63c845->enter($__internal_095fcbab3e06d8f815ff7315ba6fd5d38be878fa61c0d7a6236de6edfe63c845_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_095fcbab3e06d8f815ff7315ba6fd5d38be878fa61c0d7a6236de6edfe63c845->leave($__internal_095fcbab3e06d8f815ff7315ba6fd5d38be878fa61c0d7a6236de6edfe63c845_prof);

        
        $__internal_22f2d4effe9ce82c9e20394153a6c5974158087c01c8d4a85e312e8a84f6a95c->leave($__internal_22f2d4effe9ce82c9e20394153a6c5974158087c01c8d4a85e312e8a84f6a95c_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_ec0f1b96ac57931bb1cfd3e11aec4b35a84ca738014f3de17ba0808949c14200 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ec0f1b96ac57931bb1cfd3e11aec4b35a84ca738014f3de17ba0808949c14200->enter($__internal_ec0f1b96ac57931bb1cfd3e11aec4b35a84ca738014f3de17ba0808949c14200_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_163ffaeaf73ae6980583f754a8ebfae6cd9f74d0e2c71e7a52cede97505d54f7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_163ffaeaf73ae6980583f754a8ebfae6cd9f74d0e2c71e7a52cede97505d54f7->enter($__internal_163ffaeaf73ae6980583f754a8ebfae6cd9f74d0e2c71e7a52cede97505d54f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_163ffaeaf73ae6980583f754a8ebfae6cd9f74d0e2c71e7a52cede97505d54f7->leave($__internal_163ffaeaf73ae6980583f754a8ebfae6cd9f74d0e2c71e7a52cede97505d54f7_prof);

        
        $__internal_ec0f1b96ac57931bb1cfd3e11aec4b35a84ca738014f3de17ba0808949c14200->leave($__internal_ec0f1b96ac57931bb1cfd3e11aec4b35a84ca738014f3de17ba0808949c14200_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_30195186b3cddca910e758ee35c7b769c22367f88369839c2060c7c7068ad641 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_30195186b3cddca910e758ee35c7b769c22367f88369839c2060c7c7068ad641->enter($__internal_30195186b3cddca910e758ee35c7b769c22367f88369839c2060c7c7068ad641_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a5f4eea084e18596f710ed480573e8ccf6879f5734df475d3c5ea34bf3560407 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a5f4eea084e18596f710ed480573e8ccf6879f5734df475d3c5ea34bf3560407->enter($__internal_a5f4eea084e18596f710ed480573e8ccf6879f5734df475d3c5ea34bf3560407_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_a5f4eea084e18596f710ed480573e8ccf6879f5734df475d3c5ea34bf3560407->leave($__internal_a5f4eea084e18596f710ed480573e8ccf6879f5734df475d3c5ea34bf3560407_prof);

        
        $__internal_30195186b3cddca910e758ee35c7b769c22367f88369839c2060c7c7068ad641->leave($__internal_30195186b3cddca910e758ee35c7b769c22367f88369839c2060c7c7068ad641_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/var/www/html/GestionStock/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
