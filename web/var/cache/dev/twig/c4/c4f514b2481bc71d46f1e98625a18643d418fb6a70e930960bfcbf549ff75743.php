<?php

/* StockBundle:Transporteur:addTransporteur.html.twig */
class __TwigTemplate_14b3ce38999283c4f7495fd92d60734af26fa18ddbe07d50246b0b759969c6d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:Transporteur:addTransporteur.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_43712870b6809456cc02a7bf40f2a04e7684bbcf86bf5e0d240434db65067f53 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_43712870b6809456cc02a7bf40f2a04e7684bbcf86bf5e0d240434db65067f53->enter($__internal_43712870b6809456cc02a7bf40f2a04e7684bbcf86bf5e0d240434db65067f53_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Transporteur:addTransporteur.html.twig"));

        $__internal_b0e7d75550c9842ec92ab8256150905f4c397e3d6f9fd8f1db6a0ab1d6e7a017 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b0e7d75550c9842ec92ab8256150905f4c397e3d6f9fd8f1db6a0ab1d6e7a017->enter($__internal_b0e7d75550c9842ec92ab8256150905f4c397e3d6f9fd8f1db6a0ab1d6e7a017_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Transporteur:addTransporteur.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_43712870b6809456cc02a7bf40f2a04e7684bbcf86bf5e0d240434db65067f53->leave($__internal_43712870b6809456cc02a7bf40f2a04e7684bbcf86bf5e0d240434db65067f53_prof);

        
        $__internal_b0e7d75550c9842ec92ab8256150905f4c397e3d6f9fd8f1db6a0ab1d6e7a017->leave($__internal_b0e7d75550c9842ec92ab8256150905f4c397e3d6f9fd8f1db6a0ab1d6e7a017_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_6e5b81f08a0b3635177da39808b641a487b08959c7efc5b67754c04d60f8650f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6e5b81f08a0b3635177da39808b641a487b08959c7efc5b67754c04d60f8650f->enter($__internal_6e5b81f08a0b3635177da39808b641a487b08959c7efc5b67754c04d60f8650f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_7a4ab4b5f0cb6b9d5fc708a8d0c914791615f0aaf7ad46fe9c1d2ff7427f84d2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7a4ab4b5f0cb6b9d5fc708a8d0c914791615f0aaf7ad46fe9c1d2ff7427f84d2->enter($__internal_7a4ab4b5f0cb6b9d5fc708a8d0c914791615f0aaf7ad46fe9c1d2ff7427f84d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    ";
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form');
        echo "
";
        
        $__internal_7a4ab4b5f0cb6b9d5fc708a8d0c914791615f0aaf7ad46fe9c1d2ff7427f84d2->leave($__internal_7a4ab4b5f0cb6b9d5fc708a8d0c914791615f0aaf7ad46fe9c1d2ff7427f84d2_prof);

        
        $__internal_6e5b81f08a0b3635177da39808b641a487b08959c7efc5b67754c04d60f8650f->leave($__internal_6e5b81f08a0b3635177da39808b641a487b08959c7efc5b67754c04d60f8650f_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Transporteur:addTransporteur.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 3,  40 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}
{% block body %}
    {{ form(form) }}
{% endblock %}", "StockBundle:Transporteur:addTransporteur.html.twig", "/var/www/html/GestionStock/src/StockBundle/Resources/views/Transporteur/addTransporteur.html.twig");
    }
}
