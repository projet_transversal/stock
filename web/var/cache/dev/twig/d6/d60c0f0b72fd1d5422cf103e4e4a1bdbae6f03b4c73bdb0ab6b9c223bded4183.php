<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_aca36345c497fcd62565644c68fc76e643688c89ec949dbe917e7ab5f0451438 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_60acf32cf9e9327e5c72fabfac8f8f37917ae9bedd6105293a8c870bfd9cf3f1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_60acf32cf9e9327e5c72fabfac8f8f37917ae9bedd6105293a8c870bfd9cf3f1->enter($__internal_60acf32cf9e9327e5c72fabfac8f8f37917ae9bedd6105293a8c870bfd9cf3f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_f72db3602eba89b73a294f8951d702596a32e3052c3e481b2ce5725d35bf6c79 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f72db3602eba89b73a294f8951d702596a32e3052c3e481b2ce5725d35bf6c79->enter($__internal_f72db3602eba89b73a294f8951d702596a32e3052c3e481b2ce5725d35bf6c79_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_60acf32cf9e9327e5c72fabfac8f8f37917ae9bedd6105293a8c870bfd9cf3f1->leave($__internal_60acf32cf9e9327e5c72fabfac8f8f37917ae9bedd6105293a8c870bfd9cf3f1_prof);

        
        $__internal_f72db3602eba89b73a294f8951d702596a32e3052c3e481b2ce5725d35bf6c79->leave($__internal_f72db3602eba89b73a294f8951d702596a32e3052c3e481b2ce5725d35bf6c79_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_a4bb5b0a0ecb947928d7062f8c4acb952c95c9be89b819f253eab7205c9deb23 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a4bb5b0a0ecb947928d7062f8c4acb952c95c9be89b819f253eab7205c9deb23->enter($__internal_a4bb5b0a0ecb947928d7062f8c4acb952c95c9be89b819f253eab7205c9deb23_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_b1af5ad1043263244361900d3ef905f729db3dc0eb9aa403819b1244737b56db = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b1af5ad1043263244361900d3ef905f729db3dc0eb9aa403819b1244737b56db->enter($__internal_b1af5ad1043263244361900d3ef905f729db3dc0eb9aa403819b1244737b56db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_b1af5ad1043263244361900d3ef905f729db3dc0eb9aa403819b1244737b56db->leave($__internal_b1af5ad1043263244361900d3ef905f729db3dc0eb9aa403819b1244737b56db_prof);

        
        $__internal_a4bb5b0a0ecb947928d7062f8c4acb952c95c9be89b819f253eab7205c9deb23->leave($__internal_a4bb5b0a0ecb947928d7062f8c4acb952c95c9be89b819f253eab7205c9deb23_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_344fe8b7483fb8f8bc9f364b66be8c1a673be83aa4425b34e6b8cc9357b6dd68 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_344fe8b7483fb8f8bc9f364b66be8c1a673be83aa4425b34e6b8cc9357b6dd68->enter($__internal_344fe8b7483fb8f8bc9f364b66be8c1a673be83aa4425b34e6b8cc9357b6dd68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_bff6cfd2f363c39ae387c6c0c23e1ef5ea3c8f6eb3d785f8b69d454e1aae147f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bff6cfd2f363c39ae387c6c0c23e1ef5ea3c8f6eb3d785f8b69d454e1aae147f->enter($__internal_bff6cfd2f363c39ae387c6c0c23e1ef5ea3c8f6eb3d785f8b69d454e1aae147f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_bff6cfd2f363c39ae387c6c0c23e1ef5ea3c8f6eb3d785f8b69d454e1aae147f->leave($__internal_bff6cfd2f363c39ae387c6c0c23e1ef5ea3c8f6eb3d785f8b69d454e1aae147f_prof);

        
        $__internal_344fe8b7483fb8f8bc9f364b66be8c1a673be83aa4425b34e6b8cc9357b6dd68->leave($__internal_344fe8b7483fb8f8bc9f364b66be8c1a673be83aa4425b34e6b8cc9357b6dd68_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_6a408c1ba02c1ad1a51bc6a88e3ce3f233dce7db4454933b03f30d6ec9e8f51a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6a408c1ba02c1ad1a51bc6a88e3ce3f233dce7db4454933b03f30d6ec9e8f51a->enter($__internal_6a408c1ba02c1ad1a51bc6a88e3ce3f233dce7db4454933b03f30d6ec9e8f51a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_4906abd76ba8e86aee4250cfa7378ddd550084dcb0d5e515a909c866cb79138b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4906abd76ba8e86aee4250cfa7378ddd550084dcb0d5e515a909c866cb79138b->enter($__internal_4906abd76ba8e86aee4250cfa7378ddd550084dcb0d5e515a909c866cb79138b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_4906abd76ba8e86aee4250cfa7378ddd550084dcb0d5e515a909c866cb79138b->leave($__internal_4906abd76ba8e86aee4250cfa7378ddd550084dcb0d5e515a909c866cb79138b_prof);

        
        $__internal_6a408c1ba02c1ad1a51bc6a88e3ce3f233dce7db4454933b03f30d6ec9e8f51a->leave($__internal_6a408c1ba02c1ad1a51bc6a88e3ce3f233dce7db4454933b03f30d6ec9e8f51a_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/var/www/html/GestionStock/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
