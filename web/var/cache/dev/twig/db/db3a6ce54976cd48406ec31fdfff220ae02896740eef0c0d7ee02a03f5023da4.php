<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_d720dd1a4f07c573a84561cee845fba5ee4d90c9d59358e80285f28f430311e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ace58025890604d9969358cb9dcec167b91618642cdef5eadb8e406c31d77b14 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ace58025890604d9969358cb9dcec167b91618642cdef5eadb8e406c31d77b14->enter($__internal_ace58025890604d9969358cb9dcec167b91618642cdef5eadb8e406c31d77b14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_8df538b00faee6e5a3209840f8b3604d3d68e946439156b895f9b67dd58cf63a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8df538b00faee6e5a3209840f8b3604d3d68e946439156b895f9b67dd58cf63a->enter($__internal_8df538b00faee6e5a3209840f8b3604d3d68e946439156b895f9b67dd58cf63a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ace58025890604d9969358cb9dcec167b91618642cdef5eadb8e406c31d77b14->leave($__internal_ace58025890604d9969358cb9dcec167b91618642cdef5eadb8e406c31d77b14_prof);

        
        $__internal_8df538b00faee6e5a3209840f8b3604d3d68e946439156b895f9b67dd58cf63a->leave($__internal_8df538b00faee6e5a3209840f8b3604d3d68e946439156b895f9b67dd58cf63a_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_f7d627a94655a61256fe579410f195c8abe3d4c418d32c5352c7cdb4aa108133 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f7d627a94655a61256fe579410f195c8abe3d4c418d32c5352c7cdb4aa108133->enter($__internal_f7d627a94655a61256fe579410f195c8abe3d4c418d32c5352c7cdb4aa108133_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_6ea359fc4cf75b48ce435fb058a16a77f5f686db962ee1edab0b45ce5cd2adba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6ea359fc4cf75b48ce435fb058a16a77f5f686db962ee1edab0b45ce5cd2adba->enter($__internal_6ea359fc4cf75b48ce435fb058a16a77f5f686db962ee1edab0b45ce5cd2adba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_6ea359fc4cf75b48ce435fb058a16a77f5f686db962ee1edab0b45ce5cd2adba->leave($__internal_6ea359fc4cf75b48ce435fb058a16a77f5f686db962ee1edab0b45ce5cd2adba_prof);

        
        $__internal_f7d627a94655a61256fe579410f195c8abe3d4c418d32c5352c7cdb4aa108133->leave($__internal_f7d627a94655a61256fe579410f195c8abe3d4c418d32c5352c7cdb4aa108133_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_c9ba0b93893f72da2c4528a9bdf17efd6b7da1e27d15934aff27a411536a2a27 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c9ba0b93893f72da2c4528a9bdf17efd6b7da1e27d15934aff27a411536a2a27->enter($__internal_c9ba0b93893f72da2c4528a9bdf17efd6b7da1e27d15934aff27a411536a2a27_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_a4458ddd401aed8fe79ae3f8a8eaa090aee2d92ba40fde2827df8d61e6130364 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a4458ddd401aed8fe79ae3f8a8eaa090aee2d92ba40fde2827df8d61e6130364->enter($__internal_a4458ddd401aed8fe79ae3f8a8eaa090aee2d92ba40fde2827df8d61e6130364_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_a4458ddd401aed8fe79ae3f8a8eaa090aee2d92ba40fde2827df8d61e6130364->leave($__internal_a4458ddd401aed8fe79ae3f8a8eaa090aee2d92ba40fde2827df8d61e6130364_prof);

        
        $__internal_c9ba0b93893f72da2c4528a9bdf17efd6b7da1e27d15934aff27a411536a2a27->leave($__internal_c9ba0b93893f72da2c4528a9bdf17efd6b7da1e27d15934aff27a411536a2a27_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_ec47c4e911681cc1f186b1238e84a25ba127d00e2cc78a5a0b142d4c09446b39 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ec47c4e911681cc1f186b1238e84a25ba127d00e2cc78a5a0b142d4c09446b39->enter($__internal_ec47c4e911681cc1f186b1238e84a25ba127d00e2cc78a5a0b142d4c09446b39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_6cd690233479c89a7feb4025e50fc3e00ed02e34dfc713cf5ee2ed141474631b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6cd690233479c89a7feb4025e50fc3e00ed02e34dfc713cf5ee2ed141474631b->enter($__internal_6cd690233479c89a7feb4025e50fc3e00ed02e34dfc713cf5ee2ed141474631b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_6cd690233479c89a7feb4025e50fc3e00ed02e34dfc713cf5ee2ed141474631b->leave($__internal_6cd690233479c89a7feb4025e50fc3e00ed02e34dfc713cf5ee2ed141474631b_prof);

        
        $__internal_ec47c4e911681cc1f186b1238e84a25ba127d00e2cc78a5a0b142d4c09446b39->leave($__internal_ec47c4e911681cc1f186b1238e84a25ba127d00e2cc78a5a0b142d4c09446b39_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/var/www/html/GestionStock/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
