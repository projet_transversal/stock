<?php

/* StockBundle:Societe:addSociete.html.twig */
class __TwigTemplate_6ed76d5c718615c235f23ece5e62142f9c5d021ab5d56b74f3c40c75dc995b16 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "StockBundle:Societe:addSociete.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_475965a8c8a5fd1a034a24a924f67927817d30c3ee2660842b97871ac3d76c6b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_475965a8c8a5fd1a034a24a924f67927817d30c3ee2660842b97871ac3d76c6b->enter($__internal_475965a8c8a5fd1a034a24a924f67927817d30c3ee2660842b97871ac3d76c6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Societe:addSociete.html.twig"));

        $__internal_e976989624fd4de29735bd65509dc26d22d84c46d9be518a6409a9bcbcc77800 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e976989624fd4de29735bd65509dc26d22d84c46d9be518a6409a9bcbcc77800->enter($__internal_e976989624fd4de29735bd65509dc26d22d84c46d9be518a6409a9bcbcc77800_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "StockBundle:Societe:addSociete.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_475965a8c8a5fd1a034a24a924f67927817d30c3ee2660842b97871ac3d76c6b->leave($__internal_475965a8c8a5fd1a034a24a924f67927817d30c3ee2660842b97871ac3d76c6b_prof);

        
        $__internal_e976989624fd4de29735bd65509dc26d22d84c46d9be518a6409a9bcbcc77800->leave($__internal_e976989624fd4de29735bd65509dc26d22d84c46d9be518a6409a9bcbcc77800_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_7a029ad38da08ab59b16535e2d4e03e19ad315d20edf34ef18411d2af14c397c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7a029ad38da08ab59b16535e2d4e03e19ad315d20edf34ef18411d2af14c397c->enter($__internal_7a029ad38da08ab59b16535e2d4e03e19ad315d20edf34ef18411d2af14c397c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a58c1edfb7a46b832f886bcdc3772e4f904f3cf8127f736df4190227ab803f34 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a58c1edfb7a46b832f886bcdc3772e4f904f3cf8127f736df4190227ab803f34->enter($__internal_a58c1edfb7a46b832f886bcdc3772e4f904f3cf8127f736df4190227ab803f34_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    ";
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form');
        echo "
";
        
        $__internal_a58c1edfb7a46b832f886bcdc3772e4f904f3cf8127f736df4190227ab803f34->leave($__internal_a58c1edfb7a46b832f886bcdc3772e4f904f3cf8127f736df4190227ab803f34_prof);

        
        $__internal_7a029ad38da08ab59b16535e2d4e03e19ad315d20edf34ef18411d2af14c397c->leave($__internal_7a029ad38da08ab59b16535e2d4e03e19ad315d20edf34ef18411d2af14c397c_prof);

    }

    public function getTemplateName()
    {
        return "StockBundle:Societe:addSociete.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 3,  40 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}
{% block body %}
    {{ form(form) }}
{% endblock %}", "StockBundle:Societe:addSociete.html.twig", "/var/www/html/GestionStock/src/StockBundle/Resources/views/Societe/addSociete.html.twig");
    }
}
